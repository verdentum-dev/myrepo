<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
        $system_name = $this->db->get_where('settings', array('type' => 'system_name'))->row()->description;
        $system_title = $this->db->get_where('settings', array('type' => 'system_title'))->row()->description;
        ?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Neon Admin Panel" />
        <meta name="author" content="" />

        <title><?php echo get_phrase('login'); ?> | <?php echo $system_title; ?></title>


        <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
        <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
        <link rel="stylesheet" href="assets/css/bootstrap.css">
        <link rel="stylesheet" href="assets/css/neon-core.css">
        <link rel="stylesheet" href="assets/css/neon-theme.css">
        <link rel="stylesheet" href="assets/css/neon-forms.css">
        <link rel="stylesheet" href="assets/css/custom.css">

        <script src="assets/js/jquery-1.11.0.min.js"></script>

        <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="shortcut icon" href="assets/images/favicon.png">
        <style type="text/css">
            .login-page .login-header {
                padding-top: 200px;
            }

            .login-page .login-header {
                background-color: #262626;
            }
            .login-page .login-form .form-group .input-group {
                background-color: transparent;
                border:1px solid #807F6E;
            }

            .btn-primary {
                background-color: #807F6E;
            }

        </style>
    </head>
    <body class="page-body login-page login-form-fall" data-url="http://neon.dev" style="background-color: #262626;">


        <!-- This is needed when you send requests via Ajax -->
        <script type="text/javascript">
            var baseurl = '<?php echo base_url(); ?>';
        </script>

        <div class="login-container">

            <div class="login-header">

                <div class="login-content" style="width:100%;">

                    <!-- <a href="<?php echo base_url(); ?>" class="logo">
                        <img src="assets/images/logo.png" height="40" alt="" />
                    </a> -->

                    <p class="description" style="color:#FFFFFF;margin:20px 0;">
						<img src="assets/images/logo-verd.png" style="color:#FFFFFF;vertical-align:top;" alt="Verdentum" height="24">
						<span style="color:#807F6E;display:inline-block;padding-top:2px;margin-left:5px;font-size:20px;">Professional</span>
					</p>
                    <!-- <p class="description">
                    <h2 style="color:#cacaca; font-weight:100;">
                        <?php echo $system_name; ?>
                    </h2>
                    </p> -->

                    <!-- progress bar indicator -->
                    <div class="login-progressbar-indicator">
                        <h3>43%</h3>
                        <span>logging in...</span>
                    </div>
                </div>

            </div>

            <div class="login-progressbar">
                <div></div>
            </div>

            <div class="login-form">

                <div class="login-content">

                    <div class="form-login-error">
                        <h3>Invalid login</h3>
                        <p>Please enter correct email and password!</p>
                    </div>

                    <form method="post" role="form" id="form_login">

                        <div class="form-group">

                            <div class="input-group">
                                <div class="input-group-addon">
                                    <!-- <i class="entypo-user"></i> -->
                                </div>

                                <input type="text" class="form-control" name="email" id="email" placeholder="Email" autocomplete="off" data-mask="email" />
                            </div>

                        </div>

                        <div class="form-group">

                            <div class="input-group">
                                <div class="input-group-addon">
                                    <!-- <i class="entypo-key"></i> -->
                                </div>

                                <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" />
                            </div>

                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block btn-login">
                                <i class="entypo-login"></i>
                                Login
                            </button>
                        </div>


                    </form>



                </div>

            </div>

        </div>


        <!-- Bottom Scripts -->
        <script src="assets/js/gsap/main-gsap.js"></script>
        <script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
        <script src="assets/js/bootstrap.js"></script>
        <script src="assets/js/joinable.js"></script>
        <script src="assets/js/resizeable.js"></script>
        <script src="assets/js/neon-api.js"></script>
        <script src="assets/js/jquery.validate.min.js"></script>
        <script src="assets/js/neon-login.js"></script>
        <script src="assets/js/neon-custom.js"></script>
        <script src="assets/js/neon-demo.js"></script>

        <!-- <script type="text/javascript">
            $(function(){
                $(".login-page").addClass('logging-in'); // This will hide the login form and init the progress bar
                    
                    
                // Hide Errors
                $(".form-login-error").slideUp('fast');

                // We will wait till the transition ends                
                setTimeout(function()
                {
                    var random_pct = 25 + Math.round(Math.random() * 30);
                    
                    // The form data are subbmitted, we can forward the progress to 70%
                    neonLogin.setPercentage(40 + random_pct);
                                            
                    // Send data to the server
                    $.ajax({
                        url: baseurl + 'index.php?login/ajax_login',
                        method: 'POST',
                        dataType: 'json',
                        data: {
                            email: 'admin@helpageindia.com',
                            password: '1234',
                        },
                        error: function()
                        {
                            alert("An error occoured!");
                        },
                        success: function(response)
                        {
                            // Login status [success|invalid]
                            var login_status = response.login_status;
                                                            
                            // Form is fully completed, we update the percentage
                            neonLogin.setPercentage(100);
                            
                            
                            // We will give some time for the animation to finish, then execute the following procedures    
                            setTimeout(function()
                            {
                                // If login is invalid, we store the 
                                if(login_status == 'invalid')
                                {
                                    $(".login-page").removeClass('logging-in');
                                    neonLogin.resetProgressBar(true);
                                }
                                else
                                if(login_status == 'success')
                                {
                                    // Redirect to login page
                                    setTimeout(function()
                                    {
                                        var redirect_url = baseurl;
                                        
                                        if(response.redirect_url && response.redirect_url.length)
                                        {
                                            redirect_url = response.redirect_url;
                                        }
                                        
                                        window.location.href = redirect_url;
                                    }, 400);
                                }
                                
                            }, 1000);
                        }
                    });
                        
                    
                }, 650);
            });
        </script> -->

    </body>
</html>