<div class="row">
        <div class="col-md-12">

            <div class="panel panel-primary" data-collapsed="0">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h3><?php echo get_phrase('edit_medicine'); ?></h3>
                    </div>
                </div>

                <div class="panel-body">
                    <div id="success"></div>
                    <form class="form-horizontal form-groups-bordered" id="editMedicine">

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('name'); ?></label>

                            <div class="col-sm-5">
                                <input type="text" name="name" class="form-control" id="field-1" value="<?php echo $medicine['name']; ?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('Preparation'); ?></label>

                            <div class="col-sm-5">
                                <select name="preparation" class="form-control">
                                    <option value=""><?php echo get_phrase('select_preparation'); ?></option>
                                        <option value="Tab" <?php if($medicine['preparation'] == "Tab") echo 'selected="selected"'; ?>>Tab</option>
                                        <option value="Emulsion" <?php if($medicine['preparation'] == "Emulsion") echo 'selected="selected"'; ?>>Emulsion</option>
                                        <option value="Syrup" <?php if($medicine['preparation'] == "Syrup") echo 'selected="selected"'; ?>>Syrup</option>
                                        <option value="Ointment" <?php if($medicine['preparation'] == "Ointment") echo 'selected="selected"'; ?>>Ointment</option>
                                        <option value="Powder" <?php if($medicine['preparation'] == "Powder") echo 'selected="selected"'; ?>>Powder</option>
                                        <option value="Drops" <?php if($medicine['preparation'] == "Drops") echo 'selected="selected"'; ?>>Drops</option>
                                        <option value="Inhaler" <?php if($medicine['preparation'] == "Inhaler") echo 'selected="selected"'; ?>>Inhaler</option>
                                        <option value="Cream" <?php if($medicine['preparation'] == "Cream") echo 'selected="selected"'; ?>>Cream</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('composition'); ?></label>

                            <div class="col-sm-9">
                                <textarea name="composition" class="form-control" id="field-ta"><?php echo $medicine['composition']; ?></textarea>
                            </div>
                        </div>

                        <div class="col-sm-3 control-label col-sm-offset-2">
                        <button class="btn btn-success" type="submit">Update</button>
                        </div>
                    </form>

                </div>

            </div>

        </div>
    </div>


    <script>
  //ajex call for question update
  $('#editMedicine').on('submit', function(e) {
    e.preventDefault();
    $('button[type="submit"]').attr('disabled', 'disabled').html('Updating Medicine...');
    $('.alert').remove();

    $('input[name="name"]').next('span').remove();
    $('select[name="preparation"]').next('span').remove();
    $('textarea[name="composition"]').next('span').remove();
    
    var basic = new FormData($(this)[0]);
    basic.append('medicine_id', <?php echo $medicine['medicine_id'];?>);
    $.ajax({
     url: '<?php echo base_url(); ?>index.php?admin/updmedicine/',
     type: 'POST',
     data: basic,
     processData: false,
     contentType: false,
     error: function() {
      $('button[type="submit"]').removeAttr('disabled').html('Update');
     },
     success: function(data) {
        var data = JSON.parse(data);
      if(data.status > 0){
        
        $('input[name="name"]').after('<span class="error">'+data.name+'</span>');
        $('select[name="preparation"]').after('<span class="error">'+data.preparation+'</span>');
        $('textarea[name="composition"]').after('<span class="error">'+data.composition+'</span>');
      }
      if(typeof data.msg !== 'undefined'){
        $('#success').html('<div class="alert alert-success">'+data.msg+'</div>');
      }
      $('button[type="submit"]').removeAttr('disabled').html('Update');
     }
   });
  });
 </script>