<div class="row" style="margin-top: -20px;">
	<div class="col-md-12" style="background-color: white; margin-bottom: 10px;">
  
    <div style="margin-top: 20px;">
       <div class="col-md-3">
       	 <div class="col-md-12" style="background-color: #009999; border-radius:5px;">
       	 <h2 style="padding-left: 10px; color: white; font-size: 40px;"><?php echo $by_day;?> Patient</h2>
         <a href="">
       	 	<h3 style="padding: 10px; color: white;"> View By Day</h3>
         </a>
       	 </div>
       </div>

       <div class="col-md-3">
         <div class="col-md-12" style="background-color: #0099ff; border-radius:5px;">
         <h2 style="padding-left: 10px; color: white; font-size: 40px;"><?php echo $by_week;?> Patient</h2>
         <a href="">
            <h3 style="padding: 10px; color: white;"> View By Week</h3>
         </a>
         </div>
       </div>

       <div class="col-md-3">
       	 <div class="col-md-12" style="background-color: #009933; border-radius:5px;">
       	 <h2 style="padding-left: 10px; color: white; font-size: 40px;"><?php echo $by_month;?> Patient</h2>
         <a href="">
       	 	<h3 style="padding: 10px; color: white;"> View By Month</h3>
         </a>
       	 </div>
       </div>

       <div class="col-md-3">
       	 <div class="col-md-12" style="background-color: #669900; border-radius:5px;">
       	 <h2 style="padding-left: 10px; color: white; font-size: 40px;"><?php echo $by_year;?> Patient</h2>
         <a href="">
       	 	<h3 style="padding: 10px; color: white;"> View By Year</h3>
         </a>
       	 </div>
       </div>
    </div>   
 
      <div class="col-md-12">
            <div style="background-color: #262626; color: white; font-size:10px; padding-left:20px; height:40px; margin-top: 20px;">
            <p style="padding: 5px; font-size:20px;">Total No. Patient Treated</p>
            </div>
      </div>        

   <div class="col-md-12">
   	<table class="table table-bordered table-striped datatable" id="table-2">
    <thead>
        <tr>
            <th><?php echo get_phrase('MMU Name');?></th>
            <th><?php echo get_phrase('No of Patient Visited');?></th>
            <th><?php echo get_phrase('No of Patient Treated');?></th>
            <th><?php echo get_phrase('No. of Medicine distributed');?></th>
            <th><?php echo get_phrase('People below poverty line');?></th>
            <th><?php echo get_phrase('People with habits');?></th>
            <th><?php echo get_phrase('People with chronicaal desease');?></th>
        </tr>
    </thead>

    <tbody><?php
        /*echo "<pre>";
        print_r($records);*/
        foreach ($records as $val) { ?>
            <tr>
                <td>
                    <a href="<?php echo base_url('index.php?admin/by_loc/'.$val['unit_id']);?>">
                      <?php echo $val['mmu_name']; ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url('index.php?admin/by_mmu/'.$val['unit_id']);?>">
                       <?php echo $val['bplandpatcount']['patcount']; ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url('index.php?admin/treated_patient/'.$val['unit_id']);?>">
                       <?php echo $val['treatmentcount']['treatmentcount']; ?>
                    </a>
                </td>
                <td><?php 
                    if($val['medicinecount']['medicinecount'] == ''){?>
                        <a href="<?php echo base_url('index.php?admin/medicine_distributed/'.$val['unit_id']);?>">
                            <?php echo "0";?>
                        </a>
                    <?php }else{?>
                        <a href="<?php echo base_url('index.php?admin/medicine_distributed/'.$val['unit_id']);?>">
                            <?php echo $val['medicinecount']['medicinecount'];?>
                        </a>
                    <?php }  ?></td>
                <td><?php 
                    if($val['bplandpatcount']['bplcount'] == ''){{?>
                           <a href="<?php echo base_url('index.php?admin/bpl_patient/'.$val['unit_id']);?>">
                                <?php echo "0";?>
                            </a>
                     <?php }}else{?>
                         <a href="<?php echo base_url('index.php?admin/bpl_patient/'.$val['unit_id']);?>">
                           <?php echo $val['bplandpatcount']['bplcount'];?>
                         </a>
                     <?php }  ?></td>
                <td><?php 
                 if($val['habitcount']['habitcount'] == ''){{?>
                           <a href="<?php echo base_url('index.php?admin/patient_habit/'.$val['unit_id']);?>">
                                <?php echo "0";?>
                            </a>
                     <?php }}else{?>
                         <a href="<?php echo base_url('index.php?admin/patient_habit/'.$val['unit_id']);?>">
                           <?php echo $val['habitcount']['habitcount'];?>
                         </a>
                     <?php }
                     ?></td>
                <td><?php 
                if($val['chroniccount']['chroniccount'] == ''){{?>
                           <a href="<?php echo base_url('index.php?admin/chronic_disease/'.$val['unit_id']);?>">
                                <?php echo "0";?>
                            </a>
                     <?php }}else{?>
                         <a href="<?php echo base_url('index.php?admin/chronic_disease/'.$val['unit_id']);?>">
                           <?php echo $val['chroniccount']['chroniccount'];?>
                         </a>
                     <?php }?></td>
            </tr><?php
        } ?>           
    </tbody>
</table>
   </div>

</div>
<div class="col-md-12">
  <div class="col-md-12">
            <div style="background-color: #262626; color: white; font-size:10px; padding-left:20px; height:40px; margin-top: 20px;">
            <p style="padding: 5px; font-size:20px;">Doctors</p>
            </div>
      </div>   
     <!-- <div class="col-md-4 form-group">
       <select class="form-control" id = "state">
        <option>Select State</option><?php
            foreach ($state as $st) { ?>
                <option value = "<?php echo $st['state_id']; ?>"><?php echo $st['state_name']; ?></option><?php
            } ?>
       </select>
     </div>
     <div class="col-md-4 form-group">
       <select class="form-control" id = "mmu">
        <option>Select MMU</option>
       </select>
     </div>
     <div class="col-md-4 form-group">
       <select class="form-control" id = "mmulocation">
        <option>Select Location</option>
       </select>
     </div> -->

     <div class="col-md-12 margin-top-20">
      
    <table class="table table-bordered table-striped datatable" id="doctor_table">
    <thead>
        <tr>
            <th><?php echo get_phrase('S.No');?></th>
            <th><?php echo get_phrase('Doctors Name');?></th>
            <th><?php echo get_phrase('Region');?></th>
            <th><?php echo get_phrase('No Of Patient Treated');?></th>
            <th><?php echo get_phrase('No Of Days Attended');?></th>
            <th><?php echo get_phrase('No of Medicine Prescribed');?></th>
        </tr>
    </thead>

    <tbody  id="doctorslist"><?php
        $i = 1;
        foreach ($doctor as $d) { ?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $d['name']; ?></td>
                <td><?php echo $d['mmu_name']; ?></td>
                <td><?php echo $d['patientcount']['patientcount']; ?></td>
                <td><?php echo $d['datecount']['datecount']; ?></td>
                <td><?php 
                    if($d['medicinecount']['medicinecount'] == ''){
                        echo 0;
                    }else{
                        echo $d['medicinecount']['medicinecount'];
                        } ?></td>
            </tr><?php
            $i++;
        } ?>              
    </tbody>
</table>
   
     </div>
</div>
<style type="text/css">
	#mmu_name_head
	{
		background-color: #9999ff;
		color: white;
	}

	#mmu_name
	{
		background-color: #00e600;
		color: white;
		margin-top:-20px;
	}

	#no_treatment
	{
		background-color: teal;
		color: white;
		font-weight:bold;
	}
</style>
<!-- <img src="<?php echo base_url(); ?>uploads/world_view.png" alt="worldview" style="width:100%;"> 
 -->

 <script type="text/javascript">
    $('#state').on('change', function() {
    var state_id=this.value;
        $.ajax({
            url: "<?php echo base_url();?>index.php?admin/get_mmubystate",
            type: "POST",
            data: "state_id="+state_id,
            success: function(response)
            {
             if (response.length == 0){   
                   $('#mmu').html("<option>Mmu Not Found</option>");
                }
                else{   
                   $('#mmu').html(response);
                }
            }
        });
    })

    $('#mmu').on('change', function() {
    var mmu_id=this.value;
        $.ajax({
            url: "<?php echo base_url();?>index.php?admin/get_mmulocation",
            type: "POST",
            data: "mmu_id="+mmu_id,
            success: function(response)
            {
             if (response.length == 0){   
                   $('#mmulocation').html("<option>No mmu locations found</option>");
                }
                else{   
                   $('#mmulocation').html(response);
                }
            }
        });
    })

    $('#mmulocation').on('change', function() {
        var state_id =  $('#state').val();
        var mmu_id =  $('#mmu').val();
        var mmulocation_id =  $('#mmulocation').val();
        data = { 
                mmu_id: mmu_id,
                state_id: state_id,
                mmulocation_id:mmulocation_id
        };
        $.ajax({
            url: "<?php echo base_url();?>index.php?admin/get_doctor",
            type: "POST",
            dataType: "json",
            data: data,
            success: function(data){                
                if (data.length == 0){   
                   $('#doctorslist').html("<tr><td colspan = '6'>No Information found</td></tr>");
                }else{
                    $('#doctorslist').html(data);
                    data.forEach(function(currentValue, index) {    
                        $('#doctorslist').append('<tr><td>'+(index+1)+'</td>'
                        +'<td>'+currentValue.name+'</td>'
                        +'<td>'+currentValue.treatmentcount+'</td>'
                        +'<td>'+currentValue.patientcount+'</td>'
                        +'<td>'+currentValue.mmuloc_name+'</td>'
                        +'<td>'+currentValue.mmu_name+'</td>');
                    });
                }               
            }
        });
    })
</script>


<script type="text/javascript">
    jQuery(window).load(function ()
    {
        var $ = jQuery;

        $("#table-2").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });

        // Highlighted rows
        $("#table-2 tbody input[type=checkbox]").each(function (i, el)
        {
            var $this = $(el),
                    $p = $this.closest('tr');

            $(el).on('change', function ()
            {
                var is_checked = $this.is(':checked');

                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
            });
        });

        // Replace Checboxes
        $(".pagination a").click(function (ev)
        {
            replaceCheckboxes();
        });
    });
</script>

<script type="text/javascript">
    jQuery(window).load(function ()
    {
        var $ = jQuery;

        $("#doctor_table").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });

        // Highlighted rows
        $("#doctor_table tbody input[type=checkbox]").each(function (i, el)
        {
            var $this = $(el),
                    $p = $this.closest('tr');

            $(el).on('change', function ()
            {
                var is_checked = $this.is(':checked');

                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
            });
        });

        // Replace Checboxes
        $(".pagination a").click(function (ev)
        {
            replaceCheckboxes();
        });
    });
</script>