<!-- <button onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/add_pharmacist/');" 
    class="btn btn-primary pull-right">
        <?php echo get_phrase('add_pharmacist'); ?>
</button> -->

<a href="<?php echo base_url();?>index.php?admin/medicine/" class="btn btn-primary pull-right">Back</a>
<div style="clear:both;"></div>
<br>
<table class="table table-bordered table-striped datatable" id="table-2">
    <thead>
        <tr>
            <th><?php echo get_phrase('Patient Name');?></th>
            <th><?php echo get_phrase('Medicine Name');?></th>
            <th><?php echo get_phrase('no_of_units');?></th>
            <th><?php echo get_phrase('days');?></th>
            <th><?php echo get_phrase('date');?></th>
            <!-- <th><?php echo get_phrase('options');?></th> -->
        </tr>
    </thead>

    <tbody>
        <?php foreach ($allmedicine as $row) { ?>   
            <tr>
                <td><?php echo $row['patname'];?></td>
                <td><?php echo $row['med_name'];?></td>
                <td><?php echo $row['quantity'];?></td>
                <td><?php echo $row['days'];?></td>
                <td><?php echo $row['date'];?></td>
                <!-- <td>
                    <a  href="#" class="btn btn-default btn-sm btn-icon icon-left">
                            <i class="entypo-pencil"></i>
                            Edit
                    </a>
                    <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"s>
                            <i class="entypo-cancel"></i>
                            Delete
                    </a>
                </td> -->
            </tr>
        <?php } ?>
    </tbody>
</table>

<script type="text/javascript">
    jQuery(window).load(function ()
    {
        var $ = jQuery;

        $("#table-2").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });

        // Highlighted rows
        $("#table-2 tbody input[type=checkbox]").each(function (i, el)
        {
            var $this = $(el),
                    $p = $this.closest('tr');

            $(el).on('change', function ()
            {
                var is_checked = $this.is(':checked');

                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
            });
        });

        // Replace Checboxes
        $(".pagination a").click(function (ev)
        {
            replaceCheckboxes();
        });
    });
</script>