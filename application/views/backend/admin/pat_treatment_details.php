<div class="row">
	<div class="col-md-12" style="background-color: white; margin-bottom: 10px;">
        <div style="background-color: #00004d; color: white; font-size:10px; padding-left: 5px;">
        	<p style="padding: 5px;">Patient Treatment History</p>
        </div>

   <div class="" style="margin-top:10px;">
   	<table class="table table-bordered table-striped datatable" id="table-2">
    <thead>
        <tr>
            <th><?php echo get_phrase('Patient Name');?></th>
            <th><?php echo get_phrase('Treatment Date');?></th>
            <th><?php echo get_phrase('Disease');?></th>
            <th><?php echo get_phrase('Disease Type');?></th>
            <th><?php echo get_phrase('Medicine');?></th>
        </tr>
    </thead>

    <tbody>
    <?php
        /*echo "<pre>";
        print_r($records);*/
        foreach ($pat_treatment_details as $mpat) { ?>
            <tr>
                <td><?php echo $mpat['patname'];?></td>
                <td><?php echo $mpat['treat_date'];?></td>
                <td><?php echo $mpat['dis_name'];?></td>
                <td><?php echo $mpat['dis_type'];?></td>
                <td><?php echo $mpat['name'];?></td>
            </tr><?php
        } ?> 
    </tbody>
</table>
   </div>

</div>
<style type="text/css">
	#mmu_name_head
	{
		background-color: #9999ff;
		color: white;
	}

	#mmu_name
	{
		background-color: #00e600;
		color: white;
		margin-top:-20px;
	}

	#no_treatment
	{
		background-color: teal;
		color: white;
		font-weight:bold;
	}
</style>


<script type="text/javascript">
    jQuery(window).load(function ()
    {
        var $ = jQuery;

        $("#table-2").dataTable({
            "sPaginationType": "bootstrap",
            /*"sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"*/
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });

        // Highlighted rows
        $("#table-2 tbody input[type=checkbox]").each(function (i, el)
        {
            var $this = $(el),
                    $p = $this.closest('tr');

            $(el).on('change', function ()
            {
                var is_checked = $this.is(':checked');

                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
            });
        });

        // Replace Checboxes
        $(".pagination a").click(function (ev)
        {
            replaceCheckboxes();
        });
    });
</script>

