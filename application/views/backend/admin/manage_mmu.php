<!-- <button type="button" class="btn btn-primary pull-right">
        <?php echo get_phrase('add MMU'); ?>
</button> -->
<a href="<?php echo base_url(); ?>index.php?admin/add_mmu" class="btn btn-primary pull-right">Add MMU</a>
<div style="clear:both;"></div>
<br>
<table class="table table-bordered table-striped datatable" id="table-2">
    <thead>
        <tr>
            <th><?php echo get_phrase('Sl. No.');?></th>
            <th><?php echo get_phrase('State');?></th>
            <th><?php echo get_phrase('District');?></th>
            <th><?php echo get_phrase('MMU');?></th>
            <th><?php echo get_phrase('MMU Site');?></th>
            <th><?php echo get_phrase('Vehicle number');?></th>
            <th><?php echo get_phrase('Status');?></th>
            <th><?php echo get_phrase('options');?></th>
        </tr>
    </thead>

    <tbody>  
            <?php if(count($mmuunit) > 0) { ?>
            <?php $i=0; foreach ($mmuunit as $unit) { $i++; ?>

            <tr>
                <td><?php echo $i; ?>.</td>
                <td><?php echo $unit['state_name']; ?></td>
                <td><?php echo $unit['dist_name']; ?></td>
                <td><?php echo $unit['mmu_name']; ?></td>
                <td><a href="<?php echo base_url(); ?>index.php?admin/mmu_location/<?php echo $unit['unit_id']; ?>">Add / Edit Site</a></td>
                <td><?php echo $unit['mmu_number']; ?></td>
                <td>
                <?php 
                    if($unit['status']==0)
                    {
                        echo 'Running';
                    }
                    else{
                        echo '';
                    }
                ?></td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php?admin/edit_mmu/<?php echo $unit['unit_id']; ?>" class="btn btn-default btn-sm btn-icon icon-left">
                            <i class="entypo-pencil"></i>
                            Edit
                    </a>
                    <a href="#" 
                        class="btn btn-danger btn-sm btn-icon icon-left" onclick="return checkDelete();">
                            <i class="entypo-cancel"></i>
                            Delete
                    </a>
                </td>
            </tr>
            <?php } ?>
              <?php } else { ?>
            <tr>
                <td colspan="8">No Unit found</td>
            </tr>
                <?php } ?>
            
    </tbody>
</table>