<table class="table table-bordered table-striped dataTable" id="table-2">
    <thead>
        <tr>
            <th>Patient ID</th>
            <th>Name</th>
            <th>Age</th>
            <th>Gender</th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td><a href="<?php echo base_url(); ?>index.php?admin/patient_details">333443</a></td>
            <td><a href="<?php echo base_url(); ?>index.php?admin/patient_details">Jivan Kumar</a></td>
            <td>30</td>
            <td>Male</td>
        </tr>
        <tr>
            <td><a href="<?php echo base_url(); ?>index.php?admin/patient_details">333443</a></td>
            <td><a href="<?php echo base_url(); ?>index.php?admin/patient_details">Amit Lalwani</a></td>
            <td>42</td>
            <td>Male</td>
        </tr>
        <tr>
            <td><a href="<?php echo base_url(); ?>index.php?admin/patient_details">333444</a></td>
            <td><a href="<?php echo base_url(); ?>index.php?admin/patient_details">Geetika V. Ponduchiru</a></td>
            <td>63</td>
            <td>Female</td>
        </tr>
    </tbody>
</table>

<script type="text/javascript">
    jQuery(window).load(function ()
    {
        var $ = jQuery;

        $("#table-2").dataTable({
            "sPaginationType": "bootstrap",
            /*"sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"*/
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });

        // Highlighted rows
        $("#table-2 tbody input[type=checkbox]").each(function (i, el)
        {
            var $this = $(el),
                    $p = $this.closest('tr');

            $(el).on('change', function ()
            {
                var is_checked = $this.is(':checked');

                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
            });
        });

        // Replace Checboxes
        $(".pagination a").click(function (ev)
        {
            replaceCheckboxes();
        });
    });
</script>