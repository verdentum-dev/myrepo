<div class="col-md-12" style="margin-top: 0px; background-color: gray; padding-top: 30px;">
<div class="col-md-6 col-md-offset-3">
<?php
    foreach($posts as $post) {  ?>
		<div class="panel" style="border:1px solid #e6e6e6;">
		  <div class="panel-heading" style="background-color: #e6e6e6;">
		  	<img src="<?php echo base_url('uploads/spo_image/'.$post['image'])?>" style="width: 50px; height: 50px; border-radius: 50%;">
		  	<span style="padding-left:10px;"><?php echo $post['name'];?></span><br>
		  	<span style="padding-left: 60px;"><?php echo $post['date'];?></span>
		  </div>
		  <div class="panel-body">
		    <p><?php echo $post['text'];?></p>
		    <?php foreach ($post['images'] as $key => $image) { ?>
		    <img width="100%" src="<?php echo base_url(); ?>uploads/post_image/<?php echo $image['image_name'];?>" alt="...">
		    <?php } ?>
		    <p> <?php echo $post['comment'];?> Comments</p>
		  </div>
		</div>
<?php } ?>
	<div style="margin-bottom: 40px;">
		<button class="btn btn-primary btn-block">View More...</button>
	</div>
</div>
</div>