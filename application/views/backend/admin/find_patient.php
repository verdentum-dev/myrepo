<?php $patient_info = $this->db->get('patient')->result_array(); ?>
<div style="clear:both;"></div>
<br>

<div class="row">
    <div class="col-sm-9">
        <label for="patient_id" class="control-label"><?php echo get_phrase('search_patient_name_or_patient_id'); ?> (Click submit to view sample profile)</label>

        <div class="form-group">
            <select name="patient_id" class="select2" id="patient_id">
                <option value=""><?php echo get_phrase('select_patient'); ?></option>
                <?php foreach ($patient_info as $row) { ?>
                <option value="<?php echo $row['patient_id']; ?>"><?php echo $row['name']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>

    <div class="col-sm-3">
        <label>&nbsp;</label>

        <div class="form-group text-center">
            <button class="btn btn-primary"><?php echo get_phrase('submit'); ?></button>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(window).load(function ()
    {
        var $ = jQuery;

        $('button').on('click', function(ev) {
            window.location.href = "<?php echo base_url(); ?>index.php?admin/patient_details";
        });

        /*$('#patient_id').on('change', function(ev) {
            if($(this).val().length > 0) {
                $('button').removeClass('disabled');
            } else {
                $('button').addClass('disabled');
            }
        });*/
        /*$("#table-2").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });

        // Highlighted rows
        $("#table-2 tbody input[type=checkbox]").each(function (i, el)
        {
            var $this = $(el),
                    $p = $this.closest('tr');

            $(el).on('change', function ()
            {
                var is_checked = $this.is(':checked');

                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
            });
        });

        // Replace Checboxes
        $(".pagination a").click(function (ev)
        {
            replaceCheckboxes();
        });*/
    });
</script>