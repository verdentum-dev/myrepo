<!-- <?php echo form_open(base_url() . 'index.php?admin/add_defaultdoctor', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top'));
?>
<?php
  if($this->session->flashdata('succ')) {
    echo '<div class="alert alert-success">'.$this->session->flashdata('succ').'</div>';
  }
  if($this->session->flashdata('err')) {
    echo '<div class="alert alert-danger">'.$this->session->flashdata('err').'</div>';
  }
?> -->
<div id="success"></div>
<form class="form-horizontal form-groups-bordered" id="defaultDoctor">
<div style="clear:both;"></div>
<br>
<h2>Locations</h2>
  <!-- <p>The form below contains three radio buttons. The last option is disabled:</p> -->
  <?php $i=0; foreach ($mmu_location as $row) { $checked = ++$i == 1? ' checked' : ''; ?>
    <div class="radio">
      <label><input type="radio" name="name" value="<?php echo $row['mmuloc_id']?>" <?php echo $checked ?>><?php echo $row['mmuloc_name']?></label>
    </div>
    <?php } ?>
    <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5 text-left">
                        <!-- <button type="submit" class="btn btn-info"><?php echo get_phrase('make as default'); ?></button> -->
                        <button class="btn btn-info" type="submit">Make as Default</button>
                    </div>
                </div>
  </form>


  <script>
  //ajex call for question update
  $('#defaultDoctor').on('submit', function(e) {
    e.preventDefault();
    $('button[type="submit"]').attr('disabled', 'disabled').html('Checking...');
    $('.alert').remove();

    $('input[name="name"]').next('span').remove();
    
    var basic = new FormData($(this)[0]);
    basic.append('doctor_id', <?php echo $this->uri->segment(3);?>);
    $.ajax({
     url: '<?php echo base_url(); ?>index.php?admin/add_defaultdoctor/',
     type: 'POST',
     data: basic,
     processData: false,
     contentType: false,
     error: function() {
      $('button[type="submit"]').removeAttr('disabled').html('Update');
     },
     success: function(data) {
        var data = JSON.parse(data);
      if(data.status > 0){
        
        $('input[name="name"]').after('<span class="error">'+data.name+'</span>');
      }
      if(typeof data.msg !== 'undefined'){
        $('#success').html('<div class="alert alert-success">'+data.msg+'</div>');
      }
      $('button[type="submit"]').removeAttr('disabled').html('Update');
     }
   });
  });
 </script>
