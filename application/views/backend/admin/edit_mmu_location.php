<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" >

            <div class="panel-heading">
                <div class="panel-title">
                    <?php echo get_phrase('Edit MMU Location'); ?>
                </div>
            </div>

            <div class="panel-body">
             <div id="success"></div>
                <form class = "form-horizontal form-groups-bordered validate" id = "updatelocation">
                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo get_phrase('MMU'); ?></label>
                        <div class="col-sm-5">
                            <select name="mmuunit" class="selectboxit">
                                    <option value="">Select MMU Name</option>
                                    <?php foreach ($mmuunit as $unit) {
                                        if($mmulocation_details->mmu_unit == $unit['unit_id']){ ?>
                                        <option value = "<?php echo $unit['unit_id']; ?>" selected  ><?php echo $unit['mmu_name']; ?></option><?php
                                                }
                                        echo '<option value="'.$unit['unit_id'].'" '.set_select('mmuunit', $unit['unit_id']).'>'.$unit['mmu_name'].'</option>';
                                    } ?>
                            </select>
                            <?php echo form_error('mmuunit'); ?>
                        </div>
                    </div>


                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo get_phrase('MMU Location Name'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="mmu_location" value="<?php echo $mmulocation_details->mmuloc_name; ?>">
                            <?php echo form_error('mmu_location'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-3 control-label col-sm-offset-2">
                            <input type="submit" class="btn btn-success" value="Update">
                        </div>
                    </div>
                </form>

            </div>

        </div>

    </div>
</div>
<script type="text/javascript">
    $(function(){
        $( "#datepicker" ).datepicker({
            endDate : new Date()
        });

        $('#updatelocation').on('submit', function(event) {
            event.preventDefault();
            $('button[type="submit"]').attr('disabled', 'disabled').html('Updating mmu location...');
            $('.alert').remove();
           
            $('selectboxit[name="mmuunit"]').next('span').remove();
            $('input[name="mmu_location"]').next('span').remove();
            
            var basic = new FormData($(this)[0]);
            basic.append('mmu_location_id', '<?php echo $mmulocation_details->mmuloc_id; ?>');
            $.ajax({
                url: '<?php echo base_url(); ?>index.php?admin/updatemmu_location/',
                type: 'POST',
                data: basic,
                processData: false,
                contentType: false,
                error: function() {
                    $('button[type="submit"]').removeAttr('disabled').html('Update');
                },
                success: function(data) {
                    var data = JSON.parse(data);
                    if(data.status > 0){
                        $('selectboxit[name="mmuunit"]').after('<span class="error">'+data.mmuunit+'</span>');
                        $('input[name="mmu_location"]').after('<span class="error">'+data.mmu_location+'</span>');
                    }
                    if(typeof data.msg !== 'undefined'){
                        $('#success').html('<div class="alert alert-success">'+data.msg+'</div>');
                    }
                    $('button[type="submit"]').removeAttr('disabled').html('Update');
                }
            });
        });
    });
</script>
