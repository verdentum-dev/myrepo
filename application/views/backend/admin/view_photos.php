<div class="row">
	<div class="col-md-12" style="background-color: white; margin-bottom: 10px;">
    <div style="background-color: #00004d; color: white; font-size:10px; padding-left: 5px;">
    	<p style="padding: 5px;">Updated Post</p>
    </div>

		    <div class="form-group">
		    	<select class="form-control" id="mmu_name">
		    		<option value="">Select MMU</option>
			    		<?php
			             foreach($mmu_loc as $mmu) {  ?>
			    		    <option value="<?php echo $mmu['unit_id'];?>"><?php echo $mmu['mmu_name'];?></option>
			    		  <?php }?>    
		    	</select>
		    </div>

		    <div class="all_images">
			    <?php
			    $num = count($post);
			    if($num > 0 && $num <= 50)
			    {
				    foreach($post as $post) 
				    	{?>
						    <div style="width: 10%; height: 130px; background-color: white; float: left;">
							  <img src="<?php echo base_url('uploads/post_image/'.$post['image_name'])?>" style="width:100%; height: 130px; float: left; padding-right:5px; padding-top: 5px;">
							</div>
						 <?php }
				} 

			    if($num > 50)
					{
						foreach($post as $post) 
				    	{?>
						    <div style="width: 10%; height: 130px; background-color: white; float: left;">
							  <img src="<?php echo base_url('uploads/post_image/'.$post['image_name'])?>" style="width:100%; height: 130px; float: left; padding-right:5px; padding-top: 5px;">
							</div>
						 <?php }
						 echo '<button class="btn btn-primary btn-block" style="margin-top:20px;">';
						 echo 'View All';
						 echo '</button>';
					}

				if($num < 0){
				 	echo '<h3 class="msg">';
				 	echo "Image Not Fund !";
				 	echo '</h3>';
				 	} ?>	 		
			</div>

			<div id="img_by_mmu">
			   
			</div>
            
          
    </div>
     
</div>
<style type="text/css">
	#mmu_name_head
	{
		background-color: #9999ff;
		color: white;
	}

	

	.msg{
		color: red;
		text-align: center;
		font-size: 20px;
	}
</style>

<script type="text/javascript">
    $('#mmu_name').on('change', function() {
    var mmu_name=this.value;
        $.ajax({
            url: "<?php echo base_url();?>index.php?admin/filter_image",
            type: "POST",
            data: "mmu_name="+mmu_name,
            success: function(response)
            {
            if (!$.trim(response)){  
            	 $('.all_images').hide();
                   $('#img_by_mmu').html("<p class='msg'>Image Not Found !</p>");
                }
                else{ 
                   $('.all_images').hide();   
                   $('#img_by_mmu').html(response);
                }
            }
        });
    })
</script>

<!--  <script type="text/javascript">
        function codeAddress() {
            alert('ok');
        }
        window.onload = codeAddress;
        </script> -->