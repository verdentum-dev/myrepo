<?php /*echo form_open_multipart(base_url() . 'index.php?admin/add_doctor', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top'));*/
?>
<!-- <?php
  if($this->session->flashdata('succ')) {
    echo '<div class="alert alert-success">'.$this->session->flashdata('succ').'</div>';
  }
  if($this->session->flashdata('err')) {
    echo '<div class="alert alert-danger">'.$this->session->flashdata('err').'</div>';
  }
?> -->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" >
            <div class="panel-heading">
                <div class="panel-title">
                    <?php echo get_phrase('Add Doctor'); ?>
                </div>
            </div>

            <div class="panel-body">
                <div id="success"></div>
                <form class="form-horizontal form-groups-bordered" id="adddoctor">

                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo get_phrase('Name'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="name" value="<?php echo set_value('name');?>">
                            <?php echo form_error('name'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo get_phrase('Mrc No.'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="mrc" value="<?php echo set_value('mrc');?>">
                            <?php echo form_error('mrc'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo get_phrase('Email-ID'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="email" value="<?php echo set_value('email');?>">
                            <?php echo form_error('email'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo get_phrase('Password'); ?></label>
                        <div class="col-sm-5">
                            <input type="Password" class="form-control" name="pass" value="<?php echo set_value('pass');?>">
                            <?php echo form_error('pass'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo get_phrase('Phone'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="phone" value="<?php echo set_value('phone');?>">
                            <?php echo form_error('phone'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo get_phrase('MMU Name'); ?></label>
                        <div class="col-sm-5">
                            <select name="mmu" class="selectboxit">
                                <option value="">Select MMU Name</option>
                                    <?php foreach ($allmmuunit as $mmu) {
                                         echo '<option value="'.$mmu['unit_id'].'" '.set_select('mmu', $mmu['unit_id']).'>'.$mmu['mmu_name'].'</option>';
                                    } ?>
                            </select>
                            <p class="error" id="errmmu"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo get_phrase('Department'); ?></label>
                        <div class="col-sm-5">
                            <select name="department" class="selectboxit">
                                <option value="">Select Department Name</option>
                                    <?php foreach ($alldepartment as $department) {
                                         echo '<option value="'.$department['department_id'].'" '.set_select('department', $department['department_id']).'>'.$department['name'].'</option>';
                                    } ?>
                            </select>
                            <p class="error" id="errdept"></p>
                        </div>
                    </div>
                    <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo get_phrase('image'); ?></label>

                            <div class="col-sm-5">

                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                                        <img src="http://placehold.it/200x150" alt="...">
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                    <div>
                                        <span class="btn btn-white btn-file">
                                            <span class="fileinput-new">Select image</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="image" accept="image/*">
                                        </span>
                                        <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo get_phrase('Address'); ?></label>
                        <div class="col-sm-5">
                            <textarea name="address" class="form-control" id="field-ta"><?php echo set_value('address');?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo get_phrase('Profile'); ?></label>
                        <div class="col-sm-5">
                            <textarea name="profile" class="form-control html5editor" id="field-ta" data-stylesheet-url="assets/css/wysihtml5-color.css"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-success"><?php echo get_phrase('save'); ?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('#adddoctor').on('submit', function(event) {
            event.preventDefault();
            $('button[type="submit"]').attr('disabled', 'disabled').html('Uploading doctor...');
            $('.alert').remove();

            $('input[name="name"]').next('span').remove();
            $('input[name="mrc"]').next('span').remove();
            $('input[name="email"]').next('span').remove();
            $('input[name="pass"]').next('span').remove();
            $('#errmmu').html('');
            $('#errdept').html('');
            $('textarea[name="address"]').next('span').remove();
            $('textarea[name="profile"]').next('span').remove();
            $('input[name="phone"]').next('span').remove();
            $('input[name="image"]').next('span').remove();
           
            var basic = new FormData($(this)[0]);
            $.ajax({
                url: '<?php echo base_url(); ?>index.php?admin/insert_doctor/',
                type: 'POST',
                data: basic,
                processData: false,
                contentType: false,
                error: function() {
                    $('button[type="submit"]').removeAttr('disabled').html('Submit');
                },
                success: function(data) {
                    var data = JSON.parse(data);
                    if(data.status > 0){                
                        $('input[name="name"]').after('<span class="error">'+data.name+'</span>');
                        $('input[name="mrc"]').after('<span class="error">'+data.mrc+'</span>');
                        $('input[name="email"]').after('<span class="error">'+data.email+'</span>');
                        $('input[name="pass"]').after('<span class="error">'+data.pass+'</span>');
                        $('input[name="phone"]').after('<span class="error">'+data.phone+'</span>');
                        $('#errmmu').html(data.mmu);
                        $('#errdept').html(data.department);
                    }
                    if(typeof data.msg !== 'undefined'){
                        $('#success').html('<div class="alert alert-success">'+data.msg+'</div>');
                        $('form')[0].reset();
                        var selectBox = $("select").selectBoxIt();
                        $(selectBox).selectBoxIt('selectOption', 0);
                    }
                    $('button[type="submit"]').removeAttr('disabled').html('Submit');
                }
            });
        });
    });
</script>

