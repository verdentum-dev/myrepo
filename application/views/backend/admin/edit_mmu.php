
<div class="row">
	<div class="col-md-12">

		<div class="panel panel-primary" >

			<div class="panel-heading">
				<div class="panel-title">
					<?php echo get_phrase('Add MMU'); ?>
				</div>
			</div>

			<div class="panel-body">
				 <div id="success"></div>
				<form class="form-horizontal form-groups-bordered" id="editmmu">

				<div class="form-group">
					<label  class="col-sm-3 control-label"><?php echo get_phrase('State Name'); ?></label>
					<div class="col-sm-5">
						<select name="state" class="selectboxit">
							<option value="">Select State Name</option><?php 
							foreach ($states as $state) {
								if($getmmuby_id->state_id == $state['state_id']){ ?>
                                    <option value = "<?php echo $state['state_id']; ?>" selected  ><?php echo $state['state_name']; ?></option><?php
                                            }
								echo '<option value="'.$state['state_id'].'">'.$state['state_name'].'</option>';
							} ?>
						</select>
					</div>
				</div>


				<div class="form-group">
					<label  class="col-sm-3 control-label"><?php echo get_phrase('District Name'); ?></label>
					<div class="col-sm-5">
						<select name="district" class="selectboxit">
							<option value="">Select District Name</option><?php 
							foreach ($district as $d) {
								if($getmmuby_id->dist_id == $d['dist_id']){ ?>
                                    <option value = "<?php echo $d['dist_id']; ?>" selected  ><?php echo $d['dist_name']; ?></option><?php
                                }
                                echo '<option value="'.$d['dist_id'].'">'.$d['dist_name'].'</option>';
							} ?>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label"><?php echo get_phrase('City Or Town Name'); ?></label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="city" value = "<?php echo $getmmuby_id->city_town; ?>" required>
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label"><?php echo get_phrase('Nagar Panchayat Or Municipality'); ?></label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="panchayat" value = "<?php echo $getmmuby_id->panchayat_municipality; ?>" required>
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label"><?php echo get_phrase('MMU Name'); ?></label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="mmu_name" value = "<?php echo $getmmuby_id->mmu_name; ?>" required>
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label"><?php echo get_phrase('MMU Number'); ?></label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="mmu_number" value = "<?php echo $getmmuby_id->mmu_number; ?>" required>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-3 control-label col-sm-offset-2">
                        <input type="submit" class="btn btn-success" value="Update">
                    </div>
				</div>
				</form>

			</div>

		</div>

	</div>
</div>


<?php echo form_close(); ?>

<script type="text/javascript">
	$(function() {
		$('select[name="state"]').on('change', function() {
			var elem = $(this);
			if(elem.val().length != 0) elem.next('span').next('p').remove();

			$('select[name="district"]').html('<option value="">Select District Name</option>');

			$.ajax({
				url: '<?php echo base_url(); ?>index.php?admin/get_district',
				type: 'POST',
				data: {
					state: elem.val()
				},
				dataType: 'json',
				error: function() {
				},
				success: function(data) {
					if(data.districts.length > 0) {
						data.districts.forEach(function(district) {
							$('select[name="district"]').append('<option value="'+district.dist_id+'">'+district.dist_name+'</option>');
						});
						$('select[name="district"]').data("selectBox-selectBoxIt").refresh();
					}
				}
			});
		});

		$('select[name="district"]').on('change', function() {
			var elem = $(this);
			if(elem.val().length != 0) elem.next('span').next('p').remove();
		});
	});
</script>
<script type="text/javascript">
    $(function(){
        $('#editmmu').on('submit', function(event) {
            event.preventDefault();
            $('button[type="submit"]').attr('disabled', 'disabled').html('Updating mmu...');
            $('.alert').remove();

            $('selectboxit[name="state"]').next('span').remove();
            $('selectboxit[name="district"]').next('span').remove();
            $('input[name="city"]').next('span').remove();
            $('input[name="panchayat"]').next('span').remove();
            $('input[name="mmu_number"]').next('span').remove();
                        
            var basic = new FormData($(this)[0]);
            basic.append('mmu_id', '<?php echo $getmmuby_id->unit_id; ?>');
            $.ajax({
                url: '<?php echo base_url(); ?>index.php?admin/updatemmu/',
                type: 'POST',
                data: basic,
                processData: false,
                contentType: false,
                error: function() {
                    $('button[type="submit"]').removeAttr('disabled').html('Update');
                },
                success: function(data) {
                    var data = JSON.parse(data);
                    if(data.status > 0){
                        
                      	$('selectboxit[name="state"]').after('<span class="error">'+data.state+'</span>');
                        $('selectboxit[name="district"]').after('<span class="error">'+data.district+'</span>');
                        $('input[name="city"]').after('<span class="error">'+data.city+'</span>');
                        $('input[name="panchayat"]').after('<span class="error">'+data.panchayat+'</span>');
                        $('input[name="mmu_number"]').after('<span class="error">'+data.mmu_number+'</span>');
                    }
                    if(typeof data.msg !== 'undefined'){
                        $('#success').html('<div class="alert alert-success">'+data.msg+'</div>');
                    }
                    $('button[type="submit"]').removeAttr('disabled').html('Update');
                }
            });
        });
    });
</script>