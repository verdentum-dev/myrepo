<div class="row">

    <div class="col-md-12">
          <div class="col-lg-9">
              <!-- Panel Kitchen Sink -->
              <div class="panel">
                <div class="panel-body">
                  <div class="col-md-12" style="background-color:#2a2a2d;">
                    <p style="color: white; padding-top:5px; font-size: 17px;">Today's Patient report</p>
                  </div>
                  <table class="table table-bordered table-striped datatable" id="table-1">
                        <thead>
                            <tr>
                                <th><?php echo get_phrase('Location');?></th>
                                <th><?php echo get_phrase('No. of Patient');?></th>
                                <th><?php echo get_phrase('Male');?></th>
                                <th><?php echo get_phrase('Female');?></th>
                            </tr>
                        </thead>

                        <tbody> 
                        <?php 
                        foreach ($today_pat_by_mmu as $key => $pc) 
                              {?>  
                                <tr>
                                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">
                                    <?php echo $pc['pcount']['mmu_name']?></a></td>
                                    <td>
                                        <?php echo $pc['pcount']['count'];?>    
                                    </td>
                                    <td><?php 
                                        if(($pc['pcount']['male']) == '')
                                            {
                                                echo "0";
                                            }
                                        else
                                            {
                                                echo $pc['pcount']['male'];
                                            }
                                        ?>    </td>
                                    <td><?php
                                        if(($pc['pcount']['female']) == '')
                                            {
                                                echo "0";
                                            }
                                        else
                                            {
                                                echo $pc['pcount']['female'];
                                            }
                                        ?></td>
                                </tr>
                              <?php }?>
                        </tbody>
                    </table>

                </div>
              </div>
            </div>  
            <div class="col-lg-3">
              <div class="col-md-12" style="background-color: gray; padding-bottom: 30px; margin-top: 16px;">
                <p style="font-size: 50px; color: #f2f2f2; text-align: center; padding-top: 50px;"><?php echo $pat_by_day;?></p>
                <p style="color: white; text-align: center; font-size: 25px;">Patient Today</p>
              </div>

              <div class="col-md-12" style="background-color: gray; padding-bottom: 30px; margin-top: 16px;">
                <p style="font-size: 50px; color: #f2f2f2; text-align: center; padding-top: 50px;"><?php echo $pat_by_month;?></p>
                <p style="color: white; text-align: center; font-size: 25px;">Patient This Month</p>
              </div>
            </div>       

    </div>



<div class="col-md-12">
      <div class="col-lg-9">
          <!-- Panel Kitchen Sink -->
          <div class="panel">
            <div class="panel-body">
             <div class="col-md-12" style="background-color:#2a2a2d;">
                <p style="color: white; padding-top:5px; font-size: 17px;">Today's Medicine report</p>
              </div>
            <table class="table table-bordered table-striped datatable" id="table-2">
                  <thead>
                      <tr>
                          <th><?php echo get_phrase('Location');?></th>
                          <th><?php echo get_phrase('Prescribed');?></th>
                          <th><?php echo get_phrase('Distributed');?></th>
                      </tr>
                  </thead>

                  <tbody>
                  <?php 
                    foreach ($today_med_by_mmu as $key => $med) 
                          {?>  
                             <tr>
                              <td class="tablesaw-cell-persist">
                                <?php echo $med['mmu_name'];?>
                              </td>
                              <td>
                              <?php 
                                        if(($med['medicinecount']['mcount']) == '')
                                            {
                                                echo "0";
                                            }
                                        else
                                            {
                                                echo $med['medicinecount']['mcount'];
                                            }
                                        ?></td>
                              <td>
                              <?php 
                                        if(($med['medicinecount']['mcount']) == '')
                                            {
                                                echo "0";
                                            }
                                        else
                                            {
                                                echo $med['medicinecount']['mcount'];
                                            }
                                        ?></td>
                            </tr>
                          <?php }?>
                  </tbody>
              </table>
            </div>
          </div>
        </div>
         <div class="col-lg-3">
              <div class="col-md-12" style="background-color: gray; padding-bottom: 30px; margin-top: 16px;">
                <p style="font-size: 50px; color: #f2f2f2; text-align: center; padding-top: 50px;"><?php echo $med_by_day->quantity;?></p>
                <p style="color: white; text-align: center; font-size: 25px;">Medicine Today</p>
              </div>

               <div class="col-md-12" style="background-color: gray;  margin-top: 16px;">
                <p style="font-size: 50px; color: #f2f2f2; text-align: center; padding-top: 50px;"><?php echo $med_by_month->quantity;?></p>
                <p style="color: white; text-align: center; font-size: 25px; padding-bottom: 30px;">Medicine Distributed This Month</p>
              </div>
            </div> 
    </div>
</div>


<script type="text/javascript">
    jQuery(window).load(function ()
    {
        var $ = jQuery;

        $("#table-2").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });

        // Highlighted rows
        $("#table-2 tbody input[type=checkbox]").each(function (i, el)
        {
            var $this = $(el),
                    $p = $this.closest('tr');

            $(el).on('change', function ()
            {
                var is_checked = $this.is(':checked');

                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
            });
        });

        // Replace Checboxes
        $(".pagination a").click(function (ev)
        {
            replaceCheckboxes();
        });
    });
</script>


<script type="text/javascript">
    jQuery(window).load(function ()
    {
        var $ = jQuery;

        $("#table-1").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });

        // Highlighted rows
        $("#table-1 tbody input[type=checkbox]").each(function (i, el)
        {
            var $this = $(el),
                    $p = $this.closest('tr');

            $(el).on('change', function ()
            {
                var is_checked = $this.is(':checked');

                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
            });
        });

        // Replace Checboxes
        $(".pagination a").click(function (ev)
        {
            replaceCheckboxes();
        });
    });
</script>

<!-- <img src="<?php echo base_url(); ?>uploads/dashboard.png" alt="dashboard" style="width:100%;"> -->

<!-- <div class="row">
    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/doctor">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="icon"><i class="fa fa-user-md"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('doctor'); ?>"
                     data-duration="1500" data-delay="0">0</div>
                <h3><?php echo get_phrase('doctor'); if($this->db->count_all('doctor') > 1) echo 's'; ?></h3>
            </div>
        </a>
    </div>

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/patient">
            <div class="tile-stats tile-white-red">
                <div class="icon"><i class="fa fa-user"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('patient'); ?>" 
                     data-duration="1500" data-delay="0">0</div>
                <h3><?php echo get_phrase('patient'); if($this->db->count_all('patient') > 1) echo 's'; ?></h3>
            </div>
        </a>
    </div>

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/medicine">
            <div class="tile-stats tile-white-orange">
                <div class="icon"><i class="fa fa-medkit"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('medicine'); ?>" 
                     data-duration="1500" data-delay="0">0</div>
                <h3><?php echo get_phrase('medicine'); if($this->db->count_all('medicine') > 1) echo 's'; ?></h3>
            </div>
        </a>
    </div>

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/nurse">
            <div class="tile-stats tile-white-aqua">
                <div class="icon"><i class="fa fa-plus-square"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('nurse'); ?>" 
                     data-duration="1500" data-delay="0">0 &pound;</div>
                <h3><?php echo get_phrase('nurse') ?></h3>
            </div>
        </a>
    </div>

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/pharmacist">
            <div class="tile-stats tile-white-blue">
                <div class="icon"><i class="fa fa-medkit"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('pharmacist'); ?>" 
                     data-duration="1500" data-delay="0">0 &pound;</div>
                <h3><?php echo get_phrase('pharmacist') ?></h3>
            </div>
        </a>
    </div>
</div>

<br />

<div class="row">
    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/laboratorist">
            <div class="tile-stats tile-white-cyan">
                <div class="icon"><i class="fa fa-user"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('laboratorist'); ?>" 
                     data-duration="1500" data-delay="0">0 &pound;</div>
                <h3><?php echo get_phrase('laboratorist') ?></h3>
            </div>
        </a>
    </div>

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/accountant">
            <div class="tile-stats tile-white-purple">
                <div class="icon"><i class="fa fa-money"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('accountant'); ?>" 
                     data-duration="1500" data-delay="0">0 &pound;</div>
                <h3><?php echo get_phrase('accountant') ?></h3>
            </div>
        </a>
    </div>

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/payment_history">
            <div class="tile-stats tile-white-pink">
                <div class="icon"><i class="fa fa-list-alt"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('invoice'); ?>" 
                     data-duration="1500" data-delay="0">0 &pound;</div>
                <h3><?php echo get_phrase('payment') ?></h3>
            </div>
        </a>
    </div>

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/medicine">
            <div class="tile-stats tile-white-orange">
                <div class="icon"><i class="fa fa-medkit"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('medicine'); ?>" 
                     data-duration="1500" data-delay="0">0 &pound;</div>
                <h3><?php echo get_phrase('medicine') ?></h3>
            </div>
        </a>
    </div>
</div>

<br />

<div class="row">
    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/operation_report">
            <div class="tile-stats tile-white-green">
                <div class="icon"><i class="fa fa-wheelchair"></i></div>
                <div class="num" data-start="0" data-end="<?php echo count($this->db->get_where('report', array('type' => 'operation'))->result_array());?>" 
                     data-duration="1500" data-delay="0"></div>
                <h3><?php echo get_phrase('operation_report') ?></h3>
            </div>
        </a>
    </div>

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/birth_report">
            <div class="tile-stats tile-white-brown">
                <div class="icon"><i class="fa fa-github-alt"></i></div>
                <div class="num" data-start="0" data-end="<?php echo count($this->db->get_where('report', array('type' => 'birth'))->result_array());?>" 
                     data-duration="1500" data-delay="0"></div>
                <h3><?php echo get_phrase('birth_report') ?></h3>
            </div>
        </a>
    </div>

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/death_report">
            <div class="tile-stats tile-white-plum">
                <div class="icon"><i class="fa fa-ban"></i></div>
                <div class="num" data-start="0" data-end="<?php echo count($this->db->get_where('report', array('type' => 'death'))->result_array());?>" 
                     data-duration="1500" data-delay="0"></div>
                <h3><?php echo get_phrase('death_report') ?></h3>
            </div>
        </a>
    </div>

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/system_settings">
            <div class="tile-stats tile-white-gray">
                <div class="icon"><i class="fa fa-h-square"></i></div>
                <div class="num">&nbsp;</div>
                <h3><?php echo get_phrase('settings') ?></h3>
            </div>
        </a>
    </div>
</div> -->