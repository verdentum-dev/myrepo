<?php echo form_open(base_url() . 'index.php?admin/add_mmu', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top'));
?>
<div class="row">
	<div class="col-md-12">

		<div class="panel panel-primary" >

			<div class="panel-heading">
				<div class="panel-title">
					<?php echo get_phrase('Add MMU'); ?>
				</div>
			</div>

			<div class="panel-body">

				<div class="form-group">
					<label  class="col-sm-3 control-label"><?php echo get_phrase('State Name'); ?></label>
					<div class="col-sm-5">
						<select name="state" class="selectboxit">
							<option value="">Select State Name</option>
							<?php foreach ($states as $state) {
								echo '<option value="'.$state['state_id'].'">'.$state['state_name'].'</option>';
							} ?>
						</select>
					</div>
				</div>


				<div class="form-group">
					<label  class="col-sm-3 control-label"><?php echo get_phrase('District Name'); ?></label>
					<div class="col-sm-5">
						<select name="district" class="selectboxit">
							<option value="">Select District Name</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label"><?php echo get_phrase('City Or Town Name'); ?></label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="city" required>
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label"><?php echo get_phrase('Nagar Panchayat Or Municipality'); ?></label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="panchayat" required>
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label"><?php echo get_phrase('MMU Name'); ?></label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="mmu_name" required>
					</div>
				</div>

				<div class="form-group">
					<label  class="col-sm-3 control-label"><?php echo get_phrase('MMU Number'); ?></label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="mmu_number" required>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-5">
						<button type="submit" class="btn btn-info"><?php echo get_phrase('save'); ?></button>
					</div>
				</div>

			</div>

		</div>

	</div>
</div>


<?php echo form_close(); ?>

<script type="text/javascript">
	$(function() {
		$('select[name="state"]').on('change', function() {
			var elem = $(this);
			if(elem.val().length != 0) elem.next('span').next('p').remove();

			$('select[name="district"]').html('<option value="">Select District Name</option>');

			$.ajax({
				url: '<?php echo base_url(); ?>index.php?admin/get_district',
				type: 'POST',
				data: {
					state: elem.val()
				},
				dataType: 'json',
				error: function() {
				},
				success: function(data) {
					if(data.districts.length > 0) {
						data.districts.forEach(function(district) {
							$('select[name="district"]').append('<option value="'+district.dist_id+'">'+district.dist_name+'</option>');
						});
						$('select[name="district"]').data("selectBox-selectBoxIt").refresh();
					}
				}
			});
		});

		$('select[name="district"]').on('change', function() {
			var elem = $(this);
			if(elem.val().length != 0) elem.next('span').next('p').remove();
		});

		$('form').on('submit', function(ev) {
			var state = $('select[name="state"]');
			var district = $('select[name="district"]');
			state.next('span').next('p').remove();
			district.next('span').next('p').remove();

			if(state.val().length == 0) {
				state.next('span').after('<p class="error">This field is required.</p>');
				ev.preventDefault();
			}

			if(district.val().length == 0) {
				district.next('span').after('<p class="error">This field is required.</p>')
				ev.preventDefault();
			}
		});
	});
</script>