<?php $doctor_info = $this->db->get('doctor')->result_array(); ?>
<?php $patient_info = $this->db->get('patient')->result_array(); ?>
<?php if(!isset($show_patient)) { ?>
<div class="row">
    <div class="col-sm-9">
        <label for="patient_id" class="control-label"><?php echo get_phrase('search_patient_name_or_patient_id'); ?></label>

        <div class="form-group">
            <select name="patient_id" class="select2" id="patient_id">
                <option value=""><?php echo get_phrase('select_patient'); ?></option>
                <?php foreach ($patient_info as $row) { ?>
                <option value="<?php echo $row['patient_id']; ?>" selected><?php echo $row['name']; ?></option>
                <?php } ?>
            </select>
        </div>
    </div>

    <div class="col-sm-3">
        <label>&nbsp;</label>

        <div class="form-group text-center">
            <button class="btn btn-primary disabled"><?php echo get_phrase('submit'); ?></button>
        </div>
    </div>
</div>
<?php } ?>

<div class="row">
    <div class="col-md-3">
      <!-- Page Widget -->
      <div class="widget widget-shadow text-center">
        <div class="widget-header">
          <div class="widget-header-content">
            <img src="<?php echo base_url(); ?>uploads/profile.jpg" class="img-circle" width="40px" height="40px">
            <h3 class="profile-user">Jivan Kumar</h3>
            <p class="profile-job">Id No : 333443</p>
            <p>Gender: Male | Age: 30</p>
            <p>Location: Agartala</p>
            <p>Last Visited: 21 Jan 2017 21:00</p>
          </div>
        </div>
        
      </div>
      <!-- End Page Widget -->
    </div>

    <div class="col-md-9">

        <div class="panel panel-primary" data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    <h4 style="color:#FFF;"><?php echo get_phrase('enter_below_details'); ?></h4>
                </div>
            </div>

            <div class="panel-body">

                <form role="form" class="form-horizontal form-groups-bordered" action="<?php echo base_url(); ?>index.php?admin/add_checkup/create" method="post" enctype="multipart/form-data">

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('date'); ?></label>

                        <div class="col-sm-7">
                            <div class="date-and-time">
                                <input type="text" name="date_timestamp" class="form-control datepicker" data-format="D, dd MM yyyy" placeholder="date here">
                                <input type="text" name="time_timestamp" class="form-control timepicker" data-template="dropdown" data-show-seconds="false" data-default-time="00:05 AM" data-show-meridian="false" data-minute-step="5"  placeholder="time here">
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('report_type'); ?></label>

                        <div class="col-sm-5">
                            <select name="report_type" class="form-control">
                                <option value=""><?php echo get_phrase('select_report_type'); ?></option>
                                <option value="xray"><?php echo get_phrase('xray'); ?></option>
                                <option value="blood_test"><?php echo get_phrase('blood_test'); ?></option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('upload_picture'); ?></label>

                        <div class="col-sm-5">

                            <input type="file" name="file_name" class="form-control file2 inline btn btn-primary" data-label="<i class='glyphicon glyphicon-file'></i> Browse" />

                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('document_type'); ?></label>

                        <div class="col-sm-5">
                            <select name="document_type" class="form-control">
                                <option value=""><?php echo get_phrase('select_document_type'); ?></option>
                                <option value="image"><?php echo get_phrase('image'); ?></option>
                                <option value="doc"><?php echo get_phrase('doc'); ?></option>
                                <option value="pdf"><?php echo get_phrase('pdf'); ?></option>
                                <option value="excel"><?php echo get_phrase('excel'); ?></option>
                                <option value="other"><?php echo get_phrase('other'); ?></option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('prescription_details'); ?></label>

                        <div class="col-sm-9">
                            <textarea name="description" class="form-control html5editor" id="field-ta" data-stylesheet-url="assets/css/wysihtml5-color.css"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('no_of_units_of_medicine'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" name="phone" class="form-control" id="field-1" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('allergies'); ?></label>

                        <div class="col-sm-9">
                            <textarea name="description" class="form-control html5editor" id="field-ta" data-stylesheet-url="assets/css/wysihtml5-color.css"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('additional_note'); ?></label>

                        <div class="col-sm-9">
                            <textarea name="description" class="form-control html5editor" id="field-ta" data-stylesheet-url="assets/css/wysihtml5-color.css"></textarea>
                        </div>
                    </div>
                    
                    <input type="hidden" name="prescription_id" value="<?php echo $prescription_id; ?>">

                    <div class="col-sm-3 control-label col-sm-offset-2">
                        <input type="submit" class="btn btn-success" value="Submit">
                    </div>
                </form>

            </div>

        </div>

    </div>
</div>