<!-- <button onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/add_medicine/');" 
    class="btn btn-primary pull-right">
        <?php echo get_phrase('add_medicine'); ?>
</button> -->
<a href="<?php echo base_url();?>index.php?admin/add_medicine/" class="btn btn-primary pull-right">Add Medicine</a>
<div style="clear:both;"></div>
<br>
<table class="table table-bordered table-striped datatable" id="table-2">
    <thead>
        <tr>
            <th><?php echo get_phrase('name'); ?></th>
            <th><?php echo get_phrase('no_of_units'); ?></th>
            <th><?php echo get_phrase('units_sold'); ?></th>
            <th><?php echo get_phrase('available_in_stock'); ?></th>
            <th><?php echo get_phrase('Option'); ?></th>
        </tr>
    </thead>

    <tbody>
        <?php foreach ($medicine_info as $row) { ?>   
            <tr>
                <td><?php echo $row['name'] ?></td>
                <td><a href="<?php echo base_url();?>index.php?admin/medicine_stock/<?php echo $row['medicine_id'] ?>"><?php echo $row['no_of_unit'] ?></a></td>
                <td><!-- <a href="<?php echo base_url();?>index.php?admin/medicine_sold/<?php echo $row['medicine_id'] ?>"> --><?php echo $row['units_sold'] ?><!-- </a> --></td>
                <td><?php echo $row['no_of_unit'] - $row['units_sold'] ?></td>
                <!-- <td><?php echo $row['manufacturing_company'] ?></td>
                <td><?php echo $row['status'] ?></td> -->
                <td>
                    <a href="<?php echo base_url(); ?>index.php?admin/medicine_update/<?php echo $row['medicine_id'] ?>" 
                        class="btn btn-default btn-sm btn-icon icon-left">
                        <i class="entypo-pencil"></i>
                        Edit
                    </a>
                    <!-- <a href="<?php echo base_url(); ?>index.php?admin/medicine/delete/<?php echo $row['medicine_id'] ?>" 
                       class="btn btn-danger btn-sm btn-icon icon-left" onclick="return checkDelete();">
                        <i class="entypo-cancel"></i>
                        Delete
                    </a> -->
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<script type="text/javascript">
    jQuery(window).load(function ()
    {
        var $ = jQuery;

        $("#table-2").dataTable({
            "sPaginationType": "bootstrap",
            /*"sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"*/
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });

        // Highlighted rows
        $("#table-2 tbody input[type=checkbox]").each(function (i, el)
        {
            var $this = $(el),
                    $p = $this.closest('tr');

            $(el).on('change', function ()
            {
                var is_checked = $this.is(':checked');

                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
            });
        });

        // Replace Checboxes
        $(".pagination a").click(function (ev)
        {
            replaceCheckboxes();
        });
    });
</script>