<?php echo form_open(base_url() . 'index.php?mmuadmin/add_mmu_location', array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top'));
?>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" >

            <div class="panel-heading">
                <div class="panel-title">
                    <?php echo get_phrase('Add MMU Site'); ?>
                </div>
            </div>

            <div class="panel-body">

            <div class="form-group">
                    <label  class="col-sm-3 control-label"><?php echo get_phrase('MMU'); ?></label>
                    <div class="col-sm-5">
                        <select name="mmuunit" class="selectboxit">
                                <option value="">Select MMU Name</option>
                                <?php foreach ($mmuunit as $unit) {
                                    echo '<option value="'.$unit['unit_id'].'" '.set_select('mmuunit', $unit['unit_id']).'>'.$unit['mmu_name'].'</option>';
                                } ?>
                        </select>
                        <?php echo form_error('mmuunit'); ?>
                    </div>
                </div>


                <div class="form-group">
                    <label  class="col-sm-3 control-label"><?php echo get_phrase('MMU Site Name'); ?></label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="mmu_location" value="<?php echo set_value('mmu_location');?>">
                        <?php echo form_error('mmu_location'); ?>
                    </div>

                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('save'); ?></button>
                    </div>
                </div>

            </div>

        </div>

    </div>
</div>


</form>