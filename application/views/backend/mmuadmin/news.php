<style type="text/css">
	input[type="file"] {
	  display: block;
	}
	.imageThumb {
	  max-height: 75px;
	  border: 2px solid;
	  padding: 1px;
	  cursor: pointer;
	}
	.pip {
	  display: inline-block;
	  margin: 10px 10px 0 0;
	}
	.remove {
	  display: block;
	  background: #444;
	  border: 1px solid black;
	  color: white;
	  text-align: center;
	  cursor: pointer;
	}
	.remove:hover {
	  background: white;
	  color: black;
	}
</style>
<div class="col-md-12" style="margin-top: 0px; padding-top: 30px;">

<div class="col-md-6 col-md-offset-3">
	<div class="panel">
		<div class="panel-heading" style="font-size: 18px; background-color: #f2f2f2; padding-left:30px; padding-top: 20px;">
			Add Post
		</div>
		<div class="panel-body" style="background-color: #f2f2f2;">
			
			<?php echo form_open_multipart(base_url().'index.php?mmuadmin/post', array('class' => 'form-group', 'method' => 'post','id'=>'target'));
                ?>
				<div class="col-md-12">
					<?php form_error('text'); ?>
					<textarea class="form-control" placeholder="Write here ..." rows="5" name="text"></textarea>
					<p class="error error-text" style="font-size: 10px;"></p>
				</div>
				<div class="col-md-6" style="margin-top: 10px;">
					<select class="form-control" name="mmu">
						<option value="">Select MMU Site</option><?php 
						foreach ($m_loc as $mloc) {
                            echo '<option value="'.$mloc['mmuloc_id'].'" '.set_select('m_loc', $mloc['mmuloc_id']).'>'.$mloc['mmuloc_name'].'</option>';
                        } ?>
					</select>
					<p class="error error-mmu" style="font-size: 10px;"></p>
				</div>
				<div class="col-md-6" style="margin-top: 10px;">
					<select class="form-control" name="post_type">
						<option value="">Select Activity Type</option>
						<option value="Daily Visit">Daily Visit</option>
						<option value="Health Camp">Health Camp</option>
					</select>
					<p class="error error-posttype" style="font-size: 10px;"></p>
				</div>
				<div class="col-md-12" style="margin-top: 10px;">
                    <div class="fileinput fileinput-new" data-provides="fileinput"><input type="hidden" value="" name="image[]">
                        
                       <div class="field" align="left">
						  <h4>Upload your images</h4>
						  <input type="file" id="files" name="image[]" multiple />
						</div>
                    </div>
				</div>

				<div class="col-md-12" style="margin-top: 15px;">
					<button type="submit" class="btn btn-success btn-block" id="submit_btn">Post</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div id="google_canvas" style="display: none;"></div>
<!-- <input type="text" name="lat" id="lat">
<input type="text" name="long" id="long"> -->
<div class="col-md-6 col-md-offset-3">
<?php
    foreach($posts as $post) {  ?>
		<div class="panel" style="border:1px solid #e6e6e6;">
		  <div class="panel-heading" style="background-color: #e6e6e6;">
		  	<img src="<?php echo base_url('uploads/spo_image/'.$post['image'])?>" style="width: 50px; height: 50px; border-radius: 50%;">
		  	<span style="padding-left:10px;"><?php echo $post['name'];?></span><br>
		  	<span style="padding-left: 60px;"><?php echo $post['date'];?></span>
		  </div>
		  <div class="panel-body">
		    <p><?php echo $post['text'];?></p>
		    <?php foreach ($post['images'] as $key => $image) { ?>
		    <img width="100%" src="<?php echo base_url(); ?>uploads/post_image/<?php echo $image['image_name'];?>" alt="...">
		    <?php } ?>
		    <p> <?php echo $post['comment'];?> Comments</p>
		  </div>
		</div>
<?php } ?>
	<div style="margin-bottom: 40px;">
		<button class="btn btn-primary btn-block">View More...</button>
	</div>
</div>
</div>

<script src="http://maps.googleapis.com/maps/api/js?v=3.exp&sensor=true"></script>
	
	<script>
		$("#submit_btn").click(function () {
			event.preventDefault(); 
			navigator.geolocation.getCurrentPosition(success, error);
			function success(position) {
				var lat = position.coords.latitude;
				var long = position.coords.longitude;

				$('#target').append('<input type="hidden" name="lat" value="'+lat+'">');
				$('#target').append('<input type="hidden" name="long" value="'+long+'">');
				$("#target" ).submit();

			}

			function error() {
				$('#target').append('<input type="hidden" name="lat" value="">');
				$('#target').append('<input type="hidden" name="long" value="">');
				$("#target" ).submit();   
			}
		});
		$('#target').on('submit', function(event){
			$('.error').html('');
			$("[name='text']").each(function(ev) {
                var value = $(this).val();
                if(typeof value === 'undefined' || value.length == 0) {
                    $(this).parent().find('.error').html('Status is mandatory.');
                    event.preventDefault();
                }
            });
            $("[name='mmu']").each(function(ev) {
                var value = $(this).val();
                if(typeof value === 'undefined' || value.length == 0) {
                    $(this).parent().find('.error').html('MMU Site is mandatory.');
                    event.preventDefault();
                }
            });
            $("[name='post_type']").each(function(ev) {
                var value = $(this).val();
                if(typeof value === 'undefined' || value.length == 0) {
                    $(this).parent().find('.error').html('Activity is mandatory.');
                    event.preventDefault();
                }
            });
		});

		$(document).ready(function() {
		  if (window.File && window.FileList && window.FileReader) {
		    $("#files").on("change", function(e) {
		      var files = e.target.files,
		        filesLength = files.length;
		      for (var i = 0; i < filesLength; i++) {
		        var f = files[i]
		        var fileReader = new FileReader();
		        fileReader.onload = (function(e) {
		          var file = e.target;
		          $("<span class=\"pip\">" +
		            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
		            "<br/><span class=\"remove\">Remove image</span>" +
		            "</span>").insertAfter("#files");
		          $(".remove").click(function(){
		            $(this).parent(".pip").remove();
		          });
		          
		          // Old code here
		          /*$("<img></img>", {
		            class: "imageThumb",
		            src: e.target.result,
		            title: file.name + " | Click to remove"
		          }).insertAfter("#files").click(function(){$(this).remove();});*/
		          
		        });
		        fileReader.readAsDataURL(f);
		      }
		    });
		  } else {
		    alert("Your browser doesn't support to File API")
		  }
		});

	</script>
	
		

