<?php
	if(count($data) != 0){
		foreach ($data as $d) { ?>		
			<div class="row">
			    <div class="col-md-12">
			        <div class="panel panel-primary" data-collapsed="0">
			            <div class="panel-heading">
			            	<div class="panel-title">
			                	<button type="button" class="btn btn-info" data-toggle="collapse" data-target="#<?php echo $d['t_id'] ?>">			               
				                	Treatment Date : <?php
				                		$date = strtotime($d['treat_date']);
										echo date('j F Y', $date); ?>
				            	</button>
				            </div>  
			            </div>
			            
			            <div class="panel-body collapse" id="<?php echo $d['t_id'] ?>" style="overflow-x: scroll;">
			            	<table class = "table table-boarder">
			            		<thead>
			            			<tr>
			            				<th colspan="6"><b>Vitals</b></th>
			            			</tr>
			            			<tr style="background-color: #eff1ef;">
			            				<th>LOW-BP</th>
			            				<th>HIGH-BP</th>
			            				<th>Pulse</th>
			            				<th>Weight(kgs)</th>
			            				<th>Height(cm)</th>
			            				<th>Temp(F)</th>
			            			</tr>
			            		</thead>
			            		<tbody>
			            			<tr>
			            				<td><?php 
			            					if($d['bplow'] != ''){
			            						echo $d['bplow'];
			            					}else{
			            						echo "-";
			            					} ?>
			            				</td>
			            				<td><?php 
			            					if($d['bphigh'] != ''){
			            						echo $d['bphigh'];
			            					}else{
			            						echo "-";
			            					} ?>
			            				</td>
			            				<td><?php 
			            					if($d['pulse'] != ''){
			            						echo $d['pulse'];
			            					}else{
			            						echo "-";
			            					} ?>
			            				</td>
			            				<td><?php 
			            					if($d['weight'] != ''){
			            						echo $d['weight'];
			            					}else{
			            						echo "-";
			            					} ?>
			            				</td>
			            				<td><?php 
			            					if($d['height'] != ''){
			            						echo $d['height'];
			            					}else{
			            						echo "-";
			            					} ?>
			            				</td>
			            				<td><?php 
			            					if($d['temp'] != ''){
			            						echo $d['temp'];
			            					}else{
			            						echo "-";
			            					} ?>
			            				</td>
			            			</tr>
			            		</tbody>
			            	</table>
			            	<table class = "table table-boarder">
			            		<thead>
			            			<tr>
			            				<th colspan="6"><b>Diagnosis</b></th>
			            			</tr>
			            			<tr style="background-color: #eff1ef;">
			            				<th>STP</th>
			            				<th>LTP</th>
			            				<th>PD</th>
			            				<th>Diagnosis</th>
			            			</tr>
			            		</thead>
			            		<tbody>
			            			<tr>
			            				<td><?php 
			            					if($d['stp'] != ''){
			            						echo $d['stp'];
			            					}else{
			            						echo "-";
			            					} ?>
			            				</td>
			            				<td><?php 
			            					if($d['ltp'] != ''){
			            						echo $d['ltp'];
			            					}else{
			            						echo "-";
			            					} ?>
			            				</td>
			            				<td><?php 
			            					if($d['pd'] != ''){
			            						echo $d['pd'];
			            					}else{
			            						echo "-";
			            					} ?>
			            				</td>
			            				<td><?php 
			            					if($d['diag'] != ''){
			            						echo $d['diag'];
			            					}else{
			            						echo "-";
			            					} ?>
			            				</td>
			            			</tr>
			            		</tbody>
			            	</table>
			            	<div class="row">
			            		<div class="col-md-4">
			            			<table class="table table-boarder">
			            				<thead>
			            					<tr>
			            						<th colspan="2">
				            						<b>Current Disease</b>
				            					</th>
			            					</tr>            					
			            					<tr style="background-color: #eff1ef;">
			            						<th>
				            						Suffering From
				            					</th>
				            					<th>
				            						Disease Type
				            					</th>
			            					</tr>
			            				</thead>
			            				<tbody><?php
			            					foreach ($d['pd_details'] as $pd) { 
			            						if($pd['type'] == 'current'){ ?>
			            							<tr>
					            						<td><?php 
							            					if($pd['dis_name'] != ''){
							            						echo $pd['dis_name'];
							            					}else{
							            						echo "-";
							            					} ?>
					            						</td>
					            						<td><?php 
							            					if($pd['dis_type'] != ''){
							            						echo $pd['dis_type'];
							            					}else{
							            						echo "-";
							            					} ?>
					            						</td>
					            					</tr><?php
			            						}
			            					} ?>		            					
			            				</tbody>
			            			</table>
			            		</div>
			            		<div class="col-md-4">
			            			<table class="table table-boarder">
			            				<thead>
			            					<tr>
			            						<th colspan="2">
				            						<b>Past Disease</b>
				            					</th>
			            					</tr>
			            					<tr style="background-color: #eff1ef;">
			            						<th>
				            						Suffered From
				            					</th>
				            					<th>
				            						Disease Type
				            					</th>
			            					</tr>            					
			            				</thead>
			            				<tbody><?php
			            					foreach ($d['pd_details'] as $pd) { 
			            						if($pd['type'] == 'past'){ ?>
			            							<tr>
					            						<td><?php 
							            					if($pd['dis_name'] != ''){
							            						echo $pd['dis_name'];
							            					}else{
							            						echo "-";
							            					} ?>
					            						</td>
					            						<td><?php 
							            					if($pd['dis_type'] != ''){
							            						echo $pd['dis_type'];
							            					}else{
							            						echo "-";
							            					} ?>
					            						</td>
					            					</tr><?php
			            						}
			            					} ?>
			            				</tbody>
			            			</table>
			            		</div>
			            		<div class="col-md-4">
			            			<table class="table table-boarder">
			            				<thead>
			            					<tr>
			            						<th colspan="3">
			            							<b>Doctor Incharge</b>
			            						</th>
			            					</tr>
			            					<tr style="background-color: #eff1ef;">
			            						<th>Doctor Name</th>
			            					</tr>
			            				</thead>
			            				<tbody>
			            					<tr>
			            						<td><?php
			            							echo $d['name']; ?>		            							
			            						</td>
			            					</tr>
			            				</tbody>
			            			</table>
			            		</div>
			            	</div>
			            	<div class="row">
			            		<div class="col-md-6">
			            			<table class="table table-boarder">
			            				<thead>
			            					<tr>
			            						<th colspan="3">
			            							<b>Blood Glucose</b>
			            						</th>
			            					</tr>
			            					<tr style="background-color: #eff1ef;">
			            						<th>Fasting</th>
			            						<th>PP</th>
			            						<th>Random</th>
			            					</tr>
			            				</thead>
			            				<tbody>
			            					<tr>
			            						<td><?php 
					            					if($d['fasting'] != ''){
					            						echo $d['fasting'];
					            					}else{
					            						echo "-";
					            					} ?>
			            						</td>
			            						<td><?php 
					            					if($d['pp'] != ''){
					            						echo $d['pp'];
					            					}else{
					            						echo "-";
					            					} ?>
			            						</td>
			            						<td><?php 
					            					if($d['random'] != ''){
					            						echo $d['random'];
					            					}else{
					            						echo "-";
					            					} ?>
			            						</td>
			            					</tr>
			            				</tbody>
			            			</table>
			            		</div>
			            		<div class="col-md-6">
			            			<table class="table table-boarder">
					    				<thead>
					    					<tr>
					    						<th colspan="3">
					    							<b>Prescribe Medicine</b>
					    						</th>
					    					</tr>
					    					<tr style="background-color: #eff1ef;">
					    						<th>Medicine Name</th>
					    						<th>Quatity</th>
					    						<th>Days</th>
					    					</tr>
					    				</thead>
					    				<tbody><?php
					    					foreach ($d['pm_details'] as $pm) { ?>
					    						<tr>
						    						<td><?php
						    							if($pm['name'] != ''){
						    								echo $pm['name'];
						    							}else{
						    								echo "-";
						    							} ?>			    							
						    						</td>
						    						<td><?php
						    							if($pm['quantity'] != ''){
						    								echo $pm['quantity'];
						    							}else{
						    								echo "-";
						    							} ?>
						    						</td>
						    						<td><?php
						    							if($pm['day'] != ''){
						    								echo $pm['day'];
						    							}else{
						    								echo "-";
						    							} ?>
						    						</td>
						    					</tr><?php
					    					}?>		    					
					    				</tbody>
					    			</table>
			            		</div>		            		
			            	</div>		            	
			           	</div>
			        </div>
			    </div>    
			</div><?php 
		}
	}else{ ?>
		<div class="row">
		    <div class="col-md-12">
		        <div class="panel panel-primary">		            		            
		            <div class="panel-body">
		            	<h3> No Treatments Found</h3>
		           	</div>	
		        </div>
		    </div>
		</div><?php		            
	}
	
	/*echo "<pre>";
	print_r($data);*/
?>
