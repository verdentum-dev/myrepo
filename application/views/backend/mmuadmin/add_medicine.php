<?php $medicine_category_info = $this->db->get('medicine_category')->result_array(); ?>
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    <h3><?php echo get_phrase('add_medicine'); ?></h3>
                </div>
            </div>

            <div class="panel-body">

                <form role="form" class="form-horizontal form-groups-bordered" action="<?php echo base_url(); ?>index.php?mmuadmin/add_medicine" method="post" enctype="multipart/form-data">

                    <div class="form-group">
                        <label for="field-name" class="col-sm-3 control-label"><?php echo get_phrase('name'); ?></label>
                        <div class="col-sm-7">
                            <input class="form-control" id="field-name" value="<?php echo set_value('name');?>" name="name" >
                            <p class="error" id="err-name"></p>
                            <?php echo form_error('name'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-composition" class="col-sm-3 control-label"><?php echo get_phrase('composition'); ?></label>

                        <div class="col-sm-7">
                            <textarea name="composition" class="form-control" id="field-composition" rows="5" ><?=set_value('composition')?></textarea>
                            <p class="error" id="err-composition"></p>
                            <?php echo form_error('composition'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-preparation" class="col-sm-3 control-label"><?php echo get_phrase('preparation'); ?></label>

                        <div class="col-sm-7">
							<select name="preparation" id="field-preparation" class="selectboxit" >
								<option value="">Select Preparation</option>
								<option value="Tab" <?php echo set_select('preparation', 'Tab'); ?>>Tab</option>
								<option value="Emulsion" <?php echo set_select('preparation', 'Emulsion'); ?>>Emulsion</option>
								<option value="Syrup" <?php echo set_select('preparation', 'Syrup'); ?>>Syrup</option>
								<option value="Ointment" <?php echo set_select('preparation', 'Ointment'); ?>>Ointment</option>
								<option value="Powder" <?php echo set_select('preparation', 'Powder'); ?>>Powder</option>
								<option value="Drops" <?php echo set_select('preparation', 'Drops'); ?>>Drops</option>
								<option value="Inhaler" <?php echo set_select('preparation', 'Inhaler'); ?>>Inhaler</option>
								<option value="Cream" <?php echo set_select('preparation', 'Cream'); ?>>Cream</option>
							</select>
                            <p class="error" id="err-preparation"></p>
                            <?php echo form_error('preparation'); ?>
                        </div>
                    </div>

                    <!-- <div class="form-group">
                        <label  class="col-sm-3 control-label"><?php echo get_phrase('MMU'); ?></label>
                        <div class="col-sm-7">
                            <select name="mmuunit" class="selectboxit" id="field-mmuunit" >
                                    <option value="">Select MMU Name</option>
                                    <?php foreach ($mmuunit as $unit) {
                                        echo '<option value="'.$unit['unit_id'].'" '.set_select('mmuunit', $unit['unit_id']).'>'.$unit['mmu_name'].'</option>';
                                    } ?>
                            </select>
                            <p class="error" id="err-mmuunit"></p>
                            <?php echo form_error('mmuunit'); ?>
                        </div>
                    </div> -->

                    <div class="form-group">
                        <label for="field-unit" class="col-sm-3 control-label"><?php echo get_phrase('no_of_unit'); ?></label>
                        <div class="col-sm-7">
							<input type="text" class="form-control" id="field-unit" value="<?php echo set_value('unit');?>" name="unit" >
                            <p class="error" id="err-unit"></p>
                            <?php echo form_error('unit'); ?>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="field-batch" class="col-sm-3 control-label"><?php echo get_phrase('batch_no'); ?></label>

                        <div class="col-sm-7">
                            <input class="form-control" id="field-batch" value="<?php echo set_value('batch');?>" name="batch" >
                            <p class="error" id="err-batch"></p>
                            <?php echo form_error('batch'); ?>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="field-expiry" class="col-sm-3 control-label"><?php echo get_phrase('expiry_date'); ?></label>

                        <div class="col-sm-7">
							<input type="text" class="form-control datepicker" id="field-expiry"  name="expiry">
                            <p class="error" id="err-expiry"></p>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="field-company" class="col-sm-3 control-label"><?php echo get_phrase('manufacturing_company'); ?></label>

                        <div class="col-sm-7">
							<input type="text" class="form-control" id="field-company" value="<?php echo set_value('company');?>" name="company" >
                            <p class="error" id="err-company"></p>
                            <?php echo form_error('company'); ?>
                        </div>
                    </div>
                    

                    <div class="col-sm-3 control-label col-sm-offset-2">
                        <input type="submit" name="submit" class="btn btn-success" value="Submit">
                    </div>
                </form>

            </div>

        </div>

    </div>
</div>

<!-- Page Script -->
<script type="text/javascript">
$(function(){
    $('#field-expiry').datepicker({
        format: 'yyyy-mm-dd',
        startDate: new Date()
    });

	var lastResults = [];
    //AJAX
    $('#field-name').select2({
		placeholder: 'Search Existing/Enter New Medicine Name',
		allowClear: true,
		tags: true,
		tokenSeparators: [","],
		ajax: {
            url: "<?php echo base_url(); ?>index.php?mmuadmin/get_medicine",
            dataType: 'json',
            type: 'POST',
            delay: 250,
            data: function (term, page) {
                return {
                    q: term, // search term
                    page: page ? page : 1
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                lastResults = data.items;

                return {
                    results: data.items,
                    pagination: {
                        more: data.remaining > 0
                    }
                };
            }
        },
        createSearchChoice: function (term) {
        	var count = 0;
        	lastResults.map(function(value, index) {
        		if($.trim(value.name) == $.trim(term)) count++;
        	});
        	if(count == 0) {
				return {
					id: $.trim(term),
					text: $.trim(term)
				};
			}
		},
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 1,
        maximumSelectionSize: 1,
		formatResult: formatRepo,
		formatSelection: formatRepoSelection
    }).on("select2-removed", function(e) {
    	$('[name="preparation"]').val('').trigger('change');
    	$('[name="preparation"]').data("selectBox-selectBoxIt").enable();
    	$('[name="composition"]').val('');
    	$('[name="composition"]').removeAttr('readonly');
    });

    //AJAX
    $('#field-batch').select2({
        placeholder: 'Search Existing/Enter New Batch Number',
        allowClear: true,
        tags: true,
        tokenSeparators: [","],
        ajax: {
            url: "<?php echo base_url(); ?>index.php?mmuadmin/get_medicinebatch",
            dataType: 'json',
            type: 'POST',
            delay: 250,
            data: function (term, page) {
                return {
                    q: term, // search term
                    med: $('#field-name').val(), // medicine id
                    page: page ? page : 1
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                lastResults = data.items;

                return {
                    results: data.items,
                    pagination: {
                        more: data.remaining > 0
                    }
                };
            }
        },
        createSearchChoice: function (term) {
            var count = 0;
            lastResults.map(function(value, index) {
                if($.trim(value.batch_no) == $.trim(term)) count++;
            });
            if(count == 0) {
                return {
                    id: $.trim(term),
                    text: $.trim(term)
                };
            }
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 1,
        maximumSelectionSize: 1,
        formatResult: formatBatchRepo,
        formatSelection: formatBatchRepoSelection
    }).on("select2-removed", function(e) {
        $('[name="expiry"]').val('');
        $('[name="expiry"]').removeAttr("disabled");
    });

    $('form').on('submit', function(ev) {
        $('#err-name, #err-composition, #err-preparation, #err-mmuunit, #err-unit, #err-batch, #err-expiry, #err-company').html('');
        console.log(typeof($('#field-preparation').attr('disabled')));

        if($('#field-name').val().length === 0) {
            $('#err-name').html('Medicine name is mandatory.');
            ev.preventDefault();
        }

        if($('#field-composition').val().length === 0) {
            $('#err-composition').html('Medicine composition is mandatory.');
            ev.preventDefault();
        }

        if($('#field-preparation').val().length === 0) {
            $('#err-preparation').html('Medicine preparation is mandatory.');
            ev.preventDefault();
        }

        /*if($('#field-mmuunit').val().length === 0) {
            $('#err-mmuunit').html('MMU Unit is mandatory.');
            ev.preventDefault();
        }*/

        if($('#field-unit').val().length === 0) {
            $('#err-unit').html('Number of units is mandatory.');
            ev.preventDefault();
        }

        if($('#field-batch').val().length === 0) {
            $('#err-batch').html('Batch number is mandatory.');
            ev.preventDefault();
        }

        if($('#field-expiry').val().length === 0) {
            $('#err-expiry').html('Expiry date is mandatory.');
            ev.preventDefault();
        }

        if($('#field-company').val().length === 0) {
            $('#err-company').html('Manufacturing Company name is mandatory.');
            ev.preventDefault();
        }
    });
});

function formatRepo (repo) {
	if (repo.loading) return repo.text;

	if(typeof repo.name === 'undefined') {
		var markup = "<div class='p-10'>This Medicine is Not Available in Our Inventory<br/>"+
			"<small>Create a new medicine named - <strong>" + repo.text + "</strong></small><br/>"+
			"<small>Upon submit a new medicine will be created</small>"+
		"</div>";
	} else {
		var markup = "<div class='p-10'>" +
			repo.name+"<br/>"+
			"<small>" + repo.preparation + "</small><br/>"+
			"<small>Composition: " + repo.composition + "</small>"+
		"</div>";
	}

	return markup;
}

function formatRepoSelection (repo) {
	if(repo.preparation) {
		$('[name="preparation"]').val(repo.preparation).trigger('change');
		$('[name="preparation"]').data("selectBox-selectBoxIt").disable();
	}
	if(repo.composition) {
		$('[name="composition"]').val(repo.composition);
		$('[name="composition"]').attr('readonly', 'readonly');
	}
	return repo.name ? repo.name : repo.text;
}

function formatBatchRepo (repo) {
    if (repo.loading) return repo.text;

    if(typeof repo.batch_no === 'undefined') {
        var markup = "<div class='p-10'>This Batch is Not Available in Our Inventory<br/>"+
            "<small>Create a new batch number - <strong>" + repo.text + "</strong></small><br/>"+
            "<small>Upon submit a new batch number will be created</small>"+
        "</div>";
    } else {
        var markup = "<div class='p-10'>" +
            repo.batch_no+"<br/>"+
            "<small>Expiry Date: " + repo.exp_date + "</small>"+
        "</div>";
    }

    return markup;
}

function formatBatchRepoSelection (repo) {
    if(repo.exp_date) {
        $('[name="expiry"]').val(repo.exp_date);
        $('[name="expiry"]').attr("disabled", "disabled");
    } else {
        $('[name="expiry"]').removeAttr("disabled");
    }
    return repo.batch_no ? repo.batch_no : repo.text;
}
</script>
<!-- <link href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" crossorigin="anonymous" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="></script> -->