<div style="clear:both;"></div>
<br>
<table class="table table-bordered table-striped datatable" id="table-2">
    <thead>
        <tr>
            <th><?php echo get_phrase('Sl. No.');?></th>
            <th><?php echo get_phrase('State');?></th>
            <th><?php echo get_phrase('District');?></th>
            <th><?php echo get_phrase('MMU');?></th>
            <th><?php echo get_phrase('MMU Site');?></th>
            <th><?php echo get_phrase('Vehicle number');?></th>
            <th><?php echo get_phrase('Status');?></th>
        </tr>
    </thead>

    <tbody>  
            <?php if(count($mmuunit) > 0) { ?>
            <?php $i=0; foreach ($mmuunit as $unit) { $i++; ?>

            <tr>
                <td><?php echo $i; ?>.</td>
                <td><?php echo $unit['state_name']; ?></td>
                <td><?php echo $unit['dist_name']; ?></td>
                <td><?php echo $unit['mmu_name']; ?></td>
                <td><a href="<?php echo base_url(); ?>index.php?mmuadmin/mmu_location/<?php echo $unit['unit_id']; ?>">Add / Edit Site</a></td>
                <td><?php echo $unit['mmu_number']; ?></td>
                <td>
                <?php 
                    if($unit['status']==0)
                    {
                        echo 'Running';
                    }
                    else{
                        echo '';
                    }
                ?></td>
            </tr>
            <?php } ?>
              <?php } else { ?>
            <tr>
                <td colspan="8">No Unit found</td>
            </tr>
                <?php } ?>
            
    </tbody>
</table>