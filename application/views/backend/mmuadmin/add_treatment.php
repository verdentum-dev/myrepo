 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css" />

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" >
            <div class="panel-heading">
                <div class="panel-title">
                    Patient Name : <?php echo $patient_deatils['name']; ?> 
                    <div>Date : <?php echo date("j F Y"); ?></div>
                </div>
            </div>

            <div class="panel-body">
                <div id="success"></div>
                <form class="form-horizontal form-groups-bordered" id="addtreatment" action="index.php?mmuadmin/upload_treatment/<?php echo $this->uri->segment(3); ?>" method = "post" >
                   <!--  <div style="background-color: black;color: white;"><center>Vital</center></div> -->
                    <div class="panel">
                        <div class="panel-heading" style="background-color: gray;color: white;">Vitals</div>
                        <div class="panel-body">
                            <div class="col-md-1">
                                <label  class="control-label"><?php echo get_phrase('BP'); ?></label>
                            </div>                                
                            <div class="col-md-4">
                                <input type="number" class="form-control" name="lowbp" placeholder="low">
                                <p class="error err-lowbp"></p>
                            </div>
                            <div class="col-md-4">
                                <input type="number" class="form-control" name="highbp" placeholder="high" value="<?php echo set_value('highbp');?>" >
                                <p class="error err-highbp"></p>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <label  class="control-label"><?php echo get_phrase('Pulse'); ?></label>
                                    <input type="number" class="form-control" name="pulse" placeholder="pulse" value="<?php echo set_value('pulse');?>" >
                                    <p class="error err-pluse"></p>
                                </div>

                                <div class="col-md-6">
                                    <label  class="control-label"><?php echo get_phrase('Weight(Kgs)'); ?></label>
                                    <input type="number" class="form-control" name="paweight" placeholder="Enter Patient Weight" value="<?php echo set_value('paweight');?>" >
                                    <p class="error err-highbp"></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label  class="control-label"><?php echo get_phrase('Height(CM)'); ?></label>
                                    <input type="number" class="form-control" name="paheight" placeholder="Enter Patient Height" value="<?php echo set_value('paheight');?>">
                                </div> 

                                <div class="col-md-6">
                                    <label  class="control-label"><?php echo get_phrase('Temp(F)'); ?></label>
                                    <input type="number" class="form-control" name="patemp" placeholder="Enter Patient Temp" value="<?php echo set_value('patemp');?>">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-heading" style="background-color: gray;color: white;">Diagnosis</div>
                        <div class="panel-body">

                            <div class="col-md-3">
                                <label  class="control-label"><?php echo get_phrase('Diagnosis'); ?></label>
                                <select data-live-search="true" data-live-search-style="startsWith" name="diagnosis" class="selectpicker form-control">
                                    <option value="">Select Diagnosis</option><?php
                                    foreach ($diagnosis_list as $dio) { ?>
                                        <option value="<?php echo $dio['diag_id']?>"><?php echo $dio['diag_name'];  ?></option><?php
                                    } ?>                                    
                                </select>
                            </div>

                            <div class="col-md-3">
                                <label  class="control-label"><?php echo get_phrase('STP'); ?></label>
                                <input type="number" class="form-control" name="pastp" placeholder="Enter STP" value="<?php echo set_value('pastp');?>">                                   
                            </div>

                            <div class="col-md-3">
                                <label  class="control-label"><?php echo get_phrase('LTP'); ?></label> 
                                <input type="number" class="form-control" name="paltp" placeholder="Enter LTP" value="<?php echo set_value('paltp');?>">
                            </div>

                            <div class="col-md-3">
                                <label  class="control-label"><?php echo get_phrase('PD'); ?></label>
                                <input type="number" class="form-control" name="papd" placeholder="Enter PD" value="<?php echo set_value('papd');?>">
                            </div> 
                        </div>
                    </div>
                    
                    <div class="panel-heading" style="background-color: gray;color: white;">Current Disease<font color="red"> *</font></div>
                    <div class="panel-body">
                        <div class="row" id="crtdiseasetemplate" style="margin-bottom:20px;border-bottom:1px solid #949494;padding-bottom:20px;">
                            <div class="row col-xs-11">                          
                                <div class="col-md-6">
                                    <label class="labelname control-label"><?php echo get_phrase('current_suffering_from') ?></label>                            
                                    <select name="currentdisease[]" class="form-control">
                                        <option value="">Select Disease</option><?php
                                        foreach ($disease_list as $dl) { ?>
                                            <option value="<?php echo $dl['dis_id']?>"><?php echo $dl['dis_name'];  ?></option><?php
                                        } ?>
                                    </select>
                                    <p class="error err-crtdisease"></p>
                                </div>

                                <div class="col-md-6">
                                    <label class="control-label"><?php echo get_phrase('current_disease_type') ?></label> 
                                    <select name="currentdiseasetype[]" class="form-control">
                                        <option value="">Select Disease Type</option>
                                        <option value="chronic">Chronic</option>
                                        <option value="Common">Common</option>
                                        <option value="Serious">Serious</option>
                                    </select>
                                    <p class="error err-crtdiseasetype"></p>
                                </div>
                            </div>
                            <div class="col-xs-1 text-center"><br/>
                                <button type="button" class="btn btn-default addcrtdisease"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div>

                    <div class="panel-heading" style="background-color: gray;color: white;">Past History of the Patient</div>
                    <div class="panel-body">
                        <div class="row" id="pastdiseasetemplate" style="margin-bottom:20px;border-bottom:1px solid #949494;padding-bottom:20px;">
                            <div class="row col-xs-11">
                                <div class="col-md-6">
                                    <label class="control-label"><?php echo get_phrase('past_suffering_from') ?></label>                            
                                    <select name="pastdisease[]" class="form-control">
                                        <option value="">Select Disease</option><?php
                                        foreach ($disease_list as $dl) { ?>
                                            <option value="<?php echo $dl['dis_id']?>"><?php echo $dl['dis_name'];  ?></option><?php
                                        } ?>
                                    </select>
                                    <?php echo form_error('pastdisease'); ?>                            
                                </div>

                                <div class="col-md-6">
                                    <label class="control-label"><?php echo get_phrase('past_disease_type') ?></label> 
                                    <select name="pastdiseasetype[]" class="form-control">
                                        <option value="">Select Disease Type</option>
                                        <option value="chronic">Chronic</option>
                                        <option value="Common">Common</option>
                                        <option value="Serious">Serious</option>
                                    </select>
                                    <?php echo form_error('pastdiseasetype'); ?>                            
                                </div> 
                            </div>
                            <div class="col-xs-1 text-center"><br/>
                                <button type="button" class="btn btn-default addpastdisease "><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div>
                    

                    <div class="panel">
                        <div class="panel-heading" style="background-color: gray;color: white;">Blood Glucose</div>
                        <div class="panel-body">                            
                            <div class="col-md-4">
                                <label  class="control-label"><?php echo get_phrase('Fasting'); ?></label>
                                <input type="number" class="form-control" name="fasting" placeholder="Enter Fasting" value="<?php echo set_value('fasting');?>">
                                <?php echo form_error('fasting'); ?>
                            </div>

                            <div class="col-md-4">
                                <label  class="control-label"><?php echo get_phrase('PP'); ?></label>
                                <input type="number" class="form-control" name="pp" placeholder="Enter PP" value="<?php echo set_value('pp');?>">
                                <?php echo form_error('pp'); ?>
                            </div>

                            <div class="col-md-4">
                                <label  class="control-label"><?php echo get_phrase('random'); ?></label>
                                <input type="number" class="form-control" name="random" placeholder="Enter Random" value="<?php echo set_value('random');?>">
                                <?php echo form_error('random'); ?>
                            </div>                            
                        </div>
                    </div>

                    <div class="panel-heading" style="background-color: gray;color: white;">Prescribe Medicine</div>
                    <div class="panel-body">
                        <div class="row" id="medTemplate" style="margin-bottom:20px;border-bottom:1px solid #949494;padding-bottom:20px;">
                            <div class="row col-xs-11">
                                <div class="col-md-6 name">
                                    <label for="field-name" class="labelname control-label"><?php echo get_phrase('medicine_name'); ?></label>
                                    <input type="text" class="form-control" name="name[]">
                                    <p class="error err-med"></p>
                                </div>

                                <div class="col-md-3">
                                    <label for="field-preparation" class="control-label"><?php echo get_phrase('batch_no'); ?></label>
                                    <select name="batch[]" class="form-control">
                                        <option value="">Select Batch</option>
                                    </select>
                                    <p class="error err-batch"></p>
                                </div>

                                <div class="col-md-3">
                                    <label class="labelname control-label"><?php echo get_phrase('available_units'); ?></label>
                                    <div class="form-control available"></div>
                                </div>

                                <div class="clearfix"></div>
                            
                                <div class="col-md-4">
                                    <label class="labelname control-label"><?php echo get_phrase('exp_date'); ?></label>
                                    <div class="form-control expiry"></div>
                                </div>

                                <div class="col-md-4">
                                    <label for="field-preparation" class="labelname control-label"><?php echo get_phrase('Quantity'); ?></label>
                                    <input type="number" class="form-control" name="quantity[]">
                                    <p class="error err-quantity"></p>
                                </div>

                                <div class="col-md-4">
                                    <label for="field-preparation" class="labelname control-label"><?php echo get_phrase('Days'); ?></label>
                                    <input type="number" class="form-control" name="days[]">
                                    <p class="error err-quantity"></p>
                                </div>

                                <div class="col-md-12">
                                    <label for="field-preparation" class="labelname control-label"><?php echo get_phrase('Enter Remark'); ?></label>
                                    <textarea name="dosage[]" class="form-control" placeholder="Enter Remark"></textarea>
                                </div>
                            </div>

                            <div class="col-xs-1 text-center"><br/><br/><br/><br><br>
                                <button type="button" class="btn btn-default addmedicine"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="text-center">
                        <input type="submit" name="submit" class="btn btn-success" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.js"></script> 
<script type="text/javascript">
    $(function(){
        $('#addtreatment')
        .on('click', '.addcrtdisease', function() {
            var $template = $('#crtdiseasetemplate'),
                $cclone    = $template
                            .clone()
                            .removeAttr('id');
            $cclone.find('.addcrtdisease').removeClass('addcrtdisease').addClass('removecrtdisease').html('<i class="fa fa-minus"></i>');
            $cclone.find('[name="currentdisease[]"]').val('');
            $cclone.find('[name="currentdiseasetype[]"]').val('');
            $(this).closest('.row').parent().after($cclone);
        })

        .on('click', '.removecrtdisease', function() {
            $(this).closest('.row').remove();
        })

        .on('click', '.addpastdisease', function() {
            var $template = $('#pastdiseasetemplate'),
                $cclone    = $template
                            .clone()
                            .removeAttr('id');
            $cclone.find('.addpastdisease').removeClass('addpastdisease').addClass('removepastdisease').html('<i class="fa fa-minus"></i>');
            $cclone.find('[name="pastdisease[]"]').val('');
            $cclone.find('[name="pastdiseasetype[]"]').val('');
            $(this).closest('.row').parent().after($cclone);
        })

        .on('click', '.removepastdisease', function() {
            $(this).closest('.row').remove();
        })

        // Add button click handler
        .on('click', '.addmedicine', function() {
            var $template = $('#medTemplate'),
                $clone    = $template
                            .clone()
                            .removeAttr('id');
            $clone.find('.addmedicine').removeClass('addmedicine').addClass('removemedicine').html('<i class="fa fa-minus"></i>');
            //$clone.find('[name="mmu[]"]').val('');
            $clone.find('[name="quantity[]"]').val('');
            $clone.find('[name="days[]"]').val('');
            $clone.find('[name="dosage[]"]').val('');
            $clone.find('.available').html('');
            $clone.find('.expiry').html('');
            $clone.find("[name='name[]']").select2("destroy");
            $clone.find("[name='batch[]']").html('<option value="">Select Batch</option>')
            $clone.find('.name').html(
                '<label for="field-name" class="labelname control-label">Name</label>\
                <input type="text" class="form-control" name="name[]">\
                <p class="error err-med"></p>'
            );
            $('[type="submit"]').parent().before($clone);
            initializeSelect2($clone.find("[name='name[]']"));
        })
        // Remove button click handler
        .on('click', '.removemedicine', function() {
            $(this).closest('.row').find("[name='name[]']").select2("val", null)
            $(this).closest('.row').find("[name='name[]']").select2("destroy");
            $(this).closest('.row').remove();
        });
        initializeSelect2($("[name='name[]']"));
        $("body").on("change", "[name='batch[]']", function(e) {
            var elem = $(this),
                medicineid = elem.closest('.row').find("[name='name[]']").select2("val")[0],
                batch = elem.val();
                $.ajax({
                    url: "index.php?mmuadmin/getmedicineunitcount",
                    type: "post",
                    dataType: "json",
                    data: {
                        medicineid: medicineid,
                        batch: batch
                    },
                    success: function(data) {
                        elem.closest('.row').find(".available").html(data.maincount)
                        elem.closest('.row').find(".expiry").html(data.exp_date);
                    }, error: function(error) {
                        console.error(error);
                    }
                });
        });

        $('#addtreatment').on('submit', function(event){
            $('.error').html('');
            $("[name='name[]']").each(function(ev) {
                var value = $(this).select2("val")[0];
                if(typeof value === 'undefined' || value.length == 0) {
                    $(this).parent().find('.error').html('Medicine name is mandatory.');
                    event.preventDefault();
                }
            });

            $("[name='batch[]']").each(function(ev) {
                var value = $(this).val();
                if(typeof value === 'undefined' || value.length == 0) {
                    $(this).parent().find('.error').html('Batch number is mandatory.');
                    event.preventDefault();
                }
            });

            $("[name='quantity[]']").each(function(ev) {
                var value = $(this).val();
                if(typeof value === 'undefined' || value.length == 0) {
                    $(this).parent().find('.error').html('Quantity is mandatory.');
                    event.preventDefault();
                }
            });

            $("[name='days[]']").each(function(ev) {
                var value = $(this).val();
                if(typeof value === 'undefined' || value.length == 0) {
                    $(this).parent().find('.error').html('Days is mandatory.');
                    event.preventDefault();
                }
            });

            $("[name='currentdisease[]']").each(function(ev) {
                var value = $(this).val();
                if(typeof value === 'undefined' || value.length == 0) {
                    $(this).parent().find('.error').html('Current Disease is mandatory.');
                    event.preventDefault();
                }
            });

            $("[name='currentdiseasetype[]']").each(function(ev) {
                var value = $(this).val();
                if(typeof value === 'undefined' || value.length == 0) {
                    $(this).parent().find('.error').html('Current Disease type is mandatory.');
                    event.preventDefault();
                }
            });

            $("[name='lowbp']").each(function(ev) {
                var value = $(this).val();
                if(value.length > 0){
                    if(value >= 50 && value <= 150) {
                                    
                    }else{
                        $(this).parent().find('.error').html('Low bp should be between 80 & 260.');
                        event.preventDefault();
                    }
                }
            });

            $("[name='highbp']").each(function(ev) {
                var value = $(this).val();
                if(value.length > 0){
                    if(value >= 80 && value <= 260) {
                    
                    }else{
                        $(this).parent().find('.error').html('High bp should be between 80 & 260.');
                        event.preventDefault();
                    }
                }                
            });

            $("[name='pulse']").each(function(ev) {
                var value = $(this).val();
                if(value.length > 0){
                    if(value.length >= 2 && value.length <= 3) {
                        
                    }else{
                        $(this).parent().find('.error').html('Pulse length should be between 2 digits & 3 digits.');
                        event.preventDefault();
                    }
                }
            });
        });
    });

    //function to initialize select2 AJAX
    function initializeSelect2(selectElementObj) {
        var all = [];
        selectElementObj.select2({
            placeholder: 'Search Medicine Name',
            allowClear: false,
            tags: true,
            tokenSeparators: [","],
            ajax: {
                url: "<?php echo base_url(); ?>index.php?mmuadmin/get_medicine",
                dataType: 'json',
                type: 'POST',
                delay: 250,
                data: function (term, page) {
                    $('[name="name[]"]').each(function(elem) {
                        var value = $(this).select2("val")[0];
                        if(typeof value !== 'undefined' && value.length > 0) {
                            console.log(value);
                            all.push(parseInt(value));
                        }
                    });

                    return {
                        q: term, // search term
                        meds: all, //All selected meds
                        page: page ? page : 1
                    };
                },
                results: function (data, page) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    lastResults = data.items;

                    return {
                        results: data.items,
                        pagination: {
                            more: data.remaining > 0
                        }
                    };
                }
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 1,
            maximumSelectionSize: 1,
            formatResult: formatRepo,
            formatSelection: formatRepoSelection
        }).on("select2-removed", function(e) {
            selectElementObj.closest('.row').find('.available, .expiry').html('');
            selectElementObj.closest('.row').find('[name="batch[]"]').val('').trigger('change');
            selectElementObj.closest('.row').find('[name="batch[]"]').html('<option value="">Select Batch</option>');
        }).on("select2-selecting", function(e) {
            element = selectElementObj;
            val = e.val;
        });
    }

    function formatRepo (repo) {
        if (repo.loading) return repo.text;

        var markup = "<div class='p-10'>" +
            repo.name+"<br/>"+
            "<small>" + repo.preparation + "</small><br/>"+
            "<small>Composition: " + repo.composition + "</small>"+
        "</div>";

        return markup;
    }

    function formatRepoSelection (repo) {
        element.closest('.row').find('.available, .expiry').html('');
        if(repo.name) {
            $.ajax({
                url: "<?php echo base_url(); ?>index.php?mmuadmin/medicinedetails",
                type: "POST",
                dataType: "json",
                data: { 
                    medicineid: val
                },
                success: function(response) {
                    if (response.medicine.length === 0){
                        element.closest('.row').find('[name="batch[]"]').html("<option value=''>No Batch Found</option>");
                    }
                    else{
                        var HTML = '<option value="">Select Batch</option>';
                        response.medicine.map(function(value, index) {
                            HTML += '<option value="'+value.batch_no+'">'+value.batch_no+'</option>';
                        });
                        element.closest('.row').find('[name="batch[]"]').html(HTML);
                    }
                }, error: function(error) {
                    console.error(error);
                }
            })
        }
        return repo.name ? repo.name : repo.text;
    }
</script>



