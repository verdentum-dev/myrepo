<!-- <button onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/add_doctor/');" 
    class="btn btn-primary pull-right">
        <?php echo get_phrase('add_doctor'); ?>
</button> -->

<a href="<?php echo base_url();?>index.php?mmuadmin/add_spo/" class="btn btn-primary pull-right">Add SPO</a>

<div style="clear:both;"></div>
<br>
<table class="table table-bordered table-striped datatable" id="table-2">
    <thead>
        <tr>
        <th><?php echo get_phrase('sl.No');?></th>
            <th><?php echo get_phrase('image');?></th>
            <th><?php echo get_phrase('name');?></th>
            <th><?php echo get_phrase('email');?></th>
            <th><?php echo get_phrase('address');?></th>
            <th><?php echo get_phrase('phone');?></th>
            <th><?php echo get_phrase('MMU');?></th>
        </tr>
    </thead>

    <tbody>
            <?php $i=0; foreach ($allspo as $spo) { $i++; ?>   
            <tr>
            <td><?php echo $i; ?>.</td>
                <td>
                    <a href="#">
                        <img src="<?php echo base_url();?>uploads/spo_image/<?php echo $spo['profile_image'];?>"
                        class="img-circle" width="40px" height="40px">
                    </a>
                </td>
                <td><a href="javascript:void(0)"><?php echo $spo['name']?></a></td>
                <td><?php echo $spo['email']?></td>
                <td><?php echo $spo['address']?></td>
                <td><?php echo $spo['phone']?></td>
                <td><?php echo $spo['mmu_name']?></td>
                
            </tr>
        <?php } ?>
              
    </tbody>
</table>

<script type="text/javascript">
    jQuery(window).load(function ()
    {
        var $ = jQuery;

        $("#table-2").dataTable({
            "sPaginationType": "bootstrap",
            /*"sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"*/
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });

        // Highlighted rows
        $("#table-2 tbody input[type=checkbox]").each(function (i, el)
        {
            var $this = $(el),
                    $p = $this.closest('tr');

            $(el).on('change', function ()
            {
                var is_checked = $this.is(':checked');

                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
            });
        });

        // Replace Checboxes
        $(".pagination a").click(function (ev)
        {
            replaceCheckboxes();
        });
    });
</script>