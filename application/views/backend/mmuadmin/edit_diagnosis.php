<div class="row">
        <div class="col-md-12">

            <div class="panel panel-primary" data-collapsed="0">

                <div class="panel-heading">
                    <div class="panel-title">
                        <h3><?php echo get_phrase('edit_medicine_distribution'); ?></h3>
                    </div>
                </div>
                
                <div class="panel-body">
                    <div id="success"></div>
                    <form class="form-horizontal form-groups-bordered" id="editMedicine">

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('Patientid'); ?></label>

                            <div class="col-sm-5">
                                <input type="text" name="patientid" class="form-control" id="field-1" value="<?php echo $medicine['patient_id']; ?>" readonly>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('medicine_name'); ?></label>

                            <div class="col-sm-5">
                                <input type="text" name="medicine_name" class="form-control" id="medicine_name" value="<?php echo $medicine['name']; ?>" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('count'); ?></label>

                            <div class="col-sm-5">
                            <input type="text" name="count" class="form-control" id="count" value="<?php echo $medicine['quantity']; ?>">
                            </div>
                        </div>

                        <div class="col-sm-3 control-label col-sm-offset-2">
                        <button class="btn btn-success" type="submit">Update</button>
                        </div>
                    </form>

                </div>

            </div>

        </div>
    </div>


    <script>
  //ajex call for question update
  $('#editMedicine').on('submit', function(e) {
    e.preventDefault();
    $('button[type="submit"]').attr('disabled', 'disabled').html('Updating Medicine...');
    $('.alert').remove();
    $('input[name="count"]').next('span').remove();
    
    var basic = new FormData($(this)[0]);
    basic.append('tm_id', <?php echo $medicine['tm_id'];?>);
    $.ajax({
     url: '<?php echo base_url(); ?>index.php?pharmacist/updtreatment/',
     type: 'POST',
     data: basic,
     processData: false,
     contentType: false,
     error: function() {
      $('button[type="submit"]').removeAttr('disabled').html('Update');
     },
     success: function(data) {
        var data = JSON.parse(data);
      if(data.status > 0){
        $('input[name="count"]').after('<span class="error">'+data.count+'</span>');
      }
      if(typeof data.msg !== 'undefined'){
        $('#success').html('<div class="alert alert-success">'+data.msg+'</div>');
      }
      $('button[type="submit"]').removeAttr('disabled').html('Update');
     }
   });
  });
 </script>