<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <h3><?php echo get_phrase('add_patient'); ?></h3>
                </div>
            </div>
            
            <div class="panel-body">
                <div id="success"></div>
                <form class="form-horizontal form-groups-bordered" action="index.php?mmuadmin/add_patient" method = "post">
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('mmu_location'); ?></label>
                        <div class="col-sm-5">
                            <select class="form-control" name="mmu_location">
                                <option value="">Select mmu location</option><?php 
                                foreach ($mmu_location as $ml) { ?>
                                  <option value="<?php echo $ml['mmuloc_id']; ?>" <?php echo set_select('mmu_location',$ml["mmuloc_id"]);?>><?php echo $ml['mmuloc_name']; ?></option> <?php
                                } ?>
                            </select>
                            <?php echo form_error('mmu_location'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('care_giver');?></label>
                        <div class="col-sm-5">
                            <input type="text" name="care_giver" class="form-control" value="<?php echo set_value('care_giver');?>" >
                            <?php echo form_error('care_giver'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
                        <div class="col-sm-5">
                            <input type="text" name="name" class="form-control" id="field-1" value="<?php echo set_value('name');?>" >
                            <?php echo form_error('name'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('birth_date');?></label>
                        <div class="col-sm-5">
                            <input type="text" name="birth_date" class="form-control"  id="dob" value="<?php echo set_value('birth_date');?>" >
                            <?php echo form_error('birth_date'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('age');?></label>
                        <div class="col-sm-5">
                            <input type="text" name="age" class="form-control" value="<?php echo set_value('age');?>" >
                            <?php echo form_error('age'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('contact_no'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" name="contact_no" value="<?php echo set_value('contact_no');?>" class="form-control" id="field-1" >
                            <?php echo form_error('contact_no'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('aadhaar_no'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" name="aadhaar_no" value="<?php echo set_value('aadhaar_no');?>" class="form-control" id="field-1" >
                            <?php echo form_error('aadhaar_no'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('gender'); ?></label>
                        <div class="col-sm-5">
                            <label class="radio-inline">
                                <input type="radio" value="MALE" name="gender" <?php echo set_radio('gender','MALE'); ?>>
                            Male</label>
                            <label class="radio-inline">
                                <input type="radio" value="FEMALE" name="gender" <?php echo set_radio('gender','FEMALE'); ?>>
                            Female</label>
                            <label class="radio-inline">
                                <input type="radio" value="OTHER" name="gender" <?php echo set_radio('gender','OTHER'); ?>>
                            Other</label>
                            <?php echo form_error('gender'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('blood_group'); ?></label>
                        <div class="col-sm-5">
                            <label class="radio-inline">
                                <input type="radio" value="A+" name="blood_group" <?php echo set_radio('blood_group','A+'); ?>>
                            A+</label>
                            <label class="radio-inline">
                                <input type="radio" value="A-" name="blood_group" <?php echo set_radio('blood_group','A-'); ?>>
                            A-</label>
                            <label class="radio-inline">
                                <input type="radio" value="B+" name="blood_group" <?php echo set_radio('blood_group','B+'); ?>>
                            B+</label>
                            <label class="radio-inline">
                                <input type="radio" value="B-" name="blood_group" <?php echo set_radio('blood_group','B-'); ?>>
                            B-</label>
                            <label class="radio-inline">
                                <input type="radio" value="AB+" name="blood_group" <?php echo set_radio('blood_group','AB+'); ?>>
                            AB+</label>
                            <label class="radio-inline">
                                <input type="radio" value="AB-" name="blood_group" <?php echo set_radio('blood_group','AB-'); ?>>
                            AB-</label>
                            <label class="radio-inline">
                                <input type="radio" value="O+" name="blood_group" <?php echo set_radio('blood_group','O+'); ?>>
                            O+</label>
                            <label class="radio-inline">
                                <input type="radio" value="O-" name="blood_group" <?php echo set_radio('blood_group','O-'); ?>>
                            O-</label>
                            <?php echo form_error('blood_group'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('address'); ?></label>
                        <div class="col-sm-9">
                            <textarea name="address" class="form-control"><?php echo set_value('address');?></textarea>
                            <?php echo form_error('address'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('family_status'); ?></label>
                        <div class="col-sm-5">
                            <label class="radio-inline">
                                <input type="radio" value="APL" name="family_status" <?php echo set_radio('family_status','APL'); ?>>
                            APL</label>
                            <label class="radio-inline">
                                <input type="radio" value="BPL" name="family_status" <?php echo set_radio('family_status','BPL'); ?>>
                            BPL</label>
                            <?php echo form_error('family_status'); ?>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('education'); ?></label>
                        <div class="col-sm-5">
                            <select class="form-control" name="education">
                                <option value="">Select education</option>
                                <option value="illiterate" <?php echo set_select('education','illiterate');?>>Illiterate</option>
                                <option value="literate" <?php echo set_select('education','literate');?>>Literate</option>
                                <option value="matriculation" <?php echo set_select('education','matriculation');?>>Matriculation</option>
                                <option value="intermediate" <?php echo set_select('education','intermediate');?>>Intermediate</option>
                                <option value="diploma" <?php echo set_select('education','diploma');?>>Diploma</option>
                                <option value="graduate" <?php echo set_select('education','graduate');?>>Graduate</option>
                                <option value="post graduate" <?php echo set_select('education','post graduate');?>>Post Graduate</option>
                            </select>
                            <?php echo form_error('education'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('occupation'); ?></label>
                        <div class="col-sm-5">
                            <select class="form-control" name="occupation">
                                <option value="">Select Occupation</option>
                                <option value="nil" <?php echo set_select('occupation','nil');?>>NIL</option>
                                <option value="house_wife" <?php echo set_select('occupation','house_wife');?>>House wife</option>
                                <option value="daily_wage_labour" <?php echo set_select('occupation','daily_wage_labour');?>>Daily Wage Labour</option>
                                <option value="business" <?php echo set_select('occupation','business');?>>Business</option>
                                <option value="service" <?php echo set_select('occupation','service');?>>Service</option>
                                <option value="farmer" <?php echo set_select('occupation','farmer');?>>Farmer</option>
                                <option value="artisan" <?php echo set_select('occupation','artisan');?>>Artisan</option>
                                <option value="pensioner" <?php echo set_select('occupation','pensioner');?>>Pensioner</option>
                                <option value="ss_pensioner" <?php echo set_select('occupation','ss_pensioner');?>>SS Pensioner</option>
                                <option value="other" <?php echo set_select('occupation','other');?>>Other</option>
                            </select>
                            <?php echo form_error('occupation'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('yearly_income'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" name="yearly_income" value="<?php echo set_value('yearly_income');?>" class="form-control">
                            <?php echo form_error('yearly_income'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('type_of_disability'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" name="type_of_disability" value="<?php echo set_value('type_of_disability');?>" class="form-control">
                            <?php echo form_error('type_of_disability'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('habits'); ?></label>
                        <div class="col-sm-5">
                            <label class="checkbox-inline"><input type="checkbox" name="habits[]" value="Beedi" <?php echo set_checkbox('habits','Beedi'); ?>>Beedi</label>
                            <label class="checkbox-inline"><input type="checkbox" name="habits[]" value="Tobacco(chewing)" <?php echo set_checkbox('habits','Tobacco(chewing)'); ?>>Tobacco(chewing)</label>
                            <label class="checkbox-inline"><input type="checkbox" name="habits[]" value="Gutka" <?php echo set_checkbox('habits','Gutka'); ?>>Gutka</label>
                            <label class="checkbox-inline"><input type="checkbox" name="habits[]" value="Alcohol" <?php echo set_checkbox('habits','Alcohol'); ?>>Alcohol</label>
                            <?php echo form_error('habits'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('movable_assets'); ?></label>
                        <div class="col-sm-5">
                            <label class="checkbox-inline"><input type="checkbox" name="movable_assets[]" value="Cattle" <?php echo set_checkbox('movable_assets','Cattle'); ?>>Cattle</label>
                            <label class="checkbox-inline"><input type="checkbox" name="movable_assets[]" value="Vehicle" <?php echo set_checkbox('movable_assets','Vehicle'); ?>>Vehicle</label>
                            <?php echo form_error('movable_assets'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('immovable_assets'); ?></label>
                        <div class="col-sm-5">
                            <label class="checkbox-inline"><input type="checkbox" name="immovable_assets[]" value="Home" <?php echo set_checkbox('immovable_assets','Home'); ?>>Home</label>
                            <label class="checkbox-inline"><input type="checkbox" name="immovable_assets[]" value="Land" <?php echo set_checkbox('immovable_assets','Land'); ?>>Land</label>
                            <?php echo form_error('immovable_assets'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('ration_card'); ?></label>
                        <div class="col-sm-5">
                            <label class="radio-inline">
                                <input type="radio" value="1" name="ration_card" <?php echo set_radio('ration_card',1); ?>>
                            Yes</label>
                            <label class="radio-inline">
                                <input type="radio" value="" name="ration_card" <?php echo set_radio('ration_card',''); ?>>
                            No</label>
                            <?php echo form_error('ration_card'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('insurance'); ?></label>
                        <div class="col-sm-5">
                            <label class="radio-inline">
                                <input type="radio" value="1" name="insurance" <?php echo set_radio('insurance',1); ?>>
                            Yes</label>
                            <label class="radio-inline">
                                <input type="radio" value="" name="insurance" <?php echo set_radio('insurance',''); ?>>
                            No</label>
                            <?php echo form_error('insurance'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('pension'); ?></label>
                        <div class="col-sm-5">
                            <label class="radio-inline">
                                <input type="radio" value="1" name="pension" <?php echo set_radio('pension',1); ?>>
                            Yes</label>
                            <label class="radio-inline">
                                <input type="radio" value="" name="pension" <?php echo set_radio('pension',''); ?>>
                            No</label>
                            <?php echo form_error('pension'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('bank_account'); ?></label>
                        <div class="col-sm-5">
                            <label class="radio-inline">
                                <input type="radio" value="1" name="bank_account" <?php echo set_radio('bank_account',1); ?>>
                            Yes</label>
                            <label class="radio-inline">
                                <input type="radio" value="" name="bank_account" <?php echo set_radio('bank_account',''); ?>>
                            No</label>
                            <?php echo form_error('bank_account'); ?>
                        </div>
                    </div>


                    <div class="col-sm-3 control-label col-sm-offset-2">
                         <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('#dob').datepicker({
            format: 'yyyy-mm-dd'
        });
    });
</script>