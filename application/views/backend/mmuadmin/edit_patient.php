<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <h3><?php echo get_phrase('edit_patient'); ?></h3>
                </div>
            </div>
            
            <div class="panel-body">
                <div id="success"></div>
                <form class="form-horizontal form-groups-bordered" action="index.php?mmuadmin/edit_patient/<?php echo $this->uri->segment(3); ?>" method = "post">
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('mmu_location'); ?></label>
                        <div class="col-sm-5">
                            <select class="form-control" name="mmu_location">
                                <option value="">Select mmu location</option><?php 
                                foreach ($mmu_location as $ml) { ?>
                                  <option value="<?php echo $ml['mmuloc_id']; ?>" <?php if($patient_detail['mmuloc'] == $ml['mmuloc_id']){
                                    echo "selected";
                                  } ?> ><?php echo $ml['mmuloc_name']; ?></option> <?php
                                } ?>
                            </select>
                            <?php echo form_error('mmu_location'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('care_giver');?></label>
                        <div class="col-sm-5">
                            <input type="text" name="care_giver" class="form-control" value="<?php echo $patient_detail['care_of']; ?>" >
                            <?php echo form_error('care_giver'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
                        <div class="col-sm-5">
                            <input type="text" name="name" class="form-control" id="field-1" value="<?php echo $patient_detail['name']; ?>" >
                            <?php echo form_error('name'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('birth_date');?></label>
                        <div class="col-sm-5">
                            <input type="text" name="birth_date" class="form-control"  id="dob" value="<?php echo $patient_detail['birth_date']; ?>" >
                            <?php echo form_error('birth_date'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('age');?></label>
                        <div class="col-sm-5">
                            <input type="text" name="age" class="form-control" value="<?php echo $patient_detail['age']; ?>" >
                            <?php echo form_error('age'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('contact_no'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" name="contact_no" value="<?php echo $patient_detail['phone']; ?>" class="form-control" id="field-1" >
                            <?php echo form_error('contact_no'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('aadhaar_no'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" name="aadhaar_no" value="<?php echo $patient_detail['adhar_no']; ?>" class="form-control" id="field-1" >
                            <?php echo form_error('aadhaar_no'); ?>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('gender'); ?></label>
                        <div class="col-sm-5">
                            <label class="radio-inline">
                                <input type="radio" value="MALE" name="gender" <?php if($patient_detail['gender'] == 'MALE'){ echo "checked"; } ?>>
                            Male</label>
                            <label class="radio-inline">
                                <input type="radio" value="FEMALE" name="gender" <?php if($patient_detail['gender'] == 'FEMALE'){ echo "checked"; } ?>>
                            Female</label>
                            <label class="radio-inline">
                                <input type="radio" value="OTHER" name="gender" <?php if($patient_detail['gender'] == 'OTHER'){ echo "checked"; } ?>>
                            Other</label>
                            <?php echo form_error('gender'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('blood_group'); ?></label>
                        <div class="col-sm-5">
                            <label class="radio-inline">
                                <input type="radio" value="A+" name="blood_group" <?php if($patient_detail['blood_group'] == 'A+'){ echo "checked"; } ?>>
                            A+</label>
                            <label class="radio-inline">
                                <input type="radio" value="A-" name="blood_group" <?php if($patient_detail['blood_group'] == 'A-'){ echo "checked"; } ?>>
                            A-</label>
                            <label class="radio-inline">
                                <input type="radio" value="B+" name="blood_group" <?php if($patient_detail['blood_group'] == 'B+'){ echo "checked"; } ?>>
                            B+</label>
                            <label class="radio-inline">
                                <input type="radio" value="B-" name="blood_group" <?php if($patient_detail['blood_group'] == 'B-'){ echo "checked"; } ?>>
                            B-</label>
                            <label class="radio-inline">
                                <input type="radio" value="AB+" name="blood_group" <?php if($patient_detail['blood_group'] == 'AB+'){ echo "checked"; } ?>>
                            AB+</label>
                            <label class="radio-inline">
                                <input type="radio" value="AB-" name="blood_group" <?php if($patient_detail['blood_group'] == 'AB-'){ echo "checked"; } ?>>
                            AB-</label>
                            <label class="radio-inline">
                                <input type="radio" value="O+" name="blood_group" <?php if($patient_detail['blood_group'] == 'O+'){ echo "checked"; } ?>>
                            O+</label>
                            <label class="radio-inline">
                                <input type="radio" value="O-" name="blood_group" <?php if($patient_detail['blood_group'] == 'O-'){ echo "checked"; } ?>>
                            O-</label>
                            <?php echo form_error('blood_group'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('address'); ?></label>
                        <div class="col-sm-9">
                            <textarea name="address" class="form-control"><?php echo $patient_detail['address'];?></textarea>
                            <?php echo form_error('address'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('family_status'); ?></label>
                        <div class="col-sm-5">
                            <label class="radio-inline">
                                <input type="radio" value="APL" name="family_status" <?php if($patient_detail['family_status'] == 'APL'){ echo "checked"; } ?>>
                            APL</label>
                            <label class="radio-inline">
                                <input type="radio" value="BPL" name="family_status" <?php if($patient_detail['family_status'] == 'BPL'){ echo "checked"; } ?>>
                            BPL</label>
                            <?php echo form_error('family_status'); ?>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('education'); ?></label>
                        <div class="col-sm-5">
                            <select class="form-control" name="education">
                                <option value="">Select education</option>
                                <option value="illiterate" <?php if($patient_detail['education'] == 'illiterate'){
                                    echo "selected";
                                  } ?>>llliterate</option>
                                <option value="literate" <?php if($patient_detail['education'] == 'literate'){
                                    echo "selected";
                                  } ?> >Literate</option>
                                <option value="matriculation" <?php if($patient_detail['education'] == 'matriculation'){
                                    echo "selected";
                                  } ?>>Matriculation</option>
                                <option value="intermediate" <?php if($patient_detail['education'] == 'intermediate'){
                                    echo "selected";
                                  } ?>>Intermediate</option>
                                <option value="diploma" <?php if($patient_detail['education'] == 'diploma'){
                                    echo "selected";
                                  } ?>>Diploma</option>
                                <option value="graduate" <?php if($patient_detail['education'] == 'graduate'){
                                    echo "selected";
                                  } ?>>Graduate</option>
                                <option value="post graduate" <?php if($patient_detail['education'] == 'post graduate'){
                                    echo "selected";
                                  } ?>>Post Graduate</option>
                            </select>
                            <?php echo form_error('education'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('occupation'); ?></label>
                        <div class="col-sm-5">
                            <select class="form-control" name="occupation">
                                <option value="">Select Occupation</option>
                                <option value="nil" <?php if($patient_detail['occupation'] == 'nil'){
                                    echo "selected";
                                  } ?>>NIL</option>
                                <option value="house_wife" <?php if($patient_detail['occupation'] == 'House wife'){
                                    echo "selected";
                                  } ?>>House wife</option>
                                <option value="daily_wage_labour" <?php if($patient_detail['occupation'] == 'Daily Wage Labour'){
                                    echo "selected";
                                  } ?>>Daily Wage Labour</option>
                                <option value="business" <?php if($patient_detail['occupation'] == 'business'){
                                    echo "selected";
                                  } ?>>Business</option>
                                <option value="service" <?php if($patient_detail['occupation'] == 'service'){
                                    echo "selected";
                                  } ?>>Service</option>
                                <option value="farmer" <?php if($patient_detail['occupation'] == 'farmer'){
                                    echo "selected";
                                  } ?>>Farmer</option>
                                <option value="artisan" <?php if($patient_detail['occupation'] == 'artisan'){
                                    echo "selected";
                                  } ?>>Artisan</option>
                                <option value="pensioner" <?php if($patient_detail['occupation'] == 'pensioner'){
                                    echo "selected";
                                  } ?>>Pensioner</option>
                                <option value="ss_pensioner" <?php if($patient_detail['occupation'] == 'ss_pensioner'){
                                    echo "selected";
                                  } ?>>SS Pensioner</option>
                                <option value="other" <?php if($patient_detail['occupation'] == 'other'){
                                    echo "selected";
                                  } ?>>Other</option>
                            </select>
                            <?php echo form_error('occupation'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('yearly_income'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" name="yearly_income" value="<?php echo $patient_detail['income'];?>" class="form-control">
                            <?php echo form_error('yearly_income'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('type_of_disability'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" name="type_of_disability" value="<?php echo $patient_detail['disability'];?>" class="form-control">
                            <?php echo form_error('type_of_disability'); ?>
                        </div>
                    </div>
                    <?php
                        if(count($patient_habitdetail) != 0){
                            foreach ($patient_habitdetail as $ph) {
                                $phabits[] = $ph['habit'];
                            }
                        }else{
                            $phabits = [];
                        }
                    ?>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('habits'); ?></label>
                        <div class="col-sm-5">
                            <label class="checkbox-inline"><input type="checkbox" name="habits[]" value="Beedi" <?php if (in_array("Beedi", $phabits)) { echo "checked"; } ?>  >Beedi</label>
                            <label class="checkbox-inline"><input type="checkbox" name="habits[]" value="Tobacco(chewing)" <?php if (in_array("Tobacco(chewing)", $phabits)) { echo "checked"; } ?> >Tobacco(chewing)</label>
                            <label class="checkbox-inline"><input type="checkbox" name="habits[]" value="Gutka" <?php if (in_array("Gutka", $phabits)) { echo "checked"; } ?> >Gutka</label>
                            <label class="checkbox-inline"><input type="checkbox" name="habits[]" value="Alcohol" <?php if (in_array("Alcohol", $phabits)) { echo "checked"; } ?> >Alcohol</label>
                            <?php echo form_error('habits'); ?>
                        </div>
                    </div>
                    <?php
                        if(count($patient_assestsdetail) != 0){
                            foreach ($patient_assestsdetail as $pa) {
                                $passests[] = $pa['name'];
                            }
                        }else{
                            $passests = [];
                        }                       
                    ?>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('movable_assets'); ?></label>
                        <div class="col-sm-5">
                            <label class="checkbox-inline"><input type="checkbox" name="movable_assets[]" value="Cattle" <?php if (in_array("Cattle", $passests)) { echo "checked"; } ?> >Cattle</label>
                            <label class="checkbox-inline"><input type="checkbox" name="movable_assets[]" value="Vehicle" <?php if (in_array("Vehicle", $passests)) { echo "checked"; } ?> >Vehicle</label>
                            <?php echo form_error('movable_assets'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('immovable_assets'); ?></label>
                        <div class="col-sm-5">
                            <label class="checkbox-inline"><input type="checkbox" name="immovable_assets[]" value="Home" <?php if (in_array("Home", $passests)) { echo "checked"; } ?>>Home</label>
                            <label class="checkbox-inline"><input type="checkbox" name="immovable_assets[]" value="Land" <?php if (in_array("Land", $passests)) { echo "checked"; } ?>>Land</label>
                            <?php echo form_error('immovable_assets'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('ration_card'); ?></label>
                        <div class="col-sm-5">
                            <label class="radio-inline">
                                <input type="radio" value="1" name="ration_card" <?php if($patient_detail['ration'] == '1'){ echo "checked"; } ?>>
                            Yes</label>
                            <label class="radio-inline">
                                <input type="radio" value="" name="ration_card" <?php if($patient_detail['ration'] == ''){ echo "checked"; } ?>>
                            No</label>
                            <?php echo form_error('ration_card'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('insurance'); ?></label>
                        <div class="col-sm-5">
                            <label class="radio-inline">
                                <input type="radio" value="1" name="insurance" <?php if($patient_detail['insurance'] == '1'){ echo "checked"; } ?>>
                            Yes</label>
                            <label class="radio-inline">
                                <input type="radio" value="" name="insurance" <?php if($patient_detail['insurance'] == ''){ echo "checked"; } ?>>
                            No</label>
                            <?php echo form_error('insurance'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('pension'); ?></label>
                        <div class="col-sm-5">
                            <label class="radio-inline">
                                <input type="radio" value="1" name="pension" <?php if($patient_detail['pension'] == '1'){ echo "checked"; } ?>>
                            Yes</label>
                            <label class="radio-inline">
                                <input type="radio" value="" name="pension" <?php if($patient_detail['pension'] == ''){ echo "checked"; } ?>>
                            No</label>
                            <?php echo form_error('pension'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('bank_account'); ?></label>
                        <div class="col-sm-5">
                            <label class="radio-inline">
                                <input type="radio" value="1" name="bank_account" <?php if($patient_detail['account'] == '1'){ echo "checked"; } ?>>
                            Yes</label>
                            <label class="radio-inline">
                                <input type="radio" value="" name="bank_account" <?php if($patient_detail['account'] == ''){ echo "checked"; } ?>>
                            No</label>
                            <?php echo form_error('bank_account'); ?>
                        </div>
                    </div>


                    <div class="col-sm-3 control-label col-sm-offset-2">
                         <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('#dob').datepicker({
            format: 'yyyy-mm-dd'
        });
    });
</script>