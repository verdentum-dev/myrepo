<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <h3><?php echo get_phrase('add_pharmacist'); ?></h3>
                </div>
            </div>

            <div class="panel-body">
                <div id="success"></div>
                <form class="form-horizontal form-groups-bordered" id="addpharmasict">
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
                        <div class="col-sm-5">
                            <input type="text" name="name" class="form-control" id="field-1" value="<?php echo set_value('name');?>" >
                            <?php echo form_error('name'); ?>
                        </div>                        
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('email'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" name="email" class="form-control" id="field-1" value="<?php echo set_value('email');?>" >
                            <?php echo form_error('email'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('password'); ?></label>
                        <div class="col-sm-5">
                            <input type="password" name="pass" value="<?php echo set_value('pass');?>" class="form-control" id="field-1" >
                            <?php echo form_error('pass'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('address'); ?></label>
                        <div class="col-sm-9">
                            <textarea name="address" class="form-control" id="field-ta"><?php echo set_value('address');?></textarea>
                            <?php echo form_error('address'); ?>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('phone'); ?></label>
                        <div class="col-sm-5">
                            <input type="text" name="phone" value="<?php echo set_value('phone');?>" class="form-control" id="field-1" >
                            <?php echo form_error('phone'); ?>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('image'); ?></label>
                        <div class="col-sm-5">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                                    <img src="http://placehold.it/200x150" alt="...">
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                <div>
                                    <span class="btn btn-white btn-file">
                                        <span class="fileinput-new">Select image</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" name="image" accept="image/*">
                                    </span>
                                    <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3 control-label col-sm-offset-2">
                         <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('#addpharmasict').on('submit', function(event) {
            event.preventDefault();
            $('button[type="submit"]').attr('disabled', 'disabled').html('Uploading pharmacist...');
            $('.alert').remove();

            $('input[name="name"]').next('span').remove();
            $('input[name="email"]').next('span').remove();
            $('input[name="pass"]').next('span').remove();
            $('textarea[name="address"]').next('span').remove();
            $('input[name="phone"]').next('span').remove();
            $('input[name="image"]').next('span').remove();
           
            var basic = new FormData($(this)[0]);
            $.ajax({
                url: '<?php echo base_url(); ?>index.php?mmuadmin/insert_pharmacist/',
                type: 'POST',
                data: basic,
                processData: false,
                contentType: false,
                error: function() {
                    $('button[type="submit"]').removeAttr('disabled').html('Submit');
                },
                success: function(data) {
                    var data = JSON.parse(data);
                    if(data.status > 0){                
                        $('input[name="name"]').after('<span class="error">'+data.name+'</span>');
                        $('input[name="email"]').after('<span class="error">'+data.email+'</span>');
                        $('input[name="pass"]').after('<span class="error">'+data.pass+'</span>');
                        $('input[name="phone"]').after('<span class="error">'+data.phone+'</span>');
                    }
                    if(typeof data.msg !== 'undefined'){
                        $('#success').html('<div class="alert alert-success">'+data.msg+'</div>');
                        $('form')[0].reset();
                        var selectBox = $("select").selectBoxIt();
                        $(selectBox).selectBoxIt('selectOption', 0);
                    }
                    $('button[type="submit"]').removeAttr('disabled').html('Submit');
                }
            });
        });
    });
</script>