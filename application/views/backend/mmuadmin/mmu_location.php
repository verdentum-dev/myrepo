<!-- <button type="button" class="btn btn-primary pull-right">
        <?php echo get_phrase('add MMU'); ?>
</button> -->
<a href="<?php echo base_url(); ?>index.php?mmuadmin/add_mmu_location" class="btn btn-primary pull-right">Add MMU Site</a>
<div style="clear:both;"></div>
<br>
<table class="table table-bordered table-striped datatable" id="table-2">
    <thead>
        <tr>
            <th><?php echo get_phrase('Sl. No.');?></th>
            <th><?php echo get_phrase('MMU');?></th>
            <th><?php echo get_phrase('MMU Site');?></th>
            <th><?php echo get_phrase('Status');?></th>
            <th><?php echo get_phrase('options');?></th>
        </tr>
    </thead>

    <tbody>  <?php if(count($mmulocation) > 0) { ?>
            <?php $i=0; foreach ($mmulocation as $location) { $i++; ?>
            <tr>
                
                <td><?php echo $i; ?>.</td>
                <td><?php echo $location['mmu_name']; ?></td>
                <td><?php echo $location['mmuloc_name']; ?></td>
                <td>Running</td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php?mmuadmin/edit_mmu_location/<?php echo $location['mmuloc_id']; ?>" class="btn btn-default btn-sm btn-icon icon-left">
                            <i class="entypo-pencil"></i>
                            Edit
                    </a>
                    <a href="#" 
                        class="btn btn-danger btn-sm btn-icon icon-left" onclick="return checkDelete();">
                            <i class="entypo-cancel"></i>
                            Delete
                    </a>
                </td>
            </tr>
            <?php } ?>
              <?php } else { ?>
            <tr>
                <td colspan="5">No location found</td>
            </tr>
                <?php } ?>
            
    </tbody>
</table>