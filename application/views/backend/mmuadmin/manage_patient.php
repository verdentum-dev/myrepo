<a href="<?php echo base_url();?>index.php?mmuadmin/add_patient/" class="btn btn-primary pull-right">Add Patient</a>
<div style="clear:both;"></div>
<br>
<table class="table table-bordered table-striped datatable" id="table-2">
    <thead>
        <tr>
            <th><?php echo get_phrase('care_of') ?></th>
            <th><?php echo get_phrase('name');?></th>
            <th><?php echo get_phrase('Blood Group');?></th>
            <th><?php echo get_phrase('MMU');?></th>
            <th><?php echo get_phrase('phone');?></th>            
        </tr>
    </thead>

    <tbody><?php
    //var_dump($patient_info);
    foreach ($patient_info as $row) { ?>   
        <tr>
            <td><?php 
                if(($row['care_of']) == '')
                {
                echo "Not Available";
                }
                else
                {    
                echo $row['care_of'];
                }
            ?></td>
            <td><?php echo $row['name']; ?></td>
            <td><?php
                if(($row['blood_group']) == '')
                {
                echo "Not Available";
                }
                else
                { 
                echo $row['blood_group'];
                } 
            ?></td>
            <td><?php echo $row['mmu_name']; ?></td>
            <td><?php 
                if(($row['phone']) == '')
                {
                echo "Not Available";
                }
                else
                { 
                echo $row['phone']; 
                }
            ?></td>
        </tr><?php 
    } ?>
    </tbody>
</table>

<script type="text/javascript">
    jQuery(window).load(function ()
    {
        var $ = jQuery;

        $("#table-2").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });

        // Highlighted rows
        $("#table-2 tbody input[type=checkbox]").each(function (i, el)
        {
            var $this = $(el),
                    $p = $this.closest('tr');

            $(el).on('change', function ()
            {
                var is_checked = $this.is(':checked');

                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
            });
        });

        // Replace Checboxes
        $(".pagination a").click(function (ev)
        {
            replaceCheckboxes();
        });
    });
</script>