<!-- <button onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/add_pharmacist/');" 
    class="btn btn-primary pull-right">
        <?php echo get_phrase('add_pharmacist'); ?>
</button> -->

<a href="<?php echo base_url();?>index.php?mmuadmin/medicine/" class="btn btn-primary pull-right">Back</a>
<div style="clear:both;"></div>
<br>
<table class="table table-bordered table-striped datatable" id="table-2">
    <thead>
        <tr>
            <th><?php echo get_phrase('Name');?></th>
            <th><?php echo get_phrase('preparation');?></th>
            <th><?php echo get_phrase('no_of_units');?></th>
            <th><?php echo get_phrase('batch_no');?></th>
            <th><?php echo get_phrase('exp_date');?></th>
            <th><?php echo get_phrase('manufacturing_company');?></th>
            <!-- <th><?php echo get_phrase('options');?></th> -->
        </tr>
    </thead>

    <tbody>
        <?php foreach ($medicine_stock as $row) { ?>   
            <tr>
                <td><?php echo $row['name']?></td>
                <td><?php echo $row['preparation']?></td>
                <td><?php echo $row['no_of_unit']?></td>
                <td><?php echo $row['batch_no']?></td>
                <td><?php echo $row['exp_date']?></td>
                <td><?php echo $row['manufacturing_company']?></td>
                <!-- <td>
                    <a  href="#" class="btn btn-default btn-sm btn-icon icon-left">
                            <i class="entypo-pencil"></i>
                            Edit
                    </a>
                    <a href="#" class="btn btn-danger btn-sm btn-icon icon-left"s>
                            <i class="entypo-cancel"></i>
                            Delete
                    </a>
                </td> -->
            </tr>
        <?php } ?>
    </tbody>
</table>

<script type="text/javascript">
    jQuery(window).load(function ()
    {
        var $ = jQuery;

        $("#table-2").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });

        // Highlighted rows
        $("#table-2 tbody input[type=checkbox]").each(function (i, el)
        {
            var $this = $(el),
                    $p = $this.closest('tr');

            $(el).on('change', function ()
            {
                var is_checked = $this.is(':checked');

                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
            });
        });

        // Replace Checboxes
        $(".pagination a").click(function (ev)
        {
            replaceCheckboxes();
        });
    });
</script>