<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <h3><?php echo get_phrase('filter_by_date'); ?></h3>
                </div>
            </div>

            <div class="panel-body text-center">
                <form class="form-inline" id="filter">
                    <div class="form-group">
                        <label class="" for="startDate">Date</label>
                        <input type="text" placeholder="select date" class="form-control datepicker" id="date" name="date" required>
                    </div>
                    <button type="submit" class="btn btn-default">Filter</button>
                </form>
            </div>
        </div>

        <div id="table" class="table-responsive"></div>

    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('#date').datepicker({
            format: 'yyyy-mm-dd'
        });

        $("#filter").on("submit", function(ev)
        {
            ev.preventDefault();
            var formData = {
                'date': $('#date').val() 
            };
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php?mmuadmin/vanstock",
                data: formData,
                dataType:'json',
                success: function(data) {
                    if(data.medicines.length == 0){
                        var table = '<table class="table table-bordered">\
                            <thead>\
                              <tr>\
                                <th rowspan="2">Medicine Name</th>\
                                <th colspan="3">On '+$('#date').val()+'</th>\
                                <th rowspan="2">Batch Number</th>\
                                <th rowspan="2">Exp Date</th>\
                              </tr>\
                              <tr>\
                                <th>Total Loaded</th>\
                                <th>Total Sold</th>\
                                <th>Total Available</th>\
                              </tr>\
                            </thead>\
                            <tbody>\
                                <tr>\
                                <td colspan="7">No Record Found</td>\
                                </tr>\
                            </tbody>\
                        </table>'; 
                    }
                    else{
                        var table = '<table class="table table-bordered">\
                            <thead>\
                              <tr>\
                                <th rowspan="2">Medicine Name</th>\
                                <th colspan="3" class="text-center">Data On '+$('#date').val()+'</th>\
                                <th rowspan="2">Batch Number</th>\
                                <th rowspan="2">Exp Date</th>\
                              </tr>\
                              <tr>\
                                <th>Total Loaded</th>\
                                <th>Total Sold</th>\
                                <th>Total Available</th>\
                              </tr>\
                            </thead>\
                            <tbody>';
                            data.medicines.map(function(value, index) {
                                var sold = value.sold == null ? 0 : value.sold;
                                table += '<tr><td>'+value.name+'</td>\
                                <td>'+value.loaded+'</td>\
                                <td>'+sold+'</td>\
                                <td>'+value.available+'</td>\
                                <td>'+value.batch_no+'</td>\
                                <td>'+value.exp_date+'</td></tr>';
                            });
                        table += '</tbody>\
                        </table>';
                    }
                    $('#table').html(table);
                }
            });
        });
    }) 
</script>