<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <h3><?php echo get_phrase('find_patient_for_treatment'); ?></h3>
                </div>
            </div>
            
            <div class="panel-body">
                <div id="success"></div>
                <div class = "row">
                    <div class = "col-md-5">
                        <div class="form-group">
                            <label for="field-1" class="control-label"><?php echo get_phrase('patient_id');?></label>
                                <input type="text" name="patient_id" class="form-control" id = "patient_id" placeholder="Enter Patient Id" >
                                <?php echo form_error('patient_id'); ?>
                            
                        </div>
                    </div>                    

                    <div class = "col-md-5">
                        <div class="form-group">
                            <label for="field-1" class="control-label"><?php echo get_phrase('mmu_location'); ?></label>
                            <select class="form-control" name="mmu_location" id = "mmu_location">
                                <option value="">Select mmu location</option><?php 
                                foreach ($mmu_location as $ml) { ?>
                                  <option value="<?php echo $ml['mmuloc_id']; ?>" <?php echo set_select('mmu_location',$ml["mmuloc_id"]);?>><?php echo $ml['mmuloc_name']; ?></option> <?php
                                } ?>
                            </select>
                            <?php echo form_error('mmu_location'); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button class = "btn btn-primary" style = "margin-top: 23px;" id = "search">Search</button> 
                    </div>                    
                </div>
            </div>
        </div>
       <div class="panel-body">            
            <div class="row">
                <table class="table table-hover" >
                    <thead>
                        <tr>
                            <th>Patient ID</th>
                            <th>Name</th>
                            <th>Age</th>
                            <th>Gender</th>
                            <th>Blood Group</th>
                            <th>Phone</th>
                            <th>Mmu</th>
                            <th>Mmu Location</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="patient_details">
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('#search').on('click',function(){
            var patient_id = $('#patient_id').val();
            var mmu_location = $('#mmu_location').val();
            if(patient_id != '' && mmu_location != ''){
                $.ajax({
                    url: "index.php?mmuadmin/getpatient_byid",
                    type: "POST",
                    dataType: "json",
                    data: {
                      patient_id : patient_id,
                      mmu_location : mmu_location
                    },
                    success: function(data){
                        if (data.length == 0){   
                            $('#patient_details').html("No Patient Found");
                        }
                        else{
                            $('#patient_details').html('');
                            data.forEach(function(currentValue, index) {
                                $('#patient_details').append('<tr>\
                                    <td>'+currentValue.patient_id+'</td>\
                                    <td>'+currentValue.name+'</td>\
                                    <td>'+currentValue.age+'</td>\
                                    <td>'+currentValue.gender+'</td>\
                                    <td>'+currentValue.blood_group+'</td>\
                                    <td>'+currentValue.phone+'</td>\
                                    <td>'+currentValue.mmu_name+'</td>\
                                    <td>'+currentValue.mmuloc_name+'</td>\
                                    <td>\
                                        <a href = "<?php echo base_url(); ?>index.php?/mmuadmin/add_treatment/'+currentValue.patient_id+'" class = "btn btn-warning">Add Treatment</button>&nbsp;&nbsp;\
                                    </td>\
                                </tr>');
                            });
                        }
                    }
                });
            }else{
                alert('Both Id and mmusite');
            }
        });
    });
</script>