<div class="sidebar-menu">
    <header class="logo-env" style="padding:12px 21px;">

        <!-- logo -->
        <div class="logo" style="">
            <a href="<?php echo base_url(); ?>">
                <img src="assets/images/logo.png" style="max-width:200px;margin-top:16px;">
            </a>
        </div>

        <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
        <div class="sidebar-mobile-menu visible-xs">
            <a href="#" class="with-animation">
                <i class="entypo-menu"></i>
            </a>
        </div>
    </header>
    <div class="sidebar-user-info">

        <div class="sui-normal">
            <a href="#" class="user-link">
            <?php   $image = $this->db->get_where('user_details', array('user_id' =>
                        $this->session->userdata('login_user_id')))->row()->profile_image; ?>
                <img src="<?php echo base_url();?>uploads/mmuadmin_image/<?php echo $image; ?>" alt="" class="img-circle" style="height:44px; width:44px;">

                <span><?php echo get_phrase('welcome'); ?>,</span>
                <strong style="color: #ffffff;"><?php 
                    echo $this->db->get_where('user_details', array('user_id' =>
                        $this->session->userdata('login_user_id')))->row()->name;
                    ?>
                </strong>
            </a>
        </div>

        <div class="sui-hover inline-links animate-in"><!-- You can remove "inline-links" class to make links appear vertically, class "animate-in" will make A elements animateable when click on user profile -->				
            <a href="<?php echo base_url(); ?>index.php?<?php echo $account_type; ?>/manage_profile">
                <i class="entypo-pencil"></i>
                <?php echo get_phrase('edit_profile'); ?>
            </a>

            <a href="<?php echo base_url(); ?>index.php?<?php echo $account_type; ?>/manage_profile">
                <i class="entypo-lock"></i>
                <?php echo get_phrase('change_password'); ?>
            </a>

            <span class="close-sui-popup">×</span><!-- this is mandatory -->			
        </div>
    </div>


    <div style="border-top:1px solid rgba(69, 74, 84, 0.7);"></div>	
    <ul id="main-menu" class="">
        <!-- add class "multiple-expanded" to allow multiple submenus to open -->
        <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->

        <!-- DASHBOARD -->
        <li class="<?php if ($page_name == 'dashboard') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?mmuadmin">
                <!-- <i class="fa fa-desktop"></i> -->
                <span><?php echo get_phrase('dashboard'); ?></span>
            </a>
        </li>

        <li class="<?php if ($page_name == 'manage_mmu') echo 'active'; ?> <?php if ($page_name == 'add_mmu') echo 'active'; ?> <?php if ($page_name == 'mmu_location') echo 'active'; ?> <?php if ($page_name == 'mmu_location') echo 'active'; ?> <?php if ($page_name == 'add_mmu_location') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?mmuadmin/manage_mmu">
                <!-- <i class="fa fa-ambulance"></i> -->
                <span><?php echo get_phrase('manage_MMU'); ?></span>
            </a>
        </li>

        <li class="<?php if ($page_name == 'manage_medicine') echo 'active'; ?> <?php if ($page_name == 'add_medicine') echo 'active'; ?> <?php if ($page_name == 'medicine_stock') echo 'active'; ?> <?php if ($page_name == 'medicine_sold') echo 'active'; ?>">
            <a href="<?php echo base_url(); ?>index.php?mmuadmin/medicine">
                <!-- <i class="fa fa-medkit"></i> -->
                <span><?php echo get_phrase('medicine'); ?></span>
            </a>
        </li>

        <li class="<?php if ($page_name == 'van_register' || $page_name == 'add_medicine_van' || $page_name == 'manage_diagnosis' || $page_name == 'edit_diagnosis') echo 'opened active'; ?> ">
            <a href="#">
                <!-- <i class="fa fa-ambulance"></i> -->
                <span><?php echo get_phrase('van'); ?></span>
            </a>
            <ul>
                <li class="<?php if ($page_name == 'van_register') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?mmuadmin/van_register">
                        <!-- <i class="fa fa-circle"></i> -->
                        <span><?php echo get_phrase('van_register'); ?></span>
                    </a>
                </li>

                <li class="<?php if ($page_name == 'add_medicine_van') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?mmuadmin/add_medicine_van">
                        <!-- <i class="fa fa-circle"></i> -->
                        <span><?php echo get_phrase('add_medicine_to_van'); ?></span>
                    </a>
                </li>

                <li class="<?php if ($page_name == 'manage_diagnosis' || $page_name == 'edit_diagnosis') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?mmuadmin/manage_diagnosis">
                        <!-- <i class="fa fa-circle"></i> -->
                        <span><?php echo get_phrase('manage_medicine_distribution'); ?></span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="<?php if ($page_name == 'manage_doctor') echo 'active'; ?> <?php if ($page_name == 'add_doc') echo 'active'; ?>">
            <a href="<?php echo base_url(); ?>index.php?mmuadmin/doctor">
                <!-- <i class="fa fa-user-md"></i> -->
                <span><?php echo get_phrase('doctors'); ?></span>
            </a>
        </li>

        <li class="<?php if ($page_name == 'spo') echo 'active'; ?> <?php if ($page_name == 'add_spo') echo 'active'; ?>">
            <a href="<?php echo base_url(); ?>index.php?mmuadmin/spo">
                <!-- <i class="fa fa-user-md"></i> -->
                <span><?php echo get_phrase('SPO'); ?></span>
            </a>
        </li>

        <li class="<?php if ($page_name == 'manage_pharmacist') echo 'active'; ?> <?php if ($page_name == 'add_pharmacist') echo 'active'; ?>">
            <a href="<?php echo base_url(); ?>index.php?mmuadmin/pharmacist">
                <!-- <i class="fa fa-medkit"></i> -->
                <span><?php echo get_phrase('pharmacist'); ?></span>
            </a>
        </li>

        <li class="<?php if ($page_name == 'manage_patient') echo 'active'; ?> <?php if ($page_name == 'add_patient') echo 'active'; ?>">
            <a href="<?php echo base_url(); ?>index.php?mmuadmin/patient">
                <!-- <i class="fa fa-medkit"></i> -->
                <span><?php echo get_phrase('Patient'); ?></span>
            </a>
        </li>

        <li class="<?php if ($page_name == 'find_patient') echo 'active'; ?> <?php if ($page_name == 'find_patient') echo 'active'; ?>">
            <a href="<?php echo base_url(); ?>index.php?mmuadmin/find_patient">
                <span><?php echo get_phrase('Find Patient'); ?></span>
            </a>
        </li>

        <li class="<?php if ($page_name == 'news') echo 'active'; ?> <?php if ($page_name == 'news') echo 'active'; ?>">
            <a href="<?php echo base_url(); ?>index.php?mmuadmin/news">
                <span><?php echo get_phrase('News'); ?></span>
            </a>
        </li>

        <li class="<?php if ($page_name == 'add_treatment') echo 'active'; ?> <?php if ($page_name == 'add_treatment') echo 'active'; ?>">
            <a href="<?php echo base_url(); ?>index.php?mmuadmin/find_pat_fortreatment">
                <span><?php echo get_phrase('add_treatment'); ?></span>
            </a>
        </li>

        <li>
            <a href="<?php echo base_url(); ?>index.php?login/logout">
                <!-- <i class="entypo-logout"></i> -->
                <span><?php echo get_phrase('log_out'); ?></span>
            </a>
        </li>
        

    </ul>

</div>