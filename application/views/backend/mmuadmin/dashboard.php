<style type="text/css">
    html,body
    {
        overflow-x: hidden;
    }

    #table-2
    {
        padding-top:100px;
    }
</style>
<div class="row">
    <!-- CALENDAR-->
    <!-- <div class="col-md-12 col-xs-12">    
        <div class="panel panel-primary " data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="fa fa-calendar"></i>
                    <?php echo get_phrase('event_schedule'); ?>
                </div>
            </div>
            <div class="panel-body" style="padding:0px;">
                <div class="calendar-env">
                    <div class="calendar-body">
                        <div id="notice_calendar"></div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <h3 style="color: white;"><?php echo get_phrase('Today patient'); ?></h3>
                </div>
            </div>

            <div class="panel-body">
                <form class="form-inline" id="filter">
                    <div class="form-group col-md-4">
                        <label>MMU ID</label>
                        <input type="text" name="mmu_id" class="form-control" readonly="" required="" value="<?php echo $mnumber;?>">
                    </div>

                    <div class="form-group col-md-4">
                        <label>Current MMU Site</label>
                        <select class="form-control" name="mmu_site" id="mmu_site">
                            <option value="">select</option>
                            <?php foreach ($m_loc as $mloc) {
                                        echo '<option value="'.$mloc['mmuloc_id'].'" '.set_select('m_loc', $mloc['mmuloc_id']).'>'.$mloc['mmuloc_name'].'</option>';
                                    } ?>
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label>Default Doctor</label>
                        <select class="form-control" name="doctor" id="doctor">
                        </select>
                    </div>


                </form>
            </div>
        </div>

        <table class="table table-bordered table-striped datatable" id="table-2">
            <thead>
                <tr>
                    <th>Patient Name</th>
                    <th>Patient ID</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody id="data"> 
                    
            </tbody>
        </table>  

        <div class="container">
            <h1 id="text"></h1>
        </div>     

    </div>
</div>


<script type="text/javascript">
    jQuery(window).load(function ()
    {
        var $ = jQuery;

        $("#table-2").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });

        // Highlighted rows
        $("#table-1 tbody input[type=checkbox]").each(function (i, el)
        {
            var $this = $(el),
                    $p = $this.closest('tr');

            $(el).on('change', function ()
            {
                var is_checked = $this.is(':checked');

                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
            });
        });

        // Replace Checboxes
        $(".pagination a").click(function (ev)
        {
            replaceCheckboxes();
        });
    });
</script>



<!-- <script type="text/javascript">
    
    $(document).ready(function()
    {
        var calendar = $('#notice_calendar');
				
        $('#notice_calendar').fullCalendar
        ({
            header:
            {
                left: 'title',
                right: 'month,agendaWeek,agendaDay today prev,next'
            },

            editable: false,
            firstDay: 1,
            height: 530,
            droppable: false,

            events:
            [
                <?php
                $notices   = $this->db->get('notice')->result_array();
                foreach ($notices as $row):
                ?>
                    {
                        title   :   "<?php echo $title = $this->db->get_where('notice' , 
                                        array('notice_id' => $row['notice_id'] ))->row()->title;?>",
                        start   :   new Date(<?php echo date('Y', $row['start_timestamp']); ?>, 
                                        <?php echo date('m', $row['start_timestamp']) - 1; ?>, 
                                        <?php echo date('d', $row['start_timestamp']); ?>),
                        end     :   new Date(<?php echo date('Y', $row['end_timestamp']); ?>, 
                                        <?php echo date('m', $row['end_timestamp']) - 1; ?>, 
                                        <?php echo date('d', $row['end_timestamp']); ?>),
                        allDay: true
                    },
                <?php endforeach ?>
            ]
        });
    });
</script> -->

<script type="text/javascript">
    $('#mmu_site').on('change', function() {
    var mmu_site=this.value;
        $.ajax({
            url: "<?php echo base_url();?>index.php?mmuadmin/filter_doctor",
            type: "POST",
            dataType: 'json',
            data: "mmu_site="+mmu_site,
            success: function(response)
            {
             if (response.doctor.length != 0){   
                   $('#doctor').html("<option>"+response.doctor[0].name+"</option>");
                }
                else{   
                   $('#doctor').html("<option>None</option>");
                }

                
                /*alert(response.patient[0]['name']);
*/
                if(response.patient.length == 0) {
                    $('#text').html('No results Found');
                    }else{
                        var HTML = '';
                        response.patient.forEach(function(contextValue, index) {
                            HTML += '<tr>'
                                        +'<td>'+contextValue.name
                                        +'</td>'
                                        +'<td>'+contextValue.patient_id
                                        +'</td>'
                                        +'<td>'
                                            +'<a href="<?php echo base_url(); ?>index.php?/mmuadmin/view_treatment/'+contextValue.patient_id+'" class="btn btn-primary btn-sm">View</a> '
                                            +'<a href="<?php echo base_url(); ?>index.php?/mmuadmin/edit_patient/'+contextValue.patient_id+'" class="btn btn-primary btn-sm">Edit</a> '
                                            +'<a href="<?php echo base_url(); ?>index.php?/mmuadmin/add_treatment/'+contextValue.patient_id+'" class="btn btn-primary btn-sm">Add</a> '
                                        +'</td>'
                                    +'</tr>'
                                  

                });
                $('#data').html(HTML);      
             }
            }
        });
    })
</script>