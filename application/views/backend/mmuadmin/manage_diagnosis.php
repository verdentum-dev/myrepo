<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    <h3><?php echo get_phrase('Search By'); ?></h3>
                </div>
            </div>

            <div class="panel-body text-center">
                <form class="form-inline" id="filter">
                    <!-- <div class="form-group">
                        <label class="" for="orderBy">Location</label>
                        <select class="form-control" name="location" id="location" required>
                        <option value="">Select Location</option>
                        <?php foreach ($alllocation as $location) { ?>
                            <option value="<?php echo $location['mmuloc_id']; ?>"><?php echo $location['mmuloc_name']; ?></option>
                        <?php } ?>
                        </select>
                    </div> -->
                    <div class="form-group">
                        <label class="" for="startDate">Date</label>
                        <input type="text" placeholder="select date" class="form-control datepicker" id="date" name="date" required>
                    </div>
                    <button type="submit" class="btn btn-default">Search</button>
                </form>
            </div>

        </div>

        <div id="table" class="table-responsive"></div>
        <!-- <table class="table table-bordered">
    <thead>
      <tr>
        <th>Full Name</th>
        <th>Date</th>
        <th>Date</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table> -->

    </div>
</div>

<script type="text/javascript">
    $(function(){
        $('#date').datepicker({
            format: 'yyyy-mm-dd'
        });

        $("#filter").on("submit", function(ev)
        {
            ev.preventDefault();
            var formData = {
                //'location': $('#location').val(),
                'date': $('#date').val() 
            };
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php?mmuadmin/diagnosis",
                data: formData,
                dataType:'json',
                success: function(data) {
                    if(data.patients.length == 0){
                        var table = '<table class="table table-bordered">\
                            <thead>\
                              <tr>\
                                <th>Patient Id</th>\
                                <th>Patient Name</th>\
                                <th>Date</th>\
                                <th>Medicine</th>\
                              </tr>\
                            </thead>\
                            <tbody>\
                                <tr>\
                                <td colspan="4">No Record Found</td>\
                                </tr>\
                            </tbody>\
                        </table>'; 
                    }
                    else{
                        var table = '<table class="table table-bordered">\
                            <thead>\
                              <tr>\
                                <th>Patient Id</th>\
                                <th>Patient Name</th>\
                                <th>Date</th>\
                                <th>Medicine</th>\
                              </tr>\
                            </thead>\
                            <tbody>';
                            data.patients.map(function(value, index) {
                                table += '<tr><td>'+value.patient_id+'</td>\
                                <td>'+value.name+'</td>\
                                <td>'+value.date+'</td>\
                                <td>';
                                if(value.medicines.length === 0) {
                                    table += 'No Medicines Found';
                                } else {
                                    value.medicines.map(function(med, key) {
                                        table += '<a href="<?php echo base_url(); ?>index.php?/mmuadmin/edit_diagnosis/'+med.tm_id+'">'+med.name+' ('+med.quantity+'Nos)</a><br/>';
                                    });
                                }
                                table += '</td></tr>';
                            });
                        table += '</tbody>\
                        </table>';
                    }
                    $('#table').html(table);
                }
            });
        });
    }) 
</script>