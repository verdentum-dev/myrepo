<!DOCTYPE html>
<html >
<head>
 <?php
        $system_name = $this->db->get_where('settings', array('type' => 'system_name'))->row()->description;
        $system_title = $this->db->get_where('settings', array('type' => 'system_title'))->row()->description;
        ?>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="author" content="Verdentum Dev Team" />

<!-- Page Title -->
<title><?php echo get_phrase('login'); ?> | <?php echo $system_title; ?></title>
<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Open+Sans'>
<link rel="stylesheet" href="<?php echo base_url();?>include/css/style.css">
<link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
<link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
<link rel="stylesheet" href="assets/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/neon-core.css">
<link rel="stylesheet" href="assets/css/neon-theme.css">
<link rel="stylesheet" href="assets/css/neon-forms.css">
<link rel="stylesheet" href="assets/css/custom.css">

        <script src="assets/js/jquery-1.11.0.min.js"></script>
</head>

<body>
<!-- This is needed when you send requests via Ajax -->
        <script type="text/javascript">
            var baseurl = '<?php echo base_url(); ?>';
        </script>

  <div class="cont" style="background-color: #262626;">
  <div class="demo" style="background-color:transparent;">
    <div class="login">
      <div class="logo_div">
      <!-- <a href="<?php echo base_url(); ?>" class="logo">
                        <img src="assets/images/logo.png" height="35" alt="" />
                    </a> -->
       <p class="description" style="color:#FFFFFF;margin:20px 0;">
            <img src="assets/images/logo-verd.png" style="color:#FFFFFF;vertical-align:top;" alt="Verdentum" height="24">
            <span style="color:#807F6E;display:inline-block;padding-top:2px;margin-left:5px;font-size:20px;">Professional</span>
          </p>
          <!-- <p class="text-center" style="margin:0;margin-top:20px;color:#FFFFFF;font-size:14px;">Brooke</p> -->
       <!--   <img src="<?php echo base_url();?>include_brooke/images/brooke-logo.png" class="blogo"> -->
      </div>

<!-- 
        <?php if(isset($username)) { ?>
      <div style="height:100%; font-size: 12px; text-align: center; padding: 40px 0px;">
      <h4 style="color:#FFFFFF; font-size:15px;">Welcome <?php echo $username; ?></h4>
      <div class="fleft" style="padding-top:15px; color:#FFFFFF">
        Ready to bring a change in society?
      </div>

      <div class="fleft" style="padding-top:15px; color:#FFFFFF">
        Checkout <a href="<?php if(isset($userid) && $userid != '') {
          if($role == 1) {
            echo base_url().'individual/viewprograms/';
          } else {
            echo base_url().'moderator/viewprograms/';
          }
        } else { echo base_url().'policymaker/home/'; } ?>" style="color:#B9F674">Your Dashboard</a> to get started.
      </div>
      <div class="fright"><a href="<?php echo base_url(); ?>auth/logout/" style="color:#CCCCCC; font-size: 15px;">Logout</a></div>
    </div>
    <?php }
    else { ?> -->

    <style type="text/css">
      .error
      {
        color: red;
        font-size: 10px;
      }
    </style>
    <form method="post" role="form" id="form_login">
      <div class="login__form">
        <div class="login__row">
          <input type="text" class="login__input name" id="email" name="email" placeholder="Email-Id" value="<?php echo set_value('email'); ?>" />
          <?php echo form_error('email'); ?>
        </div>
        <div class="login__row">
          <input type="password" class="login__input pass" type="password" id="password" name="password" placeholder="Password" value="<?php echo set_value('password'); ?>"/>
           <?php echo form_error('password'); ?>
        </div>
        <button type="submit" class="login_btn" onclick="verify();">Login</button>
        <p class="login__signup"><a href="#"> Forgot Password </a></p>
      </div>
      </form>
       <?php } ?>

    </div>
  </div>
</div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

    <script src="<?php echo base_url();?>include/js/index.js"></script>

  <script>
function isEmail( string ) {    
  if(string.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1)
  return true; else return false; 
}

$(function() {
  $('#login_form').submit(function(ev) {
    $('#email').next('p').remove();
    $('#password').next('p').remove();
    if(this.email.value === "") {
      ev.preventDefault();
      $('#email').after("<p style='color:red;font-weight:600;font-size:12px;'>Please Enter E-Mail ID or Username!</p>");
      this.email.focus();
    }
    /*else if(isEmail(this.email.value)==false) {
      ev.preventDefault();
      $('#email').after("<p style='color:#FFFFFF;font-weight:600;'>Please Enter a Valid E-Mail ID !</p>");
      this.email.focus();
    }*/
    else if(this.password.value === "") {
      ev.preventDefault();
      $('#password').after("<p style='color:red;font-weight:600;font-size:12px;'>Please Enter Password</p>");
      this.password.focus();
    }
  });
});


function forgotsubmit(){ 
var d = document.forgot_form;
if(d.email.value == ""){alert("please enter username"); d.email.focus(); return false;}
if(isEmail(d.email.value)==false){alert("Please Enter a Valid E-Mail ID !"); d.email.focus(); return false;}
document.forgot_form.submit();
}

    </script>
      <script src="assets/js/gsap/main-gsap.js"></script>
        <script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
        <script src="assets/js/bootstrap.js"></script>
        <script src="assets/js/joinable.js"></script>
        <script src="assets/js/resizeable.js"></script>
        <script src="assets/js/neon-api.js"></script>
        <script src="assets/js/jquery.validate.min.js"></script>
        <script src="assets/js/neon-login.js"></script>
        <script src="assets/js/neon-custom.js"></script>
        <script src="assets/js/neon-demo.js"></script>

</body>
</html>
