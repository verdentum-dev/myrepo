<?php $medicine_category_info = $this->db->get('medicine_category')->result_array(); ?>
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" data-collapsed="0">

            <div class="panel-heading">
                <div class="panel-title">
                    <h3><?php echo get_phrase('add_medicine_to_van'); ?></h3>
                </div>
            </div>

            <div class="panel-body">

                <form role="form" class="form-horizontal form-groups-bordered" id="medicineForm" method="post">
                    
                    <div class="row" id="medTemplate" style="margin-bottom:20px;border-bottom:1px solid #949494;padding-bottom:20px;">
                        <div class="row col-xs-11">
                            <div class="col-md-6 name">
                                <label for="field-name" class="labelname control-label"><?php echo get_phrase('name'); ?></label>
                                <input type="text" class="form-control" name="name[]">
                                <p class="error err-med"></p>
                            </div>

                            <div class="col-md-3">
                                <label for="field-preparation" class="control-label"><?php echo get_phrase('batch_no'); ?></label>
                                <select name="batch[]" class="form-control">
                                    <option value="">Select Batch</option>
                                </select>
                                <p class="error err-batch"></p>
                            </div>

                            <!-- <div class="col-md-3">
                                <label for="field-preparation" class="labelname control-label"><?php echo get_phrase('mmu_location'); ?></label>
                                <select name="mmu[]" class="form-control">
                                    <option value="">Select mmu location</option><?php
                                    foreach ($mmu_location as $ml) { ?>
                                        <option value="<?php echo $ml['mmuloc_id'] ?>"><?php echo $ml['mmuloc_name']; ?></option><?php
                                    } ?>
                                </select>
                                <p class="error err-mmu"></p>
                            </div> -->

                            <div class="col-md-3">
                                <label for="field-preparation" class="labelname control-label"><?php echo get_phrase('units'); ?></label>
                                <input type="text" class="form-control" name="requiredunitcount[]">
                                <p class="error err-req"></p>
                            </div>

                            <div class="clearfix"></div>
                        
                            <div class="col-md-6">
                                <label class="labelname control-label"><?php echo get_phrase('available_units'); ?></label>
                                <div class="form-control available"></div>
                            </div>

                            <div class="col-md-6">
                                <label class="labelname control-label"><?php echo get_phrase('exp_date'); ?></label>
                                <div class="form-control expiry"></div>
                            </div>
                        </div>

                        <div class="col-xs-1 text-center"><br/><br/><br/>
                            <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    
                    <div class="text-center">
                        <input type="submit" name="submit" class="btn btn-success" value="Submit">
                    </div>
                </form>

            </div>

        </div>

    </div>
</div>

<!-- Page Script -->
<script type="text/javascript">
var element, val;
$(function(){
    $('#medicineForm')
    // Add button click handler
    .on('click', '.addButton', function() {
        var $template = $('#medTemplate'),
            $clone    = $template
                        .clone()
                        .removeAttr('id');
        $clone.find('.addButton').removeClass('addButton').addClass('removeButton').html('<i class="fa fa-minus"></i>');
        //$clone.find('[name="mmu[]"]').val('');
        $clone.find('[name="requiredunitcount[]"]').val('');
        $clone.find('.available').html('');
        $clone.find('.expiry').html('');
        $clone.find("[name='name[]']").select2("destroy");
        $clone.find("[name='batch[]']").html('<option value="">Select Batch</option>')
        $clone.find('.name').html(
            '<label for="field-name" class="labelname control-label">Name</label>\
            <input type="text" class="form-control" name="name[]">\
            <p class="error err-med"></p>'
        );
        $('[type="submit"]').parent().before($clone);
        initializeSelect2($clone.find("[name='name[]']"));
    })
    // Remove button click handler
    .on('click', '.removeButton', function() {
        $(this).closest('.row').find("[name='name[]']").select2("val", null)
        $(this).closest('.row').find("[name='name[]']").select2("destroy");
        $(this).closest('.row').remove();
    });

    /*$("body").on("keypress", "[name='name[]']", function(e) {
        var elem = $(this);
        elem.autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "<?php echo base_url(); ?>index.php?pharmacist/get_medicine",
                    dataType: "json",
                    type: "post",
                    data: {
                        term: elem.val()
                    },
                    success: function( data ) {
                        var result = [];
                        data.map(function(value, index) {
                            result.push({
                                id: value.medicine_id,
                                value: value.name,
                            });                           
                        });
                        response( result );
                    }
                });
            },
            minLength: 1,
            select: function( event, ui ) {
                $( "[name='id[]']" ).val( ui.item.id );
                //elem.closest('row').find("[name='id[]']").val(ui.item.id);
            }
        });
    });

    $("body").on("blur", "[name='name[]']", function(e) {
        var elem = $(this),
            medicineid = elem.closest('.row').find("[name='id[]']").val();

        $.ajax({
            url: "index.php?pharmacist/medicinedetails",
            type: "post",
            data: { 
                medicineid: medicineid 
            },
            success: function(response) {
                if (!$.trim(response)){   
                    elem.closest('.row').find('[name="batch[]"]').html("<option>Batch Not Found</option>");
                }
                else{
                    elem.closest('.row').find('[name="batch[]"]').html(response);
                }
            }, error: function(error) {
                console.error(error);
            }
        })
    });*/

    initializeSelect2($("[name='name[]']"));

    $("body").on("change", "[name='batch[]']", function(e) {
        var elem = $(this),
            medicineid = elem.closest('.row').find("[name='name[]']").select2("val")[0],
            batch = elem.val();
        $.ajax({
            url: "index.php?pharmacist/getmedicineunitcount",
            type: "post",
            dataType: "json",
            data: {
                medicineid: medicineid,
                batch: batch
            },
            success: function(data) {
                elem.closest('.row').find(".available").html(data.maincount)
                elem.closest('.row').find(".expiry").html(data.exp_date);
            }, error: function(error) {
                console.error(error);
            }
        });
    });

    $('#medicineForm').on('submit', function(event) {
        $('.error').html('');

        $("[name='name[]']").each(function(ev) {
            var value = $(this).select2("val")[0];
            if(typeof value === 'undefined' || value.length == 0) {
                $(this).parent().find('.error').html('Medicine name is mandatory.');
                event.preventDefault();
            }
        });

        $("[name='batch[]']").each(function(ev) {
            var value = $(this).val();
            if(typeof value === 'undefined' || value.length == 0) {
                $(this).parent().find('.error').html('Batch number is mandatory.');
                event.preventDefault();
            }
        });

        /*$("[name='mmu[]']").each(function(ev) {
            var value = $(this).val();
            if(typeof value === 'undefined' || value.length == 0) {
                $(this).parent().find('.error').html('MMU location is mandatory.');
                event.preventDefault();
            }
        });*/

        $("[name='requiredunitcount[]']").each(function(ev) {
            var value = $(this).val();
            var reg = /^\d+$/;
            var available = parseInt($(this).closest('.row').find('.available').html());

            if(typeof value === 'undefined' || value.length == 0) {
                $(this).parent().find('.error').html('Required unit is mandatory.');
                event.preventDefault();
            } else if(!reg.test(value)) {
                $(this).parent().find('.error').html('Required unit can contain only digits.');
                event.preventDefault();
            } else if(parseInt(value) < 1) {
                $(this).parent().find('.error').html('Required unit must be greater than 0.');
                event.preventDefault();
            } else if(value.length > 7) {
                $(this).parent().find('.error').html('Required unit must be within 7 digits.');
                event.preventDefault();
            } else if(parseInt(value) > available) {
                $(this).parent().find('.error').html('Required unit should be less than available units.');
                event.preventDefault();
            }
        });
    });
});

//function to initialize select2 AJAX
function initializeSelect2(selectElementObj) {
    var all = [];
    selectElementObj.select2({
        placeholder: 'Search Medicine Name',
        allowClear: false,
        tags: true,
        tokenSeparators: [","],
        ajax: {
            url: "<?php echo base_url(); ?>index.php?pharmacist/get_medicine",
            dataType: 'json',
            type: 'POST',
            delay: 250,
            data: function (term, page) {
                $('[name="name[]"]').each(function(elem) {
                    var value = $(this).select2("val")[0];
                    if(typeof value !== 'undefined' && value.length > 0) {
                        console.log(value);
                        all.push(parseInt(value));
                    }
                });

                return {
                    q: term, // search term
                    meds: all, //All selected meds
                    page: page ? page : 1
                };
            },
            results: function (data, page) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                lastResults = data.items;

                return {
                    results: data.items,
                    pagination: {
                        more: data.remaining > 0
                    }
                };
            }
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 1,
        maximumSelectionSize: 1,
        formatResult: formatRepo,
        formatSelection: formatRepoSelection
    }).on("select2-removed", function(e) {
        selectElementObj.closest('.row').find('.available, .expiry').html('');
        selectElementObj.closest('.row').find('[name="batch[]"]').val('').trigger('change');
        selectElementObj.closest('.row').find('[name="batch[]"]').html('<option value="">Select Batch</option>');
    }).on("select2-selecting", function(e) {
        element = selectElementObj;
        val = e.val;
    });
}

function formatRepo (repo) {
    if (repo.loading) return repo.text;

    var markup = "<div class='p-10'>" +
        repo.name+"<br/>"+
        "<small>" + repo.preparation + "</small><br/>"+
        "<small>Composition: " + repo.composition + "</small>"+
    "</div>";

    return markup;
}

function formatRepoSelection (repo) {
    element.closest('.row').find('.available, .expiry').html('');
    if(repo.name) {
        $.ajax({
            url: "<?php echo base_url(); ?>index.php?pharmacist/medicinedetails",
            type: "POST",
            dataType: "json",
            data: { 
                medicineid: val
            },
            success: function(response) {
                if (response.medicine.length === 0){
                    element.closest('.row').find('[name="batch[]"]').html("<option value=''>No Batch Found</option>");
                }
                else{
                    var HTML = '<option value="">Select Batch</option>';
                    response.medicine.map(function(value, index) {
                        HTML += '<option value="'+value.batch_no+'">'+value.batch_no+'</option>';
                    });
                    element.closest('.row').find('[name="batch[]"]').html(HTML);
                }
            }, error: function(error) {
                console.error(error);
            }
        })
    }
    return repo.name ? repo.name : repo.text;
}
</script>

<!-- <link href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" crossorigin="anonymous" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="></script> -->

<style type="text/css">
    .labelname{
        text-align: left !important;
    }
    .select2-choices{
        padding: 0 !important;
    }
</style>