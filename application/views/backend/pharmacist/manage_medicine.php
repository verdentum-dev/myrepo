<!-- <button onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/add_medicine/');" 
    class="btn btn-primary pull-right">
        <?php echo get_phrase('add_medicine'); ?>
</button> -->
<div style="clear:both;"></div>
<br>

<table class="table table-bordered table-striped datatable" id="table-2">
    <thead>
        <tr>
            <!-- <th><input type="checkbox" id="selectall"/></th> -->
            <th><?php echo get_phrase('name'); ?></th>
            <th><?php echo get_phrase('Total medicine available in medical store'); ?></th>
            <th><?php echo get_phrase('Total Medicines Distributed In Van'); ?></th>
            <!-- <th><?php echo get_phrase('manufacturing_company'); ?></th> -->
            <!-- <th><?php echo get_phrase('status'); ?></th>
            <th><?php echo get_phrase('options'); ?></th> -->
        </tr>
    </thead>

    <tbody>
        <?php foreach ($count as $row) { ?>   
            <tr>
                <!-- <td><input class="checkbox1" type="checkbox" name="check[]" value="item2"></td> -->
                <td><?php echo $row['name'] ?>
                    <div class = "hide content">
                        <div class = row>
                            <div class = "col-md-12">
                                <div class="form-group">
                                    <label for="field-composition" class="col-sm-5 control-label"><?php echo get_phrase('Quatity'); ?></label>

                                    <div class="col-sm-7">
                                        <input class="form-control" id="field-name" name="name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-composition" class="col-sm-5 control-label"><?php echo get_phrase('Batch'); ?></label>

                                    <div class="col-sm-7">
                                        <input class="form-control" id="field-name" name="name" required>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label for="field-composition" class="col-sm-5 control-label"><?php echo get_phrase('Exp Date'); ?></label>

                                    <div class="col-sm-7">
                                        <input class="form-control" id="field-name" name="name" required>
                                    </div>
                                </div>                               
                            </div>
                            
                        </div>
                    </div>
                </td>
                <td><?php 
                    /*$name = $this->db->get_where('medicine_category' , array('medicine_category_id' => $row['medicine_category_id'] ))->row()->name;
                        echo $name;*/ 
                    echo $row['noofunits']['noofunits'];
                    ?>
                </td>
                <td><?php 
                    echo $row['noofunitsinvan']['noofunitsinvan'];
                    ?></td>
                <!-- <td><?php echo $row['manufacturing_company'] ?></td> -->
                <!-- <td><?php echo $row['status'] ?></td>
                <td>
                    <a  onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/edit_medicine/<?php echo $row['medicine_id'] ?>');" 
                        class="btn btn-default btn-sm btn-icon icon-left">
                        <i class="entypo-pencil"></i>
                        Edit
                    </a>
                    <a href="<?php echo base_url(); ?>index.php?pharmacist/medicine/delete/<?php echo $row['medicine_id'] ?>" 
                       class="btn btn-danger btn-sm btn-icon icon-left" onclick="return checkDelete();">
                        <i class="entypo-cancel"></i>
                        Delete
                    </a>
                    <button class = "more btn btn-info btn-xs">More..</button>
                </td> -->

            </tr>
        <?php } ?>
    </tbody>
</table>

<script type="text/javascript">
    jQuery(window).load(function ()
    {
        var $ = jQuery;

        $("#table-2").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });

        // Highlighted rows
        $("#table-2 tbody input[type=checkbox]").each(function (i, el)
        {
            var $this = $(el),
                    $p = $this.closest('tr');

            $(el).on('change', function ()
            {
                var is_checked = $this.is(':checked');

                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
            });
        });

        // Replace Checboxes
        $(".pagination a").click(function (ev)
        {
            replaceCheckboxes();
        });

        $(".more").on('click',function(){
            $('.checkbox1:checked').closest('tr').find(".content").removeClass('hide');
            //$('.more').hide();
            //$(this).hide();
            if($('.checkbox1:checked')){
                $('.more').hide();
            }
        });
        
    });

    $(document).ready(function() {
        $('#selectall').click(function(event) {  //on click 
            if(this.checked) { // check select status
                $('.checkbox1').each(function() { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"            
                });
            }else{
                $('.checkbox1').each(function() { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"  
                    $('.checkbox1:unchecked').closest('tr').find(".content").addClass('hide');
                    $('.more').show();             
                });         
            }
        });
    });
   
</script>
<style type="text/css">
    .trhide .hide{
        display:none;
    }
</style>