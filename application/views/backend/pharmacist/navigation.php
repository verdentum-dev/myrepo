<div class="sidebar-menu">
    <header class="logo-env" style="padding:12px 21px;">

        <!-- logo -->
        <div class="logo" style="">
            <a href="<?php echo base_url(); ?>">
                <img src="assets/images/logo.png" style="max-width:200px;margin-top:16px;">
            </a>
        </div>

        <!-- logo collapse icon -->
        <!-- <div class="sidebar-collapse" style="">
            <a href="#" class="sidebar-collapse-icon with-animation">

                <i class="entypo-menu"></i>
            </a>
        </div> -->

        <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
        <div class="sidebar-mobile-menu visible-xs">
            <a href="#" class="with-animation">
                <i class="entypo-menu"></i>
            </a>
        </div>
    </header>
    <p style="padding:5px 20px;color:#FFFFFF;background:#000000;margin:0;">
        <img src="assets/images/logo-verd.png" style="color:#FFFFFF;vertical-align:top;" alt="Verdentum" height="18">
        <span style="color:#807F6E;display:inline-block;padding-top:2px;margin-left:5px;">Professional</span>
    </p>
    <div class="sidebar-user-info">

        <div class="sui-normal">
                <?php   $image = $this->db->get_where('user_details', array('user_id' =>
                        $this->session->userdata('login_user_id')))->row()->profile_image; ?>
                <img src="<?php echo base_url();?>uploads/pharmacist_image/<?php echo $image; ?>" alt="" class="img-circle" style="height:44px; width:44px;">

                <span style="color: #ffffff;"><?php echo get_phrase('welcome'); ?>,</span><br>
                <strong style="color: #ffffff;"><?php 
                    echo $this->db->get_where('user_details', array('user_id' =>
                        $this->session->userdata('login_user_id')))->row()->name;
                    ?>
                </strong>
        </div>
        <!-- <div class="sui-normal">
            <a href="#" class="user-link">
            <?php $image = $this->db->get_where('user_details', array('user_id' =>
                        $this->session->userdata('login_user_id')))->row()->profile_image; ?>
                <img src="<?php echo base_url();?>uploads/pharmacist_image/<?php echo $image; ?>" alt="" class="img-circle" style="height:44px;">

                <span><?php echo get_phrase('welcome'); ?>,</span>
                <strong><?php
                    $this->db->get_where('user_details', array('user_id' =>
                        $this->session->userdata('login_user_id')))->row()->name;
                    ?>
                </strong>
            </a>
        </div> -->

        <div class="sui-hover inline-links animate-in"><!-- You can remove "inline-links" class to make links appear vertically, class "animate-in" will make A elements animateable when click on user profile -->				
            <a href="<?php echo base_url(); ?>index.php?<?php echo $account_type; ?>/manage_profile">
                <i class="entypo-pencil"></i>
                <?php echo get_phrase('edit_profile'); ?>
            </a>

            <a href="<?php echo base_url(); ?>index.php?<?php echo $account_type; ?>/manage_profile">
                <i class="entypo-lock"></i>
                <?php echo get_phrase('change_password'); ?>
            </a>

            <span class="close-sui-popup">×</span><!-- this is mandatory -->			
        </div>
    </div>


    <div style="border-top:1px solid rgba(69, 74, 84, 0.7);"></div>	
    <ul id="main-menu" class="">
        <!-- add class "multiple-expanded" to allow multiple submenus to open -->
        <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->

        <!-- DASHBOARD -->
        <li class="<?php if ($page_name == 'dashboard') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?pharmacist">
                <!-- <i class="fa fa-desktop"></i> -->
                <span><?php echo get_phrase('dashboard'); ?></span>
            </a>
        </li>
        
        <!-- <li class="<?php if ($page_name == 'manage_medicine_category') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?pharmacist/medicine_category">
                <i class="fa fa-edit"></i>
                <span><?php echo get_phrase('medicine_category'); ?></span>
            </a>
        </li> -->
        
        <!-- <li class="<?php if ($page_name == 'manage_medicine') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?pharmacist/medicine">
                <i class="fa fa-medkit"></i>
                <span><?php echo get_phrase('medicine'); ?></span>
            </a>
        </li> -->

        <li class="<?php if ($page_name == 'stock_registration' || $page_name == 'van_registration' || $page_name == 'add_medicine_van')
                        echo 'opened active'; ?> ">
            <a href="#">
                <!-- <i class="fa fa-globe"></i> -->
                <span><?php echo get_phrase('Medicine'); ?></span>
            </a>
            <ul>
                <li class="<?php if ($page_name == 'stock_registration') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?pharmacist/stock_registration">
                        <!-- <i class="fa fa-circle"></i> -->
                        <span><?php echo get_phrase('stock_register'); ?></span>
                    </a>
                </li>

                <li class="<?php if ($page_name == 'van_registration') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?pharmacist/van_registration">
                        <!-- <i class="fa fa-circle"></i> -->
                        <span><?php echo get_phrase('van_register'); ?></span>
                    </a>
                </li>

                <li class="<?php if ($page_name == 'add_medicine_van') echo 'active'; ?> ">
                    <a href="<?php echo base_url(); ?>index.php?pharmacist/add_medicine_van">
                        <!-- <i class="fa fa-circle"></i> -->
                        <span><?php echo get_phrase('add_medicine_van'); ?></span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="<?php if ($page_name == 'edit_profile') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?pharmacist/manage_diagnosis">
                <!-- <i class="fa fa-stethoscope"></i> -->
                <span><?php echo get_phrase('manage_medicine_distribution'); ?></span>
            </a>
        </li>
        
        <!-- <li class="<?php if ($page_name == 'edit_profile') echo 'active'; ?> ">
            <a href="<?php echo base_url(); ?>index.php?pharmacist/profile">
                <i class="entypo-lock"></i>
                <span><?php echo get_phrase('profile'); ?></span>
            </a>
        </li> -->

        <li>
            <a href="<?php echo base_url(); ?>index.php?login/logout">
                <!-- <i class="entypo-logout"></i> -->
                <span><?php echo get_phrase('log_out'); ?></span>
            </a>
        </li>

    </ul>

</div>