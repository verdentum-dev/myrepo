<?php $doctor_info = $this->db->get('doctor')->result_array(); ?>
<div style="clear:both;"></div>
<br>
<div class="row">
    <div class="col-md-12">

        <div class="panel panel-primary" data-collapsed="0">

        <form role="form" class="form-horizontal form-groups-bordered" action="<?php echo base_url(); ?>index.php?spo/patient/create" method="post" enctype="multipart/form-data">

            <div class="panel-heading">
                <div class="panel-title">
                    <h3><?php echo get_phrase('provide_patient_details_below'); ?></h3>
                </div>
            </div>

            <div class="panel-body">

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('name'); ?> <span style="color:red;">*</span></label>

                    <div class="col-sm-8">
                        <input type="text" name="name" class="form-control" id="field-1" >
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('birth_date'); ?> <span style="color:red;">*</span></label>

                    <div class="col-sm-8">
                        <input type="text" name="birth_date" class="form-control datepicker" id="field-1" >
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('age'); ?> <span style="color:red;">*</span></label>

                    <div class="col-sm-8">
                        <input type="number" name="age" class="form-control" id="field-1" >
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('gender'); ?> <span style="color:red;">*</span></label>

                    <div class="col-sm-8">
                        <select name="sex" class="form-control">
                            <option value=""><?php echo get_phrase('select_gender'); ?></option>
                            <option value="male"><?php echo get_phrase('male'); ?></option>
                            <option value="female"><?php echo get_phrase('female'); ?></option>
                            <option value="other"><?php echo get_phrase('other'); ?></option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('blood_group'); ?> <span style="color:red;">*</span></label>

                    <div class="col-sm-8">
                        <select name="blood_group" class="form-control">
                            <option value=""><?php echo get_phrase('select_blood_group'); ?></option>
                            <option value="A+">A+</option>
                            <option value="A-">A-</option>
                            <option value="B+">B+</option>
                            <option value="B-">B-</option>
                            <option value="AB+">AB+</option>
                            <option value="AB-">AB-</option>
                            <option value="O+">O+</option>
                            <option value="O-">O-</option>
                        </select>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('address'); ?> <span style="color:red;">*</span></label>

                    <div class="col-sm-8">
                        <textarea name="address" class="form-control" id="field-ta" style="resize:vertical;"></textarea>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('contact_number'); ?> <span style="color:red;">*</span></label>

                    <div class="col-sm-8">
                        <input type="text" name="phone" class="form-control" id="field-1" >
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('adhaar_no'); ?> <span style="color:red;">*</span></label>

                    <div class="col-sm-8">
                        <input type="text" name="phone" class="form-control" id="field-1" >
                    </div>
                </div>

                <!-- <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('email'); ?></label>

                    <div class="col-sm-8">
                        <input type="email" name="email" class="form-control" id="field-1" >
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('password'); ?></label>

                    <div class="col-sm-8">
                        <input type="password" name="password" class="form-control" id="field-1" >
                    </div>
                </div> -->

                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('family_status'); ?> <span style="color:red;">*</span></label>

                    <div class="col-sm-9">
                        <label class="col-sm-3" for="apl">
                            <input type="radio" name="family_status" id="apl" value="apl"> APL
                        </label>

                        <label class="col-sm-3" for="bpl">
                            <input type="radio" name="family_status" id="bpl" value="bpl"> BPL
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('education'); ?> <span style="color:red;">*</span></label>

                    <div class="col-sm-8">
                        <select name="education" class="form-control">
                            <option value=""><?php echo get_phrase('select_education_level'); ?></option>
                            <option value="illiterate"><?php echo get_phrase('illiterate'); ?></option>
                            <option value="literate"><?php echo get_phrase('literate'); ?></option>
                            <option value="matriculation"><?php echo get_phrase('matriculation'); ?></option>
                            <option value="intermediate"><?php echo get_phrase('intermediate'); ?></option>
                            <option value="diploma"><?php echo get_phrase('diploma'); ?></option>
                            <option value="graduate"><?php echo get_phrase('graduate'); ?></option>
                            <option value="post_graduate"><?php echo get_phrase('post_graduate'); ?></option>
                        </select>
                    </div>
                </div>


                <!-- Caregiver Information -->
                <div class="form-group">
                    <div class="col-sm-3"></div>

                    <label class="col-sm-8" for="careinfo">
                        <input type="checkbox" name="careinfo" id="careinfo"> Check this if caregiver information is available
                    </label>
                </div>

                <div class="form-group hidden" id="caregiver">
                    <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('caregiver_info'); ?></label>

                    <div class="col-sm-8" style="padding-top:10px;border:1px solid #949494;">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label" style="text-align:left !important;"><?php echo get_phrase('name'); ?> <span style="color:red;">*</span></label>

                            <div class="col-sm-9">
                                <input type="text" name="name" class="form-control" id="field-1" >
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label" style="text-align:left !important;"><?php echo get_phrase('age'); ?> <span style="color:red;">*</span></label>

                            <div class="col-sm-9">
                                <input type="number" name="age" class="form-control" id="field-1" >
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="field-ta" class="col-sm-3 control-label" style="text-align:left !important;"><?php echo get_phrase('gender'); ?> <span style="color:red;">*</span></label>

                            <div class="col-sm-9">
                                <select name="sex" class="form-control">
                                    <option value=""><?php echo get_phrase('select_gender'); ?></option>
                                    <option value="male"><?php echo get_phrase('male'); ?></option>
                                    <option value="female"><?php echo get_phrase('female'); ?></option>
                                    <option value="other"><?php echo get_phrase('other'); ?></option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="field-ta" class="col-sm-3 control-label" style="text-align:left !important;"><?php echo get_phrase('address'); ?> <span style="color:red;">*</span></label>

                            <div class="col-sm-9">
                                <textarea name="address" class="form-control" id="field-ta" style="resize:vertical;"></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label" style="text-align:left !important;"><?php echo get_phrase('contact_number'); ?> <span style="color:red;">*</span></label>

                            <div class="col-sm-9">
                                <input type="text" name="phone" class="form-control" id="field-1" >
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Caregiver Information End -->


                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('currently_suffering_from'); ?></label>

                    <div class="col-sm-8">
                        <select name="sex" class="form-control">
                            <option value=""><?php echo get_phrase('select_diseases'); ?></option>
                            <option value="diabetes"><?php echo get_phrase('diabetes'); ?></option>
                            <option value="hypertension"><?php echo get_phrase('hypertension'); ?></option>
                            <option value="tuberculosis"><?php echo get_phrase('tuberculosis'); ?></option>
                            <option value="cardiac_issues"><?php echo get_phrase('cardiac_issues'); ?></option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('disease_type'); ?></label>

                    <div class="col-sm-8">
                        <select name="sex" class="form-control">
                            <option value=""><?php echo get_phrase('select_disease_type'); ?></option>
                            <option value="chronic"><?php echo get_phrase('chronic'); ?></option>
                            <option value="common"><?php echo get_phrase('common'); ?></option>
                            <option value="serious"><?php echo get_phrase('serious'); ?></option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('past_history_of_the_patient'); ?></label>

                    <div class="col-sm-8" style="border:1px solid #949494;padding-top:10px;">
                        <div class="form-group">
                            <label for="field-ta" class="col-sm-3 control-label" style="text-align:left !important;"><?php echo get_phrase('suffered_from'); ?></label>

                            <div class="col-sm-9">
                                <select name="sex" class="form-control">
                                    <option value=""><?php echo get_phrase('select_diseases'); ?></option>
                                    <option value="diabetes"><?php echo get_phrase('diabetes'); ?></option>
                                    <option value="hypertension"><?php echo get_phrase('hypertension'); ?></option>
                                    <option value="tuberculosis"><?php echo get_phrase('tuberculosis'); ?></option>
                                    <option value="cardiac_issues"><?php echo get_phrase('cardiac_issues'); ?></option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="field-ta" class="col-sm-3 control-label" style="text-align:left !important;"><?php echo get_phrase('disease_type'); ?></label>

                            <div class="col-sm-9">
                                <select name="sex" class="form-control">
                                    <option value=""><?php echo get_phrase('select_disease_type'); ?></option>
                                    <option value="chronic"><?php echo get_phrase('chronic'); ?></option>
                                    <option value="common"><?php echo get_phrase('common'); ?></option>
                                    <option value="serious"><?php echo get_phrase('serious'); ?></option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="disability" class="col-sm-3 control-label"><?php echo get_phrase('type_of_disability'); ?></label>

                    <div class="col-sm-8">
                        <select name="disability" class="form-control select2" id="disability" multiple="">
                            <option value=""><?php echo get_phrase('select_disability_type'); ?></option>
                            <option value="blind"><?php echo get_phrase('blind'); ?></option>
                            <option value="hearing_impaired"><?php echo get_phrase('hearing_impaired'); ?></option>
                            <option value="speaking"><?php echo get_phrase('speaking'); ?></option>
                            <option value="hand"><?php echo get_phrase('hand'); ?></option>
                            <option value="leg"><?php echo get_phrase('leg'); ?></option>
                            <option value="mental_illness"><?php echo get_phrase('mental_illness'); ?></option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        <?php echo get_phrase('habits'); ?><br/>
                        (<a href="javascript:void(0);">Add More</a>)
                    </label>

                    <div class="col-sm-8" style="border:1px solid #949494;padding-top:10px;padding-right:10px;">
                        <div class="form-group">
                            <label class="col-sm-3" for="cigarette">
                                <input type="checkbox" name="habit[]" id="cigarette" value="cigarette"> <?php echo get_phrase('cigarette'); ?>
                            </label>

                            <div class="col-sm-3"><input type="text" name="year[]" class="form-control" placeholder="From Year"></div>

                            <div class="col-sm-3"><input type="text" name="month[]" class="form-control" placeholder="From Month"></div>

                            <div class="col-sm-3"><input type="text" name="day[]" class="form-control" placeholder="From day"></div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3" for="beedi">
                                <input type="checkbox" name="habit[]" id="beedi" value="beedi"> <?php echo get_phrase('beedi'); ?>
                            </label>

                            <div class="col-sm-3"><input type="text" name="year[]" class="form-control" placeholder="From Year"></div>

                            <div class="col-sm-3"><input type="text" name="month[]" class="form-control" placeholder="From Month"></div>

                            <div class="col-sm-3"><input type="text" name="day[]" class="form-control" placeholder="From day"></div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3" for="gutka">
                                <input type="checkbox" name="habit[]" id="gutka" value="gutka"> <?php echo get_phrase('gutka'); ?>
                            </label>

                            <div class="col-sm-3"><input type="text" name="year[]" class="form-control" placeholder="From Year"></div>

                            <div class="col-sm-3"><input type="text" name="month[]" class="form-control" placeholder="From Month"></div>

                            <div class="col-sm-3"><input type="text" name="day[]" class="form-control" placeholder="From day"></div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('bedridden'); ?></label>

                    <div class="col-sm-8" style="padding-top:10px;border:1px solid #949494;">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="text-align:left !important;"><?php echo get_phrase('suffering_from'); ?></label>

                            <div class="col-sm-9">
                                <select name="bedridden" class="form-control" id="bedridden">
                                    <option value=""><?php echo get_phrase('select_disease'); ?></option>
                                    <option value="none"><?php echo get_phrase('none'); ?></option>
                                    <option value="service"><?php echo get_phrase('service'); ?></option>
                                    <option value="business"><?php echo get_phrase('business'); ?></option>
                                    <option value="housewife"><?php echo get_phrase('housewife'); ?></option>
                                    <option value="daily_labor"><?php echo get_phrase('daily_labor'); ?></option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4"><input type="text" name="year[]" class="form-control" placeholder="From Year"></div>

                            <div class="col-sm-4"><input type="text" name="month[]" class="form-control" placeholder="From Month"></div>

                            <div class="col-sm-4"><input type="text" name="day[]" class="form-control" placeholder="From day"></div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="occupation" class="col-sm-3 control-label"><?php echo get_phrase('occupation'); ?></label>

                    <div class="col-sm-8">
                        <select name="occupation" class="form-control" id="occupation">
                            <option value=""><?php echo get_phrase('select_occupation'); ?></option>
                            <option value="none"><?php echo get_phrase('none'); ?></option>
                            <option value="service"><?php echo get_phrase('service'); ?></option>
                            <option value="business"><?php echo get_phrase('business'); ?></option>
                            <option value="housewife"><?php echo get_phrase('housewife'); ?></option>
                            <option value="daily_labor"><?php echo get_phrase('daily_labor'); ?></option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="income" class="col-sm-3 control-label"><?php echo get_phrase('yearly_income'); ?></label>

                    <div class="col-sm-8">
                        <input type="text" name="income" class="form-control" id="income" >
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('movable_assets'); ?></label>

                    <div class="col-sm-8">
                        <label class="col-sm-3" for="cattle">
                            <input type="checkbox" name="moveass[]" id="cattle" value="cattle"> <?php echo get_phrase('cattle'); ?>
                        </label>

                        <label class="col-sm-3" for="vehicle">
                            <input type="checkbox" name="moveass[]" id="vehicle" value="vehicle"> <?php echo get_phrase('vehicle'); ?>
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('immovable_assets'); ?></label>

                    <div class="col-sm-8">
                        <label class="col-sm-3" for="house">
                            <input type="checkbox" name="immoveass[]" id="house" value="house"> <?php echo get_phrase('house'); ?>
                        </label>

                        <label class="col-sm-3" for="land">
                            <input type="checkbox" name="immoveass[]" id="land" value="land"> <?php echo get_phrase('land'); ?>
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('ration_card'); ?></label>

                    <div class="col-sm-8">
                        <label class="col-sm-3" for="rationyes">
                            <input type="radio" name="ration" id="rationyes" value="yes"> <?php echo get_phrase('yes'); ?>
                        </label>

                        <label class="col-sm-3" for="rationno">
                            <input type="radio" name="ration" id="rationno" value="no"> <?php echo get_phrase('no'); ?>
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('insurance'); ?></label>

                    <div class="col-sm-8">
                        <label class="col-sm-3" for="insuranceyes">
                            <input type="radio" name="insurance" id="insuranceyes" value="yes"> <?php echo get_phrase('yes'); ?>
                        </label>

                        <label class="col-sm-3" for="insuranceno">
                            <input type="radio" name="insurance" id="insuranceno" value="no"> <?php echo get_phrase('no'); ?>
                        </label>

                        <div class="col-sm-6 hidden">
                            <input type="text" name="insurance_type" class="form-control" placeholder="<?php echo get_phrase('insurance_type'); ?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('pension'); ?></label>

                    <div class="col-sm-8">
                        <label class="col-sm-3" for="pensionyes">
                            <input type="radio" name="pension" id="pensionyes" value="yes"> <?php echo get_phrase('yes'); ?>
                        </label>

                        <label class="col-sm-3" for="pensionno">
                            <input type="radio" name="pension" id="pensionno" value="no"> <?php echo get_phrase('no'); ?>
                        </label>

                        <div class="col-sm-6 hidden">
                            <input type="text" name="pension_type" class="form-control" placeholder="<?php echo get_phrase('pension_type'); ?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('bank_account'); ?></label>

                    <div class="col-sm-8">
                        <label class="col-sm-3" for="bankyes">
                            <input type="radio" name="bank" id="bankyes" value="yes"> <?php echo get_phrase('yes'); ?>
                        </label>

                        <label class="col-sm-3" for="bankno">
                            <input type="radio" name="bank" id="bankno" value="no"> <?php echo get_phrase('no'); ?>
                        </label>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('image'); ?></label>

                    <div class="col-sm-8">

                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;" data-trigger="fileinput">
                                <img src="http://placehold.it/200x150" alt="...">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                            <div>
                                <span class="btn btn-white btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="file" name="image" accept="image/*">
                                </span>
                                <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <!-- <div class="panel-heading">
                <div class="panel-title">
                    <h3 class="text-center"><?php echo get_phrase('medication_details'); ?></h3>
                </div>
            </div>

            <div class="panel-body">

                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('doctor'); ?></label>

                    <div class="col-sm-7">
                        <select name="doctor_id" class="select2">
                            <option value=""><?php echo get_phrase('select_doctor'); ?></option>
                            <?php foreach ($doctor_info as $row) { ?>
                                    <option value="<?php echo $row['doctor_id']; ?>"><?php echo $row['name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('date'); ?></label>

                    <div class="col-sm-7">
                        <div class="date-and-time">
                            <input type="text" name="date_timestamp" class="form-control datepicker" data-format="D, dd MM yyyy" placeholder="date here">
                            <input type="text" name="time_timestamp" class="form-control timepicker" data-template="dropdown" data-show-seconds="false" data-default-time="00:05 AM" data-show-meridian="false" data-minute-step="5"  placeholder="time here">
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('case_history'); ?></label>

                    <div class="col-sm-9">
                        <textarea name="case_history" class="form-control html5editor" id="field-ta" data-stylesheet-url="assets/css/wysihtml5-color.css"></textarea>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('medication'); ?></label>

                    <div class="col-sm-9">
                        <textarea name="medication" class="form-control html5editor" id="field-ta" data-stylesheet-url="assets/css/wysihtml5-color.css"></textarea>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('note'); ?></label>

                    <div class="col-sm-9">
                        <textarea name="note" class="form-control html5editor" id="field-ta" data-stylesheet-url="assets/css/wysihtml5-color.css"></textarea>
                    </div>
                </div>

            </div> -->

            <div class="panel-body">
                <div class="col-sm-3 control-label col-sm-offset-2">
                    <input type="submit" class="btn btn-success" value="Submit">
                </div>
            </div>
                
        </form>                

        </div>

    </div>
</div>

<script type="text/javascript">
    jQuery(window).load(function (){
        var $ = jQuery;

        $('#careinfo').change(function() {
            if($('#careinfo').is(':checked')) {
                $('#caregiver').removeClass('hidden');
            } else {
                $('#caregiver').addClass('hidden');
            }
        });

        $('[name="insurance"]').change(function() {
            if($('[name="insurance"]:checked').val() == 'yes') {
                $('[name="insurance_type"]').parent().removeClass('hidden');
            } else {
                $('[name="insurance_type"]').parent().addClass('hidden');
            }
        });

        $('[name="pension"]').change(function() {
            if($('[name="pension"]:checked').val() == 'yes') {
                $('[name="pension_type"]').parent().removeClass('hidden');
            } else {
                $('[name="pension_type"]').parent().addClass('hidden');
            }
        });
    });
</script>