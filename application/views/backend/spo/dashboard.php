<div class="row">
<div class="col-lg-6">
          <!-- Panel Kitchen Sink -->
          <div class="panel">
            <header class="panel-heading">
              <h3 class="panel-title"><span class="panel-desc">
                  Today's Patient report
                </span>
              </h3>
            </header>

            <div class="panel-body">
              <table class="tablesaw table-striped table-bordered table-hover tablesaw-swipe tablesaw-sortable" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-4146" width="100%">
                <thead>
                  <tr>
                    <th data-tablesaw-sortable-col="" data-tablesaw-sortable-default-col="" data-tablesaw-priority="persist" class="tablesaw-cell-persist tablesaw-sortable-head tablesaw-sortable-ascending" data-sortable-numeric="false">Location</th>
                    <th data-tablesaw-sortable-col="" data-tablesaw-priority="3" class="tablesaw-sortable-head" data-sortable-numeric="">No of patients</th>
                    <th data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric="">Male</th>
                    <th data-tablesaw-sortable-col="" data-tablesaw-priority="1" class="tablesaw-sortable-head" data-sortable-numeric="">Female</th>
                    <th data-tablesaw-sortable-col="" data-tablesaw-priority="4" class="tablesaw-cell-hidden tablesaw-sortable-head" data-sortable-numeric=""></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">Agartala</a></td>
                    <td>30</td>
                    <td>10</td>
                    <td>20</td>
                    
                  </tr>
                  <tr>
                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">Allavaram</a></td>
                    <td>2</td>
                    <td>2</td>
                    <td>0</td>
                    
                  </tr>
                  <tr>
                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">Ankleshwar</a></td>
                    <td>3</td>
                    <td>2</td>
                    <td>1</td>
                    
                  </tr>
                  <tr>
                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">Bokaro</a></td>
                    <td>4</td>
                    <td>2</td>
                    <td>2</td>
                    
                  </tr>
                  <tr>
                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">Chidambaram</a></td>
                    <td>5</td>
                    <td>3</td>
                    <td>2</td>
                    
                  </tr>
                  <tr>
                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">Gellaky</a></td>
                    <td>6</td>
                    <td>3</td>
                    <td>3</td>
                    
                  </tr>
                  <tr>
                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">Hazira</a></td>
                    <td>7</td>
                    <td>3</td>
                    <td>4</td>
                    
                  </tr>
                  <tr>
                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">Jorhat</a></td>
                    <td>8</td>
                    <td>4</td>
                    <td>4</td>
                    
                  </tr>
                  <tr>
                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">Panvel</a></td>
                    <td>9</td>
                    <td>5</td>
                    <td>4</td>
                    
                  </tr>
                  <tr>
                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">Rudrasagar</a></td>
                    <td>10</td>
                    <td>5</td>
                    <td>5</td>
                    
                  </tr>
                  <tr>
                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">Vadodra</a></td>
                    <td>11</td>
                    <td>10</td>
                    <td>1</td>
                    
                  </tr>
                  
                  
                </tbody>
              </table>
            </div>
          </div>
          <!-- End Panel Kitchen Sink -->

         
          <!-- Panel Column Toggle -->
         
</div>

<div class="col-lg-6">
          <!-- Panel Kitchen Sink -->
          <div class="panel">
            <header class="panel-heading">
              <h3 class="panel-title"><span class="panel-desc">
                  Today's Medicine report
                </span>
              </h3>
            </header>

            <div class="panel-body">
              <table class="tablesaw table-striped table-bordered table-hover tablesaw-swipe tablesaw-sortable" data-tablesaw-mode="swipe" data-tablesaw-sortable="" data-tablesaw-sortable-switch="" data-tablesaw-minimap="" data-tablesaw-mode-switch="" id="table-4146" width="100%">
                <thead>
                  <tr>
                    <th data-tablesaw-sortable-col="" data-tablesaw-sortable-default-col="" data-tablesaw-priority="persist" class="tablesaw-cell-persist tablesaw-sortable-head tablesaw-sortable-ascending" data-sortable-numeric="false">Location</th>
                    <th data-tablesaw-sortable-col="" data-tablesaw-priority="3" class="tablesaw-sortable-head" data-sortable-numeric="">Prescribed</th>
                    <th data-tablesaw-sortable-col="" data-tablesaw-priority="2" class="tablesaw-sortable-head" data-sortable-numeric="">Distributed</th>
                    <th data-tablesaw-sortable-col="" data-tablesaw-priority="1" class="tablesaw-sortable-head" data-sortable-numeric="">Not Available</th>
                    
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">Agartala</a></td>
                    <td>30</td>
                    <td>10</td>
                    <td>20</td>
                    
                  </tr>
                  <tr>
                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">Allavaram</a></td>
                    <td>2</td>
                    <td>2</td>
                    <td>0</td>
                    
                  </tr>
                  <tr>
                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">Ankleshwar</a></td>
                    <td>3</td>
                    <td>2</td>
                    <td>1</td>
                    
                  </tr>
                  <tr>
                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">Bokaro</a></td>
                    <td>4</td>
                    <td>2</td>
                    <td>2</td>
                    
                  </tr>
                  <tr>
                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">Chidambaram</a></td>
                    <td>5</td>
                    <td>3</td>
                    <td>2</td>
                    
                  </tr>
                  <tr>
                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">Gellaky</a></td>
                    <td>6</td>
                    <td>3</td>
                    <td>3</td>
                    
                  </tr>
                  <tr>
                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">Hazira</a></td>
                    <td>7</td>
                    <td>3</td>
                    <td>4</td>
                    
                  </tr>
                  <tr>
                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">Jorhat</a></td>
                    <td>8</td>
                    <td>4</td>
                    <td>4</td>
                    
                  </tr>
                  <tr>
                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">Panvel</a></td>
                    <td>9</td>
                    <td>5</td>
                    <td>4</td>
                    
                  </tr>
                  <tr>
                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">Rudrasagar</a></td>
                    <td>10</td>
                    <td>5</td>
                    <td>5</td>
                    
                  </tr>
                  <tr>
                    <td class="tablesaw-cell-persist"><a href="javascript:void(0)">Vadodra</a></td>
                    <td>11</td>
                    <td>10</td>
                    <td>1</td>
                    
                  </tr>
                  
                  
                </tbody>
              </table>
            </div>
          </div>
          <!-- End Panel Kitchen Sink -->

         
          <!-- Panel Column Toggle -->
         
</div>


</div>

<img src="<?php echo base_url(); ?>uploads/dashboard.png" alt="dashboard" style="width:100%;">

<!-- <div class="row">
    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/doctor">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="icon"><i class="fa fa-user-md"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('doctor'); ?>"
                     data-duration="1500" data-delay="0">0</div>
                <h3><?php echo get_phrase('doctor'); if($this->db->count_all('doctor') > 1) echo 's'; ?></h3>
            </div>
        </a>
    </div>

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/patient">
            <div class="tile-stats tile-white-red">
                <div class="icon"><i class="fa fa-user"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('patient'); ?>" 
                     data-duration="1500" data-delay="0">0</div>
                <h3><?php echo get_phrase('patient'); if($this->db->count_all('patient') > 1) echo 's'; ?></h3>
            </div>
        </a>
    </div>

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/medicine">
            <div class="tile-stats tile-white-orange">
                <div class="icon"><i class="fa fa-medkit"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('medicine'); ?>" 
                     data-duration="1500" data-delay="0">0</div>
                <h3><?php echo get_phrase('medicine'); if($this->db->count_all('medicine') > 1) echo 's'; ?></h3>
            </div>
        </a>
    </div>

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/nurse">
            <div class="tile-stats tile-white-aqua">
                <div class="icon"><i class="fa fa-plus-square"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('nurse'); ?>" 
                     data-duration="1500" data-delay="0">0 &pound;</div>
                <h3><?php echo get_phrase('nurse') ?></h3>
            </div>
        </a>
    </div>

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/pharmacist">
            <div class="tile-stats tile-white-blue">
                <div class="icon"><i class="fa fa-medkit"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('pharmacist'); ?>" 
                     data-duration="1500" data-delay="0">0 &pound;</div>
                <h3><?php echo get_phrase('pharmacist') ?></h3>
            </div>
        </a>
    </div>
</div>

<br />

<div class="row">
    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/laboratorist">
            <div class="tile-stats tile-white-cyan">
                <div class="icon"><i class="fa fa-user"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('laboratorist'); ?>" 
                     data-duration="1500" data-delay="0">0 &pound;</div>
                <h3><?php echo get_phrase('laboratorist') ?></h3>
            </div>
        </a>
    </div>

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/accountant">
            <div class="tile-stats tile-white-purple">
                <div class="icon"><i class="fa fa-money"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('accountant'); ?>" 
                     data-duration="1500" data-delay="0">0 &pound;</div>
                <h3><?php echo get_phrase('accountant') ?></h3>
            </div>
        </a>
    </div>

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/payment_history">
            <div class="tile-stats tile-white-pink">
                <div class="icon"><i class="fa fa-list-alt"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('invoice'); ?>" 
                     data-duration="1500" data-delay="0">0 &pound;</div>
                <h3><?php echo get_phrase('payment') ?></h3>
            </div>
        </a>
    </div>

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/medicine">
            <div class="tile-stats tile-white-orange">
                <div class="icon"><i class="fa fa-medkit"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('medicine'); ?>" 
                     data-duration="1500" data-delay="0">0 &pound;</div>
                <h3><?php echo get_phrase('medicine') ?></h3>
            </div>
        </a>
    </div>
</div>

<br />

<div class="row">
    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/operation_report">
            <div class="tile-stats tile-white-green">
                <div class="icon"><i class="fa fa-wheelchair"></i></div>
                <div class="num" data-start="0" data-end="<?php echo count($this->db->get_where('report', array('type' => 'operation'))->result_array());?>" 
                     data-duration="1500" data-delay="0"></div>
                <h3><?php echo get_phrase('operation_report') ?></h3>
            </div>
        </a>
    </div>

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/birth_report">
            <div class="tile-stats tile-white-brown">
                <div class="icon"><i class="fa fa-github-alt"></i></div>
                <div class="num" data-start="0" data-end="<?php echo count($this->db->get_where('report', array('type' => 'birth'))->result_array());?>" 
                     data-duration="1500" data-delay="0"></div>
                <h3><?php echo get_phrase('birth_report') ?></h3>
            </div>
        </a>
    </div>

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/death_report">
            <div class="tile-stats tile-white-plum">
                <div class="icon"><i class="fa fa-ban"></i></div>
                <div class="num" data-start="0" data-end="<?php echo count($this->db->get_where('report', array('type' => 'death'))->result_array());?>" 
                     data-duration="1500" data-delay="0"></div>
                <h3><?php echo get_phrase('death_report') ?></h3>
            </div>
        </a>
    </div>

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/system_settings">
            <div class="tile-stats tile-white-gray">
                <div class="icon"><i class="fa fa-h-square"></i></div>
                <div class="num">&nbsp;</div>
                <h3><?php echo get_phrase('settings') ?></h3>
            </div>
        </a>
    </div>
</div> -->