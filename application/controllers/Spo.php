<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 	
 * 	@author : Joyonto Roy
 * 	date	: 1 August, 2014
 * 	http://codecanyon.net/user/Creativeitem
 * 	http://creativeitem.com
 */

class Spo extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');

        /* cache control */
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    /*     * *default function, redirects to login page if no spo logged in yet** */

    public function index() {
        if ($this->session->userdata('spo_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');
        if ($this->session->userdata('spo_login') == 1)
            redirect(base_url() . 'index.php?spo/patient', 'refresh');
    }

    /*     * *ADMIN DASHBOARD** */

    function dashboard() {
        if ($this->session->userdata('spo_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $page_data['page_name'] = 'dashboard';
        $page_data['page_title'] = 'SPO Dashboard';
        $this->load->view('backend/index', $page_data);
    }

    function patient($task = "", $patient_id = "") {
        if ($this->session->userdata('spo_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $email = $_POST['email'];
            $patient = $this->db->get_where('patient', array('email' => $email))->row()->name;
            if ($patient == null) {
                $this->crud_model->save_patient_info();
                $this->session->set_flashdata('message', get_phrase('patient_info_saved_successfuly'));
            } else {
                $this->session->set_flashdata('message', get_phrase('duplicate_email'));
            }
            redirect(base_url() . 'index.php?spo/patient');
        }

        if ($task == "update") {
                $this->crud_model->update_patient_info($patient_id);
                $this->session->set_flashdata('message', get_phrase('patient_info_updated_successfuly'));
                redirect(base_url() . 'index.php?spo/patient');
        }

        if ($task == "delete") {
            $this->crud_model->delete_patient_info($patient_id);
            redirect(base_url() . 'index.php?spo/patient');
        }

        $data['patient_info'] = $this->crud_model->select_patient_info();
        $data['page_name'] = 'manage_patient';
        $data['page_title'] = get_phrase('manage_patient');
        $this->load->view('backend/index', $data);
    }

    function find_patient($task = "", $patient_id = "") {
        if ($this->session->userdata('spo_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['patient_info'] = $this->crud_model->select_patient_info();
        $data['page_name'] = 'find_patient';
        $data['page_title'] = get_phrase('find_patient');
        $this->load->view('backend/index', $data);
    }

    function add_patient($task = "") {
        if ($this->session->userdata('spo_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $email = $_POST['email'];
            $patient = $this->db->get_where('patient', array('email' => $email))->row()->name;
            if ($patient == null) {
                $patient_id = $this->crud_model->save_patient_info();
                //$this->crud_model->save_prescription_info($patient_id);
                $this->session->set_flashdata('message', get_phrase('patient_info_saved_successfuly'));
            } else {
                $this->session->set_flashdata('message', get_phrase('duplicate_email'));
            }
            redirect(base_url() . 'index.php?spo/find_patient');
        }

        $data['page_name'] = 'add_patient';
        $data['page_title'] = get_phrase('register_patient');
        $this->load->view('backend/index', $data);
    }

    function patient_details($task = "") {
        if ($this->session->userdata('spo_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['page_name'] = 'patient_details';
        $data['page_title'] = get_phrase('patient_details');
        $this->load->view('backend/index', $data);
    }

    function checkup($task = "", $prescription_id = "", $menu_check = '', $patient_id = '') {
        if ($this->session->userdata('spo_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $this->crud_model->save_prescription_info();
            $this->session->set_flashdata('message', get_phrase('prescription_info_saved_successfuly'));
            redirect(base_url() . 'index.php?spo/checkup');
        }

        if ($task == "update") {
            $this->crud_model->update_prescription_info($prescription_id);
            $this->session->set_flashdata('message', get_phrase('prescription_info_updated_successfuly'));
            if ($menu_check == 'from_prescription')
                redirect(base_url() . 'index.php?spo/checkup');
            else
                redirect(base_url() . 'index.php?spo/medication_history/' . $patient_id);
        }

        if ($task == "delete") {
            $this->crud_model->delete_prescription_info($prescription_id);
            if ($menu_check == 'from_prescription')
                redirect(base_url() . 'index.php?spo/checkup');
            else
                redirect(base_url() . 'index.php?spo/medication_history/' . $patient_id);
        }

        $data['prescription_info'] = $this->crud_model->select_all_prescription_info();
        $data['menu_check'] = 'from_prescription';
        $data['page_name'] = 'checkup';
        $data['page_title'] = get_phrase('checkup');
        $this->load->view('backend/index', $data);
    }

    function checkup_report($task = "", $diagnosis_report_id = "") {
        if ($this->session->userdata('spo_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $this->crud_model->save_diagnosis_report_info();
            $this->session->set_flashdata('message', get_phrase('checkup_report_info_saved_successfuly'));
            redirect(base_url() . 'index.php?spo/checkup');
        }

        if ($task == "delete") {
            $this->crud_model->delete_diagnosis_report_info($diagnosis_report_id);
            $this->session->set_flashdata('message', get_phrase('checkup_report_info_deleted_successfuly'));
            redirect(base_url() . 'index.php?spo/checkup');
        }
    }

    function add_checkup($task = "", $diagnosis_report_id = "") {
        if ($this->session->userdata('spo_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $this->crud_model->save_diagnosis_report_info();
            $this->session->set_flashdata('message', get_phrase('checkup_report_info_saved_successfuly'));
            redirect(base_url() . 'index.php?spo/patient_details');
        }

        if ($task == "new") {
            $data['show_patient']  = true;
        }

        $data['page_name']  = 'add_checkup';
        $data['page_title'] = get_phrase('add_checkup');
        $this->load->view('backend/index', $data);
    }

}
