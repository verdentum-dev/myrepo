<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pharmacist extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
    }
    
    function index() 
    {
        if ($this->session->userdata('pharmacist_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }
        
        $data['page_name']      = 'dashboard';
        $data['page_title']     = get_phrase('pharmacist_dashboard');
        $this->load->view('backend/index', $data);
    }
    
    function medicine_category($task = "", $medicine_category_id = "")
    {
        if ($this->session->userdata('pharmacist_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }
                
        if ($task == "create")
        {
            $this->crud_model->save_medicine_category_info();
            $this->session->set_flashdata('message' , get_phrase('medicine_category_info_saved_successfuly'));
            redirect(base_url() .  'index.php?pharmacist/medicine_category');
        }
        
        if ($task == "update")
        {
            $this->crud_model->update_medicine_category_info($medicine_category_id);
            $this->session->set_flashdata('message' , get_phrase('medicine_category_info_updated_successfuly'));
            redirect(base_url() .  'index.php?pharmacist/medicine_category');
        }
        
        if ($task == "delete")
        {
            $this->crud_model->delete_medicine_category_info($medicine_category_id);
            redirect(base_url() .  'index.php?pharmacist/medicine_category');
        }
        
        $data['medicine_category_info'] = $this->crud_model->select_medicine_category_info();
        $data['page_name']              = 'manage_medicine_category';
        $data['page_title']             = get_phrase('medicine_category');
        $this->load->view('backend/index', $data);
    }

    function van_registration(){
        $data['page_name']      = 'van_registration';
        $data['page_title']     = get_phrase('van_register');
       
        $this->load->view('backend/index', $data);
    }
    
    function medicine($task = "", $medicine_id = "")
    {
        if ($this->session->userdata('pharmacist_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }
                
        if ($task == "create")
        {
            $this->crud_model->save_medicine_info();
            $this->session->set_flashdata('message' , get_phrase('medicine_info_saved_successfuly'));
            redirect(base_url() .'index.php?pharmacist/medicine');
        }
        
        if ($task == "update")
        {
            $this->crud_model->update_medicine_info($medicine_id);
            $this->session->set_flashdata('message' , get_phrase('medicine_info_updated_successfuly'));
            redirect(base_url() .'index.php?pharmacist/medicine');
        }
        
        if ($task == "delete")
        {
            $this->crud_model->delete_medicine_info($medicine_id);
            redirect(base_url() .'index.php?pharmacist/medicine');
        }
        
        /*$mmuid = $this->db->query("SELECT mmu_unit FROM user_details where user_id = '".$this->session->userdata('login_user_id')."' ")->row();       
        $medicinecount = $this->db->query("SELECT medicine_id, name FROM medicine")->result_array();
        foreach ($medicinecount as $key => $m) {            
            $noofunits = $this->db->query("SELECT SUM(no_of_unit) as noofunits
                FROM medicine_store AS m
                WHERE m.medicine_id = '".$m['medicine_id']."' and m.mmu_id = '".$mmuid->mmu_unit."'")->row_array();
            $medicinecount[$key]['noofunits'] = $noofunits;
        }
        $data['count'] = $medicinecount;
        $data['page_name']      = 'manage_medicine';
        $data['page_title']     = get_phrase('medicine');
       
        $this->load->view('backend/index', $data);*/
    }

    function stock_registration(){
        $mmuid = $this->db->query("SELECT mmu_unit FROM user_details where user_id = '".$this->session->userdata('login_user_id')."' ")->row();       
        $medicinecount = $this->db->query("SELECT medicine_id, name FROM medicine")->result_array();
        foreach ($medicinecount as $key => $m) {            
            $noofunits = $this->db->query("SELECT SUM(no_of_unit) as noofunits
                FROM medicine_store AS m
                WHERE m.medicine_id = '".$m['medicine_id']."' and m.mmu_id = '".$mmuid->mmu_unit."'")->row_array();
            $medicinecount[$key]['noofunits'] = $noofunits;
        }
        foreach ($medicinecount as $key => $m) {            
            $noofunitsinvan = $this->db->query("SELECT SUM(no_of_unit) as noofunitsinvan
                FROM van_stock AS m
                WHERE m.medicine_id = '".$m['medicine_id']."' and m.mmu_id = '".$mmuid->mmu_unit."'")->row_array();
            $medicinecount[$key]['noofunitsinvan'] = $noofunitsinvan;
        }
        $data['count'] = $medicinecount;
        $data['page_name']      = 'manage_medicine';
        $data['page_title']     = get_phrase('medicine');
       
        $this->load->view('backend/index', $data);
    }

    /*function add_medicine_van(){
        $mmuid = $this->db->query("SELECT mmu_unit FROM user_details where user_id = '".$this->session->userdata('login_user_id')."' ")->row();
        $data['mmu_location'] = $this->db->query("SELECT mmuloc_id,mmuloc_name FROM `mmu_locations` WHERE `mmu_unit` = '".$mmuid->mmu_unit."'")->result_array();        
        $data['page_name']      = 'add_medicine_van';
        $data['page_title']     = get_phrase('Add medicine to van');
       
        $this->load->view('backend/index', $data);
    }*/

    /*function get_medicine() {
        if ($this->session->userdata('pharmacist_login') != 1) {
            echo json_encode(array(
                'status' => 0,
                'msg' => 'Error! Session Expired'
            ));
            exit();
        }
        $term = $this->input->post('term', TRUE);
        
        $mmuid = $this->db->query("SELECT mmu_unit FROM user_details where user_id = '".$this->session->userdata('login_user_id')."' ")->row(); 
        if(isset($term)) {
            $query = $this->db->query("SELECT m.name, m.medicine_id
            FROM medicine AS m
            JOIN medicine_store AS ms ON m.medicine_id = ms.medicine_id and ms.mmu_id = '".$mmuid->mmu_unit."'
            WHERE m.name LIKE '".$term."%'");
        } else {
            $query = $this->db->query("SELECT m.name, m.medicine_id
            FROM medicine AS m
            JOIN medicine_store AS ms ON m.medicine_id = ms.medicine_id and ms.mmu_id = '".$mmuid->mmu_unit."'");
        }
        echo json_encode($query->result());
        exit();
        
    }*/

    /*function medicinedetails(){
        $mmuid = $this->db->query("SELECT mmu_unit FROM user_details where user_id = '".$this->session->userdata('login_user_id')."' ")->row();  
        $medicineid = $_POST['medicineid'];
        $query = $this->db->query("SELECT DISTINCT ms.batch_no
            FROM medicine AS m
            JOIN medicine_store AS ms ON m.medicine_id = ms.medicine_id and ms.mmu_id = '".$mmuid->mmu_unit."'
            WHERE m.medicine_id = '".$medicineid."'")->result();
        echo '<option value="">Select Batch</option>';
        foreach ($query as $q) { 
            echo '<option value="'.$q->batch_no.'">'.$q->batch_no.'</option>';
        }        
    }*/

    /*function getmedicineunitcount(){
        $medicineid = $_POST['medicineid'];
        $batchs = $_POST['batch'];
        $mmuid = $this->db->query("SELECT mmu_unit FROM user_details where user_id = '".$this->session->userdata('login_user_id')."' ")->row(); 
        $rtncount = $this->db->query("SELECT (no_of_unit - IFNULL(return_unit, 0)) as count FROM `van_stock` WHERE `mmu_id` = '".$mmuid->mmu_unit."' and medicine_id = '".$medicineid."'")->row();
        $query = $this->db->query("SELECT sum(ms.no_of_unit)-".$rtncount->count." as maincount ,ms.exp_date
            FROM medicine AS m
            JOIN medicine_store AS ms ON m.medicine_id = ms.medicine_id and ms.mmu_id = '".$mmuid->mmu_unit."' and ms.batch_no = '".$batchs."'
            WHERE m.medicine_id = '".$medicineid."' GROUP BY ms.batch_no, ms.exp_date")->row_array();
        echo json_encode($query);
        exit();
    }*/

    function add_medicine_van(){
        if($this->session->userdata('pharmacist_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        date_default_timezone_set('Asia/Kolkata');
        if(isset($_POST['submit'])) {
            foreach ($_POST['name'] as $key => $name) {
                $mstore = $this->db->query("SELECT DISTINCT exp_date, manufacturing_company
                FROM `medicine_store` 
                WHERE `batch_no` = '".$_POST['batch'][$key]."' and medicine_id = ".$name."")->row_array();

                $query = $this->db->insert('van_stock', array(
                    'user_id' => $this->session->userdata('login_user_id'),
                    'mmu_id' => $this->session->userdata('mmu_unit'),
                    /*'location_id' => $_POST['mmu'][$key],*/
                    'medicine_id' => $name,
                    'no_of_unit' => $_POST['requiredunitcount'][$key],
                    'batch_no' => $_POST['batch'][$key],
                    'exp_date' => $mstore['exp_date'],
                    'manufacturing_comp' => $mstore['manufacturing_company'],
                    'date_added' => date('Y-m-d H:i:s'),
                    'ip_address' => $this->input->ip_address(),
                    'status ' => 0
                ));
                if($query){
                    $this->session->set_flashdata("succ", "Medicine Successfully Added to Van.");
                }else{
                    $this->session->set_flashdata("err", "Sorry! System not adding");
                }
            }
            redirect($baseurl.'pharmacist/add_medicine_van');
        }

        $mmuid = $this->db->query("SELECT mmu_unit FROM user_details where user_id = ".$this->session->userdata('login_user_id')."")->row();
        $data['mmu_location'] = $this->db->query("SELECT mmuloc_id,mmuloc_name FROM `mmu_locations` WHERE `mmu_unit` = ".$mmuid->mmu_unit."")->result_array();        
        $data['page_name']      = 'add_medicine_van';
        $data['page_title']     = get_phrase('Add medicine to van');
       
        $this->load->view('backend/index', $data);
    }

    function get_medicine() {
        if ($this->session->userdata('pharmacist_login') != 1) {
            echo json_encode(array(
                'items' => array(),
                'total_count' => 0,
                'remaining' => 0
            ));
            exit();
        }

        $data = $this->input->post();
        $start = ($data['page']-1)*10;
        $limit = $data['page']*10;

        $meds = $data['meds'] == NULL ? 0 : implode(',', $data['meds']);

        if(isset($data['q'])) {
            $query = $this->db->query("SELECT medicine.medicine_id as id, medicine.name, medicine.composition, medicine.preparation
            FROM medicine
            WHERE medicine.name LIKE '".$data['q']."%'
            AND medicine.medicine_id NOT IN (".$meds.")");
        } else {
            $query = $this->db->query("SELECT medicine.medicine_id as id, medicine.name, medicine.composition, medicine.preparation
            FROM medicine
            WHERE medicine.medicine_id NOT IN (".$meds.")");
        }

        if($query->num_rows() > 0){
            echo json_encode(array(
                'items' => $query->result_array(),
                'total_count' => $query->num_rows(),
                'remaining' => $query->num_rows()>9 ? 1 : 0
            ));
            exit();
        }
        else{
            echo json_encode(array(
                'items' => array(),
                'total_count' => 0,
                'remaining' => 0
            ));
            exit();
        }
        /*$data['pharmacist_info'] = $this->crud_model->select_pharmacist_info();*/
        /*$data['pharmacist_info'] = $this->db->query("SELECT pharm.*, unit.mmu_name
                FROM pharmacist AS pharm
                JOIN mmu_unit AS unit ON unit.unit_id = pharm.mmu_unit
                WHERE pharm.status = 0")->result_array();*/
    }

    function medicinedetails(){
        $mmuid = $this->db->query("SELECT mmu_unit FROM user_details where user_id = ".$this->session->userdata('login_user_id')."")->row();
        $medicineid = $_POST['medicineid'];
        /*$medicineid = $this->db->query("SELECT medicine_id FROM medicine where name = '".$medicinename."' ")->row();*/
        $query = $this->db->query("SELECT DISTINCT ms.batch_no
            FROM medicine AS m
            JOIN medicine_store AS ms ON m.medicine_id = ms.medicine_id and ms.mmu_id = ".$mmuid->mmu_unit."
            WHERE m.medicine_id = ".$medicineid."")->result_array();

        echo json_encode(array('medicine' => $query));
        exit();
    }

    function getmedicineunitcount(){
        $medicineid = $_POST['medicineid'];
        $batch = $_POST['batch'];
        
        $mmuid = $this->db->query("SELECT mmu_unit FROM user_details where user_id = ".$this->session->userdata('login_user_id')."")->row();

        $vanstock = $this->db->query("SELECT sum(no_of_unit) AS loaded, sum(IFNULL(return_unit, 0)) AS returned FROM `van_stock` WHERE `mmu_id` = ".$mmuid->mmu_unit." and medicine_id = ".$medicineid." and batch_no = '".$batch."'")->row();
        $used = intval($vanstock->loaded) - intval($vanstock->returned);

        $query = $this->db->query("SELECT (sum(no_of_unit) - ".$used.") as maincount, exp_date
            FROM medicine_store
            WHERE medicine_id = ".$medicineid." AND mmu_id = ".$mmuid->mmu_unit." AND batch_no = '".$batch."'
            GROUP BY batch_no, exp_date")->row_array();
        /*echo $query->maincount;
        echo ",".$query->exp_date;*/
        echo json_encode($query);
        exit();
    }
    
    function manage_profile($task = "")
    {
        if ($this->session->userdata('pharmacist_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }
        
        $pharmacist_id      = $this->session->userdata('login_user_id');
        if ($task == "update")
        {
            $this->crud_model->update_pharmacist_info($pharmacist_id);
            $this->session->set_flashdata('message' , get_phrase('profile_info_updated_successfuly'));
            redirect(base_url() .'index.php?pharmacist/manage_profile');
        }
        
        if ($task == "change_password")
        {
            $password               = $this->db->get_where('pharmacist', array('pharmacist_id' => $pharmacist_id))->row()->password;
            $old_password           = sha1($this->input->post('old_password'));
            $new_password           = $this->input->post('new_password');
            $confirm_new_password   = $this->input->post('confirm_new_password');
            
            if($password==$old_password && $new_password==$confirm_new_password)
            {
                $data['password']   = sha1($new_password);
                
                $this->db->where('pharmacist_id',$pharmacist_id);
                $this->db->update('pharmacist',$data);
                
                $this->session->set_flashdata('message' , get_phrase('password_info_updated_successfuly'));
                redirect(base_url() .'index.php?pharmacist/manage_profile');
            }
            else
            {
                $this->session->set_flashdata('message' , get_phrase('password_update_failed'));
                redirect(base_url() .'index.php?pharmacist/manage_profile');
            }
        }
        
        $data['page_name']          = 'edit_profile';
        $data['page_title']         = get_phrase('profile');
        $this->load->view('backend/index', $data);
    }
    public function manage_diagnosis()
    {
        if ($this->session->userdata('pharmacist_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }
        $mmu = $this->session->userdata('mmu_unit');
        /*$data['alllocation'] = $this->db->query("SELECT * FROM mmu_locations WHERE mmu_unit = "$this->session->userdata('mmu_unit')"")->result_array();*/
        $data['alllocation'] = $this->db->query("SELECT * FROM mmu_locations WHERE mmu_unit = ".$mmu."")->result_array();

        $data['page_name']          = 'manage_diagnosis';
        $data['page_title']         = get_phrase('manage_medicine_distribution');
        $this->load->view('backend/index', $data);
    }

    public function diagnosis()
    {
        if ($this->session->userdata('pharmacist_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }
        $allpatients = $this->db->query("SELECT patient_id, name FROM patient WHERE mmuloc = ".$_POST['location']." AND status = 1")->result_array();
        /*$minvalue = $_POST['date']." 00:00:00";
        $maxvalue = $_POST['date']." 23:59:59";*/
        foreach ($allpatients as $key => $patient) {
        $this->db->distinct();
        $this->db->select('tm.tm_id, tm.quantity, m.name');
        $this->db->from('medicine AS m');
        $this->db->join('treatment_medicine AS tm', 'tm.medicine_id = m.medicine_id');
        $medicines = $this->db->where("tm.patient_id", $patient['patient_id'])->where("tm.date", $_POST['date'])->get()->result_array();
        $allpatients[$key]['medicines'] = $medicines;
        $allpatients[$key]['date'] = $_POST['date'];
    }
        /*$data['page_name'] = 'diagnosis';
        $data['page_title'] = get_phrase('diagnosis');
        $this->load->view('backend/index', $data);*/
        // echo '<pre>'; print_r($allpatients); echo '</pre>';
        echo json_encode( array(
            'patients' => $allpatients
        ) );
        exit();
    }
    public function vanstock()
    {
        if ($this->session->userdata('pharmacist_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }
        /*$_POST['date'] = "2017-05-12";*/
        $minvalue = $_POST['date']." 00:00:00";
        $maxvalue = $_POST['date']." 23:59:59";
        /*$mmuid = $this->db->query("SELECT mmu_unit FROM user_details where user_id = '".$this->session->userdata('login_user_id')."' ")->row();       
        $medicinecount = $this->db->query("SELECT medicine_id, name FROM medicine")->result_array();
        foreach ($medicinecount as $key => $m) {            
            $noofunits = $this->db->query("SELECT SUM(no_of_unit) as noofunits
                FROM van_stock AS m
                WHERE m.medicine_id = '".$m['medicine_id']."' and m.mmu_id = '".$mmuid->mmu_unit."' and date_added BETWEEN '".$minvalue."' AND '".$maxvalue."' ")->row_array();
            $medicinecount[$key]['noofunits'] = $noofunits;
        }*/
        $mmuid = $this->db->query("SELECT mmu_unit FROM user_details where user_id = ".$this->session->userdata('login_user_id')."")->row_array();
        $medicine = $this->db->query("SELECT v.*, medicine.name
                FROM van_stock AS v
                JOIN medicine AS medicine ON medicine.medicine_id = v.medicine_id
                WHERE v.mmu_id = ".$mmuid['mmu_unit']." AND v.date_added BETWEEN '".$minvalue."' AND '".$maxvalue."'")->result_array();
        /*var_dump($medicine);
        die();
        $data['count'] = $medicinecount;*/
        /*$allpatients = $this->db->query("SELECT patient_id, name FROM patient WHERE mmuloc = ".$_POST['location']." AND status = 1")->result_array();
        $minvalue = $_POST['date']." 00:00:00";
        $maxvalue = $_POST['date']." 23:59:59";
        foreach ($allpatients as $key => $patient) {
        $this->db->distinct();
        $this->db->select('tm.tm_id, tm.quantity, m.name');
        $this->db->from('medicine AS m');
        $this->db->join('treatment_medicine AS tm', 'tm.medicine_id = m.medicine_id');
        $medicines = $this->db->where("tm.patient_id", $patient['patient_id'])->where("tm.date BETWEEN '".$minvalue."' AND '".$maxvalue."'")->get()->result_array();
        $allpatients[$key]['medicines'] = $medicines;
        $allpatients[$key]['date'] = $_POST['date'];
    }*/
        /*$data['page_name'] = 'diagnosis';
        $data['page_title'] = get_phrase('diagnosis');
        $this->load->view('backend/index', $data);*/
        // echo '<pre>'; print_r($allpatients); echo '</pre>';
        echo json_encode( array(
            'medicines' => $medicine
        ) );
        exit();
    }
    public function edit_diagnosis(){
        if($this->session->userdata('pharmacist_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $tm_id = $this->uri->segment(3);
        if (empty($tm_id)) {
            show_404();
        }
        /*$this->db->query("SELECT v.*, medicine.name
                FROM van_stock AS v
                JOIN medicine AS medicine ON medicine.medicine_id = v.medicine_id
                WHERE date_added BETWEEN '".$minvalue."' AND '".$maxvalue."'")*/
        $data['medicine'] = $this->db->query("SELECT t.*, medicine.name
                FROM treatment_medicine AS t
                JOIN medicine AS medicine ON medicine.medicine_id = t.medicine_id
                WHERE t.tm_id = ".$tm_id."")->row_array();
        $data['page_name'] = 'edit_diagnosis';
        $data['page_title'] = get_phrase('edit_medicine_distribution');
        $this->load->view('backend/index', $data);
    }
    public function updtreatment(){
        if ($this->session->userdata('pharmacist_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }

        $error = array('count' => '', 'status' => 0);
        if(isset($_POST['tm_id'])) {
            if(empty($_POST['count'])) {
                $error['count'] = 'Count Field is mandatory';
                $error['status'] = 1;
            }

            if($error['status'] > 0) {
                echo json_encode($error);
                exit();
            }

            $data = array(
                'quantity' => $_POST['count']
            );
            $query = $this->db->where('tm_id', $_POST['tm_id'])->update('treatment_medicine', $data);
            if($query){
                echo json_encode(array('msg' => 'Medicine Distribution Updated Successfully'));
                exit();
            }
            else{
                echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
                exit();
            }
        }
        else {
            echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
            exit();
        }
    }
}