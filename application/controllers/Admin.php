<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 	
 * 	@author : Joyonto Roy
 * 	date	: 1 August, 2014
 * 	http://codecanyon.net/user/Creativeitem
 * 	http://creativeitem.com
 */

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');

        /* cache control */
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    /*     * *default function, redirects to login page if no admin logged in yet** */

    function alpha_dash_space($str_in){
        if (! preg_match("/^([a-z ])+$/i", $str_in)) {
            $this->form_validation->set_message('alpha_dash_space', 'The %s field may only contain alphabets characters and  spaces.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function check_mobileno($str_in){
        if (! preg_match("/^([0-9])+$/i", $str_in)) {
            $this->form_validation->set_message('check_mobileno', 'The %s field may only contain Numbers.');
            return FALSE;
        } else if(strlen($str_in)>15){
            $this->form_validation->set_message('check_mobileno', 'The %s field must be within 10 to 15 digits.');
            return FALSE;
        }else if(strlen($str_in)<10){
            $this->form_validation->set_message('check_mobileno', 'The %s field must be within 10 to 15 digits.');
            return FALSE;
        }else {
            return TRUE;
        }
    }

    public function index() {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');
        if ($this->session->userdata('admin_login') == 1)
            redirect(base_url() . 'index.php?admin/dashboard', 'refresh');
    }

    /*     * *ADMIN DASHBOARD** */

    function dashboard() {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $page_data['pat_by_day'] = $this->db->query("SELECT * FROM treatment WHERE treat_date > DATE_SUB(NOW(), INTERVAL 1 DAY)")->num_rows();
        $page_data['pat_by_month'] = $this->db->query("SELECT * FROM treatment WHERE treat_date > DATE_SUB(NOW(), INTERVAL 1 MONTH)")->num_rows();

        $page_data['med_by_day'] = $this->db->query("SELECT SUM(quantity) as quantity FROM treatment_medicine WHERE date > DATE_SUB(NOW(), INTERVAL 1 DAY)")->row();
        $page_data['med_by_month'] = $this->db->query("SELECT SUM(quantity) as quantity FROM treatment_medicine WHERE date > DATE_SUB(NOW(), INTERVAL 1 MONTH)")->row();

        $patcount=$this->db->query("SELECT unit_id,mmu_name FROM mmu_unit")->result_array();
        foreach ($patcount as $key => $pc) 
        {
            $pcnt=$pc['unit_id'];
            $pcount = $this->db->query("SELECT count(treat.p_id) as count, SUM(if(pat.gender = 'MALE',1,0)) as male,
            SUM(if(pat.gender = 'FEMALE',1,0)) as female, treat.treat_date, mu.mmu_name 
                                        FROM treatment AS treat
                                        JOIN patient AS pat ON treat.p_id = pat.patient_id and pat.mmu = $pcnt
                                        JOIN mmu_unit AS mu ON pat.mmu = mu.unit_id
                                        where treat.treat_date = curdate()")->row_array();
            $patcount[$key]['pcount'] = $pcount;

            
        }
        
        $medcount=$this->db->query("SELECT unit_id,mmu_name FROM mmu_unit")->result_array();
        foreach ($medcount as $key => $med) 
        {
                $medc=$med['unit_id'];
                $medicinecount = $this->db->query("SELECT SUM(tm.quantity) AS mcount
                        FROM treatment_medicine AS tm 
                        JOIN patient AS p ON p.patient_id = tm.patient_id
                        WHERE p.mmu= $medc && tm.date > DATE_SUB(NOW(), INTERVAL 1 DAY)")->row_array();
                $medcount[$key]['medicinecount'] = $medicinecount;
        }

        $page_data['today_pat_by_mmu']=$patcount;
        $page_data['today_med_by_mmu']=$medcount;
        $page_data['page_name'] = 'dashboard';
        $page_data['page_title'] = get_phrase('admin_dashboard');
        //var_dump($page_data['today_med_by_mmu']); exit();
        $this->load->view('backend/index', $page_data);
    }

    /*     * ***LANGUAGE SETTINGS******** */

    function manage_language($param1 = '', $param2 = '', $param3 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');

        if ($param1 == 'edit_phrase') {
            $page_data['edit_profile'] = $param2;
        }
        if ($param1 == 'update_phrase') {
            $language = $param2;
            $total_phrase = $this->input->post('total_phrase');
            for ($i = 1; $i < $total_phrase; $i++) {
                //$data[$language]	=	$this->input->post('phrase').$i;
                $this->db->where('phrase_id', $i);
                $this->db->update('language', array($language => $this->input->post('phrase' . $i)));
            }
            redirect(base_url() . 'index.php?admin/manage_language/edit_phrase/' . $language, 'refresh');
        }
        if ($param1 == 'do_update') {
            $language = $this->input->post('language');
            $data[$language] = $this->input->post('phrase');
            $this->db->where('phrase_id', $param2);
            $this->db->update('language', $data);
            $this->session->set_flashdata('message', get_phrase('settings_updated'));
            redirect(base_url() . 'index.php?admin/manage_language/', 'refresh');
        }
        if ($param1 == 'add_phrase') {
            $data['phrase'] = $this->input->post('phrase');
            $this->db->insert('language', $data);
            $this->session->set_flashdata('message', get_phrase('settings_updated'));
            redirect(base_url() . 'index.php?admin/manage_language/', 'refresh');
        }
        if ($param1 == 'add_language') {
            $language = $this->input->post('language');
            $this->load->dbforge();
            $fields = array(
                $language => array(
                    'type' => 'LONGTEXT'
                )
            );
            $this->dbforge->add_column('language', $fields);

            $this->session->set_flashdata('message', get_phrase('settings_updated'));
            redirect(base_url() . 'index.php?admin/manage_language/', 'refresh');
        }
        if ($param1 == 'delete_language') {
            $language = $param2;
            $this->load->dbforge();
            $this->dbforge->drop_column('language', $language);
            $this->session->set_flashdata('message', get_phrase('settings_updated'));

            redirect(base_url() . 'index.php?admin/manage_language/', 'refresh');
        }
        $page_data['page_name'] = 'manage_language';
        $page_data['page_title'] = get_phrase('manage_language');
        //$page_data['language_phrases'] = $this->db->get('language')->result_array();
        $this->load->view('backend/index', $page_data);
    }

    /*     * ***SITE/SYSTEM SETTINGS******** */

    function system_settings($param1 = '', $param2 = '', $param3 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');

        if ($param1 == 'do_update') {
            $this->crud_model->update_system_settings();
            $this->session->set_flashdata('message', get_phrase('settings_updated'));
            redirect(base_url() . 'index.php?admin/system_settings/', 'refresh');
        }
        if ($param1 == 'upload_logo') {
            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/logo.png');
            $this->session->set_flashdata('message', get_phrase('settings_updated'));
            redirect(base_url() . 'index.php?admin/system_settings/', 'refresh');
        }
        $page_data['page_name'] = 'system_settings';
        $page_data['page_title'] = get_phrase('system_settings');
        $page_data['settings'] = $this->db->get('settings')->result_array();
        $this->load->view('backend/index', $page_data);
    }

    // SMS settings.
    function sms_settings($param1 = '') {

        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');

        if ($param1 == 'do_update') {
            $this->crud_model->update_sms_settings();
            $this->session->set_flashdata('message', get_phrase('settings_updated'));
            redirect(base_url() . 'index.php?admin/sms_settings/', 'refresh');
        }

        $page_data['page_name'] = 'sms_settings';
        $page_data['page_title'] = get_phrase('sms_settings');
        $this->load->view('backend/index', $page_data);
    }

    /*     * ****MANAGE OWN PROFILE AND CHANGE PASSWORD** */

    function manage_profile($param1 = '', $param2 = '', $param3 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');

        if ($param1 == 'update_profile_info') {
            $data['name'] = $this->input->post('name');
            $data['email'] = $this->input->post('email');

            $this->db->where('admin_id', $this->session->userdata('login_user_id'));
            $this->db->update('admin', $data);

            $this->session->set_flashdata('message', get_phrase('profile_info_updated_successfuly'));
            redirect(base_url() . 'index.php?admin/manage_profile');
        }
        if ($param1 == 'change_password') {
            $current_password_input = sha1($this->input->post('password'));
            $new_password = sha1($this->input->post('new_password'));
            $confirm_new_password = sha1($this->input->post('confirm_new_password'));

            $current_password_db = $this->db->get_where('admin', array('admin_id' =>
                        $this->session->userdata('login_user_id')))->row()->password;

            if ($current_password_db == $current_password_input && $new_password == $confirm_new_password) {
                $this->db->where('admin_id', $this->session->userdata('login_user_id'));
                $this->db->update('admin', array('password' => $new_password));

                $this->session->set_flashdata('message', get_phrase('password_info_updated_successfuly'));
                redirect(base_url() . 'index.php?admin/manage_profile');
            } else {
                $this->session->set_flashdata('message', get_phrase('password_update_failed'));
                redirect(base_url() . 'index.php?admin/manage_profile');
            }
        }
        $page_data['page_name'] = 'manage_profile';
        $page_data['page_title'] = get_phrase('manage_profile');
        $page_data['edit_data'] = $this->db->get_where('admin', array('admin_id' => $this->session->userdata('login_user_id')))->result_array();
        $this->load->view('backend/index', $page_data);
    }

    function department($task = "", $department_id = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $this->crud_model->save_department_info();
            $this->session->set_flashdata('message', get_phrase('department_info_saved_successfuly'));
            redirect(base_url() . 'index.php?admin/department');
        }

        if ($task == "update") {
            $this->crud_model->update_department_info($department_id);
            $this->session->set_flashdata('message', get_phrase('department_info_updated_successfuly'));
            redirect(base_url() . 'index.php?admin/department');
        }

        if ($task == "delete") {
            $this->crud_model->delete_department_info($department_id);
            redirect(base_url() . 'index.php?admin/department');
        }

        $data['department_info'] = $this->crud_model->select_department_info();
        $data['page_name'] = 'manage_department';
        $data['page_title'] = get_phrase('department');
        $this->load->view('backend/index', $data);
    }

    function doctor($task = "", $doctor_id = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $email = $_POST['email'];
            $doctor = $this->db->get_where('doctor', array('email' => $email))->row()->name;

            if ($doctor == null) {
                $this->crud_model->save_doctor_info();
                $this->session->set_flashdata('message', get_phrase('doctor_info_saved_successfuly'));
            } else {
                $this->session->set_flashdata('message', get_phrase('duplicate_email'));
            }
            redirect(base_url() . 'index.php?admin/doctor');
        }

        if ($task == "update") {
          
                $this->crud_model->update_doctor_info($doctor_id);
                $this->session->set_flashdata('message', get_phrase('doctor_info_updated_successfuly'));
            
                redirect(base_url() . 'index.php?admin/doctor');
        }

        if ($task == "delete") {
            $this->crud_model->delete_doctor_info($doctor_id);
            redirect(base_url() . 'index.php?admin/doctor');
        }
        $data['doctor_info'] = $this->crud_model->select_doctor_info();
        $data['page_name'] = 'manage_doctor';
        $data['page_title'] = get_phrase('doctor');
        $this->load->view('backend/index', $data);
    }

    function patient($task = "", $patient_id = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $email = $_POST['email'];
            $patient = $this->db->get_where('patient', array('email' => $email))->row()->name;
            if ($patient == null) {
                $this->crud_model->save_patient_info();
                $this->session->set_flashdata('message', get_phrase('patient_info_saved_successfuly'));
            } else {
                $this->session->set_flashdata('message', get_phrase('duplicate_email'));
            }
            redirect(base_url() . 'index.php?admin/patient');
        }

        if ($task == "update") {
                $this->crud_model->update_patient_info($patient_id);
                $this->session->set_flashdata('message', get_phrase('patient_info_updated_successfuly'));
                redirect(base_url() . 'index.php?admin/patient');
        }

        if ($task == "delete") {
            $this->crud_model->delete_patient_info($patient_id);
            redirect(base_url() . 'index.php?admin/patient');
        }

        $data['patient_info'] = $this->crud_model->select_patient_info();
        $data['page_name'] = 'manage_patient';
        $data['page_title'] = get_phrase('find_patient');
        $this->load->view('backend/index', $data);
    }

    function find_patient($task = "", $patient_id = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['patient_info'] = $this->crud_model->select_patient_info();
        $data['page_name'] = 'find_patient';
        $data['page_title'] = get_phrase('find_patient');
        $this->load->view('backend/index', $data);
    }

    function add_patient($task = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $email = $_POST['email'];
            $patient = $this->db->get_where('patient', array('email' => $email))->row()->name;
            if ($patient == null) {
                $patient_id = $this->crud_model->save_patient_info();
                //$this->crud_model->save_prescription_info($patient_id);
                $this->session->set_flashdata('message', get_phrase('patient_info_saved_successfuly'));
            } else {
                $this->session->set_flashdata('message', get_phrase('duplicate_email'));
            }
            redirect(base_url() . 'index.php?admin/find_patient');
        }

        $data['page_name'] = 'add_patient';
        $data['page_title'] = get_phrase('register_patient');
        $this->load->view('backend/index', $data);
    }

    function patient_details($task = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['page_name'] = 'patient_details';
        $data['page_title'] = get_phrase('patient_details');
        $this->load->view('backend/index', $data);
    }

    function checkup($task = "", $prescription_id = "", $menu_check = '', $patient_id = '') {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $this->crud_model->save_prescription_info();
            $this->session->set_flashdata('message', get_phrase('prescription_info_saved_successfuly'));
            redirect(base_url() . 'index.php?admin/checkup');
        }

        if ($task == "update") {
            $this->crud_model->update_prescription_info($prescription_id);
            $this->session->set_flashdata('message', get_phrase('prescription_info_updated_successfuly'));
            if ($menu_check == 'from_prescription')
                redirect(base_url() . 'index.php?admin/checkup');
            else
                redirect(base_url() . 'index.php?admin/medication_history/' . $patient_id);
        }

        if ($task == "delete") {
            $this->crud_model->delete_prescription_info($prescription_id);
            if ($menu_check == 'from_prescription')
                redirect(base_url() . 'index.php?admin/checkup');
            else
                redirect(base_url() . 'index.php?admin/medication_history/' . $patient_id);
        }

        $data['prescription_info'] = $this->crud_model->select_all_prescription_info();
        $data['menu_check'] = 'from_prescription';
        $data['page_name'] = 'checkup';
        $data['page_title'] = get_phrase('checkup');
        $this->load->view('backend/index', $data);
    }

    function checkup_report($task = "", $diagnosis_report_id = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $this->crud_model->save_diagnosis_report_info();
            $this->session->set_flashdata('message', get_phrase('checkup_report_info_saved_successfuly'));
            redirect(base_url() . 'index.php?admin/checkup');
        }

        if ($task == "delete") {
            $this->crud_model->delete_diagnosis_report_info($diagnosis_report_id);
            $this->session->set_flashdata('message', get_phrase('checkup_report_info_deleted_successfuly'));
            redirect(base_url() . 'index.php?admin/checkup');
        }
    }

    function add_checkup($task = "", $diagnosis_report_id = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $this->crud_model->save_diagnosis_report_info();
            $this->session->set_flashdata('message', get_phrase('checkup_report_info_saved_successfuly'));
            redirect(base_url() . 'index.php?admin/patient_details');
        }

        if ($task == "new") {
            $data['show_patient']  = true;
        }

        $data['page_name']  = 'add_checkup';
        $data['page_title'] = get_phrase('add_checkup');
        $this->load->view('backend/index', $data);
    }

    function worldview() {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['no_title']  = true;
        $data['page_name']  = 'worldview';
        $data['page_title'] = get_phrase('worldview');
        $this->load->view('backend/index', $data);
    }

    function medicine_category($task = "", $medicine_category_id = "")
    {
        if ($this->session->userdata('admin_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }
                
        if ($task == "create")
        {
            $this->crud_model->save_medicine_category_info();
            $this->session->set_flashdata('message' , get_phrase('medicine_category_info_saved_successfuly'));
            redirect(base_url() .  'index.php?admin/medicine_category');
        }
        
        if ($task == "update")
        {
            $this->crud_model->update_medicine_category_info($medicine_category_id);
            $this->session->set_flashdata('message' , get_phrase('medicine_category_info_updated_successfuly'));
            redirect(base_url() .  'index.php?admin/medicine_category');
        }
        
        if ($task == "delete")
        {
            $this->crud_model->delete_medicine_category_info($medicine_category_id);
            redirect(base_url() .  'index.php?admin/medicine_category');
        }
        
        $data['medicine_category_info'] = $this->crud_model->select_medicine_category_info();
        $data['page_name']              = 'manage_medicine_category';
        $data['page_title']             = get_phrase('medicine_category');
        $this->load->view('backend/index', $data);
    }
    
    /*function medicine($task = "", $medicine_id = "")
    {
        if ($this->session->userdata('admin_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }
                
        if ($task == "create")
        {
            $this->crud_model->save_medicine_info();
            $this->session->set_flashdata('message' , get_phrase('medicine_info_saved_successfuly'));
            redirect(base_url() .'index.php?admin/medicine');
        }
        
        if ($task == "update")
        {
            $this->crud_model->update_medicine_info($medicine_id);
            $this->session->set_flashdata('message' , get_phrase('medicine_info_updated_successfuly'));
            redirect(base_url() .'index.php?admin/medicine');
        }
        
        if ($task == "delete")
        {
            $this->crud_model->delete_medicine_info($medicine_id);
            redirect(base_url() .'index.php?admin/medicine');
        }
        
        $data['medicine_info']  = $this->crud_model->select_medicine_info();
        $data['page_name']      = 'manage_medicine';
        $data['page_title']     = get_phrase('medicine');
        $this->load->view('backend/index', $data);
    }*/

    function prescription($task = "", $prescription_id = "", $menu_check = '', $patient_id = '') {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $this->crud_model->save_prescription_info();
            $this->session->set_flashdata('message', get_phrase('prescription_info_saved_successfuly'));
            redirect(base_url() . 'index.php?admin/prescription');
        }

        if ($task == "update") {
            $this->crud_model->update_prescription_info($prescription_id);
            $this->session->set_flashdata('message', get_phrase('prescription_info_updated_successfuly'));
            if ($menu_check == 'from_prescription')
                redirect(base_url() . 'index.php?admin/prescription');
            else
                redirect(base_url() . 'index.php?admin/medication_history/' . $patient_id);
        }

        if ($task == "delete") {
            $this->crud_model->delete_prescription_info($prescription_id);
            if ($menu_check == 'from_prescription')
                redirect(base_url() . 'index.php?admin/prescription');
            else
                redirect(base_url() . 'index.php?admin/medication_history/' . $patient_id);
        }

        $data['prescription_info'] = $this->crud_model->select_all_prescription_info();
        $data['menu_check'] = 'from_prescription';
        $data['page_name'] = 'manage_prescription';
        $data['page_title'] = get_phrase('prescription');
        $this->load->view('backend/index', $data);
    }

    function diagnosis_report($task = "", $diagnosis_report_id = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $this->crud_model->save_diagnosis_report_info();
            $this->session->set_flashdata('message', get_phrase('diagnosis_report_info_saved_successfuly'));
            redirect(base_url() . 'index.php?admin/prescription');
        }

        if ($task == "delete") {
            $this->crud_model->delete_diagnosis_report_info($diagnosis_report_id);
            $this->session->set_flashdata('message', get_phrase('diagnosis_report_info_deleted_successfuly'));
            redirect(base_url() . 'index.php?admin/prescription');
        }
    }

    function nurse($task = "", $nurse_id = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $email = $_POST['email'];
            $nurse = $this->db->get_where('nurse', array('email' => $email))->row()->name;
            if ($nurse == null) {
                $this->crud_model->save_nurse_info();
                $this->session->set_flashdata('message', get_phrase('nurse_info_saved_successfuly'));
            } else {
                $this->session->set_flashdata('message', get_phrase('duplicate_email'));
            }
            redirect(base_url() . 'index.php?admin/nurse');
        }

        if ($task == "update") {
                $this->crud_model->update_nurse_info($nurse_id);
                $this->session->set_flashdata('message', get_phrase('nurse_info_updated_successfuly'));
                redirect(base_url() . 'index.php?admin/nurse');
        }

        if ($task == "delete") {
            $this->crud_model->delete_nurse_info($nurse_id);
            redirect(base_url() . 'index.php?admin/nurse');
        }

        $data['nurse_info'] = $this->crud_model->select_nurse_info();
        $data['page_name'] = 'manage_nurse';
        $data['page_title'] = get_phrase('nurse');
        $this->load->view('backend/index', $data);
    }

    /*function pharmacist($task = "", $pharmacist_id = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $email = $_POST['email'];
            $pharmacist = $this->db->get_where('pharmacist', array('email' => $email))->row()->name;
            if ($pharmacist == null) {
                $this->crud_model->save_pharmacist_info();
                $this->session->set_flashdata('message', get_phrase('pharmacist_info_saved_successfuly'));
            } else {
                $this->session->set_flashdata('message', get_phrase('duplicate_email'));
            }
            redirect(base_url() . 'index.php?admin/pharmacist');
        }

        if ($task == "update") {
                $this->crud_model->update_pharmacist_info($pharmacist_id);
                $this->session->set_flashdata('message', get_phrase('pharmacist_info_updated_successfuly'));
                redirect(base_url() . 'index.php?admin/pharmacist');
        }

        if ($task == "delete") {
            $this->crud_model->delete_pharmacist_info($pharmacist_id);
            redirect(base_url() . 'index.php?admin/pharmacist');
        }

        $data['pharmacist_info'] = $this->crud_model->select_pharmacist_info();
        $data['page_name'] = 'manage_pharmacist';
        $data['page_title'] = get_phrase('pharmacist');
        $this->load->view('backend/index', $data);
    }*/

    function laboratorist($task = "", $laboratorist_id = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $email = $_POST['email'];
            $laboratorist = $this->db->get_where('laboratorist', array('email' => $email))->row()->name;
            if ($laboratorist == null) {
                $this->crud_model->save_laboratorist_info();
                $this->session->set_flashdata('message', get_phrase('laboratorist_info_saved_successfuly'));
            } else {
                $this->session->set_flashdata('message', get_phrase('duplicate_email'));
            }
            redirect(base_url() . 'index.php?admin/laboratorist');
        }

        if ($task == "update") {
                $this->crud_model->update_laboratorist_info($laboratorist_id);
                $this->session->set_flashdata('message', get_phrase('laboratorist_info_updated_successfuly'));
                redirect(base_url() . 'index.php?admin/laboratorist');
        }

        if ($task == "delete") {
            $this->crud_model->delete_laboratorist_info($laboratorist_id);
            redirect(base_url() . 'index.php?admin/laboratorist');
        }

        $data['laboratorist_info'] = $this->crud_model->select_laboratorist_info();
        $data['page_name'] = 'manage_laboratorist';
        $data['page_title'] = get_phrase('laboratorist');
        $this->load->view('backend/index', $data);
    }

    function accountant($task = "", $accountant_id = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $email = $_POST['email'];
            $accountant = $this->db->get_where('accountant', array('email' => $email))->row()->name;
            if ($accountant == null) {
                $this->crud_model->save_accountant_info();
                $this->session->set_flashdata('message', get_phrase('accountant_info_saved_successfuly'));
            } else {
                $this->session->set_flashdata('message', get_phrase('duplicate_email'));
            }
            redirect(base_url() . 'index.php?admin/accountant');
        }

        if ($task == "update") {
                $this->crud_model->update_accountant_info($accountant_id);
                $this->session->set_flashdata('message', get_phrase('accountant_info_updated_successfuly'));
                redirect(base_url() . 'index.php?admin/accountant');
        }

        if ($task == "delete") {
            $this->crud_model->delete_accountant_info($accountant_id);
            redirect(base_url() . 'index.php?admin/accountant');
        }

        $data['accountant_info'] = $this->crud_model->select_accountant_info();
        $data['page_name'] = 'manage_accountant';
        $data['page_title'] = get_phrase('accountant');
        $this->load->view('backend/index', $data);
    }

    function receptionist($task = "", $receptionist_id = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $email = $_POST['email'];
            $receptionist = $this->db->get_where('receptionist', array('email' => $email))->row()->name;
            if ($receptionist == null) {
                $this->crud_model->save_receptionist_info();
                $this->session->set_flashdata('message', get_phrase('receptionist_info_saved_successfuly'));
            } else {
                $this->session->set_flashdata('message', get_phrase('duplicate_email'));
            }
            redirect(base_url() . 'index.php?admin/receptionist');
        }

        if ($task == "update") {
                $this->crud_model->update_receptionist_info($receptionist_id);
                $this->session->set_flashdata('message', get_phrase('receptionist_info_updated_successfuly'));
                redirect(base_url() . 'index.php?admin/receptionist');
        }

        if ($task == "delete") {
            $this->crud_model->delete_receptionist_info($receptionist_id);
            redirect(base_url() . 'index.php?admin/receptionist');
        }

        $data['receptionist_info'] = $this->crud_model->select_receptionist_info();
        $data['page_name'] = 'manage_receptionist';
        $data['page_title'] = get_phrase('receptionist');
        $this->load->view('backend/index', $data);
    }

    function payment_history($task = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['invoice_info'] = $this->crud_model->select_invoice_info();
        $data['page_name'] = 'show_payment_history';
        $data['page_title'] = get_phrase('payment_history');
        $this->load->view('backend/index', $data);
    }

    function bed_allotment($task = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['bed_allotment_info'] = $this->crud_model->select_bed_allotment_info();
        $data['page_name'] = 'show_bed_allotment';
        $data['page_title'] = get_phrase('bed_allotment');
        $this->load->view('backend/index', $data);
    }

    function blood_bank($task = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['blood_bank_info'] = $this->crud_model->select_blood_bank_info();
        $data['page_name'] = 'show_blood_bank';
        $data['page_title'] = get_phrase('blood_bank');
        $this->load->view('backend/index', $data);
    }

    function blood_donor($task = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['blood_donor_info'] = $this->crud_model->select_blood_donor_info();
        $data['page_name'] = 'show_blood_donor';
        $data['page_title'] = get_phrase('blood_donor');
        $this->load->view('backend/index', $data);
    }

    /*function medicine($task = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['medicine_info'] = $this->crud_model->select_medicine_info();
        $data['page_name'] = 'show_medicine';
        $data['page_title'] = get_phrase('medicine');
        $this->load->view('backend/index', $data);
    }*/

    function operation_report($task = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['page_name'] = 'show_operation_report';
        $data['page_title'] = get_phrase('operation_report');
        $this->load->view('backend/index', $data);
    }

    function birth_report($task = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['page_name'] = 'show_birth_report';
        $data['page_title'] = get_phrase('birth_report');
        $this->load->view('backend/index', $data);
    }

    function death_report($task = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['page_name'] = 'show_death_report';
        $data['page_title'] = get_phrase('death_report');
        $this->load->view('backend/index', $data);
    }

    function notice($task = "", $notice_id = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $this->crud_model->save_notice_info();
            $this->session->set_flashdata('message', get_phrase('notice_info_saved_successfuly'));
            redirect(base_url() . 'index.php?admin/notice');
        }

        if ($task == "update") {
            $this->crud_model->update_notice_info($notice_id);
            $this->session->set_flashdata('message', get_phrase('notice_info_updated_successfuly'));
            redirect(base_url() . 'index.php?admin/notice');
        }

        if ($task == "delete") {
            $this->crud_model->delete_notice_info($notice_id);
            redirect(base_url() . 'index.php?admin/notice');
        }

        $data['notice_info'] = $this->crud_model->select_notice_info();
        $data['page_name'] = 'manage_notice';
        $data['page_title'] = get_phrase('noticeboard');
        $this->load->view('backend/index', $data);
    }
//Abhinit code
    function manage_mmu() {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        
        $data['page_name'] = 'manage_mmu';
        $data['page_title'] = get_phrase('manage_MMU');
        $data['mmuunit'] = $this->db->query("SELECT unit.*, state.state_name, district.dist_name
                FROM mmu_unit AS unit
                JOIN state_table AS state ON state.state_id = unit.state_id
                JOIN district_table AS district ON district.dist_id = unit.dist_id
                WHERE unit.status = 0")->result_array();
        $this->load->view('backend/index', $data);
    }

    function add_mmu() {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['page_name'] = 'add_mmu';
        $data['page_title'] = get_phrase('add_MMU');
        $data['states'] = $this->db->query("SELECT * FROM state_table WHERE state_status = 0")->result_array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('state','State Name','required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('backend/index', $data);
        }
        else {
            $data = array(
                'state_id' => $this->input->post('state'),
                'dist_id' => $this->input->post('district'),
                'city_town' => $this->input->post('city'),
                'panchayat_municipality' => $this->input->post('panchayat'),
                'mmu_name' => $this->input->post('mmu_name'),
                'mmu_number' => $this->input->post('mmu_number'),
                'date' => date('Y-m-d H:i:s'),
                'ip_address' => $this->input->ip_address(),
                'status' => 0
            );
            $query = $this->db->insert('mmu_unit', $data);
            if($query) {
                $this->session->set_flashdata("succ", "MMU Unit added Successfully.");
            }
            else {
                $this->session->set_flashdata("err", "Sorry! System not adding");
            }
            redirect($baseurl.'admin/add_mmu');
        }
    }

    function edit_mmu($mmu_id){
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $data['states'] = $this->db->query("SELECT * FROM state_table WHERE state_status = 0")->result_array();
        $data['district'] = $this->db->query("SELECT * FROM district_table WHERE status = 0")->result_array();
        $data['getmmuby_id'] = $this->db->query("SELECT * FROM mmu_unit WHERE unit_id = '".$mmu_id."' ")->row();
        $data['page_name'] = 'edit_mmu';
        $data['page_title'] = get_phrase('edit_mmu');
        $this->load->view('backend/index', $data);
    }

    function updatemmu(){
         if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $error = array('state' => '', 'district' => '', 'city' => '', 'panchayat' => '', 'mmu_number' => '','status' => 0);
            if(isset($_POST['mmu_id'])) {
                if(empty($_POST['state'])) {
                    $error['state'] = 'state Field is mandatory';
                    $error['status'] = 1;
                }
                if(empty($_POST['district'])) {
                    $error['district'] = 'District Field is mandatory';
                    $error['status'] = 1;
                }                
                if(empty($_POST['city'])) {
                    $error['city'] = 'City Field is mandatory';
                    $error['status'] = 1;
                }
                if(empty($_POST['panchayat'])) {
                    $error['panchayat'] = 'Panchayat Field is mandatory';
                    $error['status'] = 1;
                }
                if(empty($_POST['mmu_number'])) {
                    $error['mmu_number'] = 'mmu_number Field is mandatory';
                    $error['status'] = 1;
                }
               
                if($error['status'] > 0) {
                    echo json_encode($error);
                    exit();
                }
                $data = array(
                    'state_id' => $_POST['state'],
                    'dist_id' => $_POST['district'],
                    'city_town' => $_POST['city'],
                    'panchayat_municipality' => $_POST['panchayat'],
                    'mmu_name' => $_POST['mmu_name'],
                    'mmu_number' => $_POST['mmu_number']
                );
                $query = $this->db->where('unit_id', $_POST['mmu_id'])->update('mmu_unit', $data);
                if($query){
                    echo json_encode(array('msg' => 'mmu Updated Successfully'));
                    exit();
                }
                else{
                    echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
                    exit();
                }
            }
            else {
                echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
                exit();
            }
    }

    function mmu_location() {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $unit_id = $this->uri->segment(3);
        if (empty($unit_id)) {
            show_404();
        }

        $data['page_name'] = 'mmu_location';
        $data['page_title'] = get_phrase('MMU_location');

        $data['mmulocation'] = $this->db->query("SELECT locations.*, unit.mmu_name
            FROM mmu_locations AS locations
            JOIN mmu_unit AS unit ON unit.unit_id = locations.mmu_unit
            WHERE locations.mmu_unit = '".$unit_id."' AND locations.mmuloc_status = 0")->result_array();

        $this->load->view('backend/index', $data);
    }


    function add_mmu_location() {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['page_name'] = 'add_mmu_location';
        $data['page_title'] = get_phrase('add_MMU_site');
        $data['mmuunit'] = $this->db->query("SELECT * FROM mmu_unit WHERE status = 0")->result_array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('mmuunit','MMU','required');
        $this->form_validation->set_rules('mmu_location','MMU Site Name','required');
        if ($this->form_validation->run() == FALSE)
        {
        $this->load->view('backend/index', $data);
        }
        else {
            $data = array(
                'mmu_unit' => $this->input->post('mmuunit'),
                'mmuloc_name' => $this->input->post('mmu_location'),
                'mmuloc_status' => 0
            );
            $query = $this->db->insert('mmu_locations', $data);
            if($query){
                $this->session->set_flashdata("succ", "MMU Location added Successfully.");
            }else{
                $this->session->set_flashdata("err", "Sorry! System not adding");
            }
            redirect($baseurl.'admin/add_mmu_location');
        }
    }

    function edit_mmu_location($mmu_location){
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $data['mmulocation_details'] = $this->db->query("SELECT * FROM mmu_locations WHERE mmuloc_id = '".$mmu_location."' ")->row();
        $data['mmuunit'] = $this->db->query("SELECT * FROM mmu_unit WHERE status = 0")->result_array();
        $data['page_name'] = 'edit_mmu_location';
        $data['page_title'] = get_phrase('edit_MMU_location');
        $this->load->view('backend/index', $data);
    }

    function updatemmu_location(){
         if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $error = array('state' => '', 'mmu_number' => '','status' => 0);
            if(isset($_POST['mmu_location_id'])) {
                if(empty($_POST['mmuunit'])) {
                    $error['mmuunit'] = 'MMU unit Field is mandatory';
                    $error['status'] = 1;
                }
                if(empty($_POST['mmu_location'])) {
                    $error['mmu_location'] = 'MMU Location Field is mandatory';
                    $error['status'] = 1;
                }                
                if($error['status'] > 0) {
                    echo json_encode($error);
                    exit();
                }
                $data = array(
                    'mmu_unit' => $_POST['mmuunit'],
                    'mmuloc_name' => $_POST['mmu_location']
                );
                $query = $this->db->where('mmuloc_id', $_POST['mmu_location_id'])->update('mmu_locations', $data);
                if($query){
                    echo json_encode(array('msg' => 'MMU Location Updated Successfully'));
                    exit();
                }
                else{
                    echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
                    exit();
                }
            }
            else {
                echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
                exit();
            }
    }

    function add_doctor() {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['page_name'] = 'add_doc';
        $data['page_title'] = get_phrase('add_doctor');
        $data['allmmuunit'] = $this->db->query("SELECT * FROM mmu_unit WHERE status = 0")->result_array();
        $data['alldepartment'] = $this->db->query("SELECT * FROM department WHERE status = 0")->result_array();
        $this->load->view('backend/index', $data);
        /*$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('name','Name','required|callback_alpha_dash_space');
        $this->form_validation->set_rules('mrc','Mrc No.','required|alpha');
        $this->form_validation->set_rules('email','Email-ID','required|valid_email|callback_isEmailExistdoc');
        $this->form_validation->set_rules('pass','Password','required');
        $this->form_validation->set_rules('phone','Phone','required');
        $this->form_validation->set_rules('mmu','MMU Name','required');
        $this->form_validation->set_rules('department','Department','required');
        if ($this->form_validation->run() == FALSE)
        {
        $this->load->view('backend/index', $data);
        }
        else {

        	if(isset($_FILES['image']) && $_FILES['image']['size'] > 0) {
                    $timestamp = new DateTime();
                    $timestamp = $timestamp->format('U');
                    $imagename = $timestamp. '_' .$_FILES['image']['name'];
                    
                    move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/doctor_image/' .$imagename);
            }
            $data = array(
                    'name' => $this->input->post('name'),
                    'mrc_no' => $this->input->post('mrc'),
                    'email' => $this->input->post('email'),
                    'password' => sha1($this->input->post('pass')),
                    'address' => $this->input->post('address'),
                    'phone' => $this->input->post('phone'),
                    'department_id' => $this->input->post('department'),
                    'mmu_unit' => $this->input->post('mmu'),
                    'profile_image' => isset($imagename) ? $imagename : 'default.png',
                    'profile' => $this->input->post('profile'),
                    'date' => date('Y-m-d H:i:s'),
                    'ip_address' => $this->input->ip_address(),
                    'role_id' => 1,
                    'status' => 0
                );
                $query = $this->db->insert('user_details', $data);
                if($query)
                {
                    $this->session->set_flashdata("succ", "Doctor added Successfully.");
                    
                }
                else
                {
                    $this->session->set_flashdata("err", "Sorry! System not adding");
                }
                redirect($baseurl.'admin/add_doctor');
        }*/

    }

    public function insert_doctor(){
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $error = array('name' => '', 'mrc' => '', 'email' => '', 'pass' => '', 'mmu' => '', 'department' => '', 'address' => '', 'phone' => '', 'image' => '', 'status' => 0);            
        if(empty($_POST['name'])) {
            $error['name'] = 'Name Field is mandatory';
            $error['status'] = 1;
        }
        else if(!preg_match('/^[a-z][a-z \.]*$/i',$_POST['name'])) {
            $error['name'] = 'Please input alfa characters only';
            $error['status'] = 1;
        }
        else if(strlen($_POST['name']) > 150) {
            $error['name'] = 'Name Field must be within 150 characters';
            $error['status'] = 1;
        }

        if(empty($_POST['mrc'])) {
            $error['mrc'] = 'Mrc Field is mandatory';
            $error['status'] = 1;
        }
        else if(!preg_match('/^[a-zA-Z0-9]+$/',$_POST['mrc'])) {
            $error['mrc'] = 'Please input alfanumeric characters only';
            $error['status'] = 1;
        }
        else if(strlen($_POST['mrc']) > 150) {
            $error['mrc'] = 'MRC Field must be within 150 characters';
            $error['status'] = 1;
        }
        if(!empty($_POST['mrc'])){
            $this->load->library('form_validation');
            $this->db->select('user_id');
            $this->db->where('mrc_no', $_POST['mrc']);
            $query = $this->db->get('user_details');
            if ($query->num_rows() > 0) {
                $error['mrc'] = 'Mrc is already in use.';
                $error['status'] = 1;
            }
        }
        if(empty($_POST['email'])) {
            $error['email'] = 'Please input valid email only';
            $error['status'] = 1;
        }
        else if(!preg_match('/@.+\./',$_POST['email'])) {
            $error['email'] = 'Please enter valid Email-ID';
            $error['status'] = 1;
        }
        else if(!empty($_POST['email'])){
            $this->load->library('form_validation');
            $this->db->select('user_id');
            $this->db->where('email', $_POST['email']);
            $query = $this->db->get('user_details');
            if ($query->num_rows() > 0) {
                $error['email'] = 'Entered email id is already registered.';
                $error['status'] = 1;
            }
        }          
        if(empty($_POST['pass'])) {
            $error['pass'] = 'Password Field is mandatory';
            $error['status'] = 1;
        }

        if(empty($_POST['phone'])) {
            $error['phone'] = 'Phone No Field is mandatory';
            $error['status'] = 1;
        }
        else if(!is_numeric($_POST['phone'])) {
            $error['phone'] = 'Phone No Field must be Numeric';
            $error['status'] = 1;
        }
        else if(strlen($_POST['phone']) < 10) {
            $error['phone'] = 'Phone No must be within 10 to 15 digits';
            $error['status'] = 1;
        }
        else if(strlen($_POST['phone']) > 15) {
            $error['phone'] = 'Phone No must be within 10 to 15 digits';
            $error['status'] = 1;
        }
        else if(!empty($_POST['phone'])){
            $this->load->library('form_validation');
            $this->db->select('user_id');
            $this->db->where('phone', $_POST['phone']);
            $query = $this->db->get('user_details');
            if ($query->num_rows() > 0) {
                $error['phone'] = 'Phone No is already Used.';
                $error['status'] = 1;
            }
        }
        if(empty($_POST['mmu'])) {
            $error['mmu'] = 'Mmu Field is mandatory';
            $error['status'] = 1;
        }
        if(empty($_POST['department'])) {
            $error['department'] = 'Department Field is mandatory';
            $error['status'] = 1;
        }
        if($error['status'] > 0) {
            echo json_encode($error);
            exit();
        }
        if(isset($_FILES['image']) && $_FILES['image']['size'] > 0) {
            $timestamp = new DateTime();
            $timestamp = $timestamp->format('U');
            $imagename = $timestamp. '_' .$_FILES['image']['name'];
            move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/doctor_image/' .$imagename);
        }                
        $data = array(
            'name' => $_POST['name'],
            'mrc_no' => $_POST['mrc'],
            'email' => $_POST['email'],
            'password' => sha1($_POST['pass']),
            'address' => $_POST['address'],
            'phone' => $_POST['phone'],
            'mmu_unit' => $_POST['mmu'],
            'department_id' => $_POST['department'],
            'profile_image' => isset($imagename) ? $imagename : 'default.png',
            'profile' => $this->input->post('profile'),
            'date' => date('Y-m-d H:i:s'),
            'ip_address' => $this->input->ip_address(),
            'role_id' => 1,
            'status' => 0
        );
        $query = $this->db->insert('user_details', $data);
        if($query){
            echo json_encode(array('msg' => 'Doctor Added Successfully'));
            exit();
        }
        else{
            echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
            exit();
        }
    }

//SPO
    function spo() {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        
        $data['page_name'] = 'spo';
        $data['page_title'] = get_phrase('SPO');
        $data['allspo'] = $this->db->query("SELECT spou.*, unit.mmu_name
                FROM user_details AS spou
                JOIN mmu_unit AS unit ON unit.unit_id = spou.mmu_unit
                WHERE spou.status = 0 and role_id = 2")->result_array();
        $this->load->view('backend/index', $data);
    }


    function add_spo() {
        if($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        
        $data['page_name'] = 'add_spo';
        $data['page_title'] = get_phrase('Add SPO');
        $data['allmmuunit'] = $this->db->query("SELECT unit.*, state.state_name, district.dist_name
                FROM mmu_unit AS unit
                JOIN state_table AS state ON state.state_id = unit.state_id
                JOIN district_table AS district ON district.dist_id = unit.dist_id
                WHERE unit.status = 0")->result_array();
        $this->load->view('backend/index', $data);
        /*$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('name','Name','required|callback_alpha_dash_space');
        $this->form_validation->set_rules('email','Email-ID','required|valid_email|callback_isEmailExistspo');
        $this->form_validation->set_rules('pass','Password','required');
        $this->form_validation->set_rules('phone','Phone','required|callback_check_mobileno');
        $this->form_validation->set_rules('mmu','MMU Name','required');
        $this->form_validation->set_rules('address','Address','required');
        if ($this->form_validation->run() == FALSE)
        {
        $this->load->view('backend/index', $data);
        }
        else {
            date_default_timezone_set("UTC");
            if(isset($_FILES['image']) && $_FILES['image']['size'] > 0) {
                    $timestamp = new DateTime();
                    $timestamp = $timestamp->format('U');
                    $imagename = $timestamp. '_' .$_FILES['image']['name'];
                    move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/spo_image/' .$imagename);
                }
            $data = array(
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'password' => sha1($this->input->post('pass')),
                'address' => $this->input->post('address'),
                'phone' => $this->input->post('phone'),
                'mmu_unit' => $this->input->post('mmu'),
                'profile_image' => isset($imagename) ? $imagename : 'default.png',
                'profile' => $this->input->post('profile'),
                'date' => date('Y-m-d H:i:s'),
                'ip_address' => $this->input->ip_address(),
                'role_id' => 2,
                'status' => 0
            );
            $query = $this->db->insert('user_details', $data);
            if($query)
            {
                $this->session->set_flashdata("succ", "SPO Information saved Successfully.");
                
            }
            else
            {
                $this->session->set_flashdata("err", "Sorry! System not adding");
            }
            redirect($baseurl.'admin/add_spo');
        }*/
    }

    function insert_spo(){
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $error = array('name' => '', 'email' => '', 'pass' => '', 'mmu' => '', 'phone' => '','status' => 0);            
        if(empty($_POST['name'])) {
            $error['name'] = 'Name Field is mandatory';
            $error['status'] = 1;
        }
        else if(!preg_match('/^[a-z][a-z \.]*$/i',$_POST['name'])) {
            $error['name'] = 'Please input alfa characters only';
            $error['status'] = 1;
        }
        else if(strlen($_POST['name']) > 150) {
            $error['name'] = 'Name Field must be within 150 characters';
            $error['status'] = 1;
        }
        if(empty($_POST['email'])) {
            $error['email'] = 'Email-ID Field is mandatory';
            $error['status'] = 1;
        }
        else if(!preg_match('/@.+\./',$_POST['email'])) {
            $error['email'] = 'Please enter valid Email-ID';
            $error['status'] = 1;
        }
        else if(!empty($_POST['email'])){
            $this->load->library('form_validation');
            $this->db->select('user_id');
            $this->db->where('email', $_POST['email']);
            $query = $this->db->get('user_details');
            if ($query->num_rows() > 0) {
                $error['email'] = 'Entered email id is already registered.';
                $error['status'] = 1;
            }
        }          
        if(empty($_POST['pass'])) {
            $error['pass'] = 'Password Field is mandatory';
            $error['status'] = 1;
        }
        if(empty($_POST['mmu'])) {
            $error['mmu'] = 'Mmu Field is mandatory';
            $error['status'] = 1;
        }
        if(empty($_POST['phone'])) {
            $error['phone'] = 'Phone No Field is mandatory';
            $error['status'] = 1;
        }
        else if(!is_numeric($_POST['phone'])) {
            $error['phone'] = 'Phone No Field must be Numeric';
            $error['status'] = 1;
        }
        else if(strlen($_POST['phone']) < 10) {
            $error['phone'] = 'Phone No must be within 10 to 15 digits';
            $error['status'] = 1;
        }
        else if(strlen($_POST['phone']) > 15) {
            $error['phone'] = 'Phone No must be within 10 to 15 digits';
            $error['status'] = 1;
        }
        else if(!empty($_POST['phone'])){
            $this->load->library('form_validation');
            $this->db->select('user_id');
            $this->db->where('phone', $_POST['phone']);
            $query = $this->db->get('user_details');
            if ($query->num_rows() > 0) {
                $error['phone'] = 'Phone No is already Used.';
                $error['status'] = 1;
            }
        }
        
        if($error['status'] > 0) {
            echo json_encode($error);
            exit();
        }
        if(isset($_FILES['image']) && $_FILES['image']['size'] > 0) {
            $timestamp = new DateTime();
            $timestamp = $timestamp->format('U');
            $imagename = $timestamp. '_' .$_FILES['image']['name'];
            move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/spo_image/' .$imagename);
        }                
        $data = array(
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'password' => sha1($_POST['pass']),
            'address' => $_POST['address'],
            'phone' => $_POST['phone'],
            'mmu_unit' => $_POST['mmu'],
            'profile_image' => isset($imagename) ? $imagename : 'default.png',
            'profile' => $this->input->post('profile'),
            'date' => date('Y-m-d H:i:s'),
            'ip_address' => $this->input->ip_address(),
            'role_id' => 2,
            'status' => 0
        );
        $query = $this->db->insert('user_details', $data);
        if($query){
            echo json_encode(array('msg' => 'Spo Added Successfully'));
            exit();
        }
        else{
            echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
            exit();
        }
    }


    function pharmacist() {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['pharmacist_info'] = $this->db->query("SELECT pharm.*, unit.mmu_name
                FROM user_details AS pharm
                JOIN mmu_unit AS unit ON unit.unit_id = pharm.mmu_unit
                WHERE pharm.status = 0 and role_id = 3")->result_array();
        $data['page_name'] = 'manage_pharmacist';
        $data['page_title'] = get_phrase('pharmacist');
        $this->load->view('backend/index', $data);
    }

    function add_pharmacist() {
        if($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }        
        $data['page_name'] = 'add_pharmacist';
        $data['page_title'] = get_phrase('add_pharmacist');
        $data['allmmuunit'] = $this->db->query("SELECT unit.*, state.state_name, district.dist_name
                FROM mmu_unit AS unit
                JOIN state_table AS state ON state.state_id = unit.state_id
                JOIN district_table AS district ON district.dist_id = unit.dist_id
                WHERE unit.status = 0")->result_array();
        $this->load->view('backend/index', $data);
    }

    public function insert_pharmacist(){
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $error = array('name' => '', 'email' => '', 'pass' => '', 'mmu' => '', 'address' => '', 'phone' => '', 'status' => 0);            
		        if(empty($_POST['name'])) {
		            $error['name'] = 'Name Field is mandatory';
		            $error['status'] = 1;
		        }
		        else if(!preg_match('/^[a-z][a-z \.]*$/i',$_POST['name'])) {
		            $error['name'] = 'Please input alfa characters only';
		            $error['status'] = 1;
		        }
		        else if(strlen($_POST['name']) > 150) {
		            $error['name'] = 'Name Field must be within 150 characters';
		            $error['status'] = 1;
		        }
                if(empty($_POST['email'])) {
                    $error['email'] = 'Email-ID Field is mandatory';
                    $error['status'] = 1;
                }
                else if(!preg_match('/@.+\./',$_POST['email'])) {
		            $error['email'] = 'Please enter valid Email-ID';
		            $error['status'] = 1;
		        }
                else if(!empty($_POST['email'])){
                    $this->load->library('form_validation');
                    $this->db->select('user_id');
                    $this->db->where('email', $_POST['email']);
                    $query = $this->db->get('user_details');
                    if ($query->num_rows() > 0) {
                        $error['email'] = 'Entered email id is already registered.';
                        $error['status'] = 1;
                    }
                }          
                if(empty($_POST['pass'])) {
                    $error['pass'] = 'Password Field is mandatory';
                    $error['status'] = 1;
                }
                if(empty($_POST['mmu'])) {
                    $error['mmu'] = 'Mmu Field is mandatory';
                    $error['status'] = 1;
                }
                /*if(empty($_POST['address'])) {
                    $error['address'] = 'Address Field is mandatory';
                    $error['status'] = 1;
                }
                else if(strlen($_POST['address']) > 500) {
                    $error['address'] = 'Address Field must be within 500 characters';
                    $error['status'] = 1;
                }*/
                if(empty($_POST['phone'])) {
                    $error['phone'] = 'Phone No Field is mandatory';
                    $error['status'] = 1;
                }
                else if(!is_numeric($_POST['phone'])) {
                    $error['phone'] = 'Phone No Field must be Numeric';
                    $error['status'] = 1;
                }
                else if(strlen($_POST['phone']) < 10) {
                    $error['phone'] = 'Phone No must be within 10 to 15 digits';
                    $error['status'] = 1;
                }
                else if(strlen($_POST['phone']) > 15) {
                    $error['phone'] = 'Phone No must be within 10 to 15 digits';
                    $error['status'] = 1;
                }
                if($error['status'] > 0) {
                    echo json_encode($error);
                    exit();
                }
                if(isset($_FILES['image']) && $_FILES['image']['size'] > 0) {
                    $timestamp = new DateTime();
                    $timestamp = $timestamp->format('U');
                    $imagename = $timestamp. '_' .$_FILES['image']['name'];
                    move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/pharmacist_image/' .$imagename);
                }                
                $data = array(
                    'name' => $_POST['name'],
                    'email' => $_POST['email'],
                    'password' => sha1($_POST['pass']),
                    'address' => $_POST['address'],
                    'phone' => $_POST['phone'],
                    'mmu_unit' => $_POST['mmu'],
                    'profile_image' => isset($imagename) ? $imagename : 'default.png',
                    'profile' => $this->input->post('profile'),
                    'date' => date('Y-m-d H:i:s'),
                    'ip_address' => $this->input->ip_address(),
                    'role_id' => 3,
                    'status' => 0
                );
                $query = $this->db->insert('user_details', $data);
                if($query){
                    echo json_encode(array('msg' => 'Pharmacist Added Successfully'));
                    exit();
                }
                else{
                    echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
                    exit();
                }
    }

    function medicine() {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['medicine_info'] = $this->db->query("SELECT m.medicine_id, m.name,
                                    (SELECT SUM(no_of_unit)
                                      FROM medicine_store
                                     WHERE medicine_id = m.medicine_id
                                   ) AS no_of_unit, (SELECT (sum(no_of_unit) - sum(IFNULL(return_unit, 0))) FROM `van_stock` WHERE medicine_id = m.medicine_id) AS units_sold
                                   FROM medicine AS m")->result_array();

        $data['page_name'] = 'manage_medicine';
        $data['page_title'] = get_phrase('medicine');
        $this->load->view('backend/index', $data);
    }

    function add_medicine() {
        if($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        date_default_timezone_set('Asia/Kolkata');
        if(isset($_POST['submit'])) {
            $medicine = $this->db->where('medicine_id', $this->input->post('name'))->get('medicine');
            if($medicine->num_rows() == 0) {
                $this->db->insert('medicine', array(
                    'name' => $this->input->post('name'),
                    'preparation' => $this->input->post('preparation'),
                    'composition' => $this->input->post('composition'),
                    'date' => date('Y-m-d H:i:s'),
                    'ip_address' => $this->input->ip_address()
                ));
                $medicine_id = $this->db->insert_id();
            } else {
                $medicine_id = $this->input->post('name');
            }

            if(!isset($_POST['expiry']) || $_POST['expiry'] == NULL) {
                $batch_no = $this->input->post('batch');
                $query = $this->db->query("SELECT DISTINCT exp_date  
                FROM `medicine_store` 
                WHERE `batch_no` = '".$batch_no."' and medicine_id = '".$medicine_id."'")->row_array();
                $expiry = $query['exp_date'];
            } else {
                $batch_no = $this->input->post('batch');
                $expiry = $this->input->post('expiry');
            }

            $query = $this->db->insert('medicine_store', array(
                'medicine_id' => $medicine_id,
                'mmu_id' => $this->input->post('mmuunit'),
                'no_of_unit' => $this->input->post('unit'),
                'batch_no' => $batch_no,
                'exp_date' => $expiry,
                'manufacturing_company' => $this->input->post('company'),
                'date' => date('Y-m-d H:i:s'),
                'ip_address' => $this->input->ip_address(),
                'status ' => 0
            ));
            if($query){
                $this->session->set_flashdata("succ", "Medicine Added Successfully.");                
            }else{
                $this->session->set_flashdata("err", "Sorry! System not adding");
            }
            redirect($baseurl.'admin/add_medicine');
        }

        $data['mmuunit'] = $this->db->query("SELECT * FROM mmu_unit WHERE status = 0")->result_array();
        $data['page_name'] = 'add_medicine';
        $data['page_title'] = get_phrase('add_medicine');

        $this->load->view('backend/index', $data);
        
        /*$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('name','Name','required');
        $this->form_validation->set_rules('composition','Composition','required');
        $this->form_validation->set_rules('preparation','Preparation','required');
        $this->form_validation->set_rules('mmuunit','mmuunit','required');
        $this->form_validation->set_rules('unit','No of Unit','required');
        $this->form_validation->set_rules('batch','Batch','required');
        $this->form_validation->set_rules('company','Company','required');
        if ($this->form_validation->run() == FALSE)
        {
        $this->load->view('backend/index', $data);
        }
        else {
            exit();
            $medicine = $this->db->where('medicine_id', $this->input->post('name'))->get('medicine');
            if($medicine->num_rows() == 0) {
                $this->db->insert('medicine', array(
                    'name' => $this->input->post('name'),
                    'preparation' => $this->input->post('preparation'),
                    'composition' => $this->input->post('composition'),
                    'date' => date('Y-m-d H:i:s'),
                    'ip_address' => $this->input->ip_address()
                ));
                $medicine_id = $this->db->insert_id();
            } else {
                $medicine_id = $this->input->post('name');
            }
            if($this->input->post('expiry') != ''){
                $date = date('Y-m-d', strtotime($this->input->post('expiry')));
            }else if($this->input->post('dbexpiry') != ''){
                $date = $this->input->post('dbexpiry');
            }
            $query = $this->db->insert('medicine_store', array(
                'medicine_id' => $medicine_id,
                'mmu_id' => $this->input->post('mmuunit'),
                'no_of_unit' => $this->input->post('unit'),
                'batch_no' => $this->input->post('batch'),
                'exp_date' => date('Y-m-d', strtotime($this->input->post('expiry'))),
                'manufacturing_company' => $this->input->post('company'),
                'date' => date('Y-m-d H:i:s'),
                'ip_address' => $this->input->ip_address(),
                'status ' => 0
            ));
            if($query){
                $this->session->set_flashdata("succ", "Medicine Added Successfully.");                
            }else{
                $this->session->set_flashdata("err", "Sorry! System not adding");
            }
            redirect($baseurl.'admin/add_medicine');
        }*/
    }

    function get_medicine() {
        if ($this->session->userdata('admin_login') != 1) {
            echo json_encode(array(
                'items' => array(),
                'total_count' => 0,
                'remaining' => 0
            ));
            exit();
        }

        $data = $this->input->post();
        $start = ($data['page']-1)*10;
        $limit = $data['page']*10;

        if(isset($data['q'])) {
            $query = $this->db->query("SELECT medicine.medicine_id as id, medicine.name, medicine.composition, medicine.preparation
            FROM medicine
            WHERE medicine.name LIKE '".$data['q']."%'");
        } else {
            $query = $this->db->query("SELECT medicine.medicine_id as id, medicine.name, medicine.composition, medicine.preparation
            FROM medicine");
        }

        if($query->num_rows() > 0){
            echo json_encode(array(
                'items' => $query->result_array(),
                'total_count' => $query->num_rows(),
                'remaining' => $query->num_rows()>9 ? 1 : 0
            ));
            exit();
        }
        else{
            echo json_encode(array(
                'items' => array(),
                'total_count' => 0,
                'remaining' => 0
            ));
            exit();
        }
        /*$data['pharmacist_info'] = $this->crud_model->select_pharmacist_info();*/
        /*$data['pharmacist_info'] = $this->db->query("SELECT pharm.*, unit.mmu_name
                FROM pharmacist AS pharm
                JOIN mmu_unit AS unit ON unit.unit_id = pharm.mmu_unit
                WHERE pharm.status = 0")->result_array();*/
    }

    function get_medicinebatch(){
        if ($this->session->userdata('admin_login') != 1) {
            echo json_encode(array(
                'items' => array(),
                'total_count' => 0,
                'remaining' => 0
            ));
            exit();
        }

        $data = $this->input->post();
        $start = ($data['page']-1)*10;
        $limit = $data['page']*10;

        $keyword = $data['q'];
        $val = $data['med'];

        $query = $this->db->query("SELECT DISTINCT batch_no as id, batch_no, exp_date  
        FROM `medicine_store` 
        WHERE `batch_no` LIKE '".$keyword."%' and medicine_id = '".$val."'");

        if($query->num_rows() > 0){
            echo json_encode(array(
                'items' => $query->result_array(),
                'total_count' => $query->num_rows(),
                'remaining' => 0
            ));
            exit();
        }
        else{
            echo json_encode(array(
                'items' => array(),
                'total_count' => 0,
                'remaining' => 0
            ));
            exit();
        }
    }

    function medicine_stock() {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $medicine_id = $this->uri->segment(3);
        if (empty($medicine_id)) {
            show_404();
        }

        $data['medicine_stock'] = $this->db->query("SELECT ms.*, med.name ,med.preparation
                FROM medicine_store AS ms
                JOIN medicine AS med ON med.medicine_id = ms.medicine_id
                WHERE ms.medicine_id = '".$medicine_id."'")->result_array();
        $data['page_name'] = 'medicine_stock';
        $data['page_title'] = get_phrase('medicine_stock');
        $this->load->view('backend/index', $data);
    }

    function medicine_sold() {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $medicine_id = $this->uri->segment(3);
        if (empty($medicine_id)) {
            show_404();
        }
        $data['allmedicine'] = $this->db->query("SELECT tm.*, med.name as med_name, pat.name as patname
        	FROM treatment_medicine as tm
        	JOIN medicine as med ON med.medicine_id = tm.medicine_id
        	JOIN patient as pat ON pat.patient_id = tm.patient_id")->result_array();
        $data['page_name'] = 'medicine_sold';
        $data['page_title'] = get_phrase('medicine_sold');
        $this->load->view('backend/index', $data);
    }

    function medicine_update() {
        if($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $medicine_id = $this->uri->segment(3);
        if (empty($medicine_id)) {
            show_404();
        }

        $data['page_name'] = 'edit_medicine';
        $data['page_title'] = get_phrase('edit_medicine');
        $data['medicine'] = $this->db->query("SELECT * FROM medicine WHERE medicine_id = $medicine_id AND status = 0")->row_array();
        $this->load->view('backend/index', $data);
    }    

    function updmedicine() {
        if($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $error = array('name' => '', 'preparation' => '', 'composition' => '', 'status' => 0);
        if(isset($_POST['medicine_id'])) {
            if(empty($_POST['name'])) {
                $error['name'] = 'Name Field is mandatory';
                $error['status'] = 1;
            }
            if(empty($_POST['preparation'])) {
                $error['preparation'] = 'Preparation Field is mandatory';
                $error['status'] = 1;
            }
            if(empty($_POST['composition'])) {
                $error['composition'] = 'composition Field is mandatory';
                $error['status'] = 1;
            }

            if($error['status'] > 0) {
                echo json_encode($error);
                exit();
            }

            $data = array(
                'name' => $_POST['name'],
                'preparation' => $_POST['preparation'],
                'composition' => $_POST['composition']
            );
            $query = $this->db->where('medicine_id', $_POST['medicine_id'])->update('medicine', $data);
            if($query){
                echo json_encode(array('msg' => 'Medicine Updated Successfully'));
                exit();
            }
            else{
                echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
                exit();
            }
        }
        else {
            echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
            exit();
        }
    }

    public function isEmailExistdoc($email) {
        $this->load->library('form_validation');
        $this->db->select('doctor_id');
        $this->db->where('email', $email);
        $query = $this->db->get('doctor');
        if ($query->num_rows() > 0) {
            $this->form_validation->set_message('isEmailExistdoc', 'Entered email id is already registered.');
            return false;
        } else {
            return true;
        }
    }

    public function isEmailExistspo($email) {
        $this->load->library('form_validation');
        $this->db->select('spo_id');
        $this->db->where('email', $email);
        $query = $this->db->get('spo');
        if ($query->num_rows() > 0) {
            $this->form_validation->set_message('isEmailExistspo', 'Entered email id is already registered.');
            return false;
        } else {
            return true;
        }
    }

    public function isEmailExistpharm($email) {
        $this->load->library('form_validation');
        $this->db->select('pharmacist_id');
        $this->db->where('email', $email);
        $query = $this->db->get('pharmacist');
        if ($query->num_rows() > 0) {
            $this->form_validation->set_message('isEmailExistpharm', 'Entered email id is already registered.');
            return false;
        } else {
            return true;
        }
    }

    function get_district() {
        if ($this->session->userdata('admin_login') != 1) {
            echo json_encode(array(
                'districts' => array()
            ));
            exit();
        }
        $districts = $this->db->where('state_id', $_POST['state'])->get('district_table')->result_array();
        echo json_encode(array(
            'districts' => $districts
        ));
        exit();
    }

    function make_default() {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $doctor_id = $this->uri->segment(3);
        if (empty($doctor_id)) {
            show_404();
        }

        $data['mmu'] = $this->db->query("SELECT mmu_unit FROM doctor WHERE doctor_id = '".$doctor_id."'")->row_array();
        $data['mmu_location'] = $this->db->query("SELECT * FROM mmu_locations WHERE mmu_unit = '".$data['mmu']['mmu_unit']."'")->result_array();
        $data['page_name'] = 'default_location';
        $data['page_title'] = get_phrase('Location');
        $this->load->view('backend/index', $data);
    }

    function add_defaultdoctor() {
        if($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $error = array('name' => '', 'status' => 0);
        if(isset($_POST['doctor_id'])) {
            if(empty($_POST['name'])) {
                $error['name'] = 'Name Field is mandatory';
                $error['status'] = 1;
            }

            if($error['status'] > 0) {
                echo json_encode($error);
                exit();
            }
            $check = $this->db->query("SELECT location_id FROM default_doctor WHERE location_id = '".$_POST['name']."'");
            if ($check->num_rows() > 0) {
                echo json_encode(array('msg' => 'one doctor is working as default on this location. please choose another location.'));
                exit();
            }
            $data = array(
                'doc_id' => $_POST['doctor_id'],
                'location_id' => $_POST['name']
            );
            $query = $this->db->insert('default_doctor', $data);
            if($query){
                echo json_encode(array('msg'=>'Default doctor added Successfully.'));
                exit();
            }
            else{
                $this->session->set_flashdata("err", "Sorry! System not adding");
            }
        }
        else{
            echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
            exit();
        }
    }

    function view_analytics() {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $data['mmu'] = $this->db->query("SELECT DISTINCT mmu_name FROM mmu_unit")->result_array();
        $data['mmu_name'] = $this->db->query("SELECT DISTINCT mmu_name FROM mmu_unit")->result_array();
        $mmu_location = $this->db->query("SELECT ml.*
                FROM mmu_locations AS ml")->result_array();
                foreach ($mmu_location as $key => $d) {
                    $patientcount = $this->db->query("SELECT COUNT(patient_id) as patcount 
                            FROM patient 
                            WHERE mmuloc = '".$d['mmuloc_id']."' ")->row_array();
                    $mmu_location[$key]['patcount'] = $patientcount;
                }
        $data['mmu_med'] = $this->db->query("SELECT sum(tm.quantity) as quantity, pat.mmu, mu.mmu_name
                FROM treatment_medicine AS tm
                JOIN patient AS pat ON pat.patient_id = tm.patient_id
                JOIN mmu_unit AS mu ON mu.unit_id = pat.mmu
                GROUP BY pat.mmu")->result_array();

        $data['mmu_location'] = $mmu_location;
        $data['no_title']  = true;
        $data['page_name']  = 'view_analytics';
        $data['page_title'] = get_phrase('view_analytics');
        $this->load->view('backend/index', $data);
    }

    function view_photos() {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $data['post'] = $this->db->query("SELECT * FROM post_image WHERE image_status = 1 limit 50")->result_array();
        $data['mmu_loc'] = $this->db->query("SELECT * FROM mmu_unit")->result_array();
        $data['no_title']  = true;
        $data['page_name']  = 'view_photos';
        $data['page_title'] = get_phrase('view_photos');
        $this->load->view('backend/index', $data);
    }

    public function filter_image(){
        $mmu_id = $this->input->post('mmu_name');
        $data = $this->db->query("SELECT ud.*, ud.user_id, po.post_id, pi.image_name
                FROM user_details AS ud
                JOIN post AS po ON po.user_id = ud.user_id
                JOIN post_image AS pi ON pi.post_id = po.post_id
                WHERE ud.mmu_unit = '".$mmu_id."'")->result_array();?>
        <?php
            foreach ($data as $key => $img) {
                echo '<div style="width: 10%; height: 130px; background-color: white; float: left;">';
                echo '<img src="uploads/post_image/'.$img['image_name'].'" style="width:100%; height: 130px; float: left; padding-right:5px; padding-top: 5px; float:left"/>';
                echo '</div>';
        }     
    }

    function wall() {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $data['posts'] = $this->db->query("SELECT DISTINCT post.*, user.name, user.profile_image AS image FROM post
            left join user_details AS user ON user.user_id = post.user_id
            WHERE post.status = 1
            ORDER BY post.post_id DESC
            LIMIT 2")->result_array();

        foreach ($data['posts'] as $key => $post) {
            //get user's avatar
            /*$image = $this->db->where("user_id", $post['user_id'])->where("status", 1)->get('vd_images')->row_array();
            $posts[$key]['image'] = $image['image'];*/

            //get comments
            $this->db->distinct();
            $this->db->select('comment.*, user.name, user.profile_image AS avatar');
            $this->db->from('post_comment AS comment');
            $this->db->join('user_details AS user', 'user.user_id = comment.user_id', 'left');
            $comments = $this->db->where("comment.post_id", $post['post_id'])->order_by('comment.comment_id', 'DESC')->get('post_comment')->result_array();
            $data['posts'][$key]['comments'] = $comments;
            /*foreach ($comments as $ckey => $comment) {
                $image = $this->db->where("user_id", $comment['user_id'])->where("status", 1)->get('vd_images')->row_array();
                $posts[$key]['comments'][$ckey]['avatar'] = $image['image'];
            }*/

            //get images
            $images = $this->db->where("post_id", $post['post_id'])->where("image_status", 1)->get('post_image')->result_array();
            $data['posts'][$key]['images'] = $images;
        }
        $data['no_title']  = true;
        $data['page_name']  = 'wall';
        $data['page_title'] = get_phrase('wall');
        $this->load->view('backend/index', $data);
    }

    function view_report() {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $data['by_day'] = $this->db->query("SELECT * FROM patient WHERE added_date > DATE_SUB(NOW(), INTERVAL 1 DAY) ORDER BY patient_id DESC;")->num_rows();
        $data['by_week'] = $this->db->query("SELECT * FROM patient WHERE added_date > DATE_SUB(NOW(), INTERVAL 1 WEEK) ORDER BY patient_id DESC;")->num_rows();
        $data['by_month'] = $this->db->query("SELECT * FROM patient WHERE added_date > DATE_SUB(NOW(), INTERVAL 1 MONTH) ORDER BY patient_id DESC;")->num_rows();
        $data['by_year'] = $this->db->query("SELECT * FROM patient WHERE added_date > DATE_SUB(NOW(), INTERVAL 1 YEAR) ORDER BY patient_id DESC;")->num_rows();
        $mmuname = $this->db->query("SELECT unit_id,mmu_name FROM mmu_unit")->result_array();
        foreach ($mmuname as $key => $m) {
            $bplandpatcount = $this->db->query("SELECT count(p.patient_id) as patcount, SUM(if(p.family_status = 'BPL',1,0)) as bplcount, mu.mmu_name
                FROM patient AS p
                JOIN mmu_unit AS mu ON p.mmu = mu.unit_id 
                WHERE p.mmu = '".$m['unit_id']."'")->row_array();
            $mmuname[$key]['bplandpatcount'] = $bplandpatcount;
            
            $habitcount = $this->db->query("SELECT COUNT(ph.id) as habitcount 
                FROM patient_habit AS ph 
                JOIN patient AS p ON ph.patient_id = p.patient_id and p.mmu = '".$m['unit_id']."' ")->row_array();
            $mmuname[$key]['habitcount'] = $habitcount;
            
            $medicinecount = $this->db->query("SELECT SUM(tm.quantity) AS medicinecount 
                FROM treatment_medicine AS tm 
                JOIN patient AS p ON tm.patient_id = p.patient_id and p.mmu = '".$m['unit_id']."' ")->row_array();
            $mmuname[$key]['medicinecount'] = $medicinecount;
            
            $treatmentcount = $this->db->query("SELECT COUNT(t.t_id) AS treatmentcount 
                FROM treatment AS t
                JOIN patient AS p ON p.patient_id = t.p_id AND  p.mmu = '".$m['unit_id']."' ")->row_array();
            $mmuname[$key]['treatmentcount'] = $treatmentcount;

            $chroniccount = $this->db->query("SELECT  count(td.td_id) as chroniccount
                FROM treatment_disease AS td 
                JOIN patient AS p ON p.patient_id = td.patient_id and p.mmu = '".$m['unit_id']."'
                WHERE  td.type = 'current' and td.dis_type = 'chronic'   ")->row_array();
            $mmuname[$key]['chroniccount'] = $chroniccount;
        }
        
        $data['records'] = $mmuname;
        $doctordata = $this->db->query("SELECT d.name, d.user_id, mu.mmu_name
            FROM user_details AS d
            JOIN mmu_unit AS mu ON mu.unit_id = d.mmu_unit and role_id = 1")->result_array();
        foreach ($doctordata as $key => $d) {
            $patientcount = $this->db->query("SELECT COUNT(p_id) as patientcount 
                    FROM `treatment` 
                    WHERE `doc_id` = '".$d['user_id']."' ")->row_array();
            $doctordata[$key]['patientcount'] = $patientcount;
            $datecount = $this->db->query("SELECT COUNT(DISTINCT treat_date) as datecount
                    FROM `treatment` 
                    WHERE `doc_id` = '".$d['user_id']."' ")->row_array();
            $doctordata[$key]['datecount'] = $datecount;
            $medicinecount = $this->db->query("SELECT sum(tm.quantity) AS medicinecount
                    FROM treatment AS t
                    JOIN treatment_medicine AS tm ON tm.patient_id = t.p_id and t.t_id = tm.treatment_id
                    WHERE t.doc_id = '".$d['user_id']."' ")->row_array();
            $doctordata[$key]['medicinecount'] = $medicinecount;
        }
        $data['doctor'] = $doctordata;
        $data['state'] = $this->db->query("SELECT * FROM state_table")->result_array();
        $data['no_title']  = true;
        $data['page_name']  = 'view_report';
        $data['page_title'] = get_phrase('view_report');
        $this->load->view('backend/index',$data);
    }

    function by_mmu($unit_id) {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $data['mmu_pat'] = $this->db->query("SELECT pat.*, mu.mmu_name
                FROM patient AS pat
                JOIN mmu_unit AS mu ON mu.unit_id = pat.mmu
                WHERE pat.mmu = '".$unit_id."'")->result_array();
        $data['no_title']  = true;
        $data['page_name']  = 'patient_by_mmu';
        $data['page_title'] = get_phrase('patient_by_mmu');
        $this->load->view('backend/index', $data);
    }

    function by_loc($unit_id) {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $mmu_location = $this->db->query("SELECT ml.*, mu.mmu_name
                FROM mmu_locations AS ml
                JOIN mmu_unit AS mu ON mu.unit_id = ml.mmu_unit
                /*JOIN patient AS pat ON pat.mmuloc = ml.mmuloc_id*/
                WHERE ml.mmu_unit = '".$unit_id."'")->result_array();
                foreach ($mmu_location as $key => $d) {
                    $patientcount = $this->db->query("SELECT COUNT(patient_id) as patcount 
                            FROM patient 
                            WHERE mmuloc = '".$d['mmuloc_id']."' ")->row_array();
                    $mmu_location[$key]['patcount'] = $patientcount;
                }
        $data['mmu_location'] = $mmu_location;
        $data['no_title']  = true;
        $data['page_name']  = 'patient_by_loc';
        $data['page_title'] = get_phrase('patient_by_loc');
        $this->load->view('backend/index', $data);
    }

    function pat_by_locations($mmuloc_id) {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $data['pat_by_locations'] = $this->db->query("SELECT pat.*, ml.mmuloc_name
                FROM patient AS pat
                JOIN mmu_locations AS ml ON ml.mmuloc_id = pat.mmuloc
                WHERE pat.mmuloc = '".$mmuloc_id."'")->result_array();
        $data['no_title']  = true;
        $data['page_name']  = 'pat_by_locations';
        $data['page_title'] = get_phrase('pat_by_locations');
        $this->load->view('backend/index', $data);
    }


    function treated_patient($unit_id) {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $data['treated_patient'] = $this->db->query("SELECT pat.*, mu.mmu_name
                FROM patient AS pat
                JOIN treatment AS tm ON tm.p_id = pat.patient_id
                JOIN mmu_unit AS mu ON mu.unit_id = pat.mmu
                WHERE pat.mmu = '".$unit_id."'")->result_array();
        $data['no_title']  = true;
        $data['page_name']  = 'treated_patient';
        $data['page_title'] = get_phrase('treated_patient');
        $this->load->view('backend/index', $data);
    }

    function medicine_distributed($unit_id) {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $data['medicine_distributed'] = $this->db->query("SELECT pat.*,tm.medicine_id, tm.quantity, med.name, med.preparation, mu.mmu_name
                FROM patient AS pat
                JOIN treatment_medicine AS tm ON tm.patient_id = pat.patient_id
                JOIN mmu_unit AS mu ON mu.unit_id = pat.mmu
                JOIN medicine AS med ON med.medicine_id = tm.medicine_id
                WHERE pat.mmu = '".$unit_id."'")->result_array();
        $data['no_title']  = true;
        $data['page_name']  = 'medicine_distributed';
        $data['page_title'] = get_phrase('medicine_distributed');
        $this->load->view('backend/index', $data);
    }

    function bpl_patient($unit_id) {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $data['bpl_patient'] = $this->db->query("SELECT pat.*, mu.mmu_name
                FROM patient AS pat
                JOIN mmu_unit AS mu ON mu.unit_id = pat.mmu
                WHERE pat.mmu = '".$unit_id."' && family_status='BPL'")->result_array();
        $data['no_title']  = true;
        $data['page_name']  = 'bpl_patient';
        $data['page_title'] = get_phrase('bpl_patient');
        $this->load->view('backend/index', $data);
    }

    function patient_habit($unit_id) {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $data['patient_habit'] = $this->db->query("SELECT pat.*, mu.mmu_name, ph.habit
                FROM patient AS pat
                JOIN mmu_unit AS mu ON mu.unit_id = pat.mmu
                JOIN patient_habit AS ph ON ph.patient_id = pat.patient_id
                WHERE pat.mmu = '".$unit_id."'")->result_array();
        $data['no_title']  = true;
        $data['page_name']  = 'patient_habit';
        $data['page_title'] = get_phrase('patient_habit');
        $this->load->view('backend/index', $data);
    }

    function chronic_disease($unit_id) {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $data['chronic_disease'] = $this->db->query("SELECT pat.*, mu.mmu_name, td.dis_type
                FROM patient AS pat
                JOIN mmu_unit AS mu ON mu.unit_id = pat.mmu
                JOIN treatment_disease AS td ON td.patient_id = pat.patient_id
                WHERE pat.mmu = '".$unit_id."' && td.dis_type = 'chronic' ")->result_array();
        $data['no_title']  = true;
        $data['page_name']  = 'chronic_disease';
        $data['page_title'] = get_phrase('chronic_disease');
        $this->load->view('backend/index', $data);
    }

    function pat_treatment_details($patient_id) {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $data['pat_treatment_details'] = $this->db->query("SELECT treat.*, tm.medicine_id, med.name,td.dis_name, td.dis_type, pat.name as patname
                FROM treatment AS treat
                JOIN patient AS pat ON pat.patient_id = treat.p_id
                JOIN treatment_medicine AS tm ON tm.patient_id = treat.p_id
                JOIN treatment_disease AS td ON td.patient_id = treat.p_id
                JOIN medicine AS med ON med.medicine_id = tm.medicine_id
                WHERE treat.p_id = '".$patient_id."'")->result_array();
        $data['no_title']  = true;
        $data['page_name']  = 'pat_treatment_details';
        $data['page_title'] = get_phrase('pat_treatment_details');
        $this->load->view('backend/index', $data);
    }

    function get_mmubystate(){
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $state_id = $this->input->post('state_id');
        echo "<option>Select MMU</option>";
        $mmu = $this->db->query("SELECT * from mmu_unit where state_id='".$state_id."'")->result();
        foreach ($mmu as $m) { 
            echo '<option value="'.$m->unit_id.'">'.$m->mmu_name.'</option>';
        }
    }

    function get_mmulocation(){
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $mmu_id = $this->input->post('mmu_id');
        echo "<option>Select MMU Loction</option>";
        $mmulocation = $this->db->query("SELECT * from mmu_locations where mmu_unit='".$mmu_id."'")->result();
        foreach ($mmulocation as $ml) { 
            echo '<option value="'.$ml->mmuloc_id.'">'.$ml->mmuloc_name.'</option>';
        }
    }

    function get_doctor(){
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $state_id = $this->input->post('state_id');
        $mmu_id = $this->input->post('mmu_id');
        $mmulocation_id = $this->input->post('mmulocation_id');
        $doctorinfo = $this->db->query("SELECT COUNT(t.p_id) AS treatmentcount, COUNT(DISTINCT t.p_id) AS patientcount, d.`doctor_id`, d.`name`,mu.mmu_name,ml.mmuloc_name 
            FROM `doctor` as d
            JOIN treatment as t ON d.doctor_id = t.doc_id
            JOIN mmu_unit AS mu ON  mu.unit_id = d.mmu_unit
            JOIN mmu_locations AS ml ON ml.mmu_unit = d.mmu_unit and ml.mmuloc_id = '".$mmulocation_id."'
            WHERE d.`mmu_unit` = '".$mmu_id."' GROUP BY t.doc_id,mu.mmu_name,ml.mmuloc_name")->result_array();
        echo json_encode($doctorinfo);
        exit();
    }

    /*function get_medicinebatch($val){
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $keyword = $this->input->post('term');
        $query = $this->db->query("SELECT DISTINCT batch_no,exp_date  
                FROM `medicine_store` 
                WHERE `batch_no` LIKE '".$keyword."%' and medicine_id = '".$val."'");
        echo json_encode($query->result());
        exit();
    }*/

    function manage_mmu_admin(){
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $data['mmuadmin'] = $this->db->query("SELECT mmuadmin.*, unit.mmu_name
                FROM user_details AS mmuadmin
                JOIN mmu_unit AS unit ON unit.unit_id = mmuadmin.mmu_unit
                WHERE mmuadmin.status = 0 and role_id = 4")->result_array();
        $data['page_name']  = 'manage_mmu_admin';
        $data['page_title'] = get_phrase('manage_mmu_admin');
        $this->load->view('backend/index', $data);
    }
    function add_mmu_admin(){
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $data['allmmuunit'] = $this->db->query("SELECT * FROM mmu_unit WHERE status = 0")->result_array();
        $data['page_name']  = 'add_mmu_admin';
        $data['page_title'] = get_phrase('add_mmu_admin');
        $this->load->view('backend/index', $data);
    }
    public function insert_mmu_admin(){
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $error = array('name' => '', 'email' => '', 'pass' => '', 'mmu' => '', 'address' => '', 'phone' => '', 'status' => 0);            
                if(empty($_POST['name'])) {
                    $error['name'] = 'Name Field is mandatory';
                    $error['status'] = 1;
                }
                else if(!preg_match('/^[a-z][a-z \.]*$/i',$_POST['name'])) {
                    $error['name'] = 'Please input alfa characters only';
                    $error['status'] = 1;
                }
                else if(strlen($_POST['name']) > 150) {
                    $error['name'] = 'Name Field must be within 150 characters';
                    $error['status'] = 1;
                }
                if(empty($_POST['email'])) {
                    $error['email'] = 'Email-ID Field is mandatory';
                    $error['status'] = 1;
                }
                else if(!preg_match('/@.+\./',$_POST['email'])) {
                    $error['email'] = 'Please enter valid Email-ID';
                    $error['status'] = 1;
                }
                else if(!empty($_POST['email'])){
                    $this->load->library('form_validation');
                    $this->db->select('user_id');
                    $this->db->where('email', $_POST['email']);
                    $query = $this->db->get('user_details');
                    if ($query->num_rows() > 0) {
                        $error['email'] = 'Entered email id is already registered.';
                        $error['status'] = 1;
                    }
                }          
                if(empty($_POST['pass'])) {
                    $error['pass'] = 'Password Field is mandatory';
                    $error['status'] = 1;
                }
                if(empty($_POST['mmu'])) {
                    $error['mmu'] = 'Mmu Field is mandatory';
                    $error['status'] = 1;
                }
                /*if(empty($_POST['address'])) {
                    $error['address'] = 'Address Field is mandatory';
                    $error['status'] = 1;
                }
                else if(strlen($_POST['address']) > 500) {
                    $error['address'] = 'Address Field must be within 500 characters';
                    $error['status'] = 1;
                }*/
                if(empty($_POST['phone'])) {
                    $error['phone'] = 'Phone No Field is mandatory';
                    $error['status'] = 1;
                }
                else if(!is_numeric($_POST['phone'])) {
                    $error['phone'] = 'Phone No Field must be Numeric';
                    $error['status'] = 1;
                }
                else if(strlen($_POST['phone']) < 10) {
                    $error['phone'] = 'Phone No must be within 10 to 15 digits';
                    $error['status'] = 1;
                }
                else if(strlen($_POST['phone']) > 15) {
                    $error['phone'] = 'Phone No must be within 10 to 15 digits';
                    $error['status'] = 1;
                }
                if($error['status'] > 0) {
                    echo json_encode($error);
                    exit();
                }
                if(isset($_FILES['image']) && $_FILES['image']['size'] > 0) {
                    $timestamp = new DateTime();
                    $timestamp = $timestamp->format('U');
                    $imagename = $timestamp. '_' .$_FILES['image']['name'];
                    move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/mmuadmin_image/' .$imagename);
                }                
                $data = array(
                    'name' => $_POST['name'],
                    'email' => $_POST['email'],
                    'password' => sha1($_POST['pass']),
                    'address' => $_POST['address'],
                    'phone' => $_POST['phone'],
                    'mmu_unit' => $_POST['mmu'],
                    'profile_image' => isset($imagename) ? $imagename : 'default.png',
                    'profile' => $this->input->post('profile'),
                    'date' => date('Y-m-d H:i:s'),
                    'ip_address' => $this->input->ip_address(),
                    'role_id' => 4,
                    'status' => 0
                );
                $query = $this->db->insert('user_details', $data);
                if($query){
                    echo json_encode(array('msg' => 'MMU Admin Added Successfully'));
                    exit();
                }
                else{
                    echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
                    exit();
                }
    }

}
