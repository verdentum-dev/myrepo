<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sync extends CI_Controller {

	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			die();
		}

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('user_agent');
	}

	//Load Methods According to Client Request
	public function index()
	{
		$data = (array)json_decode(file_get_contents("php://input"));
		if(!isset($data['purpose'])) {
			$this->logout();
		}
		
		switch ($data['purpose']) {
			case 'download':
				$this->download();
				break;
			
			case 'upload':
				$this->upload($data);
				break;

			default:
				$this->logout();
				break;
		}
	}

	// Download Server Data to Local
	public function download()
	{
		// Load the DB utility class
		$this->load->dbutil();

		$prefs = array(
			'tables'        => array('user_details'),				// Array of tables to backup.
			'ignore'        => array(),						// List of tables to omit from the backup
			'format'        => 'txt',						// gzip, zip, txt
			'filename'      => 'mybackup.sql',				// File name - NEEDED ONLY WITH ZIP FILES
			'add_drop'      => FALSE,						// Whether to add DROP TABLE statements to backup file
			'add_insert'    => TRUE,						// Whether to add INSERT data to backup file
			'newline'       => ""							// Newline character used in backup file
		);
		// Backup your entire database and assign it to a variable
		$backup = $this->dbutil->backup($prefs);
		$backup = preg_replace('/##[\s\S]+?#/', '', $backup);
		$backup = preg_replace('/\n /', '', $backup);
		$backup = strstr($backup, ';');
		$backup = ltrim($backup, ';');
		$backup = preg_replace('/`/', '', $backup);
		$query = $backup;

		$prefs['tables'] = array('default_doctor');
		// Backup your entire database and assign it to a variable
		$backup = $this->dbutil->backup($prefs);
		$backup = preg_replace('/##[\s\S]+?#/', '', $backup);
		$backup = preg_replace('/\n /', '', $backup);
		$backup = strstr($backup, ';');
		$backup = ltrim($backup, ';');
		$backup = preg_replace('/`/', '', $backup);
		$query .= $backup;

		$prefs['tables'] = array('medicine');
		// Backup your entire database and assign it to a variable
		$backup = $this->dbutil->backup($prefs);
		$backup = preg_replace('/##[\s\S]+?#/', '', $backup);
		$backup = preg_replace('/\n /', '', $backup);
		$backup = strstr($backup, ';');
		$backup = ltrim($backup, ';');
		$backup = preg_replace('/`/', '', $backup);
		$query .= $backup;

		$prefs['tables'] = array('medicine_store');
		// Backup your entire database and assign it to a variable
		$backup = $this->dbutil->backup($prefs);
		$backup = preg_replace('/##[\s\S]+?#/', '', $backup);
		$backup = preg_replace('/\n /', '', $backup);
		$backup = strstr($backup, ';');
		$backup = ltrim($backup, ';');
		$backup = preg_replace('/`/', '', $backup);
		$query .= $backup;
		
		$prefs['tables'] = array('mmu_unit');
		// Backup your entire database and assign it to a variable
		$backup = $this->dbutil->backup($prefs);
		$backup = preg_replace('/##[\s\S]+?#/', '', $backup);
		$backup = preg_replace('/\n /', '', $backup);
		$backup = strstr($backup, ';');
		$backup = ltrim($backup, ';');
		$backup = preg_replace('/`/', '', $backup);
		$query .= $backup;

		$prefs['tables'] = array('mmu_locations');
		// Backup your entire database and assign it to a variable
		$backup = $this->dbutil->backup($prefs);
		$backup = preg_replace('/##[\s\S]+?#/', '', $backup);
		$backup = preg_replace('/\n /', '', $backup);
		$backup = strstr($backup, ';');
		$backup = ltrim($backup, ';');
		$backup = preg_replace('/`/', '', $backup);
		$query .= $backup;

		$prefs['tables'] = array('patient');
		// Backup your entire database and assign it to a variable
		$backup = $this->dbutil->backup($prefs);
		$backup = preg_replace('/##[\s\S]+?#/', '', $backup);
		$backup = preg_replace('/\n /', '', $backup);
		$backup = strstr($backup, ';');
		$backup = ltrim($backup, ';');
		$backup = preg_replace('/`/', '', $backup);
		$query .= $backup;

		$prefs['tables'] = array('patient_asset');
		// Backup your entire database and assign it to a variable
		$backup = $this->dbutil->backup($prefs);
		$backup = preg_replace('/##[\s\S]+?#/', '', $backup);
		$backup = preg_replace('/\n /', '', $backup);
		$backup = strstr($backup, ';');
		$backup = ltrim($backup, ';');
		$backup = preg_replace('/`/', '', $backup);
		$query .= $backup;

		$prefs['tables'] = array('patient_habit');
		// Backup your entire database and assign it to a variable
		$backup = $this->dbutil->backup($prefs);
		$backup = preg_replace('/##[\s\S]+?#/', '', $backup);
		$backup = preg_replace('/\n /', '', $backup);
		$backup = strstr($backup, ';');
		$backup = ltrim($backup, ';');
		$backup = preg_replace('/`/', '', $backup);
		$query .= $backup;

		$prefs['tables'] = array('treatment');
		// Backup your entire database and assign it to a variable
		$backup = $this->dbutil->backup($prefs);
		$backup = preg_replace('/##[\s\S]+?#/', '', $backup);
		$backup = preg_replace('/\n /', '', $backup);
		$backup = strstr($backup, ';');
		$backup = ltrim($backup, ';');
		$backup = preg_replace('/`/', '', $backup);
		$query .= $backup;

		$prefs['tables'] = array('treatment_medicine');
		// Backup your entire database and assign it to a variable
		$backup = $this->dbutil->backup($prefs);
		$backup = preg_replace('/##[\s\S]+?#/', '', $backup);
		$backup = preg_replace('/\n /', '', $backup);
		$backup = strstr($backup, ';');
		$backup = ltrim($backup, ';');
		$backup = preg_replace('/`/', '', $backup);
		$query .= $backup;

		$prefs['tables'] = array('treatment_disease');
		// Backup your entire database and assign it to a variable
		$backup = $this->dbutil->backup($prefs);
		$backup = preg_replace('/##[\s\S]+?#/', '', $backup);
		$backup = preg_replace('/\n /', '', $backup);
		$backup = strstr($backup, ';');
		$backup = ltrim($backup, ';');
		$backup = preg_replace('/`/', '', $backup);
		$query .= $backup;

		$prefs['tables'] = array('diagnosis');
		// Backup your entire database and assign it to a variable
		$backup = $this->dbutil->backup($prefs);
		$backup = preg_replace('/##[\s\S]+?#/', '', $backup);
		$backup = preg_replace('/\n /', '', $backup);
		$backup = strstr($backup, ';');
		$backup = ltrim($backup, ';');
		$backup = preg_replace('/`/', '', $backup);
		$query .= $backup;

		$prefs['tables'] = array('disease');
		// Backup your entire database and assign it to a variable
		$backup = $this->dbutil->backup($prefs);
		$backup = preg_replace('/##[\s\S]+?#/', '', $backup);
		$backup = preg_replace('/\n /', '', $backup);
		$backup = strstr($backup, ';');
		$backup = ltrim($backup, ';');
		$backup = preg_replace('/`/', '', $backup);
		$query .= $backup;

		$prefs['tables'] = array('van_stock');
		// Backup your entire database and assign it to a variable
		$backup = $this->dbutil->backup($prefs);
		$backup = preg_replace('/##[\s\S]+?#/', '', $backup);
		$backup = preg_replace('/\n /', '', $backup);
		$backup = strstr($backup, ';');
		$backup = ltrim($backup, ';');
		$backup = preg_replace('/`/', '', $backup);
		$query .= $backup;

		$this->jsonify(array(
			'query' => $query,
			'status' => 1
		));
	}

	// Upload Local Data to Server
	public function upload($data)
	{
		$queries = $data['queries'];
		foreach ($queries as $key => $query) {
			$this->db->query($query->query);
		}

		$this->jsonify(array(
			'msg' => 'Successfully synced LocalDB with remoteDB. Device is set to online mode. You can now access data online.',
			'status' => 1
		));
	}

	//logout ++++++++ session
	public function logout()
	{
		$this->jsonify(array(
			'logout' => true
		));
	}

	public function jsonify($data)
	{
		print_r(json_encode($data));
		exit();
	}
}