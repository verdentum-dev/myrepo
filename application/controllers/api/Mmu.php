<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mmu extends CI_Controller {

	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			die();
		}

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('user_agent');
	}

	//Load Methods According to Client Request
	public function index()
	{
		$data = (array)json_decode(file_get_contents("php://input"));
		if(!isset($data['purpose'])) {
			$this->logout();
		}
		
		switch ($data['purpose']) {
			case 'getmmu':
				$this->getmmu($data);
				break;
			
			default:
				$this->logout();
				break;
		}
	}

	// Get MMU
	public function getmmu($data)
	{
		date_default_timezone_set("Asia/Kolkata");

		$this->db->select('mmu.*')->from('mmu_unit AS mmu');
		$mmu = $this->db->where('mmu.unit_id', $data['mmu'])->get('mmu_unit')->row_array();
		$this->jsonify(array(
			'mmu' => $mmu,
			'status' => 1
		));
	}

	//logout ++++++++ session
	public function logout()
	{
		$this->jsonify(array(
			'logout' => true
		));
	}

	public function jsonify($data)
	{
		print_r(json_encode($data));
		exit();
	}
}