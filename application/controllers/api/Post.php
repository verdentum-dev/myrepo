<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Post extends CI_Controller {

	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			die();
		}

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('user_agent');
	}

	//Load Methods According to Client Request
	public function index()
	{
		$data = (array)json_decode(file_get_contents("php://input"));
		if(!isset($data['purpose'])) {
			$this->logout();
		}
		
		switch ($data['purpose']) {
			case 'addpost':
				$this->addpost($data);
				break;

			case 'allpost':
				$this->allpost($data);
				break;

			case 'deletepost':
				$this->deletepost($data);
				break;

			case 'allcomment':
				$this->allcomment($data);
				break;

			case 'addcomment':
				$this->addcomment($data);
				break;

			case 'deletecomment':
				$this->deletecomment($data);
				break;
			
			default:
				$this->logout();
				break;
		}
	}

	// Add Post
	public function addpost($data)
	{
		date_default_timezone_set("Asia/Kolkata");

		$postData = array(
			'user_id' => $data['id'],
			'post_type' =>'',
			'text' => isset($data['status']) ? trim($data['status']) : '',
			'mmu' => $data['mmuloc'],
			'activity' => $data['activity'],
			'lat' => isset($data['lat']) ? $data['lat'] : '',
			'lng' => isset($data['long']) ? $data['long'] : '',
			'address' => isset($data['address']) ? $data['address'] : '',
			'ip_address' => $this->input->ip_address(),
			'date' => date('Y-m-d H:i:s'),
			'status' => 1
		);
		$query = $this->db->insert('post', $postData);

		if($query) {
			$post_id = $this->db->insert_id();

			if(isset($data['imgURI']) && count($data['imgURI']) > 0) {
				if (!defined('UPLOAD_DIR')) define('UPLOAD_DIR', 'uploads/post_image/');
				foreach ($data['imgURI'] as $key => $value) {
					$crop = str_replace('data:image/*;charset=utf-8;base64,', '', $value->src);
					$crop = str_replace(' ', '+', $crop);
					$cropdata = base64_decode($crop);
					$file = uniqid() . '.jpg';
					$url = UPLOAD_DIR . $file;

					//file_put_contents(UPLOAD_DIR . $file, $cropdata);
					$this->load->model('Compress_model');
					$filename = $this->Compress_model->compress_image_mobile($cropdata, $url, 90);

					if($filename) {
						$imgData = array(
							'post_id' => $post_id,
							'image_name' => $file,
							'lat' => isset($value->lat) ? $value->lat : $postData['lat'],
							'lng' => isset($value->long) ? $value->long : $postData['lng'],
							'address' => isset($value->address) ? $value->address : $postData['address'],
							'image_status' => 1
						);
						$imageinsert = $this->db->insert('post_image', $imgData);
					}
				}
			}

			$this->jsonify(array(
				'post_id' => $post_id,
				'msg' => 'Post Uploaded Successfully...',
				'status' => 1
			));
		}
		else {
			$this->jsonify(array(
				'msg' => 'Something Went Wrong. Post is not Inserting!!!',
				'status' => 0
			));
		}
	}

	// All Post
	public function allpost($data)
	{
		date_default_timezone_set("Asia/Kolkata");

		if(isset($data['limit'])) {
			if($data['direction'] == 'down') {
				$posts = $this->db->query("SELECT DISTINCT post.*, user.name, user.profile_image AS image FROM post
				left join user_details AS user ON user.user_id = post.user_id
				WHERE post.status = 1 AND post.post_id < ".$data['limit']."
				ORDER BY post.post_id DESC
				LIMIT 10")->result_array();
			} else {
				$posts = $this->db->query("SELECT DISTINCT post.*, user.name, user.profile_image AS image FROM post
				left join user_details AS user ON user.user_id = post.user_id
				WHERE post.status = 1 AND post.post_id > ".$data['limit']."
				ORDER BY post.post_id DESC")->result_array();
			}
		} else {
			$posts = $this->db->query("SELECT DISTINCT post.*, user.name, user.profile_image AS image FROM post
			left join user_details AS user ON user.user_id = post.user_id
			WHERE post.status = 1
			ORDER BY post.post_id DESC
			LIMIT 2")->result_array();
		}

		foreach ($posts as $key => $post) {
			//get user's avatar
			/*$image = $this->db->where("user_id", $post['user_id'])->where("status", 1)->get('vd_images')->row_array();
			$posts[$key]['image'] = $image['image'];*/

			//get comments
			$this->db->distinct();
			$this->db->select('comment.*, user.name, user.profile_image AS avatar');
			$this->db->from('post_comment AS comment');
			$this->db->join('user_details AS user', 'user.user_id = comment.user_id', 'left');
			$comments = $this->db->where("comment.post_id", $post['post_id'])->order_by('comment.comment_id', 'DESC')->get('post_comment')->result_array();
			$posts[$key]['comments'] = $comments;
			/*foreach ($comments as $ckey => $comment) {
				$image = $this->db->where("user_id", $comment['user_id'])->where("status", 1)->get('vd_images')->row_array();
				$posts[$key]['comments'][$ckey]['avatar'] = $image['image'];
			}*/

			//get images
			$images = $this->db->where("post_id", $post['post_id'])->where("image_status", 1)->get('post_image')->result_array();
			$posts[$key]['images'] = $images;
		}

		$this->jsonify(array(
			'posts' => $posts,
			'status' => 1
		));
	}

	// Delete Post
	public function deletepost($data)
	{
		date_default_timezone_set("Asia/Kolkata");

		if(!isset($data['id']) || $data['id'] == '') {
			$this->jsonify(array('status' => 0, 'msg' => 'Your session has ended. Please logout and login again to continue.'));
		}
		else {
			$id = trim($data['post_id']);
			$check = $this->db->where('post_id', $id)->where('user_id', $data['id'])->get('post')->num_rows();
			if($check == 0) {
				$this->jsonify(array('status' => 0, 'msg' => 'Can not delete post.'));
			}

			$this->db->where('post_id', $id)->update('post', array('status' => 0));
			$this->jsonify(array('status' => 1));
		}
	}

	// All Comment
	public function allcomment($data)
	{
		date_default_timezone_set("Asia/Kolkata");

		$post = array(
			'user_id' => $data['id'],
			'post_id' => $data['post_id']
		);
		//get comments
		$this->db->distinct();
		$this->db->select('comment.*, user.name, user.profile_image AS avatar');
		$this->db->from('post_comment AS comment');
		$this->db->join('user_details AS user', 'user.user_id = comment.user_id', 'left');
		$comments = $this->db->where("comment.post_id", $post['post_id'])->order_by('comment.comment_id', 'DESC')->get('post_comment')->result_array();

		/*foreach ($comments as $ckey => $comment) {
			$image = $this->db->where("user_id", $comment['user_id'])->where("status", 1)->get('vd_images')->row_array();
			$comments[$ckey]['avatar'] = $image['image'];
		}*/
		$this->jsonify(array(
			'comments' => $comments,
			'status' => 1
		));
	}

	// Add Comment
	public function addcomment($data)
	{
		date_default_timezone_set("Asia/Kolkata");

		$data = array(
			'post_id' => $data['post_id'],
			'user_id' => $data['id'],
			'comment' => $data['comment'],
			'ip_address' => $this->input->ip_address(),
			'date' => date('Y-m-d H:i:s'),
			'status' => 1
		);
		$query = $this->db->insert('post_comment', $data);

		if($query) {
			$comment_id = $this->db->insert_id();
			$comment = $this->db->where('post_id', $data['post_id'])->get('post')->row_array();

			$this->db->where('post_id', $data['post_id'])->update('post', array(
				'comment' => intval($comment['comment'])+1
			));

			$this->db->select('comment.*, user.name, user.profile_image AS avatar');
			$this->db->from('post_comment AS comment');
			$this->db->join('user_details AS user', 'user.user_id = comment.user_id', 'left');
			$comments = $this->db->where("comment.comment_id", $comment_id)->get('post_comment')->row_array();
			$this->jsonify(array(
				'comment' => $comments,
				'status' => 1
			));
		}
		else {
			$this->jsonify(array(
				'msg' => 'Can not post comment',
				'status' => 0
			));
		}
	}

	// Delete Comment
	public function deletecomment($data)
	{
		date_default_timezone_set("Asia/Kolkata");
	}

	//logout ++++++++ session
	public function logout()
	{
		$this->jsonify(array(
			'logout' => true
		));
	}

	public function jsonify($data)
	{
		print_r(json_encode($data));
		exit();
	}
}