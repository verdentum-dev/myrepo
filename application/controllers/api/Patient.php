<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Patient extends CI_Controller {

	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			die();
		}

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('user_agent');
	}

	//Load Methods According to Client Request
	public function index()
	{
		$data = (array)json_decode(file_get_contents("php://input"));
		if(!isset($data['purpose'])) {
			$this->logout();
		}
		
		switch ($data['purpose']) {
			case 'addpatient':
				$this->addpatient($data);
				break;

			case 'editpatient':
				$this->editpatient($data);
				break;

			case 'searchpatient':
				$this->searchpatient($data);
				break;

			case 'getpatient':
				$this->getpatient($data);
				break;

			case 'getother':
				$this->getother($data);
				break;

			case 'allpatient':
				$this->allpatient($data);
				break;

			case 'getdisease':
				$this->getdisease();
				break;
			
			default:
				$this->logout();
				break;
		}
	}

	// Add patient
	public function addpatient($data)
	{
		date_default_timezone_set("Asia/Kolkata");
		$patient = $data['patient'];

		$data = array(
			'patient_id' => time().$patient->id,
			'care_of' => isset($patient->co) ? $patient->co : '',
			'name' => $patient->name,
			'birth_date' => isset($patient->dob) ? $patient->dob : '',
			'age' => $patient->age,
			'phone' => isset($patient->phone) ? $patient->phone : '',
			'adhar_no' => isset($patient->adhaar) ? $patient->adhaar : '',
			'gender' => $patient->gender,
			'blood_group' => isset($patient->blood) ? $patient->blood : '',
			'address' => isset($patient->address) ? $patient->address : '',
			'family_status' => $patient->family,
			'education' => $patient->edu,
			'occupation' => isset($patient->occupation) ? $patient->occupation : '',
			'occu_text' => isset($patient->occu_text) ? $patient->occu_text : '',
			'income' => isset($patient->income) ? $patient->income : '',
			'disability' => isset($patient->disability) ? $patient->disability : '',
			/*'curdisname' => $patient->curdis->name,
			'curdistype' => $patient->curdis->type,
			'pastdisname' => $patient->pastdis->name,
			'pastdistype' => $patient->pastdis->type,*/
			'ration' => $patient->ration == false ? NULL : 1,
			'insurance' => $patient->insurance == false ? NULL : 1,
			'pension' => $patient->pension == false ? NULL : 1,
			'account' => $patient->account == false ? NULL : 1,
			'mmu' => $patient->mmu,
			'mmuloc' => $patient->mmuloc,
			'added_date' => date('Y-m-d H:i:s'),
			'ip_address' => $this->input->ip_address()
		);
		$insert = $this->db->insert('patient', $data);

		if($insert) {
			$patient_id = $this->db->insert_id();
			$inc = 0;

			//Insert All Habits
			foreach ($patient->habits as $key => $habit) {
				$inc = $inc+$key+1;
				$data = array(
					'id' => (time()+$inc).$patient->id,
					'patient_id' => $patient_id,
					'habit' => $habit->value
				);
				if($habit->checked == true) {
					$this->db->insert('patient_habit', $data);
				}
			}

			//Insert All Movable Assets
			foreach ($patient->movassets as $key => $asset) {
				$inc = $inc+$key+1;
				$data = array(
					'id' => (time()+$inc).$patient->id,
					'patient_id' => $patient_id,
					'name' => $asset->value,
					'type' => 1
				);
				if($asset->checked == true) {
					$this->db->insert('patient_asset', $data);
				}
			}

			//Insert All Immovable Assets
			foreach ($patient->immovassets as $key => $asset) {
				$inc = $inc+$key+1;
				$data = array(
					'id' => (time()+$inc).$patient->id,
					'patient_id' => $patient_id,
					'name' => $asset->value,
					'type' => 0
				);
				if($asset->checked == true) {
					$this->db->insert('patient_asset', $data);
				}
			}

			$this->jsonify(array(
				'msg' => 'New patient record created. Patient added under you current MMU.',
				'status' => 1
			));
		} else {
			$this->jsonify(array(
				'msg' => 'Sorry! Cannot add patient. Please try after sometime.',
				'status' => 0
			));
		}
	}

	// Edit patient
	public function editpatient($data)
	{
		date_default_timezone_set("Asia/Kolkata");
		$patient = $data['patient'];
		$patient_id = $patient->pid;
		$id = $data['id'];

		$data = array(
			'care_of' => $patient->co,
			'name' => $patient->name,
			'birth_date' => $patient->dob,
			'age' => $patient->age,
			'phone' => $patient->phone,
			'adhar_no' => $patient->adhaar,
			'gender' => $patient->gender,
			'blood_group' => $patient->blood,
			'address' => $patient->address,
			'family_status' => $patient->family,
			'education' => $patient->edu,
			'occupation' => isset($patient->occupation) ? $patient->occupation : '',
			'occu_text' => isset($patient->occu_text) ? $patient->occu_text : '',
			'income' => isset($patient->income) ? $patient->income : '',
			'disability' => isset($patient->disability) ? $patient->disability : '',
			/*'curdisname' => $patient->curdis->name,
			'curdistype' => $patient->curdis->type,
			'pastdisname' => $patient->pastdis->name,
			'pastdistype' => $patient->pastdis->type,*/
			'ration' => $patient->ration == false ? NULL : 1,
			'insurance' => $patient->insurance == false ? NULL : 1,
			'pension' => $patient->pension == false ? NULL : 1,
			'account' => $patient->account == false ? NULL : 1
		);
		$update = $this->db->where('patient_id', $patient_id)->update('patient', $data);

		if($update) {
			//delete all habits and assets
			$this->db->where('patient_id', $patient_id)->delete('patient_habit');
			$this->db->where('patient_id', $patient_id)->delete('patient_habit');
			$inc = 0;

			//Insert All Habits
			foreach ($patient->habits as $key => $habit) {
				$inc = $inc+$key+1;
				$data = array(
					'id' => (time()+$inc).$id,
					'patient_id' => $patient_id,
					'habit' => $habit->value
				);
				if($habit->checked == true) {
					$this->db->insert('patient_habit', $data);
				}
			}

			//Insert All Movable Assets
			foreach ($patient->movassets as $key => $asset) {
				$inc = $inc+$key+1;
				$data = array(
					'id' => (time()+$inc).$id,
					'patient_id' => $patient_id,
					'name' => $asset->value,
					'type' => 1
				);
				if($asset->checked == true) {
					$this->db->insert('patient_asset', $data);
				}
			}

			//Insert All Immovable Assets
			foreach ($patient->immovassets as $key => $asset) {
				$inc = $inc+$key+1;
				$data = array(
					'id' => (time()+$inc).$id,
					'patient_id' => $patient_id,
					'name' => $asset->value,
					'type' => 0
				);
				if($asset->checked == true) {
					$this->db->insert('patient_asset', $data);
				}
			}

			$this->jsonify(array(
				'msg' => 'Patient record edited successfully.',
				'status' => 1
			));
		} else {
			$this->jsonify(array(
				'msg' => 'Sorry! Cannot edit patient. Please try after sometime.',
				'status' => 0
			));
		}
	}

	// Get patient
	public function searchpatient($data)
	{
		date_default_timezone_set("Asia/Kolkata");

		$patients = $this->db->query('SELECT * FROM patient WHERE mmu = '.$data['mmu'].' AND mmuloc = '.$data['query']->mmuloc.' AND (patient_id = "'.$data['query']->text.'" OR name LIKE "%'.$data['query']->text.'%")');

		if($patients->num_rows() === 0) {
			$this->jsonify(array(
				'msg' => 'No records found according to your query. Please try something different.',
				'status' => 0
			));
		} else {
			$this->jsonify(array(
				'patients' => $patients->result_array(),
				'status' => 1
			));
		}
	}

	// Get patient
	public function getpatient($data)
	{
		date_default_timezone_set("Asia/Kolkata");

		$this->db->select('patient.*, mmuloc.mmuloc_name')->from('patient');
		$this->db->join('mmu_locations AS mmuloc', 'mmuloc.mmuloc_id = patient.mmuloc');
		$patient = $this->db->where('patient.patient_id', $data['pid'])->get();
		if($patient->num_rows() === 0) {
			$this->jsonify(array(
				'msg' => 'Something went wrong. Please try to search again.',
				'status' => 0
			));
		} else {
			$this->jsonify(array(
				'patient' => $patient->row_array(),
				'status' => 1
			));
		}
	}

	// Get other
	public function getother($data)
	{
		date_default_timezone_set("Asia/Kolkata");

		$habits = $this->db->where('patient_id', $data['pid'])->get('patient_habit')->result_array();
		$assets = $this->db->where('patient_id', $data['pid'])->get('patient_asset')->result_array();

		$this->jsonify(array(
			'habits' => $habits,
			'assets' => $assets,
			'status' => 1
		));
	}

	// All patient
	public function allpatient($data)
	{
		date_default_timezone_set("Asia/Kolkata");
		$patients = $this->db->where('mmu', $data['mmu'])->where('mmuloc', $data['mmuloc'])->get('patient')->result_array();
		$this->jsonify(array(
			'patients' => $patients,
			'status' => 1
		));
	}

	// All disease
	public function getdisease()
	{
		date_default_timezone_set("Asia/Kolkata");
		$diseases = $this->db->get('disease')->result_array();
		$this->jsonify(array(
			'diseases' => $diseases,
			'status' => 1
		));
	}

	//logout ++++++++ session
	public function logout()
	{
		$this->jsonify(array(
			'logout' => true
		));
	}

	public function jsonify($data)
	{
		print_r(json_encode($data));
		exit();
	}
}