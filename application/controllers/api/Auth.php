<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Auth extends CI_Controller {

	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			die();
		}

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('user_agent');
	}

	//Load Methods According to Client Request
	public function index()
	{
		$data = (array)json_decode(file_get_contents("php://input"));
		if(!isset($data['purpose'])) {
			$this->logout();
		}
		
		switch ($data['purpose']) {
			case 'login':
				$this->login($data);
				break;
			
			default:
				$this->logout();
				break;
		}
	}

	// login
	public function login($data)
	{
		date_default_timezone_set("Asia/Kolkata");
		$credential = array(
			'email' => $data['user']->email,
			'password' => sha1($data['user']->password),
			'role_id' => 2
		);

		// Checking login credential for spo
		$query = $this->db->get_where('user_details', $credential);
		if ($query->num_rows() == 0) {
			$this->jsonify(array(
				'msg' => 'Invalid email and password.',
				'status' => 0
			));
		}
		
		$row = $query->row_array();
		$role = $this->db->where('id', $row['role_id'])->get('role_master')->row_array();
		$mmulocs = $this->db->where('mmu_unit', $row['mmu_unit'])->get('mmu_locations')->result_array();

		$newdata = array(
			'id'=>$row['user_id'],
			'name'=>$row['name'],
			'role'=>$role['role_name'],
			'mmu'=>$row['mmu_unit'],
			'mmulocs'=>$mmulocs,
			'image'=>$row['profile_image'],
			'email'=>$data['user']->email
		);

		//Set This Latter For Loggig User Activity
		/*$activity = array(
			'user_id'=>$row['spo_id'],
			'browser'=>$this->agent->browser(),
			'version'=>$this->agent->version(),
			'platform'=>$this->agent->platform(),
			'ip_address'=>$this->input->ip_address(),
			'date_time'=>date('Y-m-d H:i:s')
		);
		$insert = $this->db->insert('account_activity', $activity);*/

		$this->jsonify(array(
			'user' => $newdata,
			'status' => 1
		));
	}

	// login
	public function signup()
	{
		$data = (array)json_decode(file_get_contents("php://input"));
		if(isset($data['purpose']) && $data['purpose'] == 'signup') {
			$checkuser = $this->db->where('reg_email', $data['email'])->get('vd_registration')->num_rows();
	        
	        $validemail = filter_var($data['email'], FILTER_VALIDATE_EMAIL);
			
			if (!$validemail || $checkuser > 0) {
				$this->jsonify(array(
					'status' => 0,
					'msg' => !$validemail ? 'Entered email id is not valid' :  'Entered email id is already registered'
				));
				exit();
			}


			date_default_timezone_set("Asia/Kolkata");
			$salt = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
			$hashed = hash('sha256', $salt);
			$activation_code = rand(1000, 9999);
			$userData = array(
				'reg_fname' => $data['fname'],
				'reg_mname' => isset($data['mname']) ? $data['mname'] : '',
				'reg_lname' => isset($data['lname']) ? $data['lname'] : '',
				'reg_age' => isset($data['age']) ? $data['age'] : 0,
				'reg_referal' => 1,
				'reg_email' => $data['email'],
				'reg_status' => 2,
				'reg_date' => date('Y-m-d H:i:s'),
				'ip_address' => $this->input->ip_address(),
				'user_role' => 1,
				'invitehash' => $hashed,
				'activation_code' => $activation_code,
				'invite_time' => date('Y-m-d H:i:s')
			);

			$query = $this->db->insert('vd_registration', $userData);
			if($query) {
				$user_id = $this->db->insert_id();

				if(isset($data['imgURI'])) {
					define('UPLOAD_DIR', 'upload/');
					$crop = $data['imgURI'];
					$crop = str_replace('data:image/jpeg;base64,', '', $crop);
					$crop = str_replace(' ', '+', $crop);
					$cropdata = base64_decode($crop);
					$file = uniqid() . '.jpg';
					$url = UPLOAD_DIR . $file;
				} else{
					$file = 'blank_profile_other.jpg';
				}

				$imgData = array(
					'user_id' => $user_id,
					'image' => $file,
					'original_image' => isset($data['imgURI']) ? $file : '',
					'ip_address' => $this->input->ip_address(),
					'date' => date('Y-m-d H:i:s'),
					'status' => 1
				);
				$insert = $this->db->insert('vd_images', $imgData);
				if(isset($data['imgURI']) && $insert) {
					//file_put_contents(UPLOAD_DIR . $file, $cropdata);
					$this->load->model('Compress_model');
					$filename = $this->Compress_model->compress_image_mobile($cropdata, $url, 90);
				}

				$config = Array(
				  'protocol' => 'smtp',
				  'smtp_host' => 'ssl://smtp.googlemail.com',
				  'smtp_port' => 465,
				  'smtp_user' => 'production@verdentum.org', // change it to yours
				  'smtp_pass' => 'Verdentum@21', // change it to yours
				  'mailtype' => 'html',
				  'charset' => 'iso-8859-1',
				  'wordwrap' => TRUE
				);
				 
				$this->load->library('email', $config);
				$this->email->set_newline("\r\n");
				$this->email->from('info@verdentum.com','verdentum');
				$this->email->to($userData['reg_email']);
				$this->email->subject('Account Activation Mail');
				$this->email->set_mailtype("html");
				
				$emailData = array(
					'name' => $userData['reg_fname'].' '.$userData['reg_mname'].' '.$userData['reg_lname'],
					'link'=> '<a href = '.base_url().'newuser/'.$user_id.'/'.$hashed.'/>Click Here</a>',
					'code'=> $activation_code
				);
				
				$body = $this->load->view('vadmin/email.php',$emailData,TRUE);
				$this->email->message($body);
				if(!$this->email->send()) {
					$this->jsonify(array(
						'msg' => 'Thank you for registering with Verdentum. <br/>It seems we are unable to send a confirmation to your email ID. Please contact the Verdentum Support Team at suport@verdentum.org.',
						'status' => 1
					));
					exit();
				} else {
					$this->jsonify(array(
						'msg' => 'Thank you for registering with Verdentum. You will receive a confirmation email to activate your account.',
						'status' => 1
					));
					exit();
				}
			}
			else {
				$this->jsonify(array(
					'msg' => 'Oops! Some Error Occured!!! Cannot create account.',
					'status' => 0
				));
				exit();
			}
		}
	}

	//Facebook Login
	public function fblogin() {
		$user = (array)json_decode(file_get_contents("php://input"));
		if(isset($user['purpose']) && $user['purpose'] == 'fblogin') {
			$query = $this->db->query("SELECT * FROM vd_registration WHERE reg_email = '".$user['email']."'");
	        if($query->num_rows() > 0) {
				$getData = $query->row_array();
				if($getData['reg_status'] == 4) {
					$getImage = $this->db->where('user_id', $getData['reg_id'])->where('status', 1)->get('vd_images')->row_array();
		            $name = empty($getData['reg_fname']) ? $getData['reg_email'] : $getData['reg_title'].' '.$getData['reg_fname'].' '.$getData['reg_mname'].' '.$getData['reg_lname'];

		            if($getData['user_role'] == 3) {
		                $this->jsonify(array(
							'msg' => 'This mail id is registered as a Policymaker. If you are a Policymaker, please visit our website http://verdentum.org to login.',
							'status' => 0
						));
						exit();
		            }

		            $user =  array('id' => $getData['reg_id'],'name'=> $name,'role'=>$getData['user_role'],'image'=>$getImage['image']);

					$newdata = array(
						'id'=>$user['id'],
						'name'=>$user['name'],
						'role'=>$user['role'],
						'image'=>$user['image'],
						'email'=>$getData['reg_email']
					);
					$activity = array(
						'user_id'=>$user['id'],
						'browser'=>$this->agent->browser(),
						'version'=>$this->agent->version(),
						'platform'=>$this->agent->platform(),
						'ip_address'=>$this->input->ip_address(),
						'date_time'=>date('Y-m-d H:i:s')
					);
					$insert = $this->db->insert('vd_account_activity', $activity);

					$this->jsonify(array(
						'user' => $newdata,
						'status' => 1
					));
					exit();
				}
				else if($getData['reg_status'] == '0') {
					$this->jsonify(array(
						'msg' => 'Sorry your account is not approved by Verdentum Admin. Please Try after some times.',
						'status' => 0
					));
					exit();
				}
				else if($getData['reg_status'] == '3') {
					$this->jsonify(array(
						'msg' => 'Sorry your account is blocked by Verdentum Admin. Please contact verdentum admin.',
						'status' => 0
					));
					exit();
				}
				else {
					$this->jsonify(array(
						'msg' => 'Sorry your account activation is pending. Please visit registered mail address to activate you account.',
						'status' => 0
					));
					exit();
				}
			}
			else {
				date_default_timezone_set("Asia/Kolkata");
				$global = $this->db->get('vd_global')->row_array();
				$data = array(
					'reg_fname' => $user['first_name'],
					'reg_lname' => $user['last_name'],
					'reg_gender' => ucfirst($user['gender']),
					'reg_dob' => '0000-00-00',
					'reg_referal' => 1,
					'reg_email' => $user['email'],
					'reg_status' => 4,
					'reg_date' => date('Y-m-d H:i:s'),
					'ip_address' => $this->input->ip_address(),
					'user_role' => 1,
					'activation_code' => 'facebook-'.$user['id'],
					'invites' => $global['invites']
				);
				$query = $this->db->insert('vd_registration', $data);
		        if($query) {
		            $id = $this->db->insert_id();

		            if($data['reg_gender'] == 'Male') {
		                $query = $this->db->insert('vd_images', array(
		                    'user_id' => $id,
		                    'image' => 'blank_profile_male.jpg',
		                    'ip_address' => $data['ip_address'],
		                    'date' => $data['reg_date'],
		                    'status' => 1
		                ));

		            } else if($data['reg_gender'] == 'Female') {
		                $query = $this->db->insert('vd_images', array(
		                    'user_id' => $id,
		                    'image' => 'blank_profile_female.jpg',
		                    'ip_address' => $data['ip_address'],
		                    'date' => $data['reg_date'],
		                    'status' => 1
		                ));
		            } else {
		                $query = $this->db->insert('vd_images', array(
		                    'user_id' => $id,
		                    'image' => 'blank_profile_other.jpg',
		                    'ip_address' => $data['ip_address'],
		                    'date' => $data['reg_date'],
		                    'status' => 1
		                ));
		            }

		            //session start for created userid and role
		            $getData = $this->db->where('reg_id', $id)->get('vd_registration')->row_array();
	    			$getImage = $this->db->where('user_id', $id)->where('status', 1)->get('vd_images')->row_array();
		            $newdata = array(
						'id' => $id,
						'name' => $getData['reg_fname'].' '.$getData['reg_lname'],
						'role'=> $getData['user_role'],
						'image' => $getImage['image'],
						'email' => $getData['reg_email']
					);
					$activity = array(
						'user_id'=>$id,
						'browser'=>$this->agent->browser(),
						'version'=>$this->agent->version(),
						'platform'=>$this->agent->platform(),
						'ip_address'=>$this->input->ip_address(),
						'date_time'=>date('Y-m-d H:i:s')
					);
					$insert = $this->db->insert('vd_account_activity', $activity);
					
					$this->jsonify(array(
						'user' => $newdata,
						'status' => 1
					));
					exit();
				}
	    	}
	    }
	}

	// store device registrationId/token for push
	public function storenotitoken()
	{
		$data = (array)json_decode(file_get_contents("php://input"));
		if(isset($data['purpose']) && $data['purpose'] == 'storenotitoken') {
			if(!isset($data['token']) || !isset($data['id'])) {
				$this->jsonify(array(
					'status' => 1,
					'msg' => 'Token or Userid not received.'
				));
				exit();
			}

			$check = $this->db->where('token', $data['token'])->where('user_id', $data['id'])->get('vd_push_notification');
			if($check->num_rows() > 0) {
				$this->jsonify(array(
					'status' => 1,
					'token' => $data['token'],
					'msg' => 'Token already registered.'
				));
				exit();
			}

			date_default_timezone_set("Asia/Kolkata");
			$activity = array(
				'user_id'=>$data['id'],
				'token'=>$data['token'],
				'browser'=>$this->agent->browser(),
				'version'=>$this->agent->version(),
				'platform'=>$this->agent->platform(),
				'ip_address'=>$this->input->ip_address(),
				'date_time'=>date('Y-m-d H:i:s')
			);
			$insert = $this->db->insert('vd_push_notification', $activity);

			$this->jsonify(array(
				'status' => 1,
				'token' => $data['token'],
				'msg' => 'Token received. Device registered.'
			));
			exit();
		} else {
			$this->jsonify(array(
				'status' => 0,
				'msg' => 'Purpose not received.'
			));
			exit();
		}
	}

	// remove device registrationId/token
	public function removenotitoken()
	{
		$data = (array)json_decode(file_get_contents("php://input"));
		if(isset($data['purpose']) && $data['purpose'] == 'removenotitoken') {
			if(!isset($data['token']) || !isset($data['id'])) {
				$this->jsonify(array(
					'status' => 1,
					'msg' => 'Token or Userid not received.'
				));
				exit();
			}

			$check = $this->db->where('token', $data['token'])->where('user_id', $data['id'])->get('vd_push_notification');
			if($check->num_rows() == 0) {
				$this->jsonify(array(
					'status' => 1,
					'token' => $data['token'],
					'msg' => 'Token does not exist.'
				));
				exit();
			}

			$this->db->where('token', $data['token'])->where('user_id', $data['id'])->delete('vd_push_notification');

			$url = 'https://cp.pushwoosh.com/json/1.3/unregisterDevice';
			$request = json_encode(['request' => array(
				'application' => PW_APPLICATION,
				'hwid' => $data['token']
			)]);

			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
			curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $request);

			$response = curl_exec($ch);
			curl_close($ch);

			$this->jsonify(array(
				'status' => 1,
				'token' => $data['token'],
				'msg' => 'Token removed. Device unregistered.'
			));
		} else {
			$this->jsonify(array(
				'status' => 0,
				'msg' => 'Purpose not received.'
			));
		}
	}

	//logout ++++++++ session
	public function logout()
	{
		$this->jsonify(array(
			'logout' => true
		));
	}

	public function jsonify($data)
	{
		print_r(json_encode($data));
		exit();
	}
}