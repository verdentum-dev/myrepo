<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Treatment extends CI_Controller {

	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			die();
		}

		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('user_agent');
	}

	//Load Methods According to Client Request
	public function index()
	{
		$data = (array)json_decode(file_get_contents("php://input"));
		if(!isset($data['purpose'])) {
			$this->logout();
		}
		
		switch ($data['purpose']) {
			case 'addtreatment':
				$this->addtreatment($data);
				break;

			case 'gettreatment':
				$this->gettreatment($data);
				break;

			case 'allmedicine':
				$this->allmedicine($data);
				break;

			case 'getbatch':
				$this->getbatch($data);
				break;

			case 'alldoctor':
				$this->alldoctor($data);
				break;

			case 'alldiagnosis':
				$this->alldiagnosis();
				break;

			case 'getdefaultdoctor':
				$this->getdefaultdoctor($data);
				break;

			case 'alltreatment':
				$this->alltreatment($data);
				break;

			case 'getavailability':
				$this->getavailability($data);
				break;
			
			default:
				$this->logout();
				break;
		}
	}

	// Add treatment
	public function addtreatment($data)
	{
		date_default_timezone_set("Asia/Kolkata");
		$treatment = $data['treatment'];
		$id = $data['id'];

		/*$medErr = array();		
		foreach ($treatment->meds as $key => $med) {
			//Get Total Medicine
			$indb = $this->db->select('SUM(no_of_unit) as total')->where('medicine_id', $med->medicine)->get('medicine_store')->row_array();
			//Get Total Medicine Sold
			$sold = $this->db->select('SUM(quantity) as total')->where('medicine_id', $med->medicine)->get('treatment_medicine')->row_array();

			$available = $indb['total']-$sold['total'];
			if(intval($available) < intval($med->quantity)) {
				$medicine = $this->db->where('medicine_id', $med->medicine)->get('medicine')->row_array();
				array_push($medErr, '<strong>'.$medicine['name'].'</strong><br/>Available: '.$available.'<br/>Issued: '.$med->quantity);
			}
		}
		if(count($medErr) > 0) {
			$this->jsonify(array(
				'msg' => 'Some of the medicine you issued are unavailable.<br/>'.implode('<br/><br/>', $medErr),
				'status' => 0
			));
		}*/


		$data = array(
			't_id' => time().$id,
			'p_id' => $treatment->pid,
			'doc_id' => $treatment->doctor == 0 ? NULL : $treatment->doctor,
			'bphigh' => isset($treatment->bphigh) ? $treatment->bphigh : '',
			'bplow' => isset($treatment->bplow) ? $treatment->bplow : '',
			'pulse' => isset($treatment->pulse) ? $treatment->pulse : '',
			'weight' => isset($treatment->weight) ? $treatment->weight : '',
			'height' => isset($treatment->height) ? $treatment->height : '',
			'temp' => isset($treatment->temp) ? $treatment->temp : '',
			'stp' => isset($treatment->stp) ? $treatment->stp : '',
			'ltp' => isset($treatment->ltp) ? $treatment->ltp : '',
			'pd' => isset($treatment->pd) ? $treatment->pd : '',
			'diag' => isset($treatment->diagnosis) ? $treatment->diagnosis : '',
			'fasting' => isset($treatment->fasting) ? $treatment->fasting : '',
			'pp' => isset($treatment->pp) ? $treatment->pp : '',
			'random' => isset($treatment->random) ? $treatment->random : '',
			'treat_date' => date('Y-m-d'),
			'ip_address' => $this->input->ip_address()
		);
		$insert = $this->db->insert('treatment', $data);

		if($insert) {
			$treatment_id = $this->db->insert_id();
			$inc = 0;

			//Insert All Medicine
			foreach ($treatment->meds as $key => $med) {
				$inc = $inc+$key+1;
				$data = array(
					'tm_id' => (time()+$inc).$id,
					'treatment_id' => $treatment_id,
					'medicine_id' => $med->medicine,
					'batch_no' => $med->batch,
					'patient_id' => $treatment->pid,
					'dosage' => $med->dosage,
					'day' => $med->day,
					'quantity' => $med->quantity,
					'date' => date('Y-m-d')
				);
				$this->db->insert('treatment_medicine', $data);
			}

			//Insert All Current Disease
			foreach ($treatment->curr as $key => $curr) {
				$inc = $inc+$key+1;
				$disname = !isset($curr->dis_name) ? $curr->disname : $curr->dis_name;
				$data = array(
					'td_id' => (time()+$inc).$id,
					'treatment_id' => $treatment_id,
					'dis_name' => $disname,
					'dis_type' => $curr->distype,
					'patient_id' => $treatment->pid,
					'type' => 'current',
					'date' => date('Y-m-d')
				);
				$this->db->insert('treatment_disease', $data);
			}

			//Insert All Past Disease
			foreach ($treatment->past as $key => $past) {
				$inc = $inc+$key+1;
				$disname = !isset($past->dis_name) ? $past->disname : $past->dis_name;
				$data = array(
					'td_id' => (time()+$inc).$id,
					'treatment_id' => $treatment_id,
					'dis_name' => $disname,
					'dis_type' => $past->distype,
					'patient_id' => $treatment->pid,
					'type' => 'past',
					'date' => date('Y-m-d')
				);
				$this->db->insert('treatment_disease', $data);
			}

			$this->jsonify(array(
				'msg' => 'New treatment record added. View all treatment records under Find Patient section.',
				'status' => 1
			));
		} else {
			$this->jsonify(array(
				'msg' => 'Sorry! Cannot add treatment. Please try after sometime.',
				'status' => 0
			));
		}
	}

	// Get treatment
	public function gettreatment($data)
	{
		date_default_timezone_set("Asia/Kolkata");

		$this->db->select('treatment.*, IFNULL(doctor.name, "None") AS doctor_name');
		$this->db->from('treatment');
		$this->db->join('user_details AS doctor', 'doctor.user_id = treatment.doc_id', 'left');
		$this->db->where('treatment.p_id', $data['pid']);
		if(intval($data['tid']) != 0) {
			$this->db->where('treatment.t_id <', $data['tid'])->limit(1);
		}
		$treatment = $this->db->order_by('treatment.t_id', 'DESC')->get();

		if($treatment->num_rows() === 0) {
			$this->jsonify(array(
				'last' => true,
				'status' => 1
			));
		} else {
			$treatment = $treatment->row_array();

			$this->db->select('treatment_medicine.*, medicine.name, medicine.composition');
			$this->db->from('medicine');
			$this->db->join('treatment_medicine', 'treatment_medicine.medicine_id = medicine.medicine_id');
			$treatment['meds'] = $this->db->where('treatment_medicine.treatment_id', $treatment['t_id'])->get()->result_array();

			$treatment['curr'] = $this->db->where('treatment_id', $treatment['t_id'])->where('type', 'current')->get('treatment_disease')->result_array();

			$treatment['past'] = $this->db->where('treatment_id', $treatment['t_id'])->where('type', 'past')->get('treatment_disease')->result_array();

			$this->jsonify(array(
				'treatment' => $treatment,
				'status' => 1
			));
		}
	}

	// All medicine
	public function allmedicine($data)
	{
		date_default_timezone_set("Asia/Kolkata");
		
		$start = date('Y-m-d').' 00:00:00';
		$end = date('Y-m-d').' 23:59:59';

		$this->db->distinct();
		$medicine = $this->db->query("SELECT DISTINCT m.name, m.medicine_id FROM medicine AS m JOIN van_stock AS v ON v.medicine_id = m.medicine_id WHERE v.mmu_id = ".$data['mmu_id']."")->result_array();
		/*AND v.location_id = ".$data['location_id']." AND v.date_added BETWEEN '".$start."' AND '".$end."'*/

		$this->jsonify(array(
			'medicine' => $medicine,
			'status' => 1
		));
	}

	//Get Batches
	public function getbatch($data)
	{
		date_default_timezone_set("Asia/Kolkata");
		
		$start = date('Y-m-d').' 00:00:00';
		$end = date('Y-m-d').' 23:59:59';
		
		$batch = $this->db->query("SELECT distinct batch_no, exp_date FROM van_stock WHERE mmu_id = ".$data['mmu_id']." AND medicine_id = ".$data['medicine']."")->result_array();
		/*AND location_id = ".$data['location_id']." AND date_added BETWEEN '".$start."' AND '".$end."'*/

		$this->jsonify(array(
			'batch' => $batch,
			'status' => 1
		));
	}

	// All doctors
	public function alldoctor($data)
	{
		date_default_timezone_set("Asia/Kolkata");
		$doctors = $this->db->select('user_id, name')->where('mmu_unit', $data['mmu'])->where('role_id', 1)->get('user_details')->result_array();
		$this->jsonify(array(
			'doctors' => $doctors,
			'status' => 1
		));
	}

	// All diagnosis
	public function alldiagnosis()
	{
		date_default_timezone_set("Asia/Kolkata");
		$diagnosis = $this->db->get('diagnosis')->result_array();		
		$this->jsonify(array(
			'diagnosis' => $diagnosis,
			'status' => 1
		));
	}

	// Default doctor
	public function getdefaultdoctor($data)
	{
		date_default_timezone_set("Asia/Kolkata");
		$doctor = $this->db->query("SELECT user.user_id, user.name FROM user_details AS user JOIN default_doctor AS def ON def.doc_id = user.user_id WHERE def.location_id = ".$data['mmuloc']."");
		if($doctor->num_rows() > 0) {
			$this->jsonify(array(
				'doctor' => $doctor->row_array(),
				'status' => 1
			));
		} else {
			$this->jsonify(array(
				'doctor' => array(
					'user_id' => 0,
					'name' => 'None'
				),
				'status' => 1
			));
		}
	}

	// All treatment
	public function alltreatment($data)
	{
		date_default_timezone_set("Asia/Kolkata");
		$result = array();

		$this->db->select('treatment.*, IFNULL(doctor.name, "None") AS doctor_name');
		$this->db->from('treatment');
		$this->db->join('user_details AS doctor', 'doctor.user_id = treatment.doc_id', 'left');
		$this->db->where('treatment.p_id', $data['pid']);
		$treatment = $this->db->order_by('treatment.t_id', 'DESC')->get();

		if($treatment->num_rows() > 0) {
			$result = $treatment->result_array();

			foreach ($result as $key => $treatment) {
				$this->db->select('treatment_medicine.*, medicine.name, medicine.composition');
				$this->db->from('medicine');
				$this->db->join('treatment_medicine', 'treatment_medicine.medicine_id = medicine.medicine_id');
				$result[$key]['meds'] = $this->db->where('treatment_medicine.treatment_id', $treatment['t_id'])->get()->result_array();

				$result[$key]['curr'] = $this->db->where('treatment_id', $treatment['t_id'])->where('type', 'current')->get('treatment_disease')->result_array();

				$result[$key]['past'] = $this->db->where('treatment_id', $treatment['t_id'])->where('type', 'past')->get('treatment_disease')->result_array();
				
				$result[$key]['collapsed'] = true;
			}
		}

		$this->jsonify(array(
			'treatments' => $result,
			'status' => 1
		));
	}

	// Default doctor
	public function getavailability($data)
	{
		date_default_timezone_set("Asia/Kolkata");
		/*//Get Total Medicine
		$indb = $this->db->select('SUM(no_of_unit) as total')->where('medicine_id', $data['medicine'])->where('mmu_id', $data['mmu'])->get('medicine_store')->row_array();
		//Get Total Medicine Sold
		$sold = $this->db->select('SUM(treat.quantity) as total')->from('treatment_medicine AS treat')->join('patient', 'patient.patient_id = treat.patient_id')->where('patient.mmu', $data['mmu'])->where('treat.medicine_id', $data['medicine'])->get()->row_array();

		$available = $indb['total']-$sold['total'];*/

        $today = date('Y-m-d');
        $start = $today.' 00:00:00';
        $end = $today.' 23:59:59';

        $vanstock = $this->db->query("SELECT sum(no_of_unit) AS loaded, exp_date FROM `van_stock` WHERE return_unit IS NULL AND `batch_no` = '".$data['batch']."' AND `mmu_id` = ".$data['mmu']." AND medicine_id = ".$data['medicine']." GROUP BY batch_no")->row_array();
        /*AND location_id = ".$data['location_id']." AND date_added BETWEEN '".$start."' AND '".$end."'*/

        //Get Total Medicine Sold
		$sold = $this->db->select('SUM(treat.quantity) as total')->from('treatment_medicine AS treat')->join('patient', 'patient.patient_id = treat.patient_id')->where('patient.mmu', $data['mmu'])->where('treat.medicine_id', $data['medicine'])->where('treat.batch_no', $data['batch'])->get()->row_array();
		/*->where("treat.date BETWEEN '".$start."' AND '".$end."'")*/

		$available = $vanstock['loaded'] - $sold['total'];

		$this->jsonify(array(
			'available' => $available,
			'exp_date' => $vanstock['exp_date'],
			'status' => 1
		));
	}

	//logout ++++++++ session
	public function logout()
	{
		$this->jsonify(array(
			'logout' => true
		));
	}

	public function jsonify($data)
	{
		print_r(json_encode($data));
		exit();
	}
}