<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mmuadmin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
    }

    /*function index() {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['page_name'] = 'dashboard';
        $data['page_title'] = get_phrase('mmuadmin_dashboard');
        $this->load->view('backend/index', $data);
    }*/

    function index() {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $mmu_unit=$this->session->userdata("mmu_unit");
        $data['mnumber']=$this->db->get('mmu_unit')->row('mmu_number');
        $data['m_loc']=$this->db->query("SELECT mmuloc_name, mmuloc_id FROM mmu_locations WHERE mmu_unit=$mmu_unit")->result_array();
        $data['page_name'] = 'dashboard';
        $data['page_title'] = get_phrase('mmu_admin_dashboard');

        /*var_dump($data); exit();*/
        $this->load->view('backend/index', $data);
    }

    function invoice_add($task = "") {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $this->crud_model->create_invoice();
            $this->session->set_flashdata('message', get_phrase('invoice_info_saved_successfuly'));
            redirect(base_url() . 'index.php?mmuadmin/invoice_manage');
        }

        $data['page_name'] = 'add_invoice';
        $data['page_title'] = get_phrase('invoice');
        $this->load->view('backend/index', $data);
    }

    function invoice_manage($task = "", $invoice_id = "") {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "update") {
            $this->crud_model->update_invoice($invoice_id);
            $this->session->set_flashdata('message', get_phrase('invoice_info_updated_successfuly'));
            redirect(base_url() . 'index.php?mmuadmin/invoice_manage');
        }

        if ($task == "delete") {
            $this->crud_model->delete_invoice($invoice_id);
            redirect(base_url() . 'index.php?mmuadmin/invoice_manage');
        }

        $data['invoice_info'] = $this->crud_model->select_invoice_info();
        $data['page_name'] = 'manage_invoice';
        $data['page_title'] = get_phrase('invoice');
        $this->load->view('backend/index', $data);
    }

    function manage_profile($task = "") {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $accountant_id = $this->session->userdata('login_user_id');
        if ($task == "update") {
                $this->crud_model->update_accountant_info($accountant_id);
                $this->session->set_flashdata('message', get_phrase('profile_info_updated_successfuly'));
                redirect(base_url() . 'index.php?mmuadmin/manage_profile');
        }

        if ($task == "change_password") {
            $password = $this->db->get_where('accountant', array('accountant_id' => $accountant_id))->row()->password;
            $old_password = sha1($this->input->post('old_password'));
            $new_password = $this->input->post('new_password');
            $confirm_new_password = $this->input->post('confirm_new_password');

            if ($password == $old_password && $new_password == $confirm_new_password) {
                $data['password'] = sha1($new_password);

                $this->db->where('accountant_id', $accountant_id);
                $this->db->update('accountant', $data);

                $this->session->set_flashdata('message', get_phrase('password_info_updated_successfuly'));
                redirect(base_url() . 'index.php?mmuadmin/manage_profile');
            } else {
                $this->session->set_flashdata('message', get_phrase('password_update_failed'));
                redirect(base_url() . 'index.php?mmuadmin/manage_profile');
            }
        }

        $data['page_name'] = 'edit_profile';
        $data['page_title'] = get_phrase('profile');
        $this->load->view('backend/index', $data);
    }

    function form($task = "") {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['page_name'] = 'form_create';
        $data['page_title'] = get_phrase('create_form');
        $this->load->view('backend/index', $data);
    }

    function get_form_element($element_type) {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        echo $html = $this->db->get_where('form_element', array('type' => $element_type))->row()->html;
        //$this->load->view('backend/mmuadmin/form_create_body', $html);
        //echo $element_type;
    }
    function manage_mmu(){
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        
        $data['page_name'] = 'manage_mmu';
        $data['page_title'] = get_phrase('manage_MMU');
        $data['mmuunit'] = $this->db->query("SELECT unit.*, state.state_name, district.dist_name
                FROM mmu_unit AS unit
                JOIN state_table AS state ON state.state_id = unit.state_id
                JOIN district_table AS district ON district.dist_id = unit.dist_id
                WHERE unit.status = 0 AND unit.unit_id = ".$this->session->userdata('mmu_unit')."")->result_array();
        $this->load->view('backend/index', $data);

    }
    function edit_mmu($mmu_id){
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $data['states'] = $this->db->query("SELECT * FROM state_table WHERE state_status = 0")->result_array();
        $data['district'] = $this->db->query("SELECT * FROM district_table WHERE status = 0")->result_array();
        $data['getmmuby_id'] = $this->db->query("SELECT * FROM mmu_unit WHERE unit_id = '".$mmu_id."' ")->row();
        $data['page_name'] = 'edit_mmu';
        $data['page_title'] = get_phrase('edit_mmu');
        $this->load->view('backend/index', $data);
    }

    function updatemmu(){
         if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $error = array('state' => '', 'district' => '', 'city' => '', 'panchayat' => '', 'mmu_number' => '','status' => 0);
            if(isset($_POST['mmu_id'])) {
                if(empty($_POST['state'])) {
                    $error['state'] = 'state Field is mandatory';
                    $error['status'] = 1;
                }
                if(empty($_POST['district'])) {
                    $error['district'] = 'District Field is mandatory';
                    $error['status'] = 1;
                }                
                if(empty($_POST['city'])) {
                    $error['city'] = 'City Field is mandatory';
                    $error['status'] = 1;
                }
                if(empty($_POST['panchayat'])) {
                    $error['panchayat'] = 'Panchayat Field is mandatory';
                    $error['status'] = 1;
                }
                if(empty($_POST['mmu_number'])) {
                    $error['mmu_number'] = 'mmu_number Field is mandatory';
                    $error['status'] = 1;
                }
               
                if($error['status'] > 0) {
                    echo json_encode($error);
                    exit();
                }
                $data = array(
                    'state_id' => $_POST['state'],
                    'dist_id' => $_POST['district'],
                    'city_town' => $_POST['city'],
                    'panchayat_municipality' => $_POST['panchayat'],
                    'mmu_name' => $_POST['mmu_name'],
                    'mmu_number' => $_POST['mmu_number']
                );
                $query = $this->db->where('unit_id', $_POST['mmu_id'])->update('mmu_unit', $data);
                if($query){
                    echo json_encode(array('msg' => 'mmu Updated Successfully'));
                    exit();
                }
                else{
                    echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
                    exit();
                }
            }
            else {
                echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
                exit();
            }
    }
    function mmu_location() {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $unit_id = $this->uri->segment(3);
        if (empty($unit_id)) {
            show_404();
        }

        $data['page_name'] = 'mmu_location';
        $data['page_title'] = get_phrase('MMU_location');

        $data['mmulocation'] = $this->db->query("SELECT locations.*, unit.mmu_name
            FROM mmu_locations AS locations
            JOIN mmu_unit AS unit ON unit.unit_id = locations.mmu_unit
            WHERE locations.mmu_unit = '".$unit_id."' AND locations.mmuloc_status = 0")->result_array();

        $this->load->view('backend/index', $data);
    }
    function add_mmu_location() {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['page_name'] = 'add_mmu_location';
        $data['page_title'] = get_phrase('add_MMU_site');
        $data['mmuunit'] = $this->db->query("SELECT * FROM mmu_unit WHERE status = 0 AND unit_id = ".$this->session->userdata('mmu_unit')."")->result_array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('mmuunit','MMU','required');
        $this->form_validation->set_rules('mmu_location','MMU Location Name','required');
        if ($this->form_validation->run() == FALSE)
        {
        $this->load->view('backend/index', $data);
        }
        else {
            $data = array(
                'mmu_unit' => $this->input->post('mmuunit'),
                'mmuloc_name' => $this->input->post('mmu_location'),
                'mmuloc_status' => 0
            );
            $query = $this->db->insert('mmu_locations', $data);
            if($query){
                $this->session->set_flashdata("succ", "MMU Location added Successfully.");
            }else{
                $this->session->set_flashdata("err", "Sorry! System not adding");
            }
            redirect($baseurl.'mmuadmin/add_mmu_location');
        }
    }

    function edit_mmu_location($mmu_location){
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $data['mmulocation_details'] = $this->db->query("SELECT * FROM mmu_locations WHERE mmuloc_id = '".$mmu_location."' ")->row();
        $data['mmuunit'] = $this->db->query("SELECT * FROM mmu_unit WHERE status = 0 AND unit_id = ".$this->session->userdata("mmu_unit")."")->result_array();
        $data['page_name'] = 'edit_mmu_location';
        $data['page_title'] = get_phrase('edit_MMU_location');
        $this->load->view('backend/index', $data);
    }
   function updatemmu_location(){
         if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $error = array('mmuunit' => '', 'mmu_location' => '','status' => 0);
            if(isset($_POST['mmu_location_id'])) {
                if(empty($_POST['mmuunit'])) {
                    $error['mmuunit'] = 'MMU unit Field is mandatory';
                    $error['status'] = 1;
                }
                if(empty($_POST['mmu_location'])) {
                    $error['mmu_location'] = 'MMU Location Field is mandatory';
                    $error['status'] = 1;
                }                
                if($error['status'] > 0) {
                    echo json_encode($error);
                    exit();
                }
                $data = array(
                    'mmu_unit' => $_POST['mmuunit'],
                    'mmuloc_name' => $_POST['mmu_location']
                );
                $query = $this->db->where('mmuloc_id', $_POST['mmu_location_id'])->update('mmu_locations', $data);
                if($query){
                    echo json_encode(array('msg' => 'MMU Location Updated Successfully'));
                    exit();
                }
                else{
                    echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
                    exit();
                }
            }
            else {
                echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
                exit();
            }
    }
    function doctor() {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $data['page_name'] = 'manage_doctor';
        $data['page_title'] = get_phrase('manage_doctor');
        $data['doctor_info'] = $this->db->query("SELECT doc.*, unit.mmu_name
                FROM user_details AS doc
                JOIN mmu_unit AS unit ON unit.unit_id = doc.mmu_unit
                WHERE doc.status = 0 and role_id = 1 and doc.mmu_unit = ".$this->session->userdata('mmu_unit')."")->result_array();
        $this->load->view('backend/index', $data);
    }
    function add_doctor() {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['page_name'] = 'add_doc';
        $data['page_title'] = get_phrase('add_doctor');
        $data['alldepartment'] = $this->db->query("SELECT * FROM department WHERE status = 0")->result_array();
        $this->load->view('backend/index', $data);
    }

    public function insert_doctor(){
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $error = array('name' => '', 'mrc' => '', 'email' => '', 'pass' => '', 'department' => '', 'address' => '', 'phone' => '', 'image' => '', 'status' => 0);            
        if(empty($_POST['name'])) {
            $error['name'] = 'Name Field is mandatory';
            $error['status'] = 1;
        }
        else if(!preg_match('/^[a-z][a-z \.]*$/i',$_POST['name'])) {
            $error['name'] = 'Please input alfa characters only';
            $error['status'] = 1;
        }
        else if(strlen($_POST['name']) > 150) {
            $error['name'] = 'Name Field must be within 150 characters';
            $error['status'] = 1;
        }

        if(empty($_POST['mrc'])) {
            $error['mrc'] = 'Mrc Field is mandatory';
            $error['status'] = 1;
        }
        else if(!preg_match('/^[a-zA-Z0-9]+$/',$_POST['mrc'])) {
            $error['mrc'] = 'Please input alfanumeric characters only';
            $error['status'] = 1;
        }
        else if(strlen($_POST['mrc']) > 150) {
            $error['mrc'] = 'MRC Field must be within 150 characters';
            $error['status'] = 1;
        }
        if(!empty($_POST['mrc'])){
            $this->load->library('form_validation');
            $this->db->select('user_id');
            $this->db->where('mrc_no', $_POST['mrc']);
            $query = $this->db->get('user_details');
            if ($query->num_rows() > 0) {
                $error['mrc'] = 'Mrc is already in use.';
                $error['status'] = 1;
            }
        }
        if(empty($_POST['email'])) {
            $error['email'] = 'Please input valid email only';
            $error['status'] = 1;
        }
        else if(!preg_match('/@.+\./',$_POST['email'])) {
            $error['email'] = 'Please enter valid Email-ID';
            $error['status'] = 1;
        }
        else if(!empty($_POST['email'])){
            $this->load->library('form_validation');
            $this->db->select('user_id');
            $this->db->where('email', $_POST['email']);
            $query = $this->db->get('user_details');
            if ($query->num_rows() > 0) {
                $error['email'] = 'Entered email id is already registered.';
                $error['status'] = 1;
            }
        }          
        if(empty($_POST['pass'])) {
            $error['pass'] = 'Password Field is mandatory';
            $error['status'] = 1;
        }

        if(empty($_POST['phone'])) {
            $error['phone'] = 'Phone No Field is mandatory';
            $error['status'] = 1;
        }
        else if(!is_numeric($_POST['phone'])) {
            $error['phone'] = 'Phone No Field must be Numeric';
            $error['status'] = 1;
        }
        else if(strlen($_POST['phone']) < 10) {
            $error['phone'] = 'Phone No must be within 10 to 15 digits';
            $error['status'] = 1;
        }
        else if(strlen($_POST['phone']) > 15) {
            $error['phone'] = 'Phone No must be within 10 to 15 digits';
            $error['status'] = 1;
        }
        else if(!empty($_POST['phone'])){
            $this->load->library('form_validation');
            $this->db->select('user_id');
            $this->db->where('phone', $_POST['phone']);
            $query = $this->db->get('user_details');
            if ($query->num_rows() > 0) {
                $error['phone'] = 'Phone No is already Used.';
                $error['status'] = 1;
            }
        }
        if(empty($_POST['department'])) {
            $error['department'] = 'Department Field is mandatory';
            $error['status'] = 1;
        }
        if($error['status'] > 0) {
            echo json_encode($error);
            exit();
        }
        if(isset($_FILES['image']) && $_FILES['image']['size'] > 0) {
            $timestamp = new DateTime();
            $timestamp = $timestamp->format('U');
            $imagename = $timestamp. '_' .$_FILES['image']['name'];
            move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/doctor_image/' .$imagename);
        }                
        $data = array(
            'name' => $_POST['name'],
            'mrc_no' => $_POST['mrc'],
            'email' => $_POST['email'],
            'password' => sha1($_POST['pass']),
            'address' => $_POST['address'],
            'phone' => $_POST['phone'],
            'mmu_unit' => $this->session->userdata('mmu_unit'),
            'department_id' => $_POST['department'],
            'profile_image' => isset($imagename) ? $imagename : 'default.png',
            'profile' => $this->input->post('profile'),
            'date' => date('Y-m-d H:i:s'),
            'ip_address' => $this->input->ip_address(),
            'role_id' => 1,
            'status' => 0
        );
        $query = $this->db->insert('user_details', $data);
        if($query){
            echo json_encode(array('msg' => 'Doctor Added Successfully'));
            exit();
        }
        else{
            echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
            exit();
        }
    }
    //SPO
    function spo() {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        
        $data['page_name'] = 'spo';
        $data['page_title'] = get_phrase('SPO');
        $data['allspo'] = $this->db->query("SELECT spou.*, unit.mmu_name
                FROM user_details AS spou
                JOIN mmu_unit AS unit ON unit.unit_id = spou.mmu_unit
                WHERE spou.status = 0 and role_id = 2 and mmu_unit = ".$this->session->userdata("mmu_unit")."")->result_array();
        $this->load->view('backend/index', $data);
    }


    function add_spo() {
        if($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        
        $data['page_name'] = 'add_spo';
        $data['page_title'] = get_phrase('Add SPO');
        $data['allmmuunit'] = $this->db->query("SELECT unit.*, state.state_name, district.dist_name
                FROM mmu_unit AS unit
                JOIN state_table AS state ON state.state_id = unit.state_id
                JOIN district_table AS district ON district.dist_id = unit.dist_id
                WHERE unit.status = 0")->result_array();
        $this->load->view('backend/index', $data);
    }

    function insert_spo(){
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $error = array('name' => '', 'email' => '', 'pass' => '', 'phone' => '','status' => 0);            
        if(empty($_POST['name'])) {
            $error['name'] = 'Name Field is mandatory';
            $error['status'] = 1;
        }
        else if(!preg_match('/^[a-z][a-z \.]*$/i',$_POST['name'])) {
            $error['name'] = 'Please input alfa characters only';
            $error['status'] = 1;
        }
        else if(strlen($_POST['name']) > 150) {
            $error['name'] = 'Name Field must be within 150 characters';
            $error['status'] = 1;
        }
        if(empty($_POST['email'])) {
            $error['email'] = 'Email-ID Field is mandatory';
            $error['status'] = 1;
        }
        else if(!preg_match('/@.+\./',$_POST['email'])) {
            $error['email'] = 'Please enter valid Email-ID';
            $error['status'] = 1;
        }
        else if(!empty($_POST['email'])){
            $this->load->library('form_validation');
            $this->db->select('user_id');
            $this->db->where('email', $_POST['email']);
            $query = $this->db->get('user_details');
            if ($query->num_rows() > 0) {
                $error['email'] = 'Entered email id is already registered.';
                $error['status'] = 1;
            }
        }          
        if(empty($_POST['pass'])) {
            $error['pass'] = 'Password Field is mandatory';
            $error['status'] = 1;
        }
        if(empty($_POST['phone'])) {
            $error['phone'] = 'Phone No Field is mandatory';
            $error['status'] = 1;
        }
        else if(!is_numeric($_POST['phone'])) {
            $error['phone'] = 'Phone No Field must be Numeric';
            $error['status'] = 1;
        }
        else if(strlen($_POST['phone']) < 10) {
            $error['phone'] = 'Phone No must be within 10 to 15 digits';
            $error['status'] = 1;
        }
        else if(strlen($_POST['phone']) > 15) {
            $error['phone'] = 'Phone No must be within 10 to 15 digits';
            $error['status'] = 1;
        }
        else if(!empty($_POST['phone'])){
            $this->load->library('form_validation');
            $this->db->select('user_id');
            $this->db->where('phone', $_POST['phone']);
            $query = $this->db->get('user_details');
            if ($query->num_rows() > 0) {
                $error['phone'] = 'Phone No is already Used.';
                $error['status'] = 1;
            }
        }
        
        if($error['status'] > 0) {
            echo json_encode($error);
            exit();
        }
        if(isset($_FILES['image']) && $_FILES['image']['size'] > 0) {
            $timestamp = new DateTime();
            $timestamp = $timestamp->format('U');
            $imagename = $timestamp. '_' .$_FILES['image']['name'];
            move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/spo_image/' .$imagename);
        }                
        $data = array(
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'password' => sha1($_POST['pass']),
            'address' => $_POST['address'],
            'phone' => $_POST['phone'],
            'mmu_unit' => $this->session->userdata('mmu_unit'),
            'profile_image' => isset($imagename) ? $imagename : 'default.png',
            'profile' => $this->input->post('profile'),
            'date' => date('Y-m-d H:i:s'),
            'ip_address' => $this->input->ip_address(),
            'role_id' => 2,
            'status' => 0
        );
        $query = $this->db->insert('user_details', $data);
        if($query){
            echo json_encode(array('msg' => 'Spo Added Successfully'));
            exit();
        }
        else{
            echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
            exit();
        }
    }
    function pharmacist() {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['pharmacist_info'] = $this->db->query("SELECT pharm.*, unit.mmu_name
                FROM user_details AS pharm
                JOIN mmu_unit AS unit ON unit.unit_id = pharm.mmu_unit
                WHERE pharm.status = 0 and role_id = 3 and mmu_unit = ".$this->session->userdata("mmu_unit")."")->result_array();
        $data['page_name'] = 'manage_pharmacist';
        $data['page_title'] = get_phrase('pharmacist');
        $this->load->view('backend/index', $data);
    }

    function add_pharmacist() {
        if($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }        
        $data['page_name'] = 'add_pharmacist';
        $data['page_title'] = get_phrase('add_pharmacist');
        $data['allmmuunit'] = $this->db->query("SELECT unit.*, state.state_name, district.dist_name
                FROM mmu_unit AS unit
                JOIN state_table AS state ON state.state_id = unit.state_id
                JOIN district_table AS district ON district.dist_id = unit.dist_id
                WHERE unit.status = 0")->result_array();
        $this->load->view('backend/index', $data);
    }

    public function insert_pharmacist() {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $error = array('name' => '', 'email' => '', 'pass' => '', 'address' => '', 'phone' => '', 'status' => 0);            
        if(empty($_POST['name'])) {
            $error['name'] = 'Name Field is mandatory';
            $error['status'] = 1;
        }
        else if(!preg_match('/^[a-z][a-z \.]*$/i',$_POST['name'])) {
            $error['name'] = 'Please input alfa characters only';
            $error['status'] = 1;
        }
        else if(strlen($_POST['name']) > 150) {
            $error['name'] = 'Name Field must be within 150 characters';
            $error['status'] = 1;
        }
        if(empty($_POST['email'])) {
            $error['email'] = 'Email-ID Field is mandatory';
            $error['status'] = 1;
        }
        else if(!preg_match('/@.+\./',$_POST['email'])) {
            $error['email'] = 'Please enter valid Email-ID';
            $error['status'] = 1;
        }
        else if(!empty($_POST['email'])){
            $this->load->library('form_validation');
            $this->db->select('user_id');
            $this->db->where('email', $_POST['email']);
            $query = $this->db->get('user_details');
            if ($query->num_rows() > 0) {
                $error['email'] = 'Entered email id is already registered.';
                $error['status'] = 1;
            }
        }          
        if(empty($_POST['pass'])) {
            $error['pass'] = 'Password Field is mandatory';
            $error['status'] = 1;
        }
        if(empty($_POST['phone'])) {
            $error['phone'] = 'Phone No Field is mandatory';
            $error['status'] = 1;
        }
        else if(!is_numeric($_POST['phone'])) {
            $error['phone'] = 'Phone No Field must be Numeric';
            $error['status'] = 1;
        }
        else if(strlen($_POST['phone']) < 10) {
            $error['phone'] = 'Phone No must be within 10 to 15 digits';
            $error['status'] = 1;
        }
        else if(strlen($_POST['phone']) > 15) {
            $error['phone'] = 'Phone No must be within 10 to 15 digits';
            $error['status'] = 1;
        }
        if($error['status'] > 0) {
            echo json_encode($error);
            exit();
        }
        if(isset($_FILES['image']) && $_FILES['image']['size'] > 0) {
            $timestamp = new DateTime();
            $timestamp = $timestamp->format('U');
            $imagename = $timestamp. '_' .$_FILES['image']['name'];
            move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/pharmacist_image/' .$imagename);
        }                
        $data = array(
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'password' => sha1($_POST['pass']),
            'address' => $_POST['address'],
            'phone' => $_POST['phone'],
            'mmu_unit' => $this->session->userdata('mmu_unit'),
            'profile_image' => isset($imagename) ? $imagename : 'default.png',
            'profile' => $this->input->post('profile'),
            'date' => date('Y-m-d H:i:s'),
            'ip_address' => $this->input->ip_address(),
            'role_id' => 3,
            'status' => 0
        );
        $query = $this->db->insert('user_details', $data);
        if($query){
            echo json_encode(array('msg' => 'Pharmacist Added Successfully'));
            exit();
        }
        else{
            echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
            exit();
        }
    }

    function medicine() {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['medicine_info'] = $this->db->query("SELECT m.medicine_id, m.name,
                                    (SELECT SUM(no_of_unit)
                                      FROM medicine_store
                                     WHERE medicine_id = m.medicine_id AND mmu_id = ".$this->session->userdata('mmu_unit')."
                                   ) AS no_of_unit, (SELECT (sum(no_of_unit) - sum(IFNULL(return_unit, 0))) FROM `van_stock` WHERE medicine_id = m.medicine_id AND mmu_id = ".$this->session->userdata('mmu_unit').") AS units_sold
                                   FROM medicine AS m")->result_array();
        /*foreach ($medicine_info as $key => $value) {
            $distributed = $this->db
                            ->select('SUM(treat.quantity) AS total')
                            ->from('treatment_medicine AS treat')
                            ->join('patient AS pat', 'pat.patient_id = treat.patient_id')
                            ->where('pat.mmu', $this->session->userdata('mmu_unit'))
                            ->where('treat.medicine_id', $value['medicine_id'])
                            ->get()->row_array();
            $medicine_info[$key]['units_sold'] = $medicine_info[$key]['units_sold']-$distributed['total'];
        }*/

        $data['page_name'] = 'manage_medicine';
        $data['page_title'] = get_phrase('medicine');
        $this->load->view('backend/index', $data);
    }

    function add_medicine() {
        if($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        date_default_timezone_set('Asia/Kolkata');
        if(isset($_POST['submit'])) {
            $medicine = $this->db->where('medicine_id', $this->input->post('name'))->get('medicine');
            if($medicine->num_rows() == 0) {
                $this->db->insert('medicine', array(
                    'name' => $this->input->post('name'),
                    'preparation' => $this->input->post('preparation'),
                    'composition' => $this->input->post('composition'),
                    'date' => date('Y-m-d H:i:s'),
                    'ip_address' => $this->input->ip_address()
                ));
                $medicine_id = $this->db->insert_id();
            } else {
                $medicine_id = $this->input->post('name');
            }

            if(!isset($_POST['expiry']) || $_POST['expiry'] == NULL) {
                $batch_no = $this->input->post('batch');
                $query = $this->db->query("SELECT DISTINCT exp_date  
                FROM `medicine_store` 
                WHERE `batch_no` = '".$batch_no."' and medicine_id = '".$medicine_id."'")->row_array();
                $expiry = $query['exp_date'];
            } else {
                $batch_no = $this->input->post('batch');
                $expiry = $this->input->post('expiry');
            }

            $query = $this->db->insert('medicine_store', array(
                'medicine_id' => $medicine_id,
                'mmu_id' => $this->session->userdata('mmu_unit'),
                'no_of_unit' => $this->input->post('unit'),
                'batch_no' => $batch_no,
                'exp_date' => $expiry,
                'manufacturing_company' => $this->input->post('company'),
                'date' => date('Y-m-d H:i:s'),
                'ip_address' => $this->input->ip_address(),
                'status ' => 0
            ));
            if($query){
                $this->session->set_flashdata("succ", "Medicine Successfully Added To Store.");                
            }else{
                $this->session->set_flashdata("err", "Sorry! System not adding");
            }
            redirect($baseurl.'mmuadmin/add_medicine');
        }

        //$data['mmuunit'] = $this->db->query("SELECT * FROM mmu_unit WHERE status = 0")->result_array();
        $data['page_name'] = 'add_medicine';
        $data['page_title'] = get_phrase('add_medicine');

        $this->load->view('backend/index', $data);
        
        /*$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('name','Name','required');
        $this->form_validation->set_rules('composition','Composition','required');
        $this->form_validation->set_rules('preparation','Preparation','required');
        $this->form_validation->set_rules('mmuunit','mmuunit','required');
        $this->form_validation->set_rules('unit','No of Unit','required');
        $this->form_validation->set_rules('batch','Batch','required');
        $this->form_validation->set_rules('company','Company','required');
        if ($this->form_validation->run() == FALSE)
        {
        $this->load->view('backend/index', $data);
        }
        else {
            exit();
            $medicine = $this->db->where('medicine_id', $this->input->post('name'))->get('medicine');
            if($medicine->num_rows() == 0) {
                $this->db->insert('medicine', array(
                    'name' => $this->input->post('name'),
                    'preparation' => $this->input->post('preparation'),
                    'composition' => $this->input->post('composition'),
                    'date' => date('Y-m-d H:i:s'),
                    'ip_address' => $this->input->ip_address()
                ));
                $medicine_id = $this->db->insert_id();
            } else {
                $medicine_id = $this->input->post('name');
            }
            if($this->input->post('expiry') != ''){
                $date = date('Y-m-d', strtotime($this->input->post('expiry')));
            }else if($this->input->post('dbexpiry') != ''){
                $date = $this->input->post('dbexpiry');
            }
            $query = $this->db->insert('medicine_store', array(
                'medicine_id' => $medicine_id,
                'mmu_id' => $this->input->post('mmuunit'),
                'no_of_unit' => $this->input->post('unit'),
                'batch_no' => $this->input->post('batch'),
                'exp_date' => date('Y-m-d', strtotime($this->input->post('expiry'))),
                'manufacturing_company' => $this->input->post('company'),
                'date' => date('Y-m-d H:i:s'),
                'ip_address' => $this->input->ip_address(),
                'status ' => 0
            ));
            if($query){
                $this->session->set_flashdata("succ", "Medicine Added Successfully.");                
            }else{
                $this->session->set_flashdata("err", "Sorry! System not adding");
            }
            redirect($baseurl.'admin/add_medicine');
        }*/
    }

    function get_medicine() {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            echo json_encode(array(
                'items' => array(),
                'total_count' => 0,
                'remaining' => 0
            ));
            exit();
        }

        $data = $this->input->post();
        $start = ($data['page']-1)*10;
        $limit = $data['page']*10;

        $meds = (!isset($data['meds']) || $data['meds'] == NULL) ? 0 : implode(',', $data['meds']);

        if(isset($data['q'])) {
            $query = $this->db->query("SELECT medicine.medicine_id as id, medicine.name, medicine.composition, medicine.preparation
            FROM medicine
            WHERE medicine.name LIKE '".$data['q']."%'
            AND medicine.medicine_id NOT IN (".$meds.")");
        } else {
            $query = $this->db->query("SELECT medicine.medicine_id as id, medicine.name, medicine.composition, medicine.preparation
            FROM medicine
            WHERE medicine.medicine_id NOT IN (".$meds.")");
        }

        if($query->num_rows() > 0){
            echo json_encode(array(
                'items' => $query->result_array(),
                'total_count' => $query->num_rows(),
                'remaining' => $query->num_rows()>9 ? 1 : 0
            ));
            exit();
        }
        else{
            echo json_encode(array(
                'items' => array(),
                'total_count' => 0,
                'remaining' => 0
            ));
            exit();
        }
        /*$data['pharmacist_info'] = $this->crud_model->select_pharmacist_info();*/
        /*$data['pharmacist_info'] = $this->db->query("SELECT pharm.*, unit.mmu_name
                FROM pharmacist AS pharm
                JOIN mmu_unit AS unit ON unit.unit_id = pharm.mmu_unit
                WHERE pharm.status = 0")->result_array();*/
    }

    function get_medicinebatch(){
        if ($this->session->userdata('mmuadmin_login') != 1) {
            echo json_encode(array(
                'items' => array(),
                'total_count' => 0,
                'remaining' => 0
            ));
            exit();
        }

        $data = $this->input->post();
        $start = ($data['page']-1)*10;
        $limit = $data['page']*10;

        $keyword = $data['q'];
        $val = $data['med'];

        $query = $this->db->query("SELECT DISTINCT batch_no as id, batch_no, exp_date  
        FROM `medicine_store` 
        WHERE `batch_no` LIKE '".$keyword."%' and medicine_id = '".$val."'");

        if($query->num_rows() > 0){
            echo json_encode(array(
                'items' => $query->result_array(),
                'total_count' => $query->num_rows(),
                'remaining' => 0
            ));
            exit();
        }
        else{
            echo json_encode(array(
                'items' => array(),
                'total_count' => 0,
                'remaining' => 0
            ));
            exit();
        }
    }

    function medicine_stock() {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $medicine_id = $this->uri->segment(3);
        if (empty($medicine_id)) {
            show_404();
        }

        $data['medicine_stock'] = $this->db->query("SELECT ms.*, med.name ,med.preparation
                FROM medicine_store AS ms
                JOIN medicine AS med ON med.medicine_id = ms.medicine_id
                WHERE ms.medicine_id = ".$medicine_id." AND ms.mmu_id = ".$this->session->userdata('mmu_unit')."")->result_array();
        $data['page_name'] = 'medicine_stock';
        $data['page_title'] = get_phrase('medicine_stock');
        $this->load->view('backend/index', $data);
    }

    function medicine_sold() {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $medicine_id = $this->uri->segment(3);
        if (empty($medicine_id)) {
            show_404();
        }
        $data['allmedicine'] = $this->db->query("SELECT (sum(van.no_of_unit) - sum(IFNULL(van.return_unit, 0))) AS loaded, med.name, van.exp_date, van.date_added, van.batch_no
            FROM van_stock AS van
            JOIN medicine AS med ON med.medicine_id = van.medicine_id
            WHERE van.mmu_id = ".$this->session->userdata('mmu_unit')." AND van.medicine_id = ".$medicine_id."
            GROUP BY van.batch_no,van.medicine_id")->result_array();
        $data['page_name'] = 'medicine_sold';
        $data['page_title'] = get_phrase('medicine_sold');
        $this->load->view('backend/index', $data);
    }

    function medicine_update() {
        if($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $medicine_id = $this->uri->segment(3);
        if (empty($medicine_id)) {
            show_404();
        }

        $data['page_name'] = 'edit_medicine';
        $data['page_title'] = get_phrase('edit_medicine');
        $data['medicine'] = $this->db->query("SELECT * FROM medicine WHERE medicine_id = $medicine_id AND status = 0")->row_array();
        $this->load->view('backend/index', $data);
    }    

    function updmedicine() {
        if($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $error = array('name' => '', 'preparation' => '', 'composition' => '', 'status' => 0);
        if(isset($_POST['medicine_id'])) {
            if(empty($_POST['name'])) {
                $error['name'] = 'Name Field is mandatory';
                $error['status'] = 1;
            }
            if(empty($_POST['preparation'])) {
                $error['preparation'] = 'Preparation Field is mandatory';
                $error['status'] = 1;
            }
            if(empty($_POST['composition'])) {
                $error['composition'] = 'composition Field is mandatory';
                $error['status'] = 1;
            }

            if($error['status'] > 0) {
                echo json_encode($error);
                exit();
            }

            $data = array(
                'name' => $_POST['name'],
                'preparation' => $_POST['preparation'],
                'composition' => $_POST['composition']
            );
            $query = $this->db->where('medicine_id', $_POST['medicine_id'])->update('medicine', $data);
            if($query){
                echo json_encode(array('msg' => 'Medicine Updated Successfully'));
                exit();
            }
            else{
                echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
                exit();
            }
        }
        else {
            echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
            exit();
        }
    }

    function van_register() {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }

        $data['page_name']      = 'van_register';
        $data['page_title']     = get_phrase('van_register');
       
        $this->load->view('backend/index', $data);
    }
    
    function vanstock() {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }

        /*$_POST['date'] = "2017-05-12";*/
        $minvalue = "1990-01-01 00:00:00";
        $maxvalue = $_POST['date']." 23:59:59";
        /*$mmuid = $this->db->query("SELECT mmu_unit FROM user_details where user_id = '".$this->session->userdata('login_user_id')."' ")->row();       
        $medicinecount = $this->db->query("SELECT medicine_id, name FROM medicine")->result_array();
        foreach ($medicinecount as $key => $m) {            
            $noofunits = $this->db->query("SELECT SUM(no_of_unit) as noofunits
                FROM van_stock AS m
                WHERE m.medicine_id = '".$m['medicine_id']."' and m.mmu_id = '".$mmuid->mmu_unit."' and date_added BETWEEN '".$minvalue."' AND '".$maxvalue."' ")->row_array();
            $medicinecount[$key]['noofunits'] = $noofunits;
        }*/
        $medicine = $this->db->query("SELECT (SUM(v.no_of_unit) - SUM(IFNULL(v.return_unit, 0))) AS loaded, v.batch_no, v.exp_date, medicine.name, v.medicine_id
        FROM van_stock AS v
        JOIN medicine ON medicine.medicine_id = v.medicine_id
        WHERE v.mmu_id = ".$this->session->userdata('mmu_unit')." AND v.date_added BETWEEN '".$minvalue."' AND '".$maxvalue."'
        GROUP BY v.medicine_id, v.batch_no")->result_array();

        foreach ($medicine as $key => $value) {
            $this->db->select('SUM(treat.quantity) as total')->from('treatment_medicine AS treat');
            $this->db->join('patient', 'patient.patient_id = treat.patient_id');
            $this->db->where('patient.mmu', $this->session->userdata('mmu_unit'));
            $this->db->where('treat.medicine_id', $value['medicine'])->where('treat.batch_no', $value['batch_no']);
            $distributed = $this->db->get()->row_array();

            $medicine[$key]['sold'] = $distributed['total'];
            $medicine[$key]['available'] = $value['loaded'] - $distributed['total'];
        }
        /*var_dump($medicine);
        die();
        $data['count'] = $medicinecount;*/
        /*$allpatients = $this->db->query("SELECT patient_id, name FROM patient WHERE mmuloc = ".$_POST['location']." AND status = 1")->result_array();
        $minvalue = $_POST['date']." 00:00:00";
        $maxvalue = $_POST['date']." 23:59:59";
        foreach ($allpatients as $key => $patient) {
            $this->db->distinct();
            $this->db->select('tm.tm_id, tm.quantity, m.name');
            $this->db->from('medicine AS m');
            $this->db->join('treatment_medicine AS tm', 'tm.medicine_id = m.medicine_id');
            $medicines = $this->db->where("tm.patient_id", $patient['patient_id'])->where("tm.date BETWEEN '".$minvalue."' AND '".$maxvalue."'")->get()->result_array();
            $allpatients[$key]['medicines'] = $medicines;
            $allpatients[$key]['date'] = $_POST['date'];
        }*/
        /*$data['page_name'] = 'diagnosis';
        $data['page_title'] = get_phrase('diagnosis');
        $this->load->view('backend/index', $data);*/
        // echo '<pre>'; print_r($allpatients); echo '</pre>';
        echo json_encode( array(
            'medicines' => $medicine
        ));
        exit();
    }

    function add_medicine_van(){
        if($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        date_default_timezone_set('Asia/Kolkata');
        if(isset($_POST['submit'])) {
            foreach ($_POST['name'] as $key => $name) {
                $mstore = $this->db->query("SELECT DISTINCT exp_date, manufacturing_company
                FROM `medicine_store` 
                WHERE `batch_no` = '".$_POST['batch'][$key]."' and medicine_id = ".$name."")->row_array();

                $query = $this->db->insert('van_stock', array(
                    'user_id' => $this->session->userdata('login_user_id'),
                    'mmu_id' => $this->session->userdata('mmu_unit'),
                    /*'location_id' => $_POST['mmu'][$key],*/
                    'medicine_id' => $name,
                    'no_of_unit' => $_POST['requiredunitcount'][$key],
                    'batch_no' => $_POST['batch'][$key],
                    'exp_date' => $mstore['exp_date'],
                    'manufacturing_comp' => $mstore['manufacturing_company'],
                    'date_added' => date('Y-m-d H:i:s'),
                    'ip_address' => $this->input->ip_address(),
                    'status ' => 0
                ));
                if($query){
                    $this->session->set_flashdata("succ", "Medicine Successfully Added to Van.");
                }else{
                    $this->session->set_flashdata("err", "Sorry! System not adding");
                }
            }
            redirect($baseurl.'mmuadmin/add_medicine_van');
        }

        //$mmuid = $this->db->query("SELECT mmu_unit FROM user_details where user_id = ".$this->session->userdata('login_user_id')."")->row();
        //$data['mmu_location'] = $this->db->query("SELECT mmuloc_id,mmuloc_name FROM `mmu_locations` WHERE `mmu_unit` = ".$mmuid->mmu_unit."")->result_array();        
        $data['page_name']      = 'add_medicine_van';
        $data['page_title']     = get_phrase('add_medicine_to_van');
       
        $this->load->view('backend/index', $data);
    }

    function medicinedetails(){
        $mmuid = $this->db->query("SELECT mmu_unit FROM user_details where user_id = ".$this->session->userdata('login_user_id')."")->row();
        $medicineid = $_POST['medicineid'];
        /*$medicineid = $this->db->query("SELECT medicine_id FROM medicine where name = '".$medicinename."' ")->row();*/
        $query = $this->db->query("SELECT DISTINCT ms.batch_no
            FROM medicine AS m
            JOIN medicine_store AS ms ON m.medicine_id = ms.medicine_id and ms.mmu_id = ".$mmuid->mmu_unit."
            WHERE m.medicine_id = ".$medicineid."")->result_array();

        echo json_encode(array('medicine' => $query));
        exit();
    }

    function getmedicineunitcount(){
        $medicineid = $_POST['medicineid'];
        $batch = $_POST['batch'];
        
        $mmuid = $this->db->query("SELECT mmu_unit FROM user_details where user_id = ".$this->session->userdata('login_user_id')."")->row();

        $vanstock = $this->db->query("SELECT sum(no_of_unit) AS loaded, sum(IFNULL(return_unit, 0)) AS returned FROM `van_stock` WHERE `mmu_id` = ".$mmuid->mmu_unit." and medicine_id = ".$medicineid." and batch_no = '".$batch."'")->row();
        $used = intval($vanstock->loaded) - intval($vanstock->returned);

        $query = $this->db->query("SELECT (sum(no_of_unit) - ".$used.") as maincount, exp_date
            FROM medicine_store
            WHERE medicine_id = ".$medicineid." AND mmu_id = ".$mmuid->mmu_unit." AND batch_no = '".$batch."'
            GROUP BY batch_no, exp_date")->row_array();
        /*echo $query->maincount;
        echo ",".$query->exp_date;*/
        echo json_encode($query);
        exit();
    }

    public function manage_diagnosis()
    {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }

        //$mmu = $this->session->userdata('mmu_unit');
        /*$data['alllocation'] = $this->db->query("SELECT * FROM mmu_locations WHERE mmu_unit = "$this->session->userdata('mmu_unit')"")->result_array();*/
        //$data['alllocation'] = $this->db->query("SELECT * FROM mmu_locations WHERE mmu_unit = ".$mmu."")->result_array();

        $data['page_name']          = 'manage_diagnosis';
        $data['page_title']         = get_phrase('manage_medicine_distribution');
        $this->load->view('backend/index', $data);
    }

    public function diagnosis()
    {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }

        $allpatients = $this->db->query("SELECT patient_id, name FROM patient WHERE mmu = ".$this->session->userdata('mmu_unit')." AND status = 1")->result_array();
        /*$minvalue = $_POST['date']." 00:00:00";
        $maxvalue = $_POST['date']." 23:59:59";*/
        foreach ($allpatients as $key => $patient) {
            $this->db->distinct();
            $this->db->select('tm.tm_id, tm.quantity, m.name');
            $this->db->from('medicine AS m');
            $this->db->join('treatment_medicine AS tm', 'tm.medicine_id = m.medicine_id');
            $medicines = $this->db->where("tm.patient_id", $patient['patient_id'])->where("tm.date", $_POST['date'])->get()->result_array();
            $allpatients[$key]['medicines'] = $medicines;
            $allpatients[$key]['date'] = $_POST['date'];
        }
        /*$data['page_name'] = 'diagnosis';
        $data['page_title'] = get_phrase('diagnosis');
        $this->load->view('backend/index', $data);*/
        // echo '<pre>'; print_r($allpatients); echo '</pre>';
        echo json_encode( array(
            'patients' => $allpatients
        ) );
        exit();
    }

    public function edit_diagnosis()
    {
        if($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $tm_id = $this->uri->segment(3);
        if (empty($tm_id)) {
            show_404();
        }
        /*$this->db->query("SELECT v.*, medicine.name
                FROM van_stock AS v
                JOIN medicine AS medicine ON medicine.medicine_id = v.medicine_id
                WHERE date_added BETWEEN '".$minvalue."' AND '".$maxvalue."'")*/
        $data['medicine'] = $this->db->query("SELECT t.*, medicine.name
                FROM medicine
                JOIN treatment_medicine AS t ON t.medicine_id = medicine.medicine_id
                WHERE t.tm_id = ".$tm_id."")->row_array();        
        $data['page_name'] = 'edit_diagnosis';
        $data['page_title'] = get_phrase('edit_medicine_distribution');
        $this->load->view('backend/index', $data);
    }

    public function updtreatment()
    {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }

        $error = array('count' => '', 'status' => 0);
        if(isset($_POST['tm_id'])) {
            if(empty($_POST['count'])) {
                $error['count'] = 'Count Field is mandatory';
                $error['status'] = 1;
            }

            if($error['status'] > 0) {
                echo json_encode($error);
                exit();
            }

            $data = array(
                'quantity' => $_POST['count']
            );
            $query = $this->db->where('tm_id', $_POST['tm_id'])->update('treatment_medicine', $data);
            if($query){
                echo json_encode(array('msg' => 'Medicine Distribution Updated Successfully'));
                exit();
            }
            else{
                echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
                exit();
            }
        }
        else {
            echo json_encode(array('msg'=>'Sorry! Please try after sometime.'));
            exit();
        }
    }

    public function patient(){
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['patient_info'] = $this->db->query("SELECT pa.*, unit.mmu_name
                FROM patient AS pa
                JOIN mmu_unit AS unit ON unit.unit_id = pa.mmu
                WHERE pa.status = 1  and pa.mmu = ".$this->session->userdata("mmu_unit")."
                ORDER BY pa.added_date DESC")->result_array();
        $data['page_name'] = 'manage_patient';
        $data['page_title'] = get_phrase('patient');
        $this->load->view('backend/index', $data);
    }

    public function add_patient() {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $this->db->select('mmuloc_id,mmuloc_name');
        $this->db->from('mmu_locations');
        $this->db->where('mmu_unit',$this->session->userdata("mmu_unit"));
        $data['mmu_location'] = $this->db->get()->result_array();

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('mmu_location','MMU Location Name','required');
        $this->form_validation->set_rules('name','Name','required');
        $this->form_validation->set_rules('age','Age','required|min_length[2]|max_length[3]|numeric');
        $this->form_validation->set_rules('contact_no','contact_no','numeric|min_length[10]|max_length[15]|is_unique[patient.phone]');
        $this->form_validation->set_rules('aadhaar_no','aadhaar_no','numeric|exact_length[12]|is_unique[patient.adhar_no]');
        $this->form_validation->set_rules('gender','Gender','required');
        $this->form_validation->set_rules('family_status','Family Status','required');
        $this->form_validation->set_rules('education','Education','required');
        $this->form_validation->set_rules('yearly_income','Yearly Income','numeric');
        if ($this->form_validation->run() == FALSE){
            $data['page_name'] = 'add_patient';
            $data['page_title'] = get_phrase('add_patient');
            $this->load->view('backend/index', $data);
        }
        else {
            $data = array(
                'patient_id' => time().$this->session->userdata("mmu_unit"),
                'care_of' => $this->input->post('care_giver'),
                'name' => $this->input->post('name'), 
                'birth_date'=> !empty($this->input->post('birth_date')) ? $this->input->post('birth_date') : 'NULL',
                'age'=> $this->input->post('age'),
                'phone'=> $this->input->post('contact_no'),
                'adhar_no'=> $this->input->post('aadhaar_no'),
                'gender'=> $this->input->post('gender'),
                'blood_group'=> $this->input->post('blood_group'),
                'address'=> $this->input->post('address'),
                'family_status'=> $this->input->post('family_status'),
                'education'=> $this->input->post('education'),
                'occupation'=> $this->input->post('occupation'),
                'occu_text'=>'',
                'income'=> $this->input->post('yearly_income'),
                'disability'=> $this->input->post('type_of_disability'),
                'curdisname' => '',
                'curdistype' => '',
                'pastdisname' => '',
                'pastdistype' => '',
                'ration'=> !empty($this->input->post('ration_card')) ? $this->input->post('ration_card') : 'NULL',
                'insurance'=> !empty($this->input->post('insurance')) ? $this->input->post('insurance') : 'NULL',
                'pension'=> !empty($this->input->post('pension')) ? $this->input->post('pension') : 'NULL',
                'account'=>!empty($this->input->post('bank_account')) ? $this->input->post('bank_account') : 'NULL',
                'mmu'=> $this->session->userdata("mmu_unit"),
                'mmuloc'=> $this->input->post('mmu_location'),
                'added_date'=> date('Y-m-d H:i:s'),
                'ip_address'=> $this->input->ip_address(),
                'status'=> 1
            );
            $query = $this->db->insert('patient', $data);
            if($query){
                $patient_id = $this->db->insert_id();
                $inc = 0;
                foreach ($this->input->post('habits') as $key => $h){
                    $inc = $inc+$key+1;
                    $habitdata = array(
                        'id' => (time()+$inc).$this->session->userdata("mmu_unit"),
                        'patient_id' => $patient_id,
                        'habit' => $h
                    );
                    $this->db->insert('patient_habit', $habitdata);
                }
                foreach ($this->input->post('movable_assets') as $key => $ma){
                    $inc = $inc+$key+1;
                    $assetsdata = array(
                        'id' => (time()+$inc).$this->session->userdata("mmu_unit"),
                        'patient_id' => $patient_id,
                        'name' => $ma,
                        'type' => 1
                    );
                    $this->db->insert('patient_asset', $assetsdata);
                }
                foreach ($this->input->post('immovable_assets') as $key => $ima){
                    $inc = $inc+$key+1;
                    $iassetsdata = array(
                        'id' => (time()+$inc).$this->session->userdata("mmu_unit"),
                        'patient_id' => $patient_id,
                        'name' => $ima,
                        'type' => 0
                    );
                    $this->db->insert('patient_asset', $iassetsdata);
                }
                $this->session->set_flashdata("succ", "Patient  added Successfully.");
            }else{
                $this->session->set_flashdata("err", "Sorry! something went wrong");
            }
            redirect($baseurl.'mmuadmin/add_patient');
        }
    }

    public function find_patient(){
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $this->db->select('mmuloc_id,mmuloc_name');
        $this->db->from('mmu_locations');
        $this->db->where('mmu_unit',$this->session->userdata("mmu_unit"));
        $data['mmu_location'] = $this->db->get()->result_array();

        $data['page_name'] = 'find_patient';
        $data['page_title'] = get_phrase('find_patient');
        $this->load->view('backend/index', $data);
    }

    public function getpatient_byid(){
        $patient_id = $this->input->post('patient_id');
        $mmu_location = $this->input->post('mmu_location');

        $patient_details = $this->db->query("SELECT patient_id,name,age,phone,gender,blood_group,mmu_name,mmuloc_name
            FROM patient as p
            JOIN mmu_unit as mu ON p.mmu = mu.unit_id
            JOIN mmu_locations as mul ON p.mmuloc = mul.mmuloc_id
            WHERE patient_id = ".$patient_id." and mmuloc = ".$mmu_location."")->result_array();
        echo json_encode($patient_details);
        exit();
    }

    public function edit_patient($patient_id){
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $this->db->select('mmuloc_id,mmuloc_name');
        $this->db->from('mmu_locations');
        $this->db->where('mmu_unit',$this->session->userdata("mmu_unit"));
        $data['mmu_location'] = $this->db->get()->result_array();

        $this->db->select('*');
        $this->db->from('patient');
        $this->db->where('patient_id',$patient_id);
        $data['patient_detail'] = $this->db->get()->row_array();

        $this->db->select('habit');
        $this->db->from('patient_habit');
        $this->db->where('patient_id',$patient_id);
        $data['patient_habitdetail'] = $this->db->get()->result_array();

        $this->db->select('name');
        $this->db->from('patient_asset');
        $this->db->where('patient_id',$patient_id);
        $data['patient_assestsdetail'] = $this->db->get()->result_array();

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('mmu_location','MMU Location Name','required');
        $this->form_validation->set_rules('name','Name','required');
        $this->form_validation->set_rules('age','Age','required|min_length[2]|max_length[3]|numeric');
        if($this->input->post('contact_no') != $data['patient_detail']['phone']) {
            $this->form_validation->set_rules('contact_no','contact_no','numeric|min_length[10]|max_length[15]|is_unique[patient.phone]');
        }
        if($this->input->post('aadhaar_no') != $data['patient_detail']['adhar_no']) {
            $this->form_validation->set_rules('aadhaar_no','aadhaar_no','numeric|exact_length[12]|is_unique[patient.adhar_no]');
        }
        $this->form_validation->set_rules('gender','Gender','required');
        $this->form_validation->set_rules('family_status','Family Status','required');
        $this->form_validation->set_rules('education','Education','required');
        $this->form_validation->set_rules('yearly_income','Yearly Income','numeric');
        if ($this->form_validation->run() == FALSE){
            $data['page_name'] = 'edit_patient';
            $data['page_title'] = get_phrase('edit_patient');
            $this->load->view('backend/index', $data);
        }
        else {
            $data = array(
                'care_of' => $this->input->post('care_giver'),
                'name' => $this->input->post('name'), 
                'birth_date'=> !empty($this->input->post('birth_date')) ? $this->input->post('birth_date') : 'NULL',
                'age'=> $this->input->post('age'),
                'phone'=> $this->input->post('contact_no'),
                'adhar_no'=> $this->input->post('aadhaar_no'),
                'gender'=> $this->input->post('gender'),
                'blood_group'=> $this->input->post('blood_group'),
                'address'=> $this->input->post('address'),
                'family_status'=> $this->input->post('family_status'),
                'education'=> $this->input->post('education'),
                'occupation'=> $this->input->post('occupation'),
                'occu_text'=>'',
                'income'=> $this->input->post('yearly_income'),
                'disability'=> $this->input->post('type_of_disability'),
                'curdisname' => '',
                'curdistype' => '',
                'pastdisname' => '',
                'pastdistype' => '',
                'ration'=> !empty($this->input->post('ration_card')) ? $this->input->post('ration_card') : 'NULL',
                'insurance'=> !empty($this->input->post('insurance')) ? $this->input->post('insurance') : 'NULL',
                'pension'=> !empty($this->input->post('pension')) ? $this->input->post('pension') : 'NULL',
                'account'=>!empty($this->input->post('bank_account')) ? $this->input->post('bank_account') : 'NULL',
                'mmu'=> $this->session->userdata("mmu_unit"),
                'mmuloc'=> $this->input->post('mmu_location'),
            );
            $this->db->where('patient_id',$patient_id);
            $update = $this->db->update('patient', $data);
            if($update){
               $this->db->where('patient_id', $patient_id)->delete('patient_habit');
               $this->db->where('patient_id', $patient_id)->delete('patient_asset');

                $inc = 0;                
                foreach ($this->input->post('habits') as $key => $h){
                    $inc = $inc+$key+1;
                    $habitdata = array(
                        'id' => (time()+$inc).$this->session->userdata("mmu_unit"),
                        'patient_id' => $patient_id,
                        'habit' => $h
                    );
                    $this->db->insert('patient_habit', $habitdata);
                }
                foreach ($this->input->post('movable_assets') as $key => $ma){
                    $inc = $inc+$key+1;
                    $assetsdata = array(
                        'id' => (time()+$inc).$this->session->userdata("mmu_unit"),
                        'patient_id' => $patient_id,
                        'name' => $ma,
                        'type' => 1
                    );
                    $this->db->insert('patient_asset', $assetsdata);
                }
                foreach ($this->input->post('immovable_assets') as $key => $ima){
                    $inc = $inc+$key+1;
                    $iassetsdata = array(
                        'id' => (time()+$inc).$this->session->userdata("mmu_unit"),
                        'patient_id' => $patient_id,
                        'name' => $ima,
                        'type' => 0
                    );
                    $this->db->insert('patient_asset', $iassetsdata);
                }
                $this->session->set_flashdata("succ", "Patient  Updated Successfully.");
            }else{
                $this->session->set_flashdata("err", "Sorry! something went wrong");
            }
            redirect($baseurl.'mmuadmin/edit_patient/'.$patient_id.'');
        }
    }

    public function view_treatment($patient_id){
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $this->db->select('t.*,ud.name');
        $this->db->from('treatment as t, user_details as ud');
        $this->db->where('t.p_id',$patient_id)->where('t.doc_id = ud.user_id');
        $this->db->order_by('t.treat_date', 'DESC');
        $pt_details = $this->db->get()->result_array();
        foreach ($pt_details as $key => $pt) {
            $this->db->select('*');
            $this->db->from('treatment_disease');
            $this->db->where('patient_id',$patient_id)->where('treatment_id',$pt['t_id'])->where('date',$pt['treat_date']);
            $pd_details = $this->db->get()->result_array();
            $pt_details[$key]['pd_details'] = $pd_details;

            $this->db->select('tm.*,m.name');
            $this->db->from('treatment_medicine as tm, medicine as m');
            $this->db->where('tm.treatment_id',$pt['t_id'])->where('tm.date',$pt['treat_date'])->where('tm.patient_id',$patient_id);
            $this->db->where('tm.medicine_id = m.medicine_id');
            $pm_details = $this->db->get()->result_array();
            $pt_details[$key]['pm_details'] = $pm_details;
        }
        $data['data'] = $pt_details;

        $data['page_name'] = 'view_treatment_details';
        $data['page_title'] = get_phrase('view_treatment_details');
        $this->load->view('backend/index', $data);
    }

    public function filter_doctor(){

        $data['doctor']  = $this->db->query("SELECT dd.doc_id, ud.name
                FROM default_doctor AS dd
                JOIN user_details AS ud ON ud.user_id = dd.doc_id
                WHERE dd.location_id = ".$this->input->post('mmu_site')."")->result_array(); 

        $data['patient'] = $this->db->query("SELECT patient_id, name FROM patient WHERE mmuloc = ".$this->input->post('mmu_site')."")->result_array();
        echo json_encode($data);
    }

    function news() {
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $data['posts'] = $this->db->query("SELECT DISTINCT post.*, user.name, user.profile_image AS image FROM post
            left join user_details AS user ON user.user_id = post.user_id
            WHERE post.status = 1
            ORDER BY post.post_id DESC
            LIMIT 10")->result_array();

        foreach ($data['posts'] as $key => $post) {
            //get comments
            $this->db->distinct();
            $this->db->select('comment.*, user.name, user.profile_image AS avatar');
            $this->db->from('post_comment AS comment');
            $this->db->join('user_details AS user', 'user.user_id = comment.user_id', 'left');
            $comments = $this->db->where("comment.post_id", $post['post_id'])->order_by('comment.comment_id', 'DESC')->get('post_comment')->result_array();
            $data['posts'][$key]['comments'] = $comments;
            //get images
            $images = $this->db->where("post_id", $post['post_id'])->where("image_status", 1)->get('post_image')->result_array();
            $data['posts'][$key]['images'] = $images;
        }
        $mmu_unit=$this->session->userdata("mmu_unit");
        $data['m_loc']=$this->db->query("SELECT mmuloc_name, mmuloc_id FROM mmu_locations WHERE mmu_unit=$mmu_unit")->result_array();
        $data['no_title']  = true;
        $data['page_name']  = 'news';
        $data['page_title'] = get_phrase('news');
        $this->load->view('backend/index', $data);
    }


    function post(){
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        else{

            $address = $this->getAddress($this->input->post('lat'),$this->input->post('long'));

            $data = array(
                'user_id'=>$this->session->userdata('login_user_id'),
                'post_type'=>'',
                'privacy'=>'',
                'comment'=>0,
                'text' => $this->input->post('text'),
                'mmu' => $this->input->post('mmu'),
                'activity' => $this->input->post('post_type'),
                'lat' =>$this->input->post('lat'),
                'lng'=>$this->input->post('long'),
                'address'=>$address,
                'ip_address'=>$this->input->ip_address(),
                'date' => date('Y-m-d H:i:s'),
                'status'=>1,                
            );
            $query = $this->db->insert('post', $data);
            if($query){
                $post_id = $this->db->insert_id();
               
                foreach ($_FILES['image']['name'] as $key => $image) {
                    $fileName = $_FILES["image"]["name"][$key]; // The file name
                    $fileTempLoc = $_FILES["image"]["tmp_name"][$key]; // File in the PHP tmp folder
                    $fileType = $_FILES["image"]["type"][$key]; // The type of file it is
                    $fileSize = $_FILES["image"]["size"][$key]; // File size in bytes
                    $fileErrorMsg = $_FILES["image"]["error"][$key]; // 0 for false... and 1 for true
                    $type = strtolower(substr(strrchr($fileName,"."),1));
                    if($type == 'jpeg' || $type == 'jpe') { $type = 'jprg'; } // make a jpeg or jpe file a jpg file

                    
                    $new_image_name = uniqid() . '.'.'jpg';
                    move_uploaded_file($_FILES["image"]["tmp_name"][$key],"uploads/post_image/".$new_image_name);
                    $file= $new_image_name;

                    $imgData = array(
                        'post_id' => $post_id,
                        'image_name' => $file,
                        'lat' =>$this->input->post('lat'),
                        'lng'=>$this->input->post('long'),
                        'address' => $address,
                        'ip_address'=>$this->input->ip_address(),
                        'date' => date('Y-m-d H:i:s'),
                        'image_status' => 1
                    );
                    $imageinsert = $this->db->insert('post_image', $imgData);
                }
                $this->session->set_flashdata("succ", "News Added Successfully.");
            }else{
                $this->session->set_flashdata("err", "Sorry! something went wrong");
            }
            redirect($baseurl.'mmuadmin/news/');
        }
    }

    public function getAddress($lat,$long){
        if(!empty($lat) && !empty($long)){
            //Send request and receive json data by address
            $geocodeFromLatLong = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($long).'&sensor=false'); 
            $output = json_decode($geocodeFromLatLong);
            $status = $output->status;
            //Get address from json data
            $address = ($status=="OK")?$output->results[1]->formatted_address:'';
            //Return address of the given latitude and longitude
            if(!empty($address)){
                return $address;
            }else{
                return false;
            }
        }else{
            return false;   
        }
    }
    

    public function find_pat_fortreatment(){
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $this->db->select('mmuloc_id,mmuloc_name');
        $this->db->from('mmu_locations');
        $this->db->where('mmu_unit',$this->session->userdata("mmu_unit"));
        $data['mmu_location'] = $this->db->get()->result_array();

        $data['page_name'] = 'find_pat_addtreatment';
        $data['page_title'] = get_phrase('find_patient_for_treatment');
        $this->load->view('backend/index', $data);
    }

    public function add_treatment(){
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $patient_id = $this->uri->segment(3);        
        if($patient_id == ''){
            redirect($baseurl.'mmuadmin/find_pat_fortreatment');
        }

        $this->db->select('name');
        $this->db->from('patient');
        $this->db->where('patient_id',$patient_id);
        $data['patient_deatils'] = $this->db->get()->row_array();

        $this->db->select('dis_id,dis_name');
        $this->db->from('disease');
        $this->db->where('status',1);
        $data['disease_list'] = $this->db->get()->result_array();

        $this->db->select('diag_id,diag_name');
        $this->db->from('diagnosis');
        $data['diagnosis_list'] = $this->db->get()->result_array();

        $data['page_name'] = 'add_treatment';
        $data['page_title'] = get_phrase('add_treatment');
        $this->load->view('backend/index', $data);
    }

    public function upload_treatment(){
        if ($this->session->userdata('mmuadmin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $patient_id = $this->uri->segment(3);        
        if($patient_id == ''){
            redirect($baseurl.'mmuadmin/find_pat_fortreatment');
        } 
        if(isset($_POST['submit'])){
            $data = array(
                't_id' => time().$this->session->userdata("mmu_unit"),
                'p_id' => $patient_id,
                'doc_id' => 1,
                'bphigh' => !empty($this->input->post('highbp')) ? $this->input->post('highbp') : '',
                'bplow' => !empty($this->input->post('lowbp')) ? $this->input->post('lowbp') : '',
                'pulse' => !empty($this->input->post('pulse')) ? $this->input->post('pulse') : '',
                'weight' => !empty($this->input->post('paweight')) ? $this->input->post('paweight') : '',
                'height' => !empty($this->input->post('paheight')) ? $this->input->post('paheight') : '',
                'temp' => !empty($this->input->post('patemp')) ? $this->input->post('patemp') : '',
                'stp' => !empty($this->input->post('pastp')) ? $this->input->post('pastp') : '',
                'ltp' => !empty($this->input->post('paltp')) ? $this->input->post('paltp') : '',
                'pd' => !empty($this->input->post('papd')) ? $this->input->post('papd') : '',
                'diag' => !empty($this->input->post('diagnosis')) ? $this->input->post('diagnosis') : '',
                'fasting' => !empty($this->input->post('fasting')) ? $this->input->post('fasting') : '',
                'pp' => !empty($this->input->post('pp')) ? $this->input->post('pp') : '',
                'random' => !empty($this->input->post('random')) ? $this->input->post('random') : '',
                'treat_date' => date('Y-m-d'),
                'ip_address' => $this->input->ip_address(),
                'status' => 0
            );
            
            $insert = $this->db->insert('treatment', $data);
            if($insert){
                $treatment_id = $this->db->insert_id();
                $inc = 0;

                foreach ($_POST['name'] as $key => $insert) {
                    $inc = $inc+$key+1;
                    $medicinedata = array(
                        'tm_id' => (time()+$inc).$this->session->userdata("mmu_unit"),
                        'treatment_id' => $treatment_id,
                        'medicine_id' => !empty($_POST['name'][$key]) ? $_POST['name'][$key] : '',
                        'batch_no' => !empty($_POST['batch'][$key]) ? $_POST['batch'][$key] : '',
                        'patient_id' => $patient_id,
                        'dosage' => !empty($_POST['dosage'][$key]) ? $_POST['dosage'][$key] : '',
                        'day' => !empty($_POST['days'][$key]) ? $_POST['days'][$key] : '',
                        'quantity' => !empty($_POST['quantity'][$key]) ? $_POST['quantity'][$key] : '',
                        'date' => date('Y-m-d'),
                        'status' => 0
                    );
                    $this->db->insert('treatment_medicine', $medicinedata);
                }
                foreach ($_POST['currentdisease'] as $key => $cd) {
                    $inc = $inc+$key+1;
                    $crtdisdata = array(
                        'td_id' => (time()+$inc).$this->session->userdata("mmu_unit"),
                        'treatment_id' => $treatment_id,
                        'dis_name' => !empty($_POST['currentdisease'][$key]) ? $_POST['currentdisease'][$key] : '',
                        'dis_type' => !empty($_POST['currentdiseasetype'][$key]) ? $_POST['currentdiseasetype'][$key] : '',
                        'patient_id' => $patient_id,
                        'type' => 'current',
                        'date' => date('Y-m-d'),
                        'status' => 0
                    );
                    $this->db->insert('treatment_disease', $crtdisdata);
                }
                foreach ($_POST['pastdisease'] as $key => $past) {
                    $inc = $inc+$key+1;
                    $pastdisdata = array(
                        'td_id' => (time()+$inc).$this->session->userdata("mmu_unit"),
                        'treatment_id' => $treatment_id,
                        'dis_name' => !empty($_POST['pastdisease'][$key]) ? $_POST['pastdisease'][$key] : '',
                        'dis_type' => !empty($_POST['pastdiseasetype'][$key]) ? $_POST['pastdiseasetype'][$key] : '',
                        'patient_id' => $patient_id,
                        'type' => 'past',
                        'date' => date('Y-m-d'),
                        'status' => 0
                    );
                    $this->db->insert('treatment_disease', $pastdisdata);
                }
                $this->session->set_flashdata("succ", "Treatment Added Successfully.");
            }else{
                $this->session->set_flashdata("err", "Sorry! something went wrong");
            }
            redirect($baseurl.'mmuadmin/find_pat_fortreatment/');
        }
    }
}
